const { contextBridge, ipcRenderer } = require('electron');

// Listen for messages from the main process
ipcRenderer.on('log-url', (event, url) => {
   // You can add logic here to log or handle the URL from the main process
});

// Notify the main process that the application is launched from a shortcut
ipcRenderer.send('shortcut-launched');

// Expose safe APIs to the renderer process
contextBridge.exposeInMainWorld('electronAPI', {
 
  ipcRenderer: {
    send: (channel, data) => ipcRenderer.send(channel, data),
    on: (channel, func) => {
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    },
    removeListener: (channel, func) => {
      ipcRenderer.removeListener(channel, func);
    },
    once: (channel, func) => {
      ipcRenderer.once(channel, (event, ...args) => func(event, ...args));
    }
  },
 
  getSystemInfo: () => ipcRenderer.invoke('get-system-info'),
 
  openBrowserWindow: (url) => ipcRenderer.send("open-url-in-browser", url),
  isElectron: true,

  sendOCRVerificationCommand: (args) => ipcRenderer.send('run-ocr-verification', args),
  on: (channel, listener) => {
      const validChannels = ['ocr-process-output', 'ocr-process-error', 'ocr-process-close'];
      if (validChannels.includes(channel)) {
          ipcRenderer.on(channel, (event, ...args) => listener(...args));
      }
  },

  // Auto-Updater API
     autoUpdater: {
      onUpdateAvailable: (callback) => ipcRenderer.on("update-available", callback),
      onUpdateDownloaded: (callback) => ipcRenderer.on("update-downloaded", callback),
      onUpdateError: (callback) => ipcRenderer.on("update-error", callback),
      getUpdateStatus: () => ipcRenderer.invoke("get-update-status"),
      //    getUpdateStatus: () => ipcMain.on("get-update-status", (event) => {
      //   event.returnValue = {
      //     updateAvailable: isUpdateAvailable,
      //     updateDownloaded: isUpdateDownloaded,
      //   };
      // }),
      // getUpdateStatus: () => ipcRenderer.on("get-update-status", (event) => {
      //   event.returnValue = {
      //     updateAvailable: isUpdateAvailable,
      //     updateDownloaded: isUpdateDownloaded,
      //   };
      // }),

      quitAndInstall: () => ipcRenderer.send("install-update"),

      getEnvironment:() => ipcRenderer.invoke('get-environment'),
    },
 });
