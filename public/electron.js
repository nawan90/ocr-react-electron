const {
  app,
  BrowserWindow,
  protocol,
  shell,
  Menu,
  ipcMain,
   screen,
  Notification
  

} = require("electron");
const path = require("path");
const fs = require("fs");
const log = require("electron-log");
const { spawn } = require('child_process');
const { autoUpdater } = require("electron-updater");
const isDev = app.isPackaged ? false : require("electron-is-dev");
const macaddress = require('macaddress');
const os = require('os');
const si = require('systeminformation');
const { exec } = require('child_process');


//autoUpdater.autoDownload Create the main window
const gotTheLock = app.requestSingleInstanceLock();
log.transports.file.level = 'info';  // Log info and above
log.transports.file.file = path.join(app.getPath('userData'), 'logs', 'main.log'); // Define path

log.info("Starting application...");


if (!gotTheLock) {
  // If another instance is already running, quit this one
  app.quit();
} else {
  const mainMenuTemplate = [];
const menu = Menu.buildFromTemplate(mainMenuTemplate);
Menu.setApplicationMenu(menu);

let mainWindow;
const loadHtmlPath = path.join(__dirname, "statis", "load.html");
const loadCssPath = path.join(__dirname, "statis", "load.css");
log.info("Application started.");

ipcMain.handle('get-system-info', async () => {
  try {
    // Collect MAC address
    const mac = await new Promise((resolve, reject) => {
      macaddress.one((err, mac) => {
        if (err) {
          reject(err);
        } else {
          resolve(mac);
        }
      });
    });
    const command = process.platform === 'win32' 
      ? 'wmic bios get serialnumber' // For Windows
      : 'system_profiler SPHardwareDataType | grep "Serial Number"'; // For macOS/Linux

    const serialNumber = await new Promise((resolve, reject) => {
      exec(command, (error, stdout) => {
        if (error) {
          reject(error.message);
        }
        resolve(stdout.trim().split('\n')[1].trim());
      });
    });
    const networkInterfaces = await si.networkInterfaces();
    
    // Loop through the interfaces to find the active one (non-internal)
    let activeNetwork = null;
    for (const iface of networkInterfaces) {
      if (!iface.internal && iface.operstate === 'up') {
        activeNetwork = iface;
        break;
      }
    }
    const timestamp = new Date().toISOString();

    // Collect system info
    const platform = `${os.type()} ${os.release()}`;
    const appVersion = app.getVersion();
     // File path to store last opened data
     const filePath = path.join(app.getPath('userData'), 'lastOpenedByMac.json');
     let lastOpenedData = {};
 
     // Read existing data from the JSON file
     if (fs.existsSync(filePath)) {
       const fileContent = fs.readFileSync(filePath, 'utf-8');
       lastOpenedData = JSON.parse(fileContent);
     }
 
     // Update or add the current MAC address data
     lastOpenedData[mac] = {
       lastOpened: timestamp,
       platform,
       appVersion,
       serialNumber,
       typeConnection: activeNetwork.iface,
     };
 
     // Save the updated data to the file
     fs.writeFileSync(filePath, JSON.stringify(lastOpenedData, null, 2), 'utf-8');
 
     return {
       macAddress: mac,
       platform,
       appVersion,
       serialNumber,
       typeConnection: activeNetwork.iface,
       lastOpened: timestamp
     };
  } catch (error) {
    console.error('Failed to get system info:', error);
    throw error;
  }
});
const createMainWindow = async (
  finalUrl = `file://${path.join(__dirname, "../build/index.html")}`
) => {
  log.info("Initializing main window...");

  const primaryDisplay = screen.getPrimaryDisplay();
  const { width, height } = primaryDisplay.workAreaSize;

  if (!mainWindow) {
    log.info("Creating BrowserWindow instance.");
    mainWindow = new BrowserWindow({
      width,
      height,
      webPreferences: {
        contextIsolation: true,
        nodeIntegration: false,
        enableRemoteModule: false,
        preload: path.join(__dirname, "./preload.js"),
      },
      icon: path.join(__dirname, "../src/assets/images/logo-sm.png"),
      maximizable: false,
      movable: false,
      fullscreenable: true,
      frame: true,
    });

    mainWindow.on("closed", () => {
      log.info("Main window closed.");
      mainWindow = null;
    });
  }

  log.info(`Loading URL: ${loadHtmlPath}`);
  mainWindow.loadURL(`file://${loadHtmlPath}`);
  mainWindow.setMenuBarVisibility(false);
  mainWindow.webContents.openDevTools();

 
  
  mainWindow.webContents.once("dom-ready", () => {
    log.info("DOM ready. Inserting CSS...");
    fs.readFile(loadCssPath, "utf8", (err, css) => {
      if (err) {
        log.error("Failed to load CSS:", err);
      } else {
        mainWindow.webContents.insertCSS(css);
      }
    });

    setTimeout(() => {
      log.info(`Navigating to final URL: ${finalUrl}`);
      mainWindow.loadURL(finalUrl);
    }, 2000);

    // setTimeout(() => {
    //   mainWindow.reload();
    // }, 5000);
  });

  return mainWindow;
};
const protocolGetData = () => {
  log.info("Registering custom protocol handler.");
  protocol.registerHttpProtocol(
    "document-digitalization-and-ai-based-ocr",
    (request, callback) => {
      const parsedUrl = new URL(request.url);
      const params = new URLSearchParams(parsedUrl.search);
      const message = params.get("message") || "DefaultMessage";
      const timestamp = params.get("timestamp") || Date.now().toString();

      const finalUrl = `file://${path.join(__dirname, "../build/index.html")}#/load-sso?message=${encodeURIComponent(message)}&timestamp=${encodeURIComponent(timestamp)}`
      // const finalUrl = message
      //   ? `file://${path.join(__dirname, "../build/index.html")}#/login?message=${encodeURIComponent(message)}&timestamp=${encodeURIComponent(timestamp)}`
      //   : `file://${path.join(__dirname, "../build/index.html")}`;
      log.info(`Handling custom protocol. Final URL: ${finalUrl}`);
      createMainWindow(finalUrl);
      callback({ targetUrl: request.url });
    }
  );
};

// Custom protocol handling

// Function to check if the URL matches and exit the app if it does
function checkIfURLMatches(url) {
  // If the URL matches 'https://efaskon.slog.polri.go.id/', close the app
  if (url === "https://efaskon.slog.polri.go.id/") {
    console.log("URL matched! Closing the app.");
    app.quit(); // Close the Electron app
  } else {
    console.log("URL did not match. Keeping the app open.");
  }
}

// Application event handlers
app.on("ready", () => {
  log.info("App is ready.");
  protocolGetData();
  autoUpdater.checkForUpdatesAndNotify();

  ipcMain.on("open-url-in-browser", (event, url) => {
    console.log("Opening URL in browser:", url); // Debug logging

    shell.openExternal(url); // Opens the URL in the system's default browser
    monitoringTimer = setTimeout(() => {
      checkIfURLMatches(url); // After 5 seconds, check the URL
    }, 3000); // Check after 5 seconds (can be adjusted)
  

  });

  const launchType = detectLaunchType();
  log.info(`Launch type detected: ${launchType}`);

  if (launchType === "external-link") {
    log.info("Handling external link launch.");
    handleOpenFromUrl(process.argv[1]);
  } else {
    createMainWindow();
  }


});

app.on("activate", () => {
  log.info("App activated.");
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});

app.on("window-all-closed", () => {
  log.info("All windows closed.");
  if (process.platform !== "darwin") app.quit();
});

app.on("open-url", (event, url) => {
  log.info(`Open URL event triggered. URL: ${url}`);
  if (process.platform === "darwin") {
    event.preventDefault();
    handleOpenFromUrl(url);
  }
});

app.on("second-instance", (event, commandLine) => {
  log.info("Second instance detected.");
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
    handleOpenFromUrl(commandLine[commandLine.length - 1]);
  }
});

// Handle opening the app from a URL
const handleOpenFromUrl = (url) => {

  log.info(`Handling open from URL: ${url || 'No URL provided'}`);

  try {
    // Check if URL is valid and matches the custom protocol
    if (url && url.startsWith('document-digitalization-and-ai-based-ocr://')) {
      const parsedUrl = new URL(url);
      const message = parsedUrl.searchParams.get('message') || 'DefaultMessage';
      const timestamp = parsedUrl.searchParams.get('timestamp') || Date.now().toString();

      const finalUrl = `file://${path.join(__dirname, "../build/index.html")}#/load-sso?message=${encodeURIComponent(message)}&timestamp=${encodeURIComponent(timestamp)}`;
      log.info(`Navigating to final URL: ${finalUrl}`);

      createMainWindow(finalUrl);
    } else {
      // Handle shortcut launch or default behavior
      log.info('No valid protocol URL detected. Launching default window.');
      createMainWindow(); // Default window launch for shortcut
    }
  } catch (error) {
    log.error(`Failed to handle URL: ${error.message}`);
    createMainWindow(); // Fallback to default window instead of quitting
  }
};

// Detect the launch type
const detectLaunchType = () => {
  const argv = process.argv;
  log.info(`Process arguments: ${argv}`);
  if (argv.length > 1) {
    const url = argv[1];
    return url.includes("document-digitalization-and-ai-based-ocr")
      ? "external-link"
      : "shortcut";
  }
  return "shortcut";

};

log.info("Main process setup complete.");
 
ipcMain.on('run-ocr-verification', (event, arg) => {
  console.log(`Received OCR verification command: ${arg}`);

  // Example command to run
  const command = 'OCRVerificationCommand'; 
  const commandPath = path.join('C:\\OCRVerification', command); 
  const args = arg.split(' '); // Assuming the arguments are passed as a single string and need to be split

  const process = spawn(commandPath, args, { cwd: 'C:\\OCRVerification' });
  process.stdout.on('data', (data) => {
    console.log(`stdout: ${data.toString()}`);  // Convert Buffer to string
    event.sender.send('ocr-process-output', data.toString());  // Send output back to renderer
  });
  
  process.stderr.on('data', (data) => {
    console.error(`stderr: ${data.toString()}`);  // Convert Buffer to string
    event.sender.send('ocr-process-error', data.toString());  // Send errors back to renderer
  });

  process.on('close', (code) => {
    console.log(`Process exited with code ${code}`);
    event.sender.send('ocr-process-close', `Process exited with code ${code}`);  // Notify renderer of process closure
  });
});

}


// const updateChannel = environment === 'beta' ? 'beta' : 'latest';


// autoUpdater.allowPrerelease = true;

// // Check if beta mode is enabled
// const isBeta = true; // Toggle this based on your deployment

// // Set feed URL explicitly for beta or latest
// autoUpdater.setFeedURL({
//   provider: 'generic',
//   url:`http://116.90.165.46:8068/static/${updateChannel}`
//  });

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'production'; // Fallback to beta
}
// ipcMain.handle('get-environment', () => {
//   const environment = process.env.NODE_ENV || 'production';
//   console.log('Environment in main process:', environment); // Debugging log
//   return environment;
// });
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
autoUpdater.allowPrerelease = true;

log.info('Auto-updater logging enabled.');
// const environment = process.env.NODE_ENV || 'production';  // 'beta' or 'latest'
// const server = 'http://116.90.165.46:8068/static';
//  autoUpdater.setFeedURL(`${server}`);

 // Set up the auto-update configuration
autoUpdater.setFeedURL({
  provider: 'generic',
  url: 'http://116.90.165.46:8068/static/',
});

// log.info('environment', environment);
//  log.info('feedUrl',feedUrl);

// Track update states
let isUpdateAvailable = false;
let isUpdateDownloaded = false;

// Auto-Updater Events
autoUpdater.on("update-available", () => {
  isUpdateAvailable = true; 
  console.log("Update available.");
  log.info("Update available.");
});

autoUpdater.on("update-downloaded", () => {
  isUpdateDownloaded = true;
  console.log("Update downloaded.");
  log.info("Update downloaded.");

  // Show system notification
  if (Notification.isSupported()) {
    const notification = new Notification({
      title: 'Update Ready',
      body: 'Click to install the latest update.',
    });

    notification.on('click', () => {
      console.log('Notification clicked, installing update...');
      log.info('Notification clicked, installing update...');
      autoUpdater.quitAndInstall();
    });

    notification.show();
  } else {
    console.warn('Notifications are not supported on this system.');
  }
});

autoUpdater.on("error", (error) => {
  console.error("Auto-update error:", error.message);
  log.error("Auto-update error:", error.message);
});

// Check for updates initially when the app is ready
app.on('ready', () => {
  log.info("App is ready. Checking for updates...");
  console.log('Triggering checkForUpdates manually...');
  autoUpdater.checkForUpdates().then(result => {
    console.log('Update check result:', result);
  }).catch(error => {
    console.error('Auto-updater error during manual check:', error.message);
  });

  // Periodic Update Check
  setInterval(() => {
    log.info("Performing periodic update check...");
    autoUpdater.checkForUpdatesAndNotify();
  }, 5 * 60 * 1000); // Every 5 minutes
});

// IPC Handlers for Renderer Process
ipcMain.handle("get-update-status", () => {
  log.info("Handling get-update-status");

  return {
    updateAvailable: isUpdateAvailable,
    updateDownloaded: isUpdateDownloaded,
  };
});


// ipcMain.on("get-update-status", (event) => {
//   event.returnValue = {
//     updateAvailable: isUpdateAvailable,
//     updateDownloaded: isUpdateDownloaded,
//   };
// });


ipcMain.on("install-update", () => {
  log.info("User triggered install-update. Quitting and installing...");
  autoUpdater.quitAndInstall();
});

