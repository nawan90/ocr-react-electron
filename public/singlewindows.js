const {
  app,
  BrowserWindow,
  protocol,
  shell,
  Menu,
  ipcMain,
  screen,
} = require("electron");
const path = require("path");
const log = require("electron-log");

let mainWindow;

// Ensure only one instance of the app
const gotTheLock = app.requestSingleInstanceLock();

if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, argv) => {
    // Focus the existing window and handle external links
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore();
      mainWindow.focus();

      const externalLink = argv.find(arg =>
        arg.startsWith("document-digitalization-and-ai-based-ocr://")
      );
      if (externalLink) {
        log.info(`Second instance received external link: ${externalLink}`);
        mainWindow.webContents.send("open-link", externalLink); // Communicate with renderer
      }
    }
  });

  app.whenReady().then(() => {
    createMainWindow();

    const launchType = detectLaunchType();
    log.info(`Launch type detected: ${launchType}`);
    if (launchType === "external-link") {
      handleExternalLink(process.argv.find(arg =>
        arg.startsWith("document-digitalization-and-ai-based-ocr://")
      ));
    }
  });
}

// Create the main window
const createMainWindow = (url = `file://${path.join(__dirname, "index.html")}`) => {
  const { width, height } = screen.getPrimaryDisplay().workAreaSize;

  if (!mainWindow) {
    mainWindow = new BrowserWindow({
      width,
      height,
      webPreferences: {
        preload: path.join(__dirname, "preload.js"),
        contextIsolation: true,
      },
      icon: path.join(__dirname, "assets/images/logo.png"),
    });

    mainWindow.on("closed", () => (mainWindow = null));
    mainWindow.setMenuBarVisibility(false);
    mainWindow.loadURL(url);
  } else {
    mainWindow.loadURL(url);
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
};

// Detect how the app was launched
const detectLaunchType = () => {
  const argv = process.argv;
  if (argv.some(arg => arg.startsWith("document-digitalization-and-ai-based-ocr://"))) {
    return "external-link";
  }
  return "shortcut";
};

// Handle external links
const handleExternalLink = (url) => {
  if (url) {
    const parsedUrl = new URL(url);
    const params = new URLSearchParams(parsedUrl.search);
    const message = params.get("message") || "DefaultMessage";
    const timestamp = params.get("timestamp") || Date.now();

    const finalUrl = `file://${path.join(__dirname, "index.html")}#/load-sso?message=${encodeURIComponent(message)}&timestamp=${encodeURIComponent(timestamp)}`;
    createMainWindow(finalUrl);
  }
};

// Register custom protocol
app.on("ready", () => {
  protocol.registerHttpProtocol(
    "document-digitalization-and-ai-based-ocr",
    (request) => {
      const parsedUrl = new URL(request.url);
      const params = new URLSearchParams(parsedUrl.search);
      const message = params.get("message") || "DefaultMessage";
      const timestamp = params.get("timestamp") || Date.now();

      const finalUrl = `file://${path.join(__dirname, "index.html")}#/load-sso?message=${encodeURIComponent(message)}&timestamp=${encodeURIComponent(timestamp)}`;
      createMainWindow(finalUrl);
    }
  );
});

// Focus or create the main window on activation (macOS)
app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});

// Quit the app when all windows are closed
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});

