!include "MUI2.nsh"  ; Include the Modern UI macros

; Define installer and uninstaller side bitmaps (optional if already set in package.json)
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"  ; Installer sidebar
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"  ; Finish page sidebar


; Macro definition for custom actions
!macro customInstall
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"

  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend

