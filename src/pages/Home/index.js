import { useState, useEffect } from "react";
import Efaskon from "../../assets/images/logo_new/logo_new.png";
import Dashboard from "./Dashboard";
 import Footer from "../../components/Footer";
import User from "../../assets/images/iconRoleUser_button.png";
import Profil from "../../assets/images/icon/profile.png";
import { useNavigate, Link } from "react-router-dom";
import LoadSpinner from "../../components/Load/load";
import Modal2 from "../../components/Modal2";
import { useTranslation } from "react-i18next";
import "@fontsource/russo-one";
import SelectLanguage from "./SelectLanguage";
import utils from "../../utils";
import SideMenu from "./SideMenu";
import accessManager from "../../api/accessManager";
function Home() {
  const navigate = useNavigate();
  const [loadSpin, setLoadSpinner] = useState(false);
  const [profileName, setProfileName] = useState("");
   const [permission, setPermission] = useState({
    isActivityAccess: false,
    isSearchAccess: false,
    isListTemplate: false,
    isListSetting: false,
    isListUser: false,
    isListPolda: false,
    isListSatker: false,
    isListMetadata: false,
    isListDocType: false,
    isListProvinsi: false,
    isRoleAccess: false,
  });
  const { t } = useTranslation();

  useEffect(() => {
    let name = localStorage.getItem("myfullname");
    // let haveAccess = localStorage.getItem("dms.log.access");
    // setIsActivityAccess(haveAccess);

    let haveAccessLog = accessManager.getAccessLog();
    // let haveAccessSearch = JSON.parse(localStorage.getItem("dms.search.access"));
    let haveAccessRole = accessManager.getAccessRole()// JSON.parse(accessManager.getAccessRole())//JSON.parse(localStorage.getItem("dms.role.access"));
    // let haveListUser = JSON.parse(localStorage.getItem("dms.user.list"));
    let haveListPolda = accessManager.getAccessPoldaList()//JSON.parse(accessManager.getAccessPoldaList())//localStorage.getItem("dms.polda.list"));
    let haveListSatker = accessManager.getAccessSatkerList()//JSON.parse(accessManager.getAccessSatkerList())//localStorage.getItem("dms.satker.list"));
    let haveListMetadata = accessManager.getAccessMetadataList()//JSON.parse()//localStorage.getItem("dms.metadata.list"));
    let haveListDocType = accessManager.getAccessDocTypeList()//JSON.parse()//localStorage.getItem("dms.doctype.list"));
    let haveListProvinsi = accessManager.getAccessProvinceList()//JSON.parse()//localStorage.getItem("dms.province.list"));
    // let haveListTemplate = JSON.parse(localStorage.getItem("dms.template.list"));
    // let haveListDocs = JSON.parse(localStorage.getItem("dms.document.list"));
    setPermission({
      isActivityAccess: haveAccessLog,
      isListSetting: haveListDocType || haveListMetadata || haveListPolda || haveListSatker || haveListProvinsi || haveAccessRole,
    });

    setProfileName(name ? name : "Admin");
    setTimeout(() => {
      setLoadSpinner(false);
    }, 3300);
  }, [loadSpin]);

  const callLoading = () => {
    if (loadSpin == true) {
      return <LoadSpinner />;
    } else {
      return <div></div>;
    }
  };

  const toActivityLog = () => {
    navigate("/activity-log");
  };
  const toTerimaData = () => {
    navigate("/terima-data");
  };
  const toKirimData = () => {
    navigate("/kirim-data");
  };
  const toHapusConfig = () => {
    navigate("/hapus-config");
  };
  const toWebsite = () => {
    window.electronAPI.openExternal("https://www.example.com");
  };
  if (window.electronAPI && window.electronAPI.isElectron) {
  } else {
  }
  const menuWeb = () => {
    if (window.electronAPI && window.electronAPI.isElectron) {
      return <ul></ul>;
    } else {
      return (
        <ul>
          <li>
            <hr class="dropdown-divider" />
          </li>
          <li class="dropdown-item bg-info">Integrasi (Temp. Dev)</li>
          <li
            class="dropdown-item "
          // onClick={handleLaunchApp}
          >
            Open Website
          </li>
          <li class="dropdown-item " onClick={toTerimaData}>
            Terima Data Website
          </li>
          <li class="dropdown-item " onClick={toWebsite}>
            Buka Website
          </li>
          <li class="dropdown-item " onClick={toKirimData}>
            Kirim Data Website
          </li>
          <li>
            <hr class="dropdown-divider" />
          </li>
          <li class="dropdown-item bg-warning">Komponen (Temp. Dev)</li>
          <li class="dropdown-item " onClick={toHapusConfig}>
            Hapus Config
          </li>
          <li class="dropdown-item">
            <div
              onClick={() => {
                setLoadSpinner(true);
                callLoading();
              }}
            >
              Spinner
            </div>
          </li>
        </ul>
      );
    }
  };
  return (
    <div id="">
      {callLoading()}

      <div
        class="container-fluid pt-3 "
        style={{
          minHeight: "84vh",
          marginTop: "65px",
        }}
      >
        <div class="row p-2 headgradient fixed-top  bg-secondary bg-opacity-1 ">
          <div class="col-10">
            <div class="row">
              <div class="col-2">
                &nbsp; &nbsp; &nbsp;
                <img src={Efaskon} width={"18%"} />
              </div>
              <div class="col-9">
                <div
                  style={{
                    fontSize: "24px",
                    fontWeight: "bold",
                    fontFamily: "Russo One",
                    color: "#feb602",
                  }}
                >
                  e-FASKON POLRI
                </div>
                <div style={{ fontSize: "16px", color: "white" }}>{t("title_app")}</div>
              </div>
              <div class="col-1 ">
                <div className=" pt-1 float-end">
                  <SelectLanguage />
                </div>
              </div>
            </div>
          </div>
          {/* <div class="col-1 float-end">
            <div className=" pt-1 ">
              <SelectLanguage />
            </div>
            </div> */}
          <div class="col-2 bg-dark">
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4 text-white float-end">
              <li class="nav-item dropdown mt-1">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  <img src={User} width={"30px"} />
                  <a>{profileName} </a>
                </a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                  <li class="dropdown-item py-2 text-center">
                    <img className="pe-2 mb-2" src={Profil} width={"20%"} />
                    <br></br>
                    <a>{profileName} </a>
                  </li>
                  <li>
                    <hr class="dropdown-divider" />
                  </li>
                  <li class="dropdown-item">
                    <Link to="/Profile" style={{ textDecoration: "none", color: "#000" }}>
                      {t("profil_t1")}
                    </Link>
                  </li>
                  {permission.isListSetting &&
                    <li>
                      <hr class="dropdown-divider" />
                    </li>}
                  {permission.isListSetting &&
                    <li class="dropdown-item">
                      <Link to="/Setting" style={{ textDecoration: "none", color: "#000" }}>
                        {t("pengaturan_t1")}
                      </Link>
                    </li>
                  }
                  {permission.isActivityAccess && (
                    <li>
                      <hr class="dropdown-divider" />
                    </li>
                  )}
                  {permission.isActivityAccess && (
                    <li>
                      <a class="dropdown-item" onClick={toActivityLog}>
                        {t("titlle_al")}
                      </a>
                    </li>
                  )}

                  {/* <li>
                  {menuWeb()}

                  </li> */}
                  {/* <li class="dropdown-item">
                    <Link to="/" style={{ textDecoration: "none", color: "red" }}>
                      {t("keluar_t1")}
                    </Link>
                  </li> */}
                  {window.electronAPI?.isElectron ?
                    <li></li> :

                    <span>
                      <li>
                        <hr class="dropdown-divider" />
                      </li>
                      <li class="dropdown-item">
                        <Link to="/" style={{ textDecoration: "none", color: "red" }}>
                          {t("keluar_t1")}
                        </Link>
                      </li>
                    </span>
                  }

                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-2">
            <SideMenu />
          </div>
          <div class="col-10">
            <Dashboard />
          </div>
        </div>
      </div>
      <Footer />
      {/* <div id="layoutAuthentication_footer">
        <footer className="py-4 bg-light mt-auto">
          <div className="container-fluid px-4">
            <div className="d-flex align-items-center justify-content-between small">
              <div className="text-muted">Copyright &copy; Porli 2024</div>
              <div>
                <a href="#">Privacy Policy</a>
                &middot;
                <a href="#">Terms &amp; Conditions</a>
              </div>
            </div>
          </div>
        </footer>
      </div> */}
    </div>
  );
}

export default Home;
