import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import bgTemplate from "../../../assets/images/iconTemplate_button.png";
import bgDocument from "../../../assets/images/iconDocument_button.png";
import bgSearching from "../../../assets/images/iconSearching_button.png";
import bgSetting from "../../../assets/images/iconSetting_button.png";
import bgUSer from "../../../assets/images/iconUser_button.png";
import bgSharing from "../../../assets/images/iconSharing_button.png";
import { useTranslation } from "react-i18next";
import utils from "../../../utils";

function Menu() {
    const navigate = useNavigate();
    const { t } = useTranslation();
    const [isAdmin, setIsAdmin] = useState(false);
    const [usrname, setUsrname] = useState(null);
    const [isRoot, setIsRoot] = useState(false);

    const [permission, setPermission] = useState({
        isAdmin: false,
        isActivityAccess: false,
        isSearchAccess: false,
        isListTemplate: false,
        isListSetting: false,
        isListUser: false,
        isListPolda: false,
        isListSatker: false,
        isListMetadata: false,
        isListDocType: false,
        isListProvinsi: false,
        isRoleAccess: false,
    });
     useEffect(() => {
        // let isAdmin = JSON.parse(localStorage.getItem("myisadmin"));
        // setIsAdmin(isAdmin);
        // let haveAccessSearch = JSON.parse(localStorage.getItem("dms.search.access"));
        // setPermission({ isSearchAccess: haveAccessSearch, isAdmin: isAdmin });
         let name = localStorage.getItem("myfullname");
        let username = localStorage.getItem("user");
        let role = localStorage.getItem("myrole");
        let isAdmin = localStorage.getItem("myisadmin");
        let haveAccessLog = JSON.parse(localStorage.getItem("dms.log.access"));
        let haveAccessSearch = JSON.parse(localStorage.getItem("dms.search.access"));
        let haveAccessRole = JSON.parse(localStorage.getItem("dms.role.access"));
        let haveListUser = JSON.parse(localStorage.getItem("dms.user.list"));
        let haveListPolda = JSON.parse(localStorage.getItem("dms.polda.list"));
        let haveListSatker = JSON.parse(localStorage.getItem("dms.satker.list"));
        let haveListMetadata = JSON.parse(localStorage.getItem("dms.metadata.list"));
        let haveListDocType = JSON.parse(localStorage.getItem("dms.doctype.list"));
        let haveListProvinsi = JSON.parse(localStorage.getItem("dms.province.list"));
        let haveListTemplate = JSON.parse(localStorage.getItem("dms.template.list"));
        let haveListDocs = JSON.parse(localStorage.getItem("dms.document.list"));
        setPermission({
            isAdmin: isAdmin ,
            isActivityAccess: haveAccessLog,
            isSearchAccess: haveAccessSearch,
            isRoleAccess: haveAccessRole,
            isListTemplate: haveListTemplate,
            isListDocument: haveListDocs,
            isListUser: haveListUser,
            isListDocType: haveListDocType,
            isListPolda: haveListPolda,
            isListSatker: haveListSatker,
            isListMetadata: haveListMetadata,
            isListProvinsi: haveListProvinsi,
            isListSetting: haveListDocType || haveListMetadata || haveListMetadata || haveListPolda || haveListSatker || haveListProvinsi || haveAccessRole,
        });
        setUsrname(username);
        if (username === "root") {
            setIsRoot(true);
        }
         setIsAdmin(isAdmin);

    }, []);

    const toTemplate = () => {
        navigate("/template");
    };
    const toDocument = () => {
        navigate("/document");
    };
    const toSearching = () => {
         if (permission.isSearchAccess || permission.isAdmin) {
            navigate("/searching");
        } else {
            utils.SwalError(t("has_no_permission_um"));
        }
    };

    const toSetting = () => {
        navigate("/setting");
    };
    const toUser = () => {
        // navigate("/user/");
        navigate("/user/list-user");
    };

    const toSharing = () => {
        navigate("/sharing");
    };
    const toMetaData = () => {
        navigate("/setting/configure-meta-data");
    };
    
    const toVerificationTrack = () => {
        navigate("/version-track");
    };
    const toEfaskon = () => {
        if (window.electronAPI?.isElectron) {
            window.electronAPI.openBrowserWindow("https://efaskon.slog.polri.go.id/");
        } else {
            window.electronAPI.openBrowserWindow("https://efaskon.slog.polri.go.id/", "_blank");
        }
    };
    return (
        <div class="container-fluid px-2">
            <h3 class="mt-4" style={{ color: "#05129b", fontFamily: "Russo One" }}>
                {t("beranda_t1")}
            </h3>
            {/* <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
      </ol> */}
            {/* WIDGET */}
            {permission.isListTemplate == true ? 

            <div class="row">
                {/* <div class="col-xl-2 col-md-6">
                    <div class="card card-animate bgcard" style={{ cursor: "pointer" }} onClick={toTemplate}>
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 overflow-hidden">
                                    <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("templat_t1")}</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-end justify-content-between mt-4">
                                <div class=" d-flex align-items-center justify-content-between text-white">
                                    {t("lihat_um")} {t("detail_um")}
                                    <img src={bgTemplate} width={"15%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div> */}
                <div class="col-xl-2 col-md-6">
                    <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#ffbf0075, #d0650c)" }} onClick={toDocument}>
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 overflow-hidden">
                                    <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("dokumen_t1")}</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-end justify-content-between mt-4">
                                <div class=" d-flex align-items-center justify-content-between text-white">
                                    {t("lihat_um")} {t("detail_um")}
                                    <img src={bgDocument} width={"15%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div class="card bg-warning text-white mb-4" onClick={toDocument}>
            <div class="card-body fw-semibold">Document</div>
            <div class="card-footer d-flex align-items-center justify-content-between bg-light bg-opacity-25">
              <a class="small text-white stretched-link">View Details</a>
              <img src={bgDocument} width={"15%"} />
            </div>
          </div> */}
                </div>
                <div class="col-xl-2 col-md-6">
                    <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#ff58aba1, #5f126cde)" }} onClick={toSearching}>
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 overflow-hidden">
                                    <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("pencarian_t1")}</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-end justify-content-between mt-4">
                                <div class=" d-flex align-items-center justify-content-between text-white">
                                    {t("lihat_um")} {t("detail_um")}
                                    <img src={bgSearching} width={"14%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-6">
                    <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#cacacaa1, #646464de)" }} onClick={toSetting}>
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 overflow-hidden">
                                    <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("pengaturan_t1")}</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-end justify-content-between mt-4">
                                <div class=" d-flex align-items-center justify-content-between text-white">
                                    {t("lihat_um")} {t("detail_um")}
                                    <img src={bgSetting} width={"15%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-2 col-md-6">
                    <div class="card card-animate bg-danger " style={{ cursor: "pointer", backgroundImage: "linear-gradient(#ff3e3eb3, #a96262de)" }} onClick={toUser}>
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 overflow-hidden">
                                    <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("pengguna_t1")}</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-end justify-content-between mt-4">
                                <div class=" d-flex align-items-center justify-content-between text-white ">
                                    {t("lihat_um")} {t("detail_um")}
                                    <img src={bgUSer} width={"14%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   
                <div class="col-xl-2 col-md-6">
                    <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#3b97ffc2, #2e2c90c7)" }} onClick={toEfaskon}>
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 overflow-hidden">
                                    <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> Efaskon</p>
                                </div>
                            </div>
                            <div class="d-flex align-items-end justify-content-between mt-4">
                                <div class=" d-flex align-items-center justify-content-between text-white">
                                    {/* {t('lihat_um')} {t('detail_um')} */}
                                    {t("open_browser_nav")}
                                    <img src={bgSharing} width={"14%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            : 
            <div class="row">
            <div class="col-xl-4 col-md-12">
                <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#ffbf0075, #d0650c)" }} onClick={toDocument}>
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1 overflow-hidden">
                                <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("dokumen_t1")}</p>
                            </div>
                        </div>
                        <div class="d-flex align-items-end justify-content-between mt-4">
                            <div class=" d-flex align-items-center justify-content-between text-white">
                                {t("lihat_um")} {t("detail_um")}
                                <img src={bgDocument} width={"15%"} />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xl-4 col-md-12">
                <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#ff58aba1, #5f126cde)" }} onClick={toSearching}>
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1 overflow-hidden">
                                <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> {t("pencarian_t1")}</p>
                            </div>
                        </div>
                        <div class="d-flex align-items-end justify-content-between mt-4">
                            <div class=" d-flex align-items-center justify-content-between text-white">
                                {t("lihat_um")} {t("detail_um")}
                                <img src={bgSearching} width={"14%"} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-12">
                <div class="card card-animate" style={{ cursor: "pointer", backgroundImage: "linear-gradient(#3b97ffc2, #2e2c90c7)" }} onClick={toEfaskon}>
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1 overflow-hidden">
                                <p class="text-uppercase fw-bolder fs-5 text-white text-truncate mb-0"> Efaskon</p>
                            </div>
                        </div>
                        <div class="d-flex align-items-end justify-content-between mt-4">
                            <div class=" d-flex align-items-center justify-content-between text-white">
                                 {t("open_browser_nav")}
                                <img src={bgSharing} width={"14%"} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>}
       
        </div>
    );
}

export default Menu;
