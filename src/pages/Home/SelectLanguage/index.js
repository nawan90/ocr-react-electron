import React, { useState, useEffect } from "react";
import flagId from "../../../assets/images/flag/id.svg";
import flagGb from "../../../assets/images/flag/gb.svg";
import "../../../assets/css/addcss.css";
import { useTranslation } from "react-i18next";

const SelectLanguage = () => {
    const { i18n, t } = useTranslation();

    // Retrieve the language from localStorage

    const bahasa = localStorage.getItem("bahasa") || "id"; // Default to 'id' (Indonesia) if not set
 
    const changeLanguage = (lang) => {
        i18n.changeLanguage(lang);
    };
    const options = [
        { value: "id", label: "Bahasa Indonesia", img: flagId },
        { value: "en", label: "Bahasa Inggris", img: flagGb },
    ];

    // Set the default selected option based on localStorage value
    const initialOption = options.find((option) => option.value === bahasa) || options[0];
    const [selectedOption, setSelectedOption] = useState(initialOption);
    const [isOpen, setIsOpen] = useState(false); // State to track dropdown visibility
    console.log("selectedOption", selectedOption)

    // Update the language in localStorage when the selection changes
    const handleSelect = (option) => {
        setSelectedOption(option);
        localStorage.setItem("bahasa", option.value); // Update localStorage with the selected language
        setIsOpen(false); // Collapse dropdown after selection
        console.log("bahasa", option.value)
        changeLanguage(option.value);
    };

    const toggleDropdown = () => {
        setIsOpen(!isOpen); // Toggle dropdown visibility
    };
  useEffect(() => {
  if (selectedOption.value === "id"){
    localStorage.setItem('i18nextLng', 'id');
    localStorage.setItem('bahasa', 'id');
  } else {
    localStorage.setItem('i18nextLng', 'en');
    localStorage.setItem('bahasa', 'en');
  }
  }, []);
    return (
        <div className="custom-select">
            <button className="select-button" onClick={toggleDropdown}>
                <img src={bahasa === "id" ? flagId : flagGb} style={{ float: "left", marginRight: "8px", width: "25px" }} width={20} />
            </button>

            {isOpen && (
                <div className="select-options">
                    {options.map((option) => (
                        <div key={option.value} className="select-option" onClick={() => handleSelect(option)}>
                            <img src={option.img} width={20} />
                            {option.label}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default SelectLanguage;
