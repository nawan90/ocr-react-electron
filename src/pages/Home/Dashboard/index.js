import React, { useState, useEffect } from "react";
import "../../../assets/css/addcss.css";
import "../../../assets/css/button.css";
import update from "../../../assets/images/update.png";
import Mode from "../../../../package.json";
import { Doc6Month } from "./BarChart/doc_6_month.js";
import { DocumentPolda } from "./PieChart/documentPolda.js";
import { useTranslation } from "react-i18next";
import { DocPolda } from "./BarChart/doc_polda.js";
import LocalStorageHelper from "../../../api/localStorageHelper";
import { useNavigate, useLocation, Link, NavLink } from "react-router-dom";
import utils from "../../../utils/index.js";
import VerificationDashbord from "./Verifikasi/index.js";

function Dashboard() {
  const dataUser = localStorage.getItem("user");
  let base_url = process.env.REACT_APP_API_URL;
  let id_ip = "IP20240605040259";
  const navigate = useNavigate();
  const location = useLocation();
  const queryVariables = new URLSearchParams(location.search);
  const userData = LocalStorageHelper.getUserData();
  const { t } = useTranslation();
  const [isAdmin, setIsAdmin] = useState(false);
  const [data, setData] = useState(null);
  const [dataIpSet, setDataIpSet] = useState(null);
  const [poldaIdLocal, setPoldaIdLocal] = useState(null);
  const [isRoot, setIsRoot] = useState(false);
  const [usrname, setUsrname] = useState(null);
  const [lisensi, setLisensi] = useState(() => {
    // Initialize from localStorage
    const storedValue = localStorage.getItem("lisensi");
    return storedValue === "true"; // Convert string to boolean
  });
  const [updateAvailable, setUpdateAvailable] = useState(false);
  const [updateDownloaded, setUpdateDownloaded] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    const queryParamsString = location.search || location.hash.split("?")[1] || "";
    const queryParams = new URLSearchParams(queryParamsString);
    const message = queryParams.get("message");
    const timestamp = queryParams.get("timestamp");
    setData({ message, timestamp });
  }, [location]);

  useEffect(() => {
    let poldaid = localStorage.getItem("mypoldaid");
    // if ((poldaid && poldaid != "null" && poldaid != "")) {
    // }
    setPoldaIdLocal(poldaid);
  }, []);

  useEffect(() => {
    let username = localStorage.getItem("user");
    setUsrname(username);
    if (username === "root") {
      setIsRoot(true);
    }
  }, []);

  useEffect(() => {
    if (data) {
      localStorage.setItem("data web", JSON.stringify(data));
    } else {
      localStorage.setItem("data web", "null");
    }
  }, [data]);

  useEffect(() => {
    showIpSetting();
  }, []);

  const showIpSetting = async () => {
    try {
      let url = base_url + "ipsetting/" + id_ip;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      setDataIpSet(datas);
    } catch (err) {
      console.log(`${t("error_um")}`);
    }
  };
  useEffect(() => {
    const isAdmin = userData.isAdmin;
    setIsAdmin(isAdmin);
  }, []);

  const showMode = () => {
    const isLatest = /latest/.test(Mode.version);
    if (isLatest) {
      console.log('Latest version detected');
    } else {
      console.log('Not the latest version');
      return (
        <h4 style={{ background: "#5CFF5C", color: "red", fontWeight: "bold" }}>
          DEVELOPMENT MODE
        </h4>
      )
    }
  }

  useEffect(() => {
    const fetchUpdateStatus = async () => {
      try {
        if (window.electronAPI) {
          const status = await window.electronAPI.autoUpdater.getUpdateStatus();
          // Check if the response contains the expected properties
          if (status && typeof status.updateAvailable !== "undefined" && typeof status.updateDownloaded !== "undefined") {
            setUpdateAvailable(status.updateAvailable);
            setUpdateDownloaded(status.updateDownloaded);
          } else {
            console.error("Unexpected response from getUpdateStatus:", status);
          }
        }
      } catch (error) {
        console.error("Failed to fetch update status:", error);
      }
    };
    // Poll every 5 seconds to fetch the update status
    const interval = setInterval(fetchUpdateStatus, 5000);
    fetchUpdateStatus(); // Initial fetch
    return () => clearInterval(interval);
  }, []);

  const installUpdate = () => {
    window.electronAPI.autoUpdater.quitAndInstall();
  };
  useEffect(() => {
    const isAdmin = userData.isAdmin;
    setIsAdmin(isAdmin);
  }, []);

  useEffect(() => {
    if (lisensi == false) {
      utils.sweetAlertWarn("Out");
      navigate("/login");
    }
    else {
    }
  }, []);

  return (
    <div class="container-fluid px-2">
      <div class="row py-2 mt-4 mb-4 bg-light">
        {showMode()}
        <div>
          {updateDownloaded && (
            <div style={{ color: "green" }}>
              <h5>{t("diunduh_pembaruan")}</h5>
              {/* <button onClick={installUpdate}>Install Update</button> */}
              <button onClick={installUpdate} class="button-update mx-1"><div class="spinner"></div>{t("instal_pembaruan")} <img src={update} class="ml-3" width={20} /></button>
            </div>
          )}

          {errorMessage && (
            <div style={{ color: "red" }}>
              <p>{t("kesalahan_pembaruan")} {errorMessage}</p>
            </div>
          )}
        </div>
        {/* <h2 class="row mb-2 text-danger">
          TES AUTOUPDATER
         </h2> */}
        <div class="col-12 mt-4">
          {isAdmin ?
            <div>
              <div class="row mb-2">
                <div class="col-8">
                  <div class="card border border-light shadow-sm p-2 bg-body rounded">
                    <div class="card-body">
                      <h5 className="text-secondary">{t("title_doc_temp_graph_t1")}</h5>
                      <hr></hr>
                      <Doc6Month getData={dataIpSet} />
                    </div>
                  </div>
                </div>

                <div class="col-4">
                  <div class="card border border-light shadow-sm p-2 bg-body rounded">
                    <div class="card-body">
                      <h5 className="text-secondary">{t("title_doc_polda_graph_t1")}</h5>
                      <hr></hr>
                      <DocumentPolda getData={dataIpSet} />
                    </div>
                  </div>
                </div>
              </div>
              <div class="row mb-2">
                <div class="card border border-light shadow-sm p-2 bg-body rounded">
                  <div class="card-body">
                    <h5 className="text-secondary">{t("title_doc_temp_graph_t1")} Polda</h5>
                    <hr></hr>
                    <DocPolda poldaIdLocal={poldaIdLocal} getData={dataIpSet} />
                  </div>
                </div>
              </div>
            </div>
            :
            <div class="row mb-2">
              <div class="col 12">
                <div class="card border border-light shadow-sm p-2 bg-body rounded">
                  <div class="card-body">
                    <h5 className="text-secondary">{t("title_doc_temp_graph_t1")} Polda</h5>
                    <hr></hr>
                    <DocPolda poldaIdLocal={poldaIdLocal} getData={dataIpSet} />
                  </div>
                </div>
              </div>
            </div>
          }
          <VerificationDashbord />
        </div>
      </div>
    </div>
  );
}
export default Dashboard;