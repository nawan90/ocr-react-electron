import React, { useState, useEffect } from "react"; 
import user from "../../../../assets/images/iconUser_button.png";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import helper from "../../../../api/helper";

function JumlahUser() {
  let base_url = process.env.REACT_APP_API_URL;
  let url = base_url + "polridocument/@aggregation?type_name=Document&_metadata=doc_id,doc_doc_type_id&_size=100000";
  const [dataUser, setDataUser] = useState([]);
  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");
  const [allData, setAllData] = useState([]);
  const navigate = useNavigate();
  const { t } = useTranslation();
  // useEffect(() => {
  //   const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

  //   const urlValid = () => {
  //     if (ip === "116.90.165.46") {
  //       setDataIp(base_url + "@dashboard"); // Assuming base_url is defined elsewhere
  //     } else {
  //       setDataIp(ipServer + "@dashboard");
  //     }
  //   };

  //   urlValid();
  // }, [ip]);

  // useEffect(() => {
  //   if (dataIp) {
  //     showDataUser();
  //   }
  // }, [dataIp]);
  // useEffect(() => {
  //   showDataUser();
  // }, []);
 
  // const showDataUser = async () => {
  //   try {
  //      const myHeaders = new Headers();
  //     myHeaders.append("Content-Type", "application/json");
  //     myHeaders.append("Authorization", "Basic cm9vdDpyb290");

  //     const requestOptions = {
  //       method: "GET",
  //       headers: myHeaders,
  //     };
  //     var res = await fetch(dataIp, requestOptions);
  //     var datas = await res.json(); 
  //     setDataUser(datas.doc_id.total)
  //     setAllData(datas);

  //   } catch (err) {
   //   }
  // };
  const toListUser = () => {
    navigate("/user/list-user");
   };

  return (
    <div>
         <div class="card card-animate mb-2 bg-danger"  onClick={toListUser}>
            <div class="card-content">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="align-self-center">
                    <img src={user} width={40} />
                  </div>
                  <div class=" text-end">
                   { allData === undefined || allData.length == 0?(
                  <span style={{color:dataUser === undefined || dataUser.length == 0 ?"white" : "red", fontWeight:"bold", fontSize:"12px"}}>
                    <IpNotValidPage/>
                    <br/>
                  </span>  
                   )
                   :(  
                    <div>
                      {dataUser === undefined ? (
                        <div
                        style={{
                          color: "red",
                          fontWeight: "bold",
                          fontSize: "16px",
                          textAlign: "center",
                          margin: "0 auto",
                          marginTop:"180px"
                        }}
                      >
                        Data Tidak Ada
                      </div>
                      ) : (

                      <h3>{dataUser}  <span>User</span>
                   </h3>   
                  )  }
                    </div>
                   )  }
                  </div>
                </div>
              </div>
            </div>
          </div>
      
    </div>
   );
}
export default JumlahUser;
