import React, { useState, useEffect } from "react";
import template from "../../../../assets/images/icon/temp.png";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import helper from "../../../../api/helper";

function JumlahTemplate({ getData }) {
  let base_url = process.env.REACT_APP_API_URL;
  let url = base_url + "@aggregation?type_name=Template_Master&_metadata=template_id&_size=10000";
  const [dataTemplate, setDataTemplate] = useState(""); // Change to an empty string
  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");
  const { t } = useTranslation();
  const navigate = useNavigate();

  useEffect(() => {
    const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

    const urlValid = () => {
      if (ip === "116.90.165.46") {
        setDataIp(base_url + "@aggregation?type_name=Template_Master&_metadata=template_id&_size=10000");
      } else {
        setDataIp(ipServer + "@aggregation?type_name=Template_Master&_metadata=template_id&_size=10000");
      }
    };

    urlValid();
  }, [ip]);

  useEffect(() => {
    if (dataIp && !dataIp.includes("undefined")) {
      showDataTemplate();
    }
  }, [dataIp]);

  useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
  }, [getData]); // Added dependency on getData

  const showDataTemplate = async () => {
    try {
      let response = await helper.GetListDataDashboard(dataIp);
      if (response.status === 200 || response.status === 201) {
        setDataTemplate(response?.data?.template_id?.total ?? ""); // Change to an empty string if null
      }
    } catch (err) {
      console.log(`${t('error_um')}`);
    }
  };

  const toListTemplate = () => {
    navigate("/template/list-template");
  };

  return (
    <div>
      <div className="card card-animate mb-4 border border-light shadow-sm p-2 bg-body rounded" onClick={toListTemplate}>
        <div className="card-content">
          <div className="card-body">
            <div className="position-absolute start-0" style={{ zIndex: 0, marginTop: -15 }}></div>
            <div className="d-flex align-items-center justify-content-between">
              <div className="align-self-center">
                <img src={template} width={45} alt="Template Icon" />
              </div>
              <div className="text-end fw-medium">
                {dataTemplate === "" ? ( // Check for empty string
                  <span style={{ color: "white", fontWeight: "bold", fontSize: "12px" }}>
                    <IpNotValidPage />
                    <br />
                  </span>
                ) : (
                  <h1 className="text-primary">{dataTemplate}</h1>
                )}
                <span>{t("templat_t1")}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default JumlahTemplate;
