import React, { useState, useEffect } from "react";
import docWidget from "../../../../assets/images/icon/doc_widget.png";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import helper from "../../../../api/helper";

function JumlahDocument({getData}) {
  let base_url = process.env.REACT_APP_API_URL;
  const [dataDocument, setDataDocument] = useState("");
  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");

  const { t } = useTranslation();
  const navigate = useNavigate();
 
  useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
 }, []);
 
  useEffect(() => {
 
  
    const ipServer = ip ? `${ip}/db/dmsbackend/` : "http://116.90.165.46:8069/db/dmsbackend/";

    const urlValid = () => {
      if (ip === "116.90.165.46") {
        setDataIp(base_url + "polridocument/@aggregation?type_name=Document&_metadata=doc_id,doc_type_id&_size=100000");
      } else {
        setDataIp(ipServer + "polridocument/@aggregation?type_name=Document&_metadata=doc_id,doc_type_id&_size=100000");
      }
    };

    urlValid();
  }, [ip]);
  useEffect(() => {
    if (dataIp && !dataIp.includes("undefined")) {
      showDataDocument();
    }
  }, [dataIp]); 

  const showDataDocument = async () => {
    try {
      let response = await helper.GetListDataDashboard(dataIp);
      if (response.status === 200 || response.status === 201) {
        setDataDocument(response?.data?.doc_id?.total ?? "");
        // setDataDocument(response)
      }
    } catch (err) {
      console.log(`${t('error_um')}`);
    }
  };

  const toListDocument = () => {
    navigate("/document/list-document");
  };

  return (
    <div>
      <div className="card card-animate mb-4 border border-light shadow-sm p-2 bg-body rounded" onClick={toListDocument}>
        <div className="card-content">
          <div className="card-body">
            <div className="position-absolute start-0" style={{ zIndex: 0, marginTop: -15 }}></div>
            <div className="d-flex align-items-center justify-content-between">
              <div className="align-self-center">
                <img src={docWidget} width={50} />
              </div>
              <div className="text-end fw-medium">
                {dataDocument === null ? (
                  <span
                    style={{
                      color: "white",
                      fontWeight: "bold",
                      fontSize: "12px",
                    }}
                  >
                    <IpNotValidPage />
                    <br />
                  </span>
                ) : (
                  <h1 className="text-primary">{dataDocument}</h1>
                )}
                <span>{t("dokumen_t1")}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default JumlahDocument;
