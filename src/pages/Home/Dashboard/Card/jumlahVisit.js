import React, { useState, useEffect } from "react"; 
import user from "../../../../assets/images/iconUser_button.png";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import helper from "../../../../api/helper";

function JumlahVisit() {
  let base_url = process.env.REACT_APP_API_URL;
  let url = base_url + "polridocument/@aggregation?type_name=Document&_metadata=doc_id,doc_doc_type_id&_size=100000";
  const [dataDocument, setDataDocument] = useState([]);
  const [ip, setIp] = useState(window.localStorage.getItem("ipAddress"));
  const ipServer = "http://" + ip + ":8069/db/dmsbackend/"
  const { t } = useTranslation();

  // const urlValid = () => {
  //   if (ip === "116.90.165.46") {
  //     return base_url + "polridocument/@aggregation?type_name=Document&_metadata=doc_id,doc_doc_type_id&_size=100000"
  //   } else if (ip !== "116.90.165.46") {
  //     return ipServer + "polridocument/@aggregation?type_name=Document&_metadata=doc_id,doc_doc_type_id&_size=100000"
  //   }
  // }
   
  // useEffect(() => {
  //   urlValid();
  // }, []);
  
  // useEffect(() => {
  //   showDataDocument();
  // }, []);
 
  // const showDataDocument = async () => {
  //   try {
  //      const myHeaders = new Headers();
  //     myHeaders.append("Content-Type", "application/json");
  //     myHeaders.append("Authorization", "Basic cm9vdDpyb290");

  //     const requestOptions = {
  //       method: "GET",
  //       headers: myHeaders,
  //     };
  //     var res = await fetch(urlValid(), requestOptions);
  //     var datas = await res.json(); 
  //     setDataDocument(datas.doc_id.total)
  //   } catch (err) {
   //   }
  // };

  return (
    <div>
         <div class="card card-animate mb-2 bg-danger">
            <div class="card-content">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                  <div class="align-self-center">
                    <img src={user} width={40} />
                  </div>
                  <div class=" text-end">
                   {dataDocument === undefined || dataDocument.length == 0 ?
                  <span style={{color:dataDocument === undefined || dataDocument.length == 0 ?"white" : "red", fontWeight:"bold", fontSize:"12px"}}>
                    <IpNotValidPage/>
                    <br/>
                  </span>  
                   :   <h3>{dataDocument}</h3> 
                    }
                    <span>  Visits</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
    </div>
   );
}
export default JumlahVisit;
