import React, { useState, useEffect } from "react";
import polda from "../../../../assets/images/icon/polda.png";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import helper from "../../../../api/helper";

function JumlahPolda({ getData }) {
  let base_url = process.env.REACT_APP_API_URL;
  let url = base_url + "polridocument/@aggregation?type_name=Polda_Master&_metadata=polda_id&_size=1000";
  const [dataPolda, setDataPolda] = useState(""); // Initialize as an empty string

  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");
  const { t } = useTranslation();
  const navigate = useNavigate();

  useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
  }, [getData]); // Added dependency on getData

  useEffect(() => {
    const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

    const urlValid = () => {
      if (ip === "116.90.165.46") {
        setDataIp(base_url + "polridocument/@aggregation?type_name=Polda_Master&_metadata=polda_id&_size=1000");
      } else {
        setDataIp(ipServer + "polridocument/@aggregation?type_name=Polda_Master&_metadata=polda_id&_size=1000");
      }
    };

    urlValid();
  }, [ip]);

  useEffect(() => {
    if (dataIp && !dataIp.includes("undefined")) {
      showDataPolda();
    }
  }, [dataIp]);

  const showDataPolda = async () => {
    try {
      let response = await helper.GetListDataDashboard(dataIp);
      if (response.status === 200 || response.status === 201) {
        setDataPolda(response?.data?.polda_id?.total ?? ""); // Use an empty string if null
      }
    } catch (err) {
      console.log(`${t('error_um')}`);
    }
  };

  const toListUser = () => {
    navigate("/user/list-user");
   };

  return (
    <div>
      <div className="card card-animate mb-4 border border-light shadow-sm p-2 bg-body rounded" onClick={toListUser}>
        <div className="card-content">
          <div className="card-body">
            <div className="position-absolute start-0" style={{ zIndex: 0, marginTop: -15 }}></div>
            <div className="d-flex align-items-center justify-content-between">
              <div className="align-self-center">
                <img src={polda} width={50} alt="Polda Icon" />
              </div>
              <div className="text-end fw-medium">
                {dataPolda === "" ? ( // Check for empty string
                  <span
                    style={{
                      color: "white", // Use a consistent color
                      fontWeight: "bold",
                      fontSize: "12px",
                    }}
                  >
                    <IpNotValidPage />
                    <br />
                  </span>
                ) : (
                  <h1 className="text-primary">{dataPolda}</h1>
                )}
                <span>{t("pengguna_t1")}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default JumlahPolda;
