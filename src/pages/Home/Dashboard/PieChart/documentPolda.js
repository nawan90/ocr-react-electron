import React, { useState, useEffect } from "react";
import { Chart } from "react-google-charts";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import helper from "../../../../api/helper";

export function DocumentPolda({ getData }) {
  let base_url = process.env.REACT_APP_API_URL;
  const [dataDashboard, setDataDashboard] = useState({}); // Initialize as an empty object
  const [allData, setAllData] = useState([]);
  const header = [["Polda", "Total"]];
  const result = Object.keys(dataDashboard).map((key) => [key, dataDashboard[key]]);
  const mergeResult = [...header, ...result];
  const { t } = useTranslation();
  const options = {
    // title: t("title_doc_polda_graph_t1"),
  };
  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");
   
  useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
  }, [getData]); // Added dependency on getData

  useEffect(() => {
    const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

    const urlValid = () => {
      if (ip === "116.90.165.46") {
        setDataIp(base_url + "@dashboard");
      } else {
        setDataIp(ipServer + "@dashboard");
      }
    };

    urlValid();
  }, [ip]);

  useEffect(() => {
    if (dataIp && !dataIp.includes("undefined")) {
      showDataDashboard();
    }
  }, [dataIp]);

  const showDataDashboard = async () => {
    try {
      let response = await helper.GetListDataDashboard(dataIp);

      if (response.status === 200 || response.status === 201) {
        setDataDashboard(response.data.docperpolda || {}); // Ensure it's always an object
        setAllData(response.data);
      }
    } catch (err) {
      console.log(`${t('error_um')}`);
    }
  };

  return (
    <div style={{ height: "400px" }}>
      {Object.keys(dataDashboard).length === 0 ? ( // Check if the object is empty
        <div style={{ color: "red", fontWeight: "bold", fontSize: "12px", textAlign: "center", margin: "0 auto" }}>
          <h4>
            <IpNotValidPage />
          </h4>
        </div>
      ) : (
        <Chart chartType="PieChart" data={mergeResult} options={options} width={"100%"} height={"400px"} />
      )}
    </div>
  );
}
