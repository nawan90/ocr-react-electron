import { useState, useEffect } from "react";
import dataObject from '../../../../fakeData/listVerifikasi.json'
import {
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import Filters from "../../../../components/Table/filters";
import Paginations from "../../../../components/Table/pagination";
import LocalStorageHelper from "../../../../api/localStorageHelper";
import { useTranslation } from "react-i18next";



function TableVerification(props) {
  const { dataTable } = props
  const userData = LocalStorageHelper.getUserData();
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [data, setData] = useState([]);
  const { t } = useTranslation();
  
  useEffect(() => {
     if (dataTable && Object.keys(dataTable).length > 0) {
        setData(
            Object.entries(dataTable).map(([name, values]) => ({
                name,
                ...values
            }))
        );
    }
}, [dataTable]);
 
const columns = [
  {
    accessorKey: "no",
    header: "No",
    cell: ({ row }) => row.index + 1, // Auto-generate numbering
  },
  {
    accessorKey: "name",
    header: "Polda / Satker",
  },
  {
    accessorKey: "sudah_verifikasi",
    header: t("sudah_dash_ver"),
  },
  {
    accessorKey: "belum_verifikasi",
    header: t("belum_dash_ver"),
  }
];

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        const id = Math.floor(Math.random() * 10000);
        const newRow = {
          id,
          studentNumber: id,
          name: "",
          dateOfBirth: "",
          major: "",
        };
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  }); 

  return (
    <div className="p-4">
      <Filters filtering={filtering} setFiltering={setFiltering} />
      <table className="mb-2 w-full border-collapse border border-gray-300">
        <thead>
          {table.getHeaderGroups().map(headerGroup => (
            <tr key={headerGroup.id} className="bg-gray-100">
              {headerGroup.headers.map(header => (
                <th key={header.id} className="border border-gray-300 p-2">
                  {flexRender(header.column.columnDef.header, header.getContext())}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map(row => (
            <tr key={row.id} className="hover:bg-gray-50">
              {row.getVisibleCells().map(cell => (
                <td key={cell.id} className="border border-gray-300 p-2">
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <Paginations table={table} />
    </div>
  );
}

export default TableVerification;
