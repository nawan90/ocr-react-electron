import React, { useState, useEffect } from 'react'
import TableVerification from './tableVerifikasi'
import { DocumentVerfication } from './documentVerification'
import LocalStorageHelper from '../../../../api/localStorageHelper';
import { useTranslation } from "react-i18next";

function VerificationDashbord() {
    const [usrname, setUsrname] = useState(null);
    const [isRoot, setIsRoot] = useState(false);
    const [data, setData] = useState(null);
    const [tableVerification, setTableVerification] = useState([]);
  const { t } = useTranslation();

    const userData = LocalStorageHelper.getUserData();
 
    useEffect(() => {
        let username = localStorage.getItem("user");
        setUsrname(username);
        if (username === "root") {
            setIsRoot(true);
        }
    }, []);
    useEffect(() => {
        showTableVerification(); // Fetch dashboard data when poldaID changes
    }, []
    );

    const showTableVerification = async () => {
        try {
            let url = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/@dashboard_verif';
    
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);
    
            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
    
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
     
            setTableVerification(datas);
        } catch (err) {
            console.error("Failed to fetch data:", err);
        }
    };
    
    return (
        <div >
            {usrname === "root" ? (
                <div class="row mb-2">
                    <div class="col-4">
                        <div class="card border border-light shadow-sm p-2 bg-body rounded">
                            <div class="card-body">
                                <h5 className="text-secondary">{t("title_pie_dash_ver")}</h5>
                                <hr></hr>
                                <DocumentVerfication tableVerification={tableVerification} />
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="card border border-light shadow-sm p-2 bg-body rounded">
                            <div class="card-body">
                                <h5 className="text-secondary">{t("title_table_dash_ver")}</h5>
                                <hr></hr>
                                <TableVerification dataTable={tableVerification} />
                                </div>
                        </div></div>
                </div>
            ) : null}
        </div>
    )
}
export default VerificationDashbord