import React, { useState, useEffect } from "react";
import { Chart } from "react-google-charts";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import helper from "../../../../api/helper";

export function DocumentVerfication(props) {
  const {getData, tableVerification}   = props
  let base_url = process.env.REACT_APP_API_URL;
  const [dataDashboard, setDataDashboard] = useState([]);
  const [allData, setAllData] = useState([]);
  const ip = localStorage.getItem("ipAddress");
  const [dataIp, setDataIp] = useState("");
  const { t } = useTranslation();

   useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
  }, [getData]);

  useEffect(() => {
    const ipServer = ip ? `${ip}/db/dmsbackend/` : "";
    setDataIp(ip === "116.90.165.46" ? base_url + "@dashboard" : ipServer + "@dashboard");
  }, [ip]);

  useEffect(() => {
    showDataDashboard();
  }, []);

  const showDataDashboard = async () => {
    try {
      let response = await helper.GetListDataDashboard(dataIp);
      if (response.status === 200) {
        setDataDashboard(response.data.docpertype);
        setAllData(response.data);
      }
    } catch (err) {
      console.log(`${t("error_um")}`);
    }
  };
 
  function transformData(tableVerification, targetName) {
    if (!tableVerification || typeof tableVerification !== "object") {
       return [["Status", "Jumlah Dokumen"], ["Sudah Verifikasi", 0], ["Belum Verifikasi", 0]];
    }

    const target = tableVerification[targetName];

    if (!target) {
       return [["Status", "Jumlah Dokumen"], ["Sudah Verifikasi", 0], ["Belum Verifikasi", 0]];
    }

    return [
      ["Status", "Jumlah Dokumen"],
      [t("sudah_dash_ver"), target.sudah_verifikasi || 0],
      [t("belum_dash_ver"), target.belum_verifikasi || 0],
    ];
  }

  // ✅ Ensure tableVerification exists before transforming data
  const allpolda = tableVerification ? transformData(tableVerification, "allpolda") : [["Status", "Jumlah Dokumen"], ["Sudah Verifikasi", 0], ["Belum Verifikasi", 0]];
  const options = {
    title: t("subtitle_pie_dash_ver"),
  };

  return (
    <div style={{ height: "400px" }}>
      <Chart chartType="PieChart" data={allpolda} options={options} width={"100%"} height={"400px"} />
    </div>
  );
}
