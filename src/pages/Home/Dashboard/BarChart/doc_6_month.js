import React, { useState, useEffect } from "react";
import { Chart } from "react-google-charts";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import helper from "../../../../api/helper";

export const data = [
  ["Date", "SHP", "AJB", "Letter C", "Eigendom", "SKep", "Surat-Surat"],
  ["01-2024", 1000, 52, 0, 1, 324, 423],
  ["02-2024", 1170, 53, 0, 1, 875, 534],
  ["03-2024", 660, 94, 0, 1, 435, 467],
  ["04-2024", 1030, 63, 0, 1, 243, 865],
  ["05-2024", 1000, 64, 0, 1, 637, 346],
  ["06-2024", 1170, 64, 0, 1, 736, 654],
];
 

export function Doc6Month({ getData }) {
  let base_url = process.env.REACT_APP_API_URL;
   // let url = base_url + "@dashboard";
  const [ajb, setAjb] = useState([]);
  const [eigendom, setEigendom] = useState([]);
  const [sertifikatElektronik, setSertifikatElektronik] = useState([]);
  const [sertifikatHakPakai, setSertifikatHakPakai] = useState([]);
  const [sKepHapus, setSkepHapus] = useState([]);
  const [skepPsp, setSkepPsp] = useState([]);

  // const [allData, setAllData] = useState({});
  const [allData, setAllData] = useState({ docpertypebymonth: {} });

  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");
  const { t } = useTranslation();
  const options = {
    chart: {
       subtitle: t("subtitle_doc_temp_graph_t1"),  // Use `t` here to translate
    },
  };
  useEffect(() => {
    const ipServer = ip ? `${ip}` : "";

    const urlValid = () => {
      if (ip === "https://efaskon-dms.slog.polri.go.id") {
        setDataIp(base_url + "@dashboard");
      } else {
        setDataIp(ipServer + "@dashboard");
      }
    };

    urlValid();
  }, [ip]);
 
  useEffect(() => {
    if (dataIp) {
      showDataDashboard();
    }
  }, [dataIp]);
 
  useEffect(() => {
    showDataDashboard();
  }, []);
 
  // const header = [[t("bulan_t1"), "Akta Jual Beli","Sertifikat Hak Pakai", "EIGENDOM", "Sertifikat Elektronik", "Surat Keputusan (SKep) Hapus", "Surat Keputusan (SKep) PSP" ]];

  // const result = [];

  // for (const key in ajb) {
  //   if (ajb.hasOwnProperty(key)) {
  //     const value1 = ajb[key];
  //     const value2 = eigendom[key] || 0;
  //     const value3 = sertifikatElektronik[key]|| 0;
  //     const value4 = sertifikatHakPakai[key] || 0;
  //     const value5 = sKepHapus[key]|| 0;
  //     const value6 = skepPsp[key] || 0;
  //     result.push([key, value1, value2]);
  //   }
  // }

  // const mergeResult = [...header, ...result];
  // const transformData = (allData) => {
  //   const docpertypebymonth = allData?.docpertypebymonth;
  
  //   // Extract months and document types
  //   const months = Object.keys(docpertypebymonth[Object.keys(docpertypebymonth)[0]]);
  //   const documentTypes = Object.keys(docpertypebymonth);
  
  //   // Build the header row
  //   const header = ["Bulan", ...documentTypes];
  
  //   // Build the data rows
  //   const rows = months.map((month) => {
  //     const row = [month];
  //     documentTypes.forEach((docType) => {
  //       row.push(docpertypebymonth[docType][month]);
  //     });
  //     return row;
  //   });
  
  //   // Combine header and rows
  //   return [header, ...rows];
  // };
  const transformData = (allData) => {
    const docpertypebymonth = allData?.docpertypebymonth || {};
  
    if (Object.keys(docpertypebymonth).length === 0) {
      console.warn("docpertypebymonth is empty or undefined");
      return [["Bulan"]]; // Return a default header if no data is available
    }
  
    const documentTypes = Object.keys(docpertypebymonth);
    const months = Object.keys(docpertypebymonth[documentTypes[0]] || {});
  
    const header = ["Bulan", ...documentTypes];
    const rows = months.map((month) => {
      const row = [month];
      documentTypes.forEach((docType) => {
        row.push(docpertypebymonth[docType]?.[month] || 0);
      });
      return row;
    });
  
    return [header, ...rows];
  };
  
  // Get the transformed data
  // const mergeResult = transformData(allData);
  const mergeResult = allData.docpertypebymonth
  ? transformData(allData)
  : [["Bulan"]]; // Default header in case of no data


  // const showDataDashboard = async () => {
  //   try {
  //     let response = await helper.GetListDataDashboard(dataIp);

  //     if (response.status == 200 || 201) {
  //       setAjb(response.data.docpertypebymonth["Akta Jual Beli"]);
  //       setSertifikatHakPakai(response.data.docpertypebymonth["Sertifikat Hak Pakai"]);
  //       setEigendom(response.data.docpertypebymonth["EIGENDOM"]);
  //       setSertifikatElektronik(response.data.docpertypebymonth["Sertifikat Elektronik"]);
  //       setSkepHapus(response.data.docpertypebymonth["Surat Keputusan (SKep) Hapus"]);
  //       setSkepPsp(response.data.docpertypebymonth["Surat Keputusan (SKep) PSP"]); 
  //       setAllData(response.data);
  //     }
  //   } catch (err) {
  //     console.log(`${t('error_um')}`);
  //   }
  // };
  const showDataDashboard = async () => {
    try {
      let response = await helper.GetListDataDashboard(dataIp);
  
      if (response.status === 200 || response.status === 201) {
        const data = response.data;
        if (data?.docpertypebymonth) {
          setAjb(data.docpertypebymonth["Akta Jual Beli"] || {});
          setSertifikatHakPakai(data.docpertypebymonth["Sertifikat Hak Pakai"] || {});
          setEigendom(data.docpertypebymonth["EIGENDOM"] || {});
          setSertifikatElektronik(data.docpertypebymonth["Sertifikat Elektronik"] || {});
          setSkepHapus(data.docpertypebymonth["Surat Keputusan (SKep) Hapus"] || {});
          setSkepPsp(data.docpertypebymonth["Surat Keputusan (SKep) PSP"] || {});
          setAllData(data);
        } else {
          console.warn("docpertypebymonth is missing in the response");
          setAllData({ docpertypebymonth: {} });
        }
      }
    } catch (err) {
      console.error("Failed to fetch data:", err);
    }
  };
  
  return (
    <div style={{ height: "500px" }}> 
        <div>
          {allData.length === 0 || allData.docpertypebymonth == undefined ? (
            <div
              style={{
                color: "red",
                fontWeight: "bold",
                fontSize: "16px",
                textAlign: "center",
                margin: "0 auto",
                marginTop: "180px",
              }}
            >
              Data Tidak Ada
            </div>
          ) : (
            <Chart chartType="Bar" width="100%" height="500px" data={mergeResult} options={options} />
          )}
        </div>
      {/* )} */}  
    </div>
  );
}
