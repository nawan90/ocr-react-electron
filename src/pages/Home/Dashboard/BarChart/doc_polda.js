import React, { useState, useEffect, useMemo } from "react";
import { Chart } from "react-google-charts";
import IpNotValidPage from "../../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import helper from "../../../../api/helper";
import Select from "react-select";

// export const data = [
//   [
//     "Sertifikat Hak Pakai",
//     "Kep Hapus",
//     "BAST (Hibah)",
//     "Surat-surat",
//     "Kep PSP",
//     "Sertifikat Elektronik",
//     "Kep Sewa",
//     "Sertifikat Hak Milik",
//     "Akta Jual Beli",
//     "HIBAH (Bast)",
//     "EIGENDOM",
//     "Surat Keputusan (SKep) Hapus",
//     "Surat Keputusan (SKep) PSP",
//   ],
//   [10, 0, 0, 4, 0, 1, 0, 0, 1, 0, 1, 0, 0],
// ];

export function DocPolda({ getData,poldaIdLocal }) {
  let base_url = process.env.REACT_APP_API_URL;
  // let url = base_url + "@dashboard";
  const [ajb, setAjb] = useState([]);
  const [eigendom, setEigendom] = useState([]);
  const [sertifikatElektronik, setSertifikatElektronik] = useState([]);
  const [sertifikatHakPakai, setSertifikatHakPakai] = useState([]);
  const [sKepHapus, setSkepHapus] = useState([]);
  const [skepPsp, setSkepPsp] = useState([]);
  const [isAdmin, setIsAdmin] = useState(false);
  const [poldaID, setPoldaID] = useState(poldaIdLocal ? poldaIdLocal : "PD2");
  const [satkerID, setSatkerID] = useState("");
  const [poldaOptionsHtml, setPoldaOptionsHtml] = useState([]);
  const [poldaRef, setPoldaRef] = useState(null);
  const [satkerName, setSatkerName] = useState("");
  const [poldaName, setPoldaName] = useState("");
  const [satkerRef, setSatkerRef] = useState(null);
  const [satkerOptionsHtml, setSatkerOptionsHtml] = useState([]);
  const [isRolePoldaSatker, setIsRolePoldaSatker] = useState(false)

  const [dashboardPolda, setDashboardPolda] = useState({});

  const [dataIp, setDataIp] = useState("");
  const ip = localStorage.getItem("ipAddress");
  const { t } = useTranslation();
  const options = {
    // chart: {
    //   subtitle: "All Data", // Use `t` here to translate
    // },
    chart: {
      title: 'Jumlah Dokumen Berdasarkan Tipe',
      subtitle: 'Perbandingan antara jumlah setiap tipe dokumen',
    },
    bars: 'vertical', // Anda bisa mengganti ke 'horizontal' untuk bar chart horizontal
    // hAxis: {
    //   title: 'Jumlah Dokumen',
    // },
    // vAxis: {
    //   title: 'Tipe Dokumen',
    // },
    // colors: ['#1b9e77', '#d95f02', '#7570b3', '#e7298a', '#66a61e', '#e6ab02'],
  };
  useEffect(() => {
    const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

    const urlValid = () => {
      if (ip === "116.90.165.46") {
        setDataIp(base_url);
      } else {
        setDataIp(ipServer);
      }
    };

    urlValid();
  }, [ip]);

  useEffect(() => {
    if(poldaIdLocal && poldaIdLocal != "" && poldaIdLocal != "null") {
      setIsRolePoldaSatker(true)
    }
    else {
      setIsRolePoldaSatker(false)
    }
  }, [poldaIdLocal]);


  useEffect(() => {
    let poldaid = localStorage.getItem("mypoldaid");
    let satkerid = localStorage.getItem("mysatkerid");
    let roleArr = JSON.parse(localStorage.getItem("myrole"));
    if (roleArr.includes("dmsadmin")) {
      setIsRolePoldaSatker(false)
    }

    let isAdmin = JSON.parse(localStorage.getItem("myisadmin"));
    setIsAdmin(isAdmin);

    if (poldaid && poldaid != "null" && poldaid != "") {
      setPoldaID(poldaid)
    }
    // loadSatker(poldaid);
   }, []);

  const loadSatker = async (poldaId) => {
    try {
      // let url = dataIp + "/doctype/@list_doctype";

      let options = [];
      if (dataIp && dataIp != "") {
        // let res = await helper.GetListDocType(url);
        let res = await helper.GetListSatker(
          dataIp + "/polridocument/" + poldaId + "/@list_satker"
        );
        if (res && (res.status == 200 || 201 || 202 || 204)) {
          let datas = res.data;
          setSatkerRef(res);

          res.map(function (data, index) {
            // options.push(<option value={data.polda_id}>{`${data.polda_name} `}</option>);
            options.push({
              value: data.satker_id,
              label: `${data.satker_name}`,
            });
          });

          setSatkerOptionsHtml(options);
        }
      }
    } catch (err) {
      console.log("loadPolda", err);
    }
  };
  useEffect(() => {
    if (dataIp && !dataIp.includes("undefined")) {
      // showTable();
      loadPolda();
      loadSatker(poldaID);
      // loadMetadata();
    }
  }, [dataIp]);

  const loadPolda = async () => {
    try {
      // let url = dataIp + "/doctype/@list_doctype";

      let options = [];
      if (dataIp && dataIp != "") {
        // let res = await helper.GetListDocType(url);
        let res = await helper.GetListPolda(
          dataIp + "/polridocument/@list_polda"
        );
        if (res && (res.status == 200 || 201 || 202 || 204)) {
          let datas = res.data;
          setPoldaRef(res);

          res.map(function (data, index) {
            // options.push(<option value={data.polda_id}>{`${data.polda_name} `}</option>);
            options.push({ value: data.polda_id, label: `${data.polda_name}` });
          });

          setPoldaOptionsHtml(options);
        }
      }
    } catch (err) {
      console.log("loadPolda", err);
    }
  };

  const transformToChartData = (data) => {
    // Define all possible document types in the correct order
     const documentTypes = [
      "Jumlah Dokumen per Jenis Dokumen",
      "Sertifikat Hak Pakai",
      "Kep Hapus",
      "BAST (Hibah)",
      "Surat-surat",
      "Kep PSP",
      "Sertifikat Elektronik",
      "Kep Sewa",
      "Sertifikat Hak Milik",
      "Akta Jual Beli",
      "HIBAH (Bast)",
      "EIGENDOM",
      "Surat Keputusan (SKep) Hapus",
      "Surat Keputusan (SKep) PSP",
    ];

    // Extract the 'docpertype' from the data response
    const docpertype = data?.docpertype || {};
     // Prepare the first row for the document types (header row)
    const header = documentTypes;
     // Prepare the second row with the actual values from the API response
    const values = documentTypes.map((docType) => docpertype[docType] || 0);
    // console.log('transformToChartData', [header, values])
    // Return the chart data array
    return [header, values];
    // const chartData = [
    //   ['Dokumen', 'Jumlah'],
    //   ['Sertifikat Hak Pakai', 6],
    //   ['Akta Jual Beli', 1],
    //   ['Sertifikat Elektronik', 1],
    //   ['EIGENDOM', 1],
    //   ['Surat Keputusan (SKep) Hapus', 0],
    //   ['Surat Keputusan (SKep) PSP', 0],
    //   ['BAST (Hibah)', 0],
    //   ['Kep Hapus', 0],
    //   ['Kep PSP', 0],
    //   ['Surat-surat', 0],
    //   ['Kep Sewa', 0],
    //   ['Sertifikat Hak Milik', 0],
    //   ['HIBAH (Bast)', 0],
    // ];

    // return chartData
  };

  // const chartData = transformToChartData(dashboardPolda);
  const chartData = transformToChartData(dashboardPolda);
 
  const changePolda = (e) => {
    if (poldaRef) {
      if (e) {
        const { value, label } = e;
        setSatkerID(null);
        setSatkerName(null);
        setPoldaID(value);
        setPoldaName(label);
        loadSatker(value); // Load Satker options for the selected Polda
      } else {
        setPoldaID(null);
        setPoldaName(null);
      }
    }
  };
  useEffect(() => {
    if (dataIp) {
      showDataPoldaDashboard();
    }
  }, [dataIp]);

  useEffect(() => {
    showDataPoldaDashboard();
  }, []);
  // Fetch dashboard data based on selected Polda
  useEffect(() => {
    if (poldaID) {
      showDataPoldaDashboard(); // Fetch dashboard data when poldaID changes
    }
  }, [poldaID]);

  const showDataPoldaDashboard = async () => {
     if (poldaID) {
      try {
        let url = dataIp + '/@dashboard_bypolda?doc_polda_id='+poldaID
         let response = await helper.GetDashboardPerPolda(url);

        if (response.status === 200 || response.status === 201) {
          const data = response.data;
          if (data) {
            setDashboardPolda(data);
          } else {
            console.warn("No data found for the selected Polda.");
          }
        }
      } catch (err) {
        console.error("Failed to fetch data:", err);
      }
    }
  };

  return (
    <div style={{ height: "600px" }}>
      <div class="col-12 mb-5">
        <Select
          id="SelectPolda"
          value={
            poldaOptionsHtml.find((option) => option.value === poldaID) || null
          }
          onChange={changePolda}
          options={poldaOptionsHtml}
          placeholder={`${t("select_um")} ${t("titlle_lp")}`}
          className=""
          classNamePrefix="select"
          isDisabled={isRolePoldaSatker}
          // disabled={poldaIdLocal ? true : false}
          // isDisabled={ (poldaIdLocal) =>  poldaIdLocal ? true : false }

          // isOptionDisabled={(poldaIdLocal) => poldaIdLocal ? true : false}  
          // isDisabled={(poldaIdLocal) =>  poldaIdLocal && poldaIdLocal != "null" && poldaIdLocal != "" ? true : false}
          // isSearchable={ poldaIdLocal ? false : true}
        />
      </div>
      <div>
        {dashboardPolda == null || undefined ? (
          <div
            style={{
              color: "red",
              fontWeight: "bold",
              fontSize: "16px",
              textAlign: "center",
              margin: "0 auto",
              marginTop: "180px",
            }}
          >
            Data Tidak Ada
          </div>
        ) : (
          <Chart
            chartType="Bar"
            width="100%"
            height="500px"
            data={chartData}
            options={options}
          />
        )}
      </div>
      {/* )} */}
    </div>
  );
}
