import React, { useState, useEffect } from "react";
import "../../assets/css/addcss.css";
import { useNavigate } from "react-router-dom";
import efaskon from "../../assets/images/logo_new/logo_new_White.png";
import Background from "../../assets/images/bghome.png";
import scan from "../../assets/images/scan.png";
import axios from "axios";
import Helper from "../../api/helper";
import { useTranslation } from "react-i18next";
import eyeIcon from "../../assets/images/icon/view.png"; // Optional jika ingin menggunakan FontAwesome
import eyeSlashIcon from "../../assets/images/icon/hide.png";
import utils from "../../api/utils";
import { flattenBy } from "@tanstack/react-table";
import accessManager from "../../api/accessManager";
import { SignJWT } from "jose"; 
import LocalStorageHelper from "../../api/localStorageHelper";

const ListPerm = [
    "dms.polda.add",
    "dms.polda.modify",
    "dms.polda.view",
    "dms.polda.list",
    "dms.satker.add",
    "dms.satker.modify",
    "dms.satker.view",
    "dms.satker.list",
    "dms.metadata.add",
    "dms.metadata.modify",
    "dms.metadata.view",
    "dms.metadata.list",
    "dms.doctype.add",
    "dms.doctype.modify",
    "dms.doctype.view",
    "dms.doctype.list",
    "dms.template.add",
    "dms.template.modify",
    "dms.template.view",
    "dms.template.list",
    "dms.province.add",
    "dms.province.modify",
    "dms.province.view",
    "dms.province.list",
    "dms.document.add",
    "dms.document.modify",
    "dms.document.view",
    "dms.document.list",
    "dms.document.version",
    "dms.document.approve",
    "dms.theme.access",
    "dms.user.add",
    "dms.user.modify",
    "dms.user.view",
    "dms.user.list",
    "dms.search.access",
    "dms.log.access",
    "dms.role.access",
];

const SignIn = (props) => {
    const { loggedIn } = props;
    const navigate = useNavigate();
    const userData = LocalStorageHelper.getUserData();
    let base_url = process.env.REACT_APP_API_URL;
    let id_ip = "IP20240605040259";
    const [usrnm, setUsrnm] = useState("");
    const [pwrd, setPwrd] = useState("");
    const [unError, setUnError] = useState("");
    const [pwdError, setPwdError] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const { t } = useTranslation();
    const [dataIp, setDataIp] = useState([]);
    const [clientConSet, setClientConSet] = useState([]);
    const [clientCon, setClientCon] = useState([]);
    const [loading, setLoading] = useState(true);
        const [versionTrack, setVersionTrack] = useState([]);
        const [mac, setMac] = useState([]);
 const [lisensi, setLisensi] = useState(() => {
    // Initialize from localStorage
    const storedValue = localStorage.getItem("lisensi");
    return storedValue === "true"; // Convert string to boolean
  });

    useEffect(() => {
        window.localStorage.setItem("ipAddress", dataIp ? dataIp.ip_address : "");
    }, [usrnm]);

    useEffect(() => {
        getIpSetting();
    }, []);

    useEffect(() => {
        if (dataIp === undefined || null) {
            setDataIp(localStorage.getItem("ipAddress"));
        }
    }, []);

    const getIpSetting = async () => {
        try {
            let url = base_url + "ipsetting/" + id_ip;
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
            setDataIp(datas);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    }; 

    useEffect(() => {
        getDMSClientConfigSetting();
    }, []);
    useEffect(() => {
        if (!clientCon == false) {
            handleDmsbackend(); // Call handleDmsconfig after clientCon has data
        }
    }, [clientCon]);
    const getDMSClientConfigSetting = async () => {
        // try {
        //     let url = `http://localhost:8069/db/dmsbackend/dmsconfig/conf`;
        //     const myHeaders = new Headers();
        //     myHeaders.append("Content-Type", "application/json");
        //     myHeaders.append("Authorization", `Basic ` + btoa("root:Adm1nDMS2024"));
        //     const requestOptions = {
        //         method: "GET",
        //         headers: myHeaders,
        //     };
        //     var res = await fetch(url, requestOptions);
        //     var datas = await res.json();
        //     setClientConSet(datas);
        // } catch (err) {
        //     console.log(`${t("error_um")}`);
        // }
    };

    useEffect(() => {
        getDMSClientConfig();
    }, []);
    const getDMSClientConfig = async () => {
        // try {
        //     let url = `http://localhost:8069/db/dmsbackend/dmsconfig`;
        //     const myHeaders = new Headers();
        //     myHeaders.append("Content-Type", "application/json");
        //     myHeaders.append("Authorization", `Basic ` + btoa("root:Adm1nDMS2024"));
        //     const requestOptions = {
        //         method: "GET",
        //         headers: myHeaders,
        //     };
        //     var res = await fetch(url, requestOptions);
        //     var datas = await res.json();
        //     setClientCon(datas);
        // } catch (err) {
        //     console.log(`${t("error_um")}`);
        // }
    };

    const getListHistory = async () => {
        const url = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking/@list_versiontrack';
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", `Bearer ${userData.token}`);
      
        const requestOptions = {
            method: "GET",
            headers: myHeaders,
          };

        try {
            await axios.get(url, requestOptions)
            .then( (response) => {
         var datas =response.data;
         setVersionTrack(datas)
            })
            .catch((e) => {

            })
            
        } catch (error) {
        console.error('Error fetching data:', error);
        }
      }

    const handleDmsbackend = async () => {
        let url = `http://localhost:8069/db/dmsbackend/`;

        const body_data = JSON.stringify({
            "@type": "DmsConfigCollect",
            id: "dmsconfig",
        });
        // try {
        //     const response = await fetch(`${url}`, {
        //         method: "POST",
        //         headers: {
        //             "Content-Type": "application/json",
        //             Authorization: `Basic ` + btoa("root:Adm1nDMS2024"),
        //         },
        //         body: body_data,
        //     });
        // } catch (err) {
        //     console.log(`${t("error_um")}`);
        // }
    };

    const handleDmsconfig = async () => {
        let url = `http://localhost:8069/db/dmsbackend/dmsconfig/`;
        const body_data = JSON.stringify({
            conf_ipserver: "efaskon-dms.slog.polri.go.id",
            conf_methodeserver: "https",
        });
        // try {
        //     const response = await fetch(`${url}`, {
        //         method: "PATCH",
        //         headers: {
        //             "Content-Type": "application/json",
        //             Authorization: `Basic ` + btoa("root:Adm1nDMS2024"),
        //         },
        //         body: body_data,
        //     });
        // } catch (err) {
        //     console.log(`${t("error_um")}`);
        // }
    };

    const createJWT = async (payload) => {
        const secretKey = new TextEncoder().encode("Ocr_efaskon");
        const jwt = await new SignJWT(payload).setProtectedHeader({ alg: "HS256" }).sign(secretKey);
        return jwt;
    };

    const onRequestLogin = async () => {
        const payload = { usrnm, pwrd };
        const token = await createJWT(payload); 
        // getListHistory()
        localStorage.setItem("lisensi", true);

        // const systemInfo = await window.electronAPI.getSystemInfo();
        // setMac(systemInfo)
        try {
            if (dataIp && dataIp.hasOwnProperty("ip_address")) {
                let baseUrl = dataIp.ip_address + "/db/dmsbackend/";
                console.log("1");

                setIsLoading(true);
                let response = await Helper.SignInNew({ tkn: token }, baseUrl);
                if ((response && response.status == 200) || response.status == 201 || response.status == 202 || response.status == 203 || response.status == 204) {
                    localStorage.setItem("user", response.data.userid);
                    localStorage.setItem("myscrtpwd", pwrd);
                    localStorage.setItem("mytoken", response.data.token);
                    localStorage.setItem("myfullname", response.data.fullname);
                    localStorage.setItem("mypoldaid", response.data.poldaid);
                    localStorage.setItem("mypoldaname", response.data.poldaname);
                    localStorage.setItem("mysatkerid", response.data.satkerid);
                    localStorage.setItem("mysatkername", response.data.satkername);
                    localStorage.setItem("mynip", response.data.nip);
                    localStorage.setItem("myrole", JSON.stringify(response.data.role));
                    localStorage.setItem("email", response.data.email);
                    let roleArr = response.data.role;
                    let isAdmin = false;
                    let isRoot = false;
                    if (roleArr.includes("root")) {
                        isRoot = true;
                    }
                    if (roleArr.includes("superadmin") || roleArr.includes("root")) {
                        isAdmin = true;
                        localStorage.setItem("myisadmin", true);
                    } else {
                        isAdmin = false;
                        localStorage.setItem("myisadmin", false);
                    }

                    let listperm = null;
                    let strListPerm = null;
                    if (isRoot) {
                        localStorage.setItem("mylistperm", ListPerm);
                        // initializePermission(ListPerm);
                        accessManager.setAccessData(ListPerm);
                    } else {
                        listperm = await Helper.GetListPermission(baseUrl);
                        strListPerm = listperm ? JSON.stringify(listperm.data.lispermrole) : null;
                        localStorage.setItem("mylistperm", strListPerm);
                        // initializePermission(listperm.data.lispermrole);
                        accessManager.setAccessData(listperm.data.lispermrole);
                        // let value = listperm.data.roles.includes("dms");
                        let roles = listperm.data.roles;
                        let roleName = null;
                        for (let key in roles) {
                            if (key.includes("dms")) {
                                roleName = roles[key]; //key.split(".")[1];
                                localStorage.setItem("myrolename", roleName);
                                break;
                            }
                        }
                    }
                    setIsLoading(false);
                    navigate("/home");
                }
            }
            console.log("2");
        } catch (err) {
            if (err.status == 409) {
                utils.sweetAlertError("Anda belum punya otoritas untuk mengakses data ini!");
            }
            setIsLoading(false);
            console.log(err.message);
        }
    };

     

    const initializePermission = (data) => {
        if (data && data.length > 0) {
            ListPerm.forEach(function (permission) {
                let key = permission; //.replace(/\./g, "");
                let value = data.includes(permission);
                localStorage.setItem(key, value); // Menyimpan value sebagai boolean true
            });
        }
    };

    const onButtonClick = () => {
        setUnError("");
        setPwdError("");
        if ("" === usrnm) {
            setUnError("Please enter a password");
            return;
        }

        if ("" === pwrd) {
            setPwdError("Please enter a password");
            return;
        }
        handleDmsconfig();

        onRequestLogin();
    };

    const [pwdShown, setPwdShown] = useState(false);
    const togglePwdVisibility = () => {
        setPwdShown(!pwdShown);
    };

    //   useEffect(() => {
    //     if (lisensi == true) {
    //       navigate("/home");
    //     } else {
    //         navigate("/login");
    //         utils.sweetAlertWarn("Out");
    //     }          
    // }, [lisensi]);

    return (
        <main>
            <div
                className="container-fluid"
                style={{
                    backgroundImage: `url(${Background}`,
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                }}
            >
                {/* <h2 class="row mb-2 text-danger">
          TES AUTOUPDATER
         </h2> */}
                <div class="container-xl" style={{ height: 990 }}>
                    <div class="row justify-content-center">
                        <div class="col-12 " style={{ height: 80 }}></div>
                    </div>

                    <div class="row justify-content-center mx-5">
                        <div className="col-lg-5 bggradient" style={{ boxShadow: " 0px 0px 15px -4px rgba(255,255,255,1)" }}>
                            <div class="text-center mt-4">
                                <img src={efaskon} width={"80px"} />
                            </div>
                            <div className="text-center mt-3 fw-semibold text-white">
                                <h2 className="fw-medium" style={{ color: "#feb602", fontFamily: "russo one" }}>
                                    FASKON
                                </h2>
                                <h6>LOGISTIK MELALUI INOVASI, DEDIKASI DAN TEKNOLOGI</h6>
                            </div>
                            <div class="text-center mt-4" style={{ marginBottom: 70 }}>
                                <img src={scan} width={"300px"} />
                            </div>
                        </div>
                        <div className="col-lg-5 bg-white" style={{ boxShadow: " 0px 0px 15px -4px rgba(255,255,255,1)" }}>
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="mt-5">
                                        <div className="card-header text-primary">
                                            <h3 className="text-center fw-bold my-4">{t("en_log")}</h3>
                                        </div>
                                        <div className="card-body">
                                            <form>
                                                <div className=" mb-3">
                                                    <input className="form-control" id="inputUsrnm" type="text" value={usrnm} name="usrnm" onChange={(ev) => setUsrnm(ev.target.value)} placeholder="userid / akun" />
                                                    <label className="errorLabel">{unError}</label>
                                                </div>

                                                <div className=" mb-3">
                                                    <input className="form-control" id="inputPwrd" type={pwdShown ? "text" : "password"} value={pwrd} onChange={(ev) => setPwrd(ev.target.value)} name="sandi" placeholder="sandi" />

                                                    <img
                                                        src={pwdShown ? eyeIcon : eyeSlashIcon} // Atau gunakan path sesuai lokasi gambar Anda
                                                        alt="Toggle Password Visibility"
                                                        onClick={togglePwdVisibility}
                                                        className="float-end"
                                                        style={{
                                                            top: "50%",
                                                            transform: "translateY(-120%)",
                                                            cursor: "pointer",
                                                            width: "25px",
                                                            height: "26px",
                                                            marginRight: "10px",
                                                        }}
                                                    />
                                                    <label className="errorLabel">{pwdError}</label>
                                                </div>

                                                <div className="d-flex align-items-center justify-content-between mt-4 mb-0">
                                                    <a className="small" href="password.html" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                                        {t("forget_log")}
                                                    </a>

                                                    {isLoading ? (
                                                        <div class="spinner-border text-primary" role="status">
                                                            <span class="visually-hidden">Loading...</span>
                                                        </div>
                                                    ) : null}

                                                    <button className="btn btn-primary" type="button" onClick={onButtonClick}>
                                                        {t("in_log")}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Modal  */}
                        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="staticBackdropLabel">
                                            Lupa Sandi
                                        </h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p className="text-center text-primary">
                                            SILAHKAN HUBUNGI ADMIN APLIKASI e-FASKON <br></br>
                                            Di Nomor Whatsapp 0813-0012-00xx
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                            Exit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div row>
                            <div class="col- mt-4" style={{ objectFit: "scale-down" }}></div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    );
};

export default SignIn;
