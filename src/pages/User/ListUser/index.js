import { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { Modal } from "reactstrap";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import DataListUser from "../../../fakeData/listUser.json";
// import Del from "../../../assets/images/icon/trash-white.svg";
import Del from "../../../assets/images/icon/delete.png";
import Edit from "../../../assets/images/icon/edit.png";
import ModalHapus from "../../../components/Modal/modalHapus";
import toast, { Toaster } from "react-hot-toast";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import ModalInput from "../../../components/Modal/modalInput";
import Helper from "../../../api/helper";
import ModalChangePasssword from "../../../components/Modal/modalChangePassword";
import utils from "../../../api/utils";
import { useTranslation } from "react-i18next";
import accessManager from "../../../api/accessManager";

const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};
function ListUser() {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [dataIp, setDataIp] = useState("");
    const { t } = useTranslation();

    const [action, setAction] = useState("");
    const [getData, setGetData] = useState([]);
    const [getSingleShare, setGetSingleShare] = useState([]);
    const [sorting, setSorting] = useState([]);
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [onSelected, setOnSelected] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [cpUser, setCpUser] = useState();
    const [havePermission, setHavePermission] = useState(true);
    const [myUser, setMyUser] = useState(null);
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
     const ip = localStorage.getItem("ipAddress");
    let base_url = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
             showTable();
        }
    }, [dataIp]);

    useEffect(() => {
        // let isAdmin = localStorage.getItem("myisadmin");
        // let haveAccessAdd = accessManager.getAccessUserAdd(); // localStorage.getItem("dms.user.add");
        // let haveAccessModify = accessManager.getAccessUserModify(); //localStorage.getItem("dms.user.modify");
        // let haveAccessView = accessManager.getAccessUserView(); //localStorage.getItem("dms.user.view");
        // let haveAccessList = accessManager.getAccessUserList(); //localStorage.getItem("dms.user.ist");
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.user.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.user.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.user.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.user.list"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
        let username = localStorage.getItem("user");
        setMyUser(username);
    }, []);

    const showTable = async () => {
        try {
            setIsLoading(true);
            let response = await Helper.GetListUsers(dataIp + "/users");
             let datas = response.data.items;
            var modifyData = modifyDataListTemplate(datas);

            // setData(modifyData);
            // setData(data);
            if (modifyData.length > 0) {
                const lastData = modifyData[0]; // Mengambil data terakhir setelah diurutkan
             }

            setData(modifyData);
            setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
             if (err && err.hasOwnProperty("status")) {
                if (err.status == 401) {
                    // toast(`${t("session_exp_um")}`, { icon: "👏" });
                    // utils.sweetAlertError(`${t("session_exp_um")}`);
                    // navigate("/home");
                    getMyOwnList();
                }
            }
        }
    };

    const getMyOwnList = async () => {
        try {
            setHavePermission(false);
            let username = await localStorage.getItem("user");
            let response = await Helper.GetDetailUser(dataIp + "/users/" + username);
            // let roleName = await localStorage.getItem("myrolename");
             let datas = [
                {
                    creation_date: response.data.creation_date,
                    modification_date: response.data.modification_date,
                    username: response.data.username,
                    email: response.data.email,
                    name: response.data.name,
                    fullname: response.data.fullname,
                    nip: response.data.nip,
                    polda_id: response.data.polda_id,
                    polda_name: response.data.polda_name,
                    satker_id: response.data.satker_id,
                    satker_name: response.data.satker_name,
                    user_groups: response.data.user_groups,
                    user_roles: response.data.user_roles,
                    user_permissions: [],
                    type_name: "User",
                    groups: response.data.groups,
                },
            ];

            modifyDataListTemplate(datas);
            setData(datas);
            // let datas = response.data.items;
            // var modifyData = modifyDataListTemplate(datas);

            // setData(modifyData);
            // setData(data);
            // if (modifyData.length > 0) {
            //     const lastData = modifyData[0]; // Mengambil data terakhir setelah diurutkan
             // }

            // setData(modifyData);
            // setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
             if (err && err.hasOwnProperty("status")) {
                if (err.status == 401) {
                    // toast(`${t("session_exp_um")}`, { icon: "👏" });
                    utils.sweetAlertError(`${t("session_exp_um")}`);
                    navigate("/home");
                }
            }
        }
    };

    const handlerDelete = async (id) => {
        try {
            let path = getSingleShare ? getSingleShare["@id"] : "";
            let url = path;
             if (path) {
                let response = await Helper.DeleteUser(url);
                if (response.status == 201 || response.status == 204 || response.status == 200) {
                    showTable();
                }
            }
        } catch (err) {
             if (err && err.hasOwnProperty("status")) {
                if (err.status == 401) {
                    toast(`${t("session_exp_um")}`, { icon: "👏" });
                    navigate("/home");
                }
            }
        }
    };

    const formatLogCreatedDate = (dateString) => {
        const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];
        const options = { timeZone: "Asia/Jakarta", timeZoneName: "short" };
        const realdate = new Date(dateString);
        const date = new Date(realdate.getTime() + 7 * 60 * 60 * 1000);
        const day = String(date.getDate()).padStart(2, "0");
        const month = selBulan[date.getMonth()]; // Months are 0-indexed
        const year = String(date.getFullYear()); // Get last two digits of year
        const hours = String(date.getHours()).padStart(2, "0");
        const minutes = String(date.getMinutes()).padStart(2, "0");
        return `${day} ${month} ${year}  ${hours}:${minutes}`;
    };

    const modifyDataListTemplate = (data) => {
        const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];

        data?.forEach((tmpl) => {
            const cd = new Date(tmpl.creation_date);
            tmpl.creation_date = cd.getDate() + " " + selBulan[cd.getMonth()] + " " + cd.getFullYear();
            if (tmpl.modification_date) {
                tmpl.modification_date = new Date(tmpl.modification_date);
            }

            let userRoles = tmpl.user_roles;
            const dmsRoles = userRoles.filter((role) => role.includes("dms"));

            tmpl["role_name"] = dmsRoles && dmsRoles.length > 0 ? dmsRoles[0].split(".")[1] : "";
        });

        // Sorting data berdasarkan modification_date secara descending (data terbaru di atas)
        return data.sort((a, b) => new Date(b.modification_date) - new Date(a.modification_date));
    };

    const onHandleRefresh = () => {
        showTable();
    };

    const changePassword = (data) => {
         setAction("change-password");
        setCpUser(data);
        let idmodal = document.getElementById("btn_change_password");
        idmodal.click();
        // if (e) {
        //     let idmodal = document.getElementById("ModalChangePassword");
        //     let modal = new Modal(idmodal);
        //     modal.show();
        // }
    };

    const columns = [
        {
            accessorKey: "fullname",
            header: t("nama_pengguna_tu"),
            // cell: EditableCell,
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "satker_name",
            header: "Satker",
            // cell: EditableCell,
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "polda_name",
            header: "Korwil",
            // cell: EditableCell,
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "role_name",
            header: "Role",
            // cell: EditableCell,
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "creation_date",
            header: t("tanggal_daftar_tu"),
            // cell: EditableCell,
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div>
                        {!havePermission ? (
                            <div></div>
                        ) : (
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    width: "100px",
                                    gap: "4px",
                                }}
                            >
                                <button
                                    type="button"
                                    className="btn "
                                    data-bs-toggle="modal"
                                    data-bs-target="#ModalInput"
                                    onClick={() => {
                                        setAction("edit");
                                        setGetData(row.original);
                                        setOnSelected(!onSelected);
                                    }}
                                >
                                    {myUser && myUser != row.original.username && havePermissionAccess.haveAccessModify ? <img src={Edit} width={"80%"} /> : <img src={Edit} width={"25%"} />}
                                </button>
                                {myUser && myUser != row.original.username && havePermissionAccess.haveAccessModify ? (
                                    <button type="button" className="btn" data-bs-toggle="modal" data-bs-target="#modalHapus" onClick={() => setGetSingleShare(row.original)}>
                                        <img src={Del} width={"80%"} />
                                    </button>
                                ) : null}
                            </div>
                        )}
                    </div>
                );
            },
        },
    ];
    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });
    return (
        <div class="container-fluid pt-3 " style={{ height: " auto", marginTop: "75px" }}>
            <ModalHapus getData={getSingleShare} handlerDelete={handlerDelete} />
            <ModalInput getAllTable={data} keyboard="false" backdrop="static" onSelected={onSelected} action={action} getData={getData ? getData : ""} showTable={showTable} changePassword={changePassword} />
            <ModalChangePasssword keyboard="false" backdrop="static" action={action} getData={cpUser} />
            <button id="btn_change_password" style={{ display: "none" }} type="button" class="btn btn-warning col-2 mx-1" data-bs-toggle="modal" data-bs-target="#ModalChangePassword">
                {t("change_pass_um")}
            </button>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "#ffbaba",
                        color: "#ff0000 ",
                    },

                    // Default options for specific types
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-3 textfamily">
                        {" "}
                        {t("list_um")} {t("pengguna_t1")}{" "}
                    </h5>
                    <div class="row">
                        <div class="col-10">
                            <Filters filtering={filtering} setFiltering={setFiltering} />
                        </div>
                        <div class="col-1">
                            {isLoading ? (
                                <button class="btn btn-primary float-end" type="button">
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true">
                                        {" "}
                                    </span>
                                    <span> </span>
                                    {t("loading_lo")}
                                </button>
                            ) : (
                                <button class="btn btn-primary float-end" type="button" onClick={onHandleRefresh}>
                                    ⟳ {t("refresh_um")}
                                </button>
                            )}
                        </div>
                        {havePermissionAccess.haveAccessAdd ? (
                            <div class="col-1">
                                <button
                                    class="btn btn-primary float-end"
                                    type="button"
                                    data-bs-toggle="modal"
                                    data-bs-target="#ModalInput"
                                    onClick={() => {
                                        setAction("add");
                                        setOnSelected(!onSelected);
                                    }}
                                >
                                    {" "}
                                    ✙ {t("pengguna_t1")}
                                </button>
                            </div>
                        ) : null}
                    </div>
                    {/* <div class="row"> */}
                    <table className="table table-bordered table-striped table-hover">
                        <thead className="table-primary">
                            {table.getHeaderGroups().map((headerGroup) => (
                                <tr key={headerGroup.id} className="  uppercase">
                                    {headerGroup.headers.map((header) => (
                                        <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                            {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>

                        <tbody>
                            {table.getRowModel().rows.map((row) => (
                                <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                    {row.getVisibleCells().map((cell) => (
                                        <td className="px-4 py-2" key={cell.id}>
                                            {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                        </td>
                                    ))}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <Paginations table={table} />
                    {/* </div> */}
                </div>
            </div>
        </div>
    );
}

export default ListUser;
