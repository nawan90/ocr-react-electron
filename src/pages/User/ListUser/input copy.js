import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useTranslation } from "react-i18next";
import Helper from "../../../api/helper";

function InputUser(props) {
    const { showTable, getData, action, onSelected, getAllTable } = props;
    let base_url = process.env.REACT_APP_API_URL;
    const [roleOptionsHtml, setRoleOptionsHtml] = useState([]);
    const [assignPoldaOptionsHtml, setAssignPoldaOptionsHtml] = useState([]);
    const [assignSatkerOptionsHtml, setAssignSatkerOptionsHtml] = useState([]);
    const [poldaRef, setPoldaRef] = useState([]);
    const [allSatkerRef, setAllSatkerRef] = useState([]);

    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [fullname, setFullname] = useState(null);
    const [nip, setNip] = useState(null);
    const [assignPoldaId, setAssignPoldaId] = useState(null);
    const [assignSatkerId, setAssignSatkerId] = useState(null);
    const [roleId, setRoleId] = useState(null);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    const { t } = useTranslation();

    const [disButton, setDisButton] = useState(true);

    useEffect(() => {
         const ipServer = ip ? `http://${ip}/db/dmsbackend/polridocument/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + "/polridocument/"); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
         if (dataIp && !dataIp.includes("undefined")) {
            if (!roleOptionsHtml || (roleOptionsHtml && roleOptionsHtml.length <= 0)) {
                loadRoles();
            }
            if (!assignPoldaOptionsHtml || (assignPoldaOptionsHtml && assignPoldaOptionsHtml.length <= 0)) {
                loadPoldas();
            }
            if (!assignSatkerOptionsHtml || (assignSatkerOptionsHtml && assignSatkerOptionsHtml.length <= 0)) {
                loadAllSatkerRef();
            }
        }
    }, [dataIp]);

    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });
    const saveData = () => {
        notify();
    };

    const loadRoles = () => {
        modifyRoleToOptionsHtml();
    };

    const modifyRoleToOptionsHtml = (datas) => {
        let options = [];
        options.push(<option value={"admin"}>{"Admin"}</option>);
        options.push(<option value={"mabes"}>{"Mabes"}</option>);
        options.push(<option value={"polda"}>{"Polda"}</option>);
        options.push(<option value={"internal"}>{"Internal"}</option>);
        options.push(<option value={"viewer"}>{"Viewer"}</option>);
        setRoleOptionsHtml(options);
    };

    const loadAllSatkerRef = async () => {
        try {
            let datas = await Helper.GetListSatker(dataIp + "/@list_satker");
            setPoldaRef(datas);
            // setData(datas);
            modifySatkerToOptionsHtml(datas);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const loadPoldas = async () => {
        try {
            let datas = await Helper.GetListPolda(dataIp + "/@list_polda");
            setPoldaRef(datas);
            // setData(datas);
            modifyPoldaToOptionsHtml(datas);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const modifyPoldaToOptionsHtml = (datas) => {
        if (datas) {
             let options = [];
            // setDocTypeRef(datas);
            datas.map(function (data, index) {
                options.push(<option value={data.polda_id}>{data.polda_name}</option>);
            });

            setAssignPoldaOptionsHtml(options);
        }
    };

    const modifySatkerToOptionsHtml = (datas) => {
        if (datas) {
             let options = [];
            // setDocTypeRef(datas);
            datas.map(function (data, index) {
                options.push(<option value={data.satker_id}>{data.satker_name}</option>);
            });

            setAssignSatkerOptionsHtml(options);
        }
    };

    const onChangeUsername = (e) => {
        e.preventDefault();
        setUsername(e.target.value);
    };

    const onChangePassword = (e) => {
        e.preventDefault();
        setPassword(e.target.value);
    };

    const onChangeFullname = (e) => {
        e.preventDefault();
        setFullname(e.target.value);
    };

    const onChangeNip = (e) => {
        e.preventDefault();
        setNip(e.target.value);
    };

    const onChangeRole = (e) => {
         e.preventDefault();
        setRoleId(e.target.value);
    };

    const onChangeAssignPolda = (e) => {
         e.preventDefault();
        setAssignPoldaId(e.target.value);
    };

    const onChangeAssignSatker = (e) => {
         e.preventDefault();
        setAssignSatkerId(e.target.value);
    };

    const handleSubmit = async (e) => {
        let body_data = JSON.stringify({
            "@type": "User",
            password: password,
            username: username,
            fullname: fullname,
            user_roles: ["guillotina.Member"],
        });
 
        try {
            let datas = await Helper.AddUser(dataIp + "/users", body_data);
         } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    return (
        <div>
            <Toaster
                position="top-center"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    // Default options for specific types
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <form onSubmit={handleSubmit}>
                <div class="row mb-4">
                    <div class="col-6">
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("username")}</label>
                            </div>
                            <div class="col-8">
                                <input value={username} type="text " placeholder={t("username")} class="form-control" onChange={onChangeUsername} />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("password")}</label>
                            </div>
                            <div class="col-8">
                                <input value={password} type="password" placeholder="" class="form-control" onChange={onChangePassword} />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("nama_pengguna_tu")}</label>
                            </div>
                            <div class="col-8">
                                <input value={fullname} type="text " placeholder="contoh: Charlie Simon, dst" class="form-control" onChange={onChangeFullname} />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>Nip</label>
                            </div>
                            <div class="col-8">
                                <input value={nip} type="text " placeholder="contoh: 123xyz, dst" class="form-control" onChange={onChangeNip} />
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("assign_polda_tu")}</label>
                            </div>
                            <div class="col-8">
                                {/* <input type="text " placeholder="Assign ke Polda" class="form-control" /> */}
                                <select id="poldaOption" value={assignPoldaId} class="form-select" aria-label="Default select example" onChange={onChangeAssignPolda}>
                                    <option disabled>{t("select_polda_tu")}</option>
                                    {assignPoldaOptionsHtml}
                                </select>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("assign_satker_tu")}</label>
                            </div>
                            <div class="col-8">
                                {/* <input type="text " placeholder="Assign ke Polda" class="form-control" /> */}
                                <select id="satkerOption" value={assignSatkerId} class="form-select" aria-label="Default select example" onChange={onChangeAssignSatker}>
                                    <option disabled>{t("satuan_kerja_tu")}</option>
                                    {assignSatkerOptionsHtml}
                                </select>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("role_tu")}</label>
                            </div>
                            <div class="col-8">
                                {/* <input type="text " placeholder="Role" class="form-control" /> */}
                                <select id="roleOption" value={roleId} class="form-select" aria-label="Default select example" onChange={onChangeRole}>
                                    <option selected>{t("select_role_tu")}</option>
                                    {roleOptionsHtml}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    <button disabled={disButton} class={action === "add" ? "btn btn-primary col-2 mx-1" : "btn btn-success col-2  mx-1"} id="btnSave" data-bs-dismiss="modal" type="submit">
                        {t("submit_um")}
                    </button>

                    <button type="button" class="btn btn-danger col-2" data-bs-dismiss="modal" aria-label="Close" onClick={(e) => e.preventDefault()}>
                        {t("keluar_t1")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputUser;
