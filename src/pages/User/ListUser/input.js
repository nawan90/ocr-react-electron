import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useTranslation } from "react-i18next";
import Helper from "../../../api/helper";
import Swal from "sweetalert2";
import accessManager from "../../../api/accessManager";

// import ModalChangePasssword from "../../../components/Modal/modalChangePassword";
const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};
function InputUser(props) {
    const { showTable, getData, action, onSelected, getAllTable, changePassword } = props;

    let base_url = Helper.GetBaseUrl(); //process.env.REACT_APP_API_URL;
    const [roleOptionsHtml, setRoleOptionsHtml] = useState([]);
    const [assignPoldaOptionsHtml, setAssignPoldaOptionsHtml] = useState([]);
    const [assignSatkerOptionsHtml, setAssignSatkerOptionsHtml] = useState([]);
    const [poldaRef, setPoldaRef] = useState([]);
    const [allSatkerRef, setAllSatkerRef] = useState([]);

    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [fullname, setFullname] = useState(null);
    const [nip, setNip] = useState(null);
    const [assignPoldaId, setAssignPoldaId] = useState(null);
    const [assignPoldaName, setAssignPoldaName] = useState(null);
    const [assignSatkerId, setAssignSatkerId] = useState(null);
    const [assignSatkerName, setAssignSatkerName] = useState(null);
    const [roleId, setRoleId] = useState(null);
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const { t } = useTranslation();
    const [disButton, setDisButton] = useState(true);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    useEffect(() => {
         const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
         if (dataIp && !dataIp.includes("undefined")) {
            if (!roleOptionsHtml || (roleOptionsHtml && roleOptionsHtml.length <= 0)) {
                loadRoles();
            }
            if (!assignPoldaOptionsHtml || (assignPoldaOptionsHtml && assignPoldaOptionsHtml.length <= 0)) {
                loadPoldas();
            }
            if (!assignSatkerOptionsHtml || (assignSatkerOptionsHtml && assignSatkerOptionsHtml.length <= 0)) {
                loadAllSatkerRef();
            }
        }
    }, [dataIp]);

    const cekButtonAdd = username !== "" && password !== "" && fullname !== "";

    useEffect(() => {
        if (username && username != "" && fullname && fullname != "") {
            setDisButton(false);
         } else {
             setDisButton(true);
        }
    }, [username, fullname]);

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.user.add"));
        let haveAccessModify = JSON.parse(accessManager.getAccessUserModify()); //localStorage.getItem("dms.user.modify");
        let haveAccessView = JSON.parse(accessManager.getAccessUserView()); //localStorage.getItem("dms.user.view");
        let haveAccessList = JSON.parse(accessManager.getAccessUserList()); //localStorage.getItem("dms.user.List");
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);

    const validationAdd = () => {
         // if (username && username != "" && fullname && fullname != "") {
        //     setDisButton(false);
         // } else {
         //     setDisButton(true);
        // }
    };

    const validationEdit = () => {
         // // setDisButton(false);
        // if (username && username != "" && fullname && fullname != "") {
        //     setDisButton(false);
        // } else {
        //     setDisButton(true);
        // }
    };

    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });
    const saveData = () => {
        notify();
    };

    const loadRoles = async () => {
         try {
            let url = dataIp + "/dmsroleperm/@search?type_name=DMSRole&_metadata=id,rolename&_size=1000";
            let datas = await Helper.GetListRole(url);

            if (datas.status == 201 || 200 || 203 || 204) {
                let filterdata = datas.data.items.map((item) => ({
                    ...item, // Keep other properties unchanged
                    rolename: item.rolename.split(".")[1], // Modify the 'nama' property
                }));
                // setData(filterdata);
                modifyRoleToOptionsHtml(filterdata);
            }
        } catch (err) {
            console.log(`${t('error_um')}`)
        }
    };

    const modifyRoleToOptionsHtml = (datas) => {
        let options = [];
        for (var role of datas) {
             options.push(<option value={role.id}>{role.rolename}</option>);
        }
        // options.push(<option value={"mabes"}>{"Mabes"}</option>);
        // options.push(<option value={"polda"}>{"Polda"}</option>);
        // options.push(<option value={"internal"}>{"Internal"}</option>);
        // options.push(<option value={"viewer"}>{"Viewer"}</option>);
        setRoleOptionsHtml(options);
    };

    const loadAllSatkerRef = async () => {
        try {
            let datas = await Helper.GetListSatker(dataIp + "/polridocument/@list_satker");
            setAllSatkerRef(datas);
             // setData(datas);
            modifySatkerToOptionsHtml(datas);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const loadPoldas = async () => {
        try {
            let datas = await Helper.GetListPolda(dataIp + "/polridocument/@list_polda");
            setPoldaRef(datas);
             // setData(datas);
            modifyPoldaToOptionsHtml(datas);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const modifyPoldaToOptionsHtml = (datas) => {
        if (datas) {
             let options = [];
            // setDocTypeRef(datas);
            datas.map(function (data, index) {
                options.push(<option value={data.polda_id}>{data.polda_name}</option>);
            });

            setAssignPoldaOptionsHtml(options);
        }
    };

    const modifySatkerToOptionsHtml = (datas) => {
        if (datas) {
             let options = [];
            // setDocTypeRef(datas);
            datas.map(function (data, index) {
                options.push(<option value={data.satker_id}>{data.satker_name}</option>);
            });

            setAssignSatkerOptionsHtml(options);
        }
    };

    const onChangeUsername = (e) => {
        // e.preventDefault();
        setUsername(e.target.value.toLowerCase());
        checkValidate();
    };

    const onChangeNewPassword = (e) => {
        e.preventDefault();
        setPassword(e.target.value);
        checkValidate();
    };

    const onChangeFullname = (e) => {
        // e.preventDefault();
        setFullname(e.target.value);
        checkValidate();
    };

    const onChangeNip = (e) => {
        // e.preventDefault();
        setNip(e.target.value);
        checkValidate();
    };

    const onChangeRole = (e) => {
         e.preventDefault();
        setRoleId(e.target.value);
    };

    const onChangeAssignPolda = (e) => {
         e.preventDefault();
        let poldaId = e.target.value;
        let poldas = poldaRef.filter((polda) => polda.polda_id == poldaId);
        let polda = poldas ? poldas[0] : "";
        setAssignPoldaId(poldaId);
        setAssignPoldaName(polda ? polda.polda_name : "");

        if (poldaId) {
            const result = allSatkerRef.filter((satker) => satker.satker_polda_id == poldaId);
            modifySatkerToOptionsHtml(result);
        } else {
            modifySatkerToOptionsHtml(allSatkerRef);
        }
    };

    const onChangeAssignSatker = (e) => {
         e.preventDefault();
        let satkerId = e.target.value;
        let satkers = allSatkerRef.filter((obj) => obj.satker_id == satkerId);
        let satker = satkers ? satkers[0] : "";
         setAssignSatkerId(satkerId);
        setAssignSatkerName(satker ? satker.satker_name : "");
    };

    const checkValidate = () => {
         if (action === "add") {
            validationAdd();
        } else if (action === "edit") {
            validationEdit();
        }
    };

    useEffect(() => {
        if (action === "edit") {
            setUsername(getData.username);
            setPassword("");
            setFullname(getData.fullname);
            setNip(getData.nip);
            setAssignPoldaId(getData.polda_id);
            setAssignPoldaName(getData.polda_name);
            setAssignSatkerId(getData.satker_id);
            setAssignSatkerName(getData.satker_name);

            if (getData) {
                let roles = getData.user_roles;
                roles = roles.filter((role) => role != "guillotina.Member");
                let role = roles[0];
                setRoleId(role);
            }
        } else if (action === "add") {
            setUsername("");
            setPassword("");
            setFullname("");
            setNip("");
            setAssignPoldaId("");
            setAssignPoldaName("");
            setAssignSatkerId("");
            setAssignSatkerName("");
            setRoleId("");
        }
    }, [onSelected]);
    // if (getAllTable.length > 0) {
    //     const lastData = getAllTable[getAllTable.length - 1];
     //     // Additional logic to handle displaying the last data in the UI
    // }
 
    const onChangePassword = (e) => {
         e.preventDefault();
        changePassword(getData);
        // document.getElementById("closeModal").click();
        // document.getElementById("btn_change_password").click();
    };

    const sweetAlertError = (body) => {
         Swal.fire({
            title: "Error!",
            text: "" + body,
            icon: "error",
            confirmButtonText: "OK",
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        let groupItems = ["ACCESSSERVICE"];
        if (roleId == "dms.Admin" || roleId == "dms.SuperAdmin") {
            groupItems.push("superadmin");
        }
        if (assignPoldaId && !assignSatkerId) {
            groupItems.push("POLDA_" + assignPoldaId);
        }
        if (assignSatkerId) {
            groupItems.push("SATKER_" + assignSatkerId);
        }
        let userRole = roleId ? ["guillotina.Member", roleId] : ["guillotina.Member"];
        let userGroup = groupItems;
        let method = action == "edit" ? "PATCH" : "POST";
        let body_data = JSON.stringify({
            "@type": "User",
            password: password,
            username: username,
            fullname: fullname,
            nip: nip,
            user_roles: userRole,
            user_groups: userGroup,
            polda_id: assignPoldaId,
            polda_name: assignPoldaName,
            satker_id: assignSatkerId,
            satker_name: assignSatkerName,
        });
        if (action == "edit") {
            body_data = JSON.stringify({
                "@type": "User",
                fullname: fullname,
                nip: nip,
                user_roles: userRole,
                user_groups: userGroup,
                polda_id: assignPoldaId,
                polda_name: assignPoldaName,
                satker_id: assignSatkerId,
                satker_name: assignSatkerName,
            });
        }

        let url = dataIp + "/users";
        if (action == "edit") {
            url = getData["@id"];
        }

         try {
            let datas = null;

            if (action == "edit") {
                datas = await Helper.ModifyUser(url, body_data);
            } else {
                datas = await Helper.AddUser(url, body_data);
            }

             if (datas.status == 201 || 200 || 203 || 204) {
                setUsername("");
                setPassword("");
                setFullname("");
                setNip("");
                setAssignPoldaId("");
                setAssignSatkerId("");
                notify();
                // const data =   datas.json();
                 // Assuming you are updating the table with this data
                // setAllTable(data); // Assuming setAllTable updates the state holding all table data
                // await showTable();
                showTable();
                document.getElementById("closeModal").click();
            }
            // Optional: Scroll to the last page or last data
        } catch (err) {
             if (err.status == 409) {
                sweetAlertError("Nama user ini sudah terdaftar!");
            }
            console.log(`${t("error_um")}`, err.status);
        }
    };

    return (
        <div>
            <Toaster
                position="top-center"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    // Default options for specific types
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            {/* <ModalChangePasssword keyboard="false" backdrop="static" action={action} /> */}
            <form onSubmit={handleSubmit}>
                <div class="row mb-4">
                    <div class="col-6">
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>*{t("nama_pengguna_tu")}</label>
                            </div>
                            <div class="col-8">
                                <input disabled={action === "edit"} value={username} type="text " placeholder={t("nama_pengguna_tu")} class="form-control" onChange={onChangeUsername} />
                            </div>
                        </div>
                        {action !== "edit" ? (
                            <div class="row my-2">
                                <div class="col-4 fw-medium">
                                    <label>{t("pass_um")}</label>
                                </div>
                                <div class="col-8">
                                    <input disabled={action === "edit"} value={password} type="password" placeholder="" class="form-control" onChange={onChangeNewPassword} />
                                </div>
                            </div>
                        ) : null}
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>*{t("fullname_um")}</label>
                            </div>
                            <div class="col-8">
                                <input value={fullname} type="text " placeholder="contoh: Charlie Simon, dst" class="form-control" onChange={onChangeFullname} />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>NIP</label>
                            </div>
                            <div class="col-8">
                                <input value={nip} type="text " placeholder="contoh: 123xyz, dst" class="form-control" onChange={onChangeNip} />
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("assign_polda_tu")}</label>
                            </div>
                            <div class="col-8">
                                {/* <input type="text " placeholder="Assign ke Polda" class="form-control" /> */}
                                <select id="poldaOption" value={assignPoldaId} class="form-select" aria-label="Default select example" onChange={onChangeAssignPolda}>
                                    <option value="" selected>
                                        {t("select_one_ts")} {t("titlle_lp")}
                                    </option>
                                    {assignPoldaOptionsHtml}
                                </select>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("assign_satker_tu")}</label>
                            </div>
                            <div class="col-8">
                                {/* <input type="text " placeholder="Assign ke Polda" class="form-control" /> */}
                                <select id="satkerOption" value={assignSatkerId} class="form-select" aria-label="Default select example" onChange={onChangeAssignSatker}>
                                    <option value="" selected>
                                        {t("select_one_ts")} {t("titlle_ls")}
                                    </option>
                                    {assignSatkerOptionsHtml}
                                </select>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("role_tu")}</label>
                            </div>
                            <div class="col-8">
                                {/* <input type="text " placeholder="Role" class="form-control" /> */}
                                <select id="roleOption" value={roleId} class="form-select" aria-label="Default select example" onChange={onChangeRole}>
                                    <option value="" selected>
                                        {t("select_role_tu")}
                                    </option>
                                    {roleOptionsHtml}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    {havePermissionAccess.haveAccessAdd ? (
                        <button disabled={disButton} class={action === "add" ? "btn btn-primary col-2 mx-1" : "btn btn-success col-2  mx-1"} data-bs-dismiss="modal" id="btnSave" type="submit">
                            {t("submit_um")}
                        </button>
                    ) : null}

                    {action === "edit" && havePermissionAccess.haveAccessModify ? (
                        <button type="button" class="btn btn-warning col-2 mx-1" data-bs-dismiss="modal" onClick={onChangePassword}>
                            {t("change_pass_um")}
                        </button>
                    ) : null}

                    {/* <button id="btn_change_password" style={{ display: "none" }} type="button" class="btn btn-warning col-2 mx-1" data-bs-toggle="modal" data-bs-target="#ModalChangePassword">
                        {t("change_pass_um")}
                    </button> */}

                    <button type="button" class="btn btn-danger col-2 mx-1" data-bs-dismiss="modal" aria-label="Close" onClick={(e) => e.preventDefault()}>
                        {t("cancel_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputUser;
