import { useState, useEffect } from "react";
// import DataListUser from "../../../fakeData/listUser.json";
import ProfilePic from "../../../assets/images/profile/user3.png";
import Helper from "../../../api/helper";
import ModalChangePasssword from "../../../components/Modal/modalInput";
import { useTranslation } from "react-i18next";
import Swal from "../../../utils";
import Mode from "../../../../package.json";

function Profile() {
    const [username, setUsername] = useState("");
    const [fullname, setFullname] = useState("");
    const [email, setEmail] = useState("");
    const [nip, setNip] = useState("");
    const [role, setRole] = useState("");
    const [poldaName, setPoldaName] = useState("");
    const [satkerName, setSatkerName] = useState("");
    const [action, setAction] = useState("");
    let base_url = process.env.REACT_APP_API_URL;
    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    const { t } = useTranslation();

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        let fullname = localStorage.getItem("myfullname");
        let username = localStorage.getItem("user");
        let poldaName = localStorage.getItem("mypoldaname");
        let satkerName = localStorage.getItem("mysatkername");
        let email = localStorage.getItem("myemail");
        let nip = localStorage.getItem("mynip");
        let roles = localStorage.getItem("myrole");
        let role = "";
        if (roles) {
            roles = JSON.parse(roles);
            role = roles && roles.length > 0 ? roles[0] : "";
        }
        setEmail(email);
        setUsername(username);
        setFullname(fullname);
        setPoldaName(poldaName && poldaName != "null" ? poldaName : "");
        setSatkerName(satkerName && satkerName != "null" ? satkerName : "");
        setNip(nip && nip != "null" ? nip : "");
        // setRole(role && role != "null" ? role : "");
    }, []);

    const onHandleChangeStatus = (value) => {};
    const onHandleChangeRole = (value) => {};
    const onHandleChangeSatker = (value) => {};
    const onUpdateProfile = async () => {
        let body_data = JSON.stringify({
            "@type": "User",
            fullname: fullname,
            nip: nip,
            email: email,
        });

        let url = dataIp + "/users/" + username;

        try {
            let datas = null;
            datas = await Helper.ModifyUser(url, body_data);
            let status = datas.status;

            if (status == 204 || status == 203 || status == 202 || status == 1) {
                Swal.SwalSuccess("Update data berhasil");
                localStorage.setItem("myfullname", fullname);
                localStorage.setItem("mynip", nip);
                localStorage.setItem("myemail", email);
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };
    const onChangePassword = () => {
        setAction("change_password");
    };

    const onChangeFullname = (e) => {
        e.preventDefault();
        setFullname(e.target.value);
    };

    const onChangeNip = (e) => {
        e.preventDefault();
        setNip(e.target.value);
    };

    const onChangeEmail = (e) => {
        e.preventDefault();
        setEmail(e.target.value);
    };
    const showMode = () => {
        const isLatest = /latest/.test(Mode.version);
    
        if (isLatest) {
          console.log('Latest version detected');
        } else {
          console.log('Not the latest version');
          return (
            <h4 style={{ background: "#5CFF5C", color: "red", fontWeight: "bold" }}>
              DEVELOPMENT MODE
            </h4>
          )
        }
      }
    return (
        <div class="container" style={{ height: "auto", padding: "100px 10px" }}>
            <ModalChangePasssword keyboard="false" backdrop="static" action={action} />
            <div class="card">
                <div class="card-body m-3">
                {showMode()}
                    <div class="row">
                        <div class="col-6">
                            <img src={ProfilePic} class="rounded float-start" width="150" height="150" alt="..."></img>

                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="row">
                                        <h3 class="ms-2">{fullname ? fullname : "Profile Name"}</h3>
                                      
                                    </div>
                                    
                                    {/* <div class="row mt-5 ">
                                        <div class="col-5 ms-2 d-grid gap-2">
                                            <button disabled={username == "root"} type="button" className="btn btn-primary" onClick={onUpdateProfile}>
                                                Update
                                            </button>{" "}
                                        </div>
                                        <div class="col-5 ms-2 d-grid gap-2">
                                            <button disabled={username == "root"} type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ModalInput" onClick={onChangePassword}>
                                                Change Password
                                            </button>{" "}
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-1">
                        <div class="col-12">
                            <div class="row mt-4">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-username" class="form-label fw-bold">
                                            Username
                                        </label>
                                        <input disabled type="text" value={username} class="form-control" id="profile-username" placeholder="contoh: adam, eka, dst" />
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-name" class="form-label fw-bold">
                                            Nama Lengkap
                                        </label>
                                        <input disabled={username === "root"} type="text" value={fullname} class="form-control" id="profile-name" placeholder="contoh: Adam Baskoro, ST. , dr. Eka F., dst" onChange={onChangeFullname} />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-email" class="form-label fw-bold">
                                            Email
                                        </label>
                                        <input type="email" value={email} class="form-control" id="profile-email" placeholder="contoh: charlie@domain.co.id" onChange={onChangeEmail} />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-email" class="form-label fw-bold">
                                            NIP
                                        </label>
                                        <input disabled={username === "root"} type="text" value={nip} class="form-control" id="profile-email" placeholder="" onChange={onChangeNip} />
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-role" class="form-label fw-bold">
                                            Role
                                        </label>
                                        <input disabled type="text" value={role} class="form-control" id="profile-role" placeholder="" />
                                        {/* <select disabled id="profile-select-role" class="form-select" value={role} aria-label="Default select example" onChange={(e) => onHandleChangeRole(e.target.value)}>
                                            <option selected>Pilih Role Permission</option>
                                            <option value={1}>Admin</option>
                                            <option value={2}>Internal</option>
                                            <option value={3}>External</option>
                                            <option value={4}>Kapolda</option>
                                        </select> */}
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-korwil" class="form-label fw-bold">
                                            Korwil
                                        </label>
                                        <input disabled type="text" value={poldaName} class="form-control" id="profile-korwil" placeholder="" />
                                        {/* <select disabled id="profile-select-status" class="form-select" value={poldaName} aria-label="Default select example" onChange={(e) => onHandleChangeStatus(e.target.value)}>
                                            <option selected>Pilih Status</option>
                                            <option value={1}>Active</option>
                                            <option value={0}>Tidak Aktif</option>
                                        </select> */}
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="profile-satker" class="form-label fw-bold">
                                            Satker
                                        </label>
                                        <input disabled type="text" value={satkerName} class="form-control" id="profile-satker" placeholder="" />
                                        {/* <select disabled id="profile-select-satker" class="form-select" value={satkerName} aria-label="Default select example" onChange={(e) => onHandleChangeSatker(e.target.value)}>
                                            <option selected>Pilih Role Satker</option>
                                            <option value={1}>POLDA ACEH</option>
                                            <option value={2}>POLDA SUMUT</option>
                                            <option value={3}>POLDA RIAU</option>
                                            <option value={4}>POLDA JAMBI</option>
                                        </select> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Profile;
