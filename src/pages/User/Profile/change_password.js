import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useTranslation } from "react-i18next";
import Helper from "../../../api/helper";
import Swal from "sweetalert2";

function ChangePassword(props) {
    const { showTable, getData, action, onSelected, getAllTable } = props;

    let base_url = process.env.REACT_APP_API_URL;
    const [username, setUsername] = useState(null);
    const [ownPassword, setOwnPassword] = useState(null);
    const [currentPassword, setCurrentPassword] = useState(null);
    const [newPassword, setNewPassword] = useState(null);
    const [confirmNewPassword, setConfirmNewPassword] = useState(null);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    const { t } = useTranslation();

    const [disButton, setDisButton] = useState(true);

    useEffect(() => {
         const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        let user = localStorage.getItem("user");
        let ownPassword = localStorage.getItem("myscrtpwd");
        setOwnPassword(ownPassword);
        setUsername(user);
    }, []);

    const onChangeCurrentPassword = (e) => {
        e.preventDefault();
        setCurrentPassword(e.target.value.toLowerCase());
    };

    const onChangeNewPassword = (e) => {
        e.preventDefault();
        setNewPassword(e.target.value);
    };

    const onChangeConfirmNewPassword = (e) => {
        e.preventDefault();
        setConfirmNewPassword(e.target.value);
    };

    const checkValidate = () => {
         if (action === "add") {
            // validationAdd();
        } else if (action === "edit") {
            // validationEdit();
        }
    };

    const sweetAlert = (body) => {
         Swal.fire({
            title: "Error!",
            text: "" + body,
            icon: "error",
            confirmButtonText: "OK",
        });
    };

    const handleSubmit = async (e) => {
        if (ownPassword !== currentPassword) {
            sweetAlert("Current password anda salah");
            return;
        }

        if (newPassword !== confirmNewPassword) {
            sweetAlert("Password baru anda tidak valid. Pastikan konfirmasi password anda benar.");
            return;
        }

        let body_data = JSON.stringify({
            "@type": "User",
            password: newPassword,
        });

        let url = dataIp + "/users/" + username;

         try {
            let datas = null;
            datas = await Helper.ModifyUser(url, body_data);
            if (datas && (datas.status == 204 || datas.status == 203 || datas.status == 202 || datas.status == 201 || datas.status == 200)) {
                localStorage.setItem("myscrtpwd", newPassword);
                setOwnPassword(newPassword);
                setCurrentPassword("");
                setNewPassword("");
                setConfirmNewPassword("");
                document.getElementById("closeModal").click();
            }
         } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    return (
        <div>
            <Toaster
                position="top-center"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    // Default options for specific types
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <form onSubmit={handleSubmit}>
                <div class="row mb-4">
                    <div class="col-10">
                        <div class="row my-2">
                            <div class="col-5 fw-medium">
                                <label>{t("current_pass_um")}</label>
                            </div>
                            <div class="col-7">
                                <input value={currentPassword} type="password" placeholder={t("current_pass_um")} class="form-control" onChange={onChangeCurrentPassword} />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-5 fw-medium">
                                <label>{t("new_pass_um")}</label>
                            </div>
                            <div class="col-7">
                                <input value={newPassword} type="password" placeholder={t("new_pass_um")} class="form-control" onChange={onChangeNewPassword} />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-5 fw-medium">
                                <label>{t("confirm_new_pass_um")}</label>
                            </div>
                            <div class="col-7">
                                <input value={confirmNewPassword} type="password" placeholder={t("confirm_new_pass_um")} class="form-control" onChange={onChangeConfirmNewPassword} />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    <button class={action === "change_password" ? "btn btn-primary col-2 mx-1" : "btn btn-success col-2  mx-1"} id="btnSave" type="submit">
                        {t("submit_um")}
                    </button>

                    <button type="button" class="btn btn-danger col-2" data-bs-dismiss="modal" aria-label="Close" onClick={(e) => e.preventDefault()}>
                        {t("keluar_t1")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default ChangePassword;
