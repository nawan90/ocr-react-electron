import { useState, useEffect, useRef } from "react";
import {
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import DataListUser from "../../../fakeData/listUser.json";
import Del from "../../../assets/images/icon/trash-white.svg";
import ModalHapus from "../../../components/Modal/modalHapus";
import toast, { Toaster } from "react-hot-toast";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import { useTranslation } from "react-i18next";

function GantiUser() {
  const [data, setData] = useState(DataListUser);

  const [getSingleShare, setGetSingleShare] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
   const { t } = useTranslation();

  const columns = [
    {
      accessorKey: "namaUser",
      header: t("nama_pengguna_tu"),
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "satker",
      header: "Satker",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      id: "actions",
      accessorKey: "id",
      header: t("actions_um"),
      cell: function render({ getValue, state, row, column, instance }) {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              gap: "4px",
            }}
          >
            <button
              type="button"
              className="btn btn-success bg-opacity-70 text-white"
              // onClick={() => onRowView(row.original)}
            >
              {t("detail_um")}
            </button>
            <button
              type="button"
              className="btn btn-danger opacity-70"
              data-bs-toggle="modal"
              data-bs-target="#modalHapus"
              onClick={() => setGetSingleShare(row.original)}
            >
              <img src={Del} width={"35%"} />
            </button>
          </div>
        );
      },
    },
  ];
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        const id = Math.floor(Math.random() * 10000);
        const newRow = {
          id,
          studentNumber: id,
          name: "",
          dateOfBirth: "",
          major: "",
        };
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  });
  return (
    <div
      class="container-fluid"
      style={{
        height: "auto",
        padding: "100px 10px",
      }}
    >
      <ModalHapus getData={getSingleShare} />
      <Toaster
        position="bottom-left"
        reverseOrder={false}
        gutter={8}
        containerClassName=""
        containerStyle={{}}
        toastOptions={{
          // Define default options
          className: "",
          duration: 5000,
          style: {
            background: "#ffbaba",
            color: "#ff0000 ",
          },

          // Default options for specific types
          success: {
            duration: 3000,
            theme: {
              primary: "green",
              secondary: "black",
            },
          },
        }}
      />
      <div class="card">
        <div class="card-body">
          <h5 class="card-title mb-3 textfamily">
            {t("edit_um")} {t("pengguna_t1")}
          </h5>
          <div class="row">
            <Filters filtering={filtering} setFiltering={setFiltering} />
            <table className="table table-bordered table-striped table-hover">
              <thead className="table-primary">
                {table.getHeaderGroups().map((headerGroup) => (
                  <tr key={headerGroup.id} className="  uppercase">
                    {headerGroup.headers.map((header) => (
                      <th
                        key={header.id}
                        className="px-4 pr-2 py-3 font-medium text-left"
                      >
                        {header.isPlaceholder
                          ? null
                          : flexRender(
                              header.column.columnDef.header,
                              header.getContext()
                            )}
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>

              <tbody>
                {table.getRowModel().rows.map((row) => (
                  <tr
                    className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50"
                    key={row.id}
                  >
                    {row.getVisibleCells().map((cell) => (
                      <td className="px-4 py-2" key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
            <Paginations table={table} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default GantiUser;
