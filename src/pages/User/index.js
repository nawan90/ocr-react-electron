import React from "react";
// import '../../assets/css/ocr.css';
// import '../../assets/css/table-list.css';
import { useNavigate } from "react-router-dom";

import icList from "../../assets/images/list_user.png";
import icEdit from "../../assets/images/icon/edit_user.png";
import { useTranslation } from "react-i18next";

function User() {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const toGantiUser = () => {
    navigate("ganti-user");
  };

  const toListUser = () => {
    navigate("list-user");
  };

  return (
    <div id="layoutAuthentication">
      <div
        class="container-xxl"
        style={{
          height: "90vh",
          padding: "300px 10px",
        }}
      >
        <div class="row ">
          <div class="col-3"></div>
          <div class="col-3" onClick={toListUser}>
            <div
              class="card bgcard2 card-animate mb-3"
              style={{ maxwidth: "18rem" }}
            >
              <div class="card-header ">
                <h5 class="float-start text-white">
                  {t("list_um")} {t("pengguna_t1")}
                </h5>
              </div>
              <div class="card-body">
                <div class="float-end">
                  <img src={icList} width={"80%"} />
                </div>
              </div>
            </div>
          </div>
          <div class="col-3 " onClick={toGantiUser}>
            <div
              class="card bgcard2 card-animate mb-3"
              style={{ maxwidth: "18rem" }}
            >
              <div class="card-header ">
                <h5 class="float-start text-white">
                  {t("edit_um")} {t("pengguna_t1")}
                </h5>
              </div>
              <div class="card-body">
                <div class="float-end">
                  <img src={icEdit} width={"80%"} />
                </div>
              </div>
            </div>
          </div>
          <div class="col-3">
            {/* <div class="card">
            <div class="card-header">Header</div>
            <div class="card-body">
              <div class="float-end">
                <i class="fa fa-list" style={{ color: "#538cee", opacity: " 0.4", fontSize: "50px" }}></i>
              </div>
              <h3 class="float-start">Ganti User</h3>
            </div>
          </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default User;
