import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Filters from "../../components/Table/filters";
import Paginations from "../../components/Table/pagination";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import DataListUser from "../../fakeData/listActivityLog.json";
import View from "../../assets/images/icon/icon_viewer.png";
import toast, { Toaster } from "react-hot-toast";
import ModalView from "../../components/Modal/modalView";
import Helper from "../../api/helper";
import { useTranslation } from "react-i18next";
import Mode from "../../../package.json";

function ActivityLog() {
    const navigate = useNavigate();
     let base_url = process.env.REACT_APP_API_URL;
    const [data, setData] = useState(DataListUser);
    const [sorting, setSorting] = useState([]);
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [getData, setGetData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const { t } = useTranslation();
    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    const columns = [
        {
            accessorKey: "log_title",
            header: t("act_al"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "log_by_user",
            header: t("pengguna_t1"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "log_custom_create_date",
            header: t("create_date_um"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "log_desc",
            header: t("description_um"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div
                        style={{
                            textAlign: "center",
                        }}
                    >
                        <button type="button" className="btn" data-bs-toggle="modal" data-bs-target="#ModalInput" onClick={() => setGetData(row.original)}>
                            {/* {t("detail_um")} */}
                            <img src={View} width={"45%"} />
                        </button>
                    </div>
                );
            },
        },
    ];

    // useEffect(() => {
    //     const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

    //     const urlValid = () => {
    //         if (ip === "116.90.165.46") {
    //             setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
    //         } else {
    //             setDataIp(ipServer + "");
    //         }
    //     };

    //     urlValid();
    // }, [ip]);

    useEffect(() => {
         const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
             showTable();
        }
    }, [dataIp]);

    // useEffect(() => {
    //     showTable();
    // }, []);

    const formatLogCreatedDate = (dateString) => {
        const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];
        const options = { timeZone: "Asia/Jakarta", timeZoneName: "short" };
        const realdate = new Date(dateString);
        const date = new Date(realdate.getTime() );
        const day = String(date.getDate()).padStart(2, "0");
        const month = selBulan[date.getMonth()]; // Months are 0-indexed
        const year = String(date.getFullYear()); // Get last two digits of year
        const hours = String(date.getHours()).padStart(2, "0");
        const minutes = String(date.getMinutes()).padStart(2, "0");
        return `${day} ${month} ${year}  ${hours}:${minutes}`;
    };

    const showTable = async () => {
        try {
            setIsLoading(true);
            let myUrl = dataIp + "/@search?type_name=Log_Activity_User&_sort_asc=creation_date&_metadata=log_type_name,log_category,log_created_date,log_obj_id,log_by_user,log_title,log_desc,log_id,log_obj_path&_size=10000";

            let datas = await Helper.GetListActivityUser(myUrl);
            let filterdata = datas.data.items
                .map((item) => ({
                    ...item, // Keep other properties unchanged
                    log_custom_create_date: formatLogCreatedDate(item.log_created_date),
                    log_desc: "User `" + item.log_by_user + "` " + item.log_desc, // Modify the 'nama' property
                }))
                .sort((a, b) => new Date(b.log_created_date) - new Date(a.log_created_date)); // perhatikan bentuk datenya
            setData(filterdata);
            setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
             if (err && err.hasOwnProperty("status")) {
                if (err.status == 401) {
                    toast(`${t("session_exp_um")}`, { icon: "👏" });
                    navigate("/home");
                }
            }
        }
    };

    const convertToCSV = (logData) => {
        const headers = Object.keys(logData[0]); // Ambil header dari key pertama
        const csvRows = [];

        // Tambahkan header
        csvRows.push(headers.join(","));

        // Tambahkan data baris
        for (const row of logData) {
            const values = headers.map((header) => {
                const escaped = ("" + row[header]).replace(/"/g, '\\"'); // Escape double quotes
                return `"${escaped}"`; // Tambahkan double quotes di sekitar nilai
            });
            csvRows.push(values.join(","));
        }

        return csvRows.join("\n"); // Gabungkan semua baris menjadi satu string
    };

    const downloadCSV = (csv, filename) => {
        const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
        const link = document.createElement("a");
        const url = URL.createObjectURL(blob);
        link.href = url;
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        // link.click();
        // document.body.removeChild(link);
        setTimeout(() => {
            link.click();
            document.body.removeChild(link); // Menghapus link setelah klik
            URL.revokeObjectURL(url); // Membebaskan URL yang dibuat
        }, 0);
    };

    const onHandleRefresh = () => {
        showTable();
    };

    const onHandleDownload = (e) => {
        e.preventDefault();
        let csvData = convertToCSV(data);
        let date = new Date();
        downloadCSV(csvData, `log_dms_${date}.csv`);
    };

    const onHandleSendEmail = (e) => {
        e.preventDefault();
    };

    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });
    const showMode = () => {
        const isLatest = /latest/.test(Mode.version);
    
        if (isLatest) {
          console.log('Latest version detected');
        } else {
          console.log('Not the latest version');
          return (
            <h4 style={{ background: "#5CFF5C", color: "red", fontWeight: "bold" }}>
              DEVELOPMENT MODE
            </h4>
          )
        }
      }
    return (
        <div class="container-fluid pt-3 " style={{  height: " auto", marginTop: "75px" }}>
            <ModalView getData={getData ? getData : ""} />
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-3 textfamily">{t("titlle_al")}</h5>
                    {showMode()}
                    <div class="row">
                        <div class="col-10">
                            <Filters filtering={filtering} setFiltering={setFiltering} />
                        </div>
                        <div class="col-1">
                            {isLoading ? (
                                <button class="btn btn-primary float-end" type="button">
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true">
                                        {" "}
                                    </span>
                                    <span> </span>
                                    {t("loading_lo")}
                                </button>
                            ) : (
                                <button class="btn btn-primary float-end" type="button" onClick={onHandleRefresh}>
                                    ⟳ {t("refresh_um")}
                                </button>
                            )}
                        </div>
                        <div class="col-1">
                            <div class="btn-group" role="group">
                                <button id="btnshare" class="btn btn-primary float-end dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" type="button">
                                    ➣ {t("share_lo")}
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="btnshare">
                                    <li>
                                        <a class="dropdown-item" href="#" onClick={onHandleDownload}>
                                            {t("download_lo")}
                                        </a>
                                    </li>
                                    {/* <li>
                                        <a class="dropdown-item" href="#" onClick={onHandleSendEmail}>
                                            {t("send_to_email_lo")}
                                        </a>
                                    </li> */}
                                </ul>
                            </div>
                        </div>
                    </div>

                    {/* <div class="row"> */}
                    <table className="table table-bordered table-striped table-hover">
                        <thead className="table-primary">
                            {table.getHeaderGroups().map((headerGroup) => (
                                <tr key={headerGroup.id} className="  uppercase">
                                    {headerGroup.headers.map((header) => (
                                        <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                            {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>

                        <tbody>
                            {table.getRowModel().rows.map((row) => (
                                <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                    {row.getVisibleCells().map((cell) => (
                                        <td className="px-4 py-2" key={cell.id}>
                                            {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                        </td>
                                    ))}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <Paginations table={table} />
                    {/* </div> */}
                </div>
            </div>
        </div>
    );
}

export default ActivityLog;
