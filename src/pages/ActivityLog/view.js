import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import LoadSpinner from "../../components/Load/load";
import "../../assets/css/addcss.css";
import { useTranslation } from "react-i18next";
import Helper from "../../api/helper";

function ViewLogActivity(props) {
    const {
        showTable,
        getData,
        action,
        onSelected,
        getAllTable,
        closeButton,
        setCloseButton,
        handleClose,
        disButton,
        setDisButton,
        // validSatkerCode, setValidSatkerCode,validSatkerName, setValidSatkerName, validSatkerPoldaName, setValidSatkerPoldaName
    } = props;
    const { t } = useTranslation();
    const [loadSpin, setLoadSpinner] = useState(false);
    const [newContentItemMetadata, setNewContentItemMetadata] = useState("");
    const initialRows = [{ polda_metadata_id: 1, polda_metadata_name: "", polda_metadata_value: "" }];
    const [rows, setRows] = useState(initialRows);

    let base_url = process.env.REACT_APP_API_URL;
    let id_update = getData ? getData.path : "";
    const [logTitle, setLogTitle] = useState(null);
    const [logDesc, setLogDesc] = useState(null);
    const [logCategory, setLogCategory] = useState(null);
    const [logObjId, setLogObjId] = useState(null);
    const [logTypeName, setTypeName] = useState(null);
    const [logCreateDate, setLogCreateDate] = useState(null);
    const [logUser, setLogUser] = useState(null);

    let urlInput = base_url + "polridocument";
    let urlEdit = base_url + id_update;
    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });

    useEffect(() => {
        // loadKorwil();
        // showTable();
    }, []);

    useEffect(() => {
        setTimeout(() => setLoadSpinner(false), 3300);
    }, [loadSpin]);

    const loadDetailObject = async () => {
        try {
            let url = null;
            if (logTypeName == "Document") {
                url = base_url + "/polridocument/";
            } else {
            }

             // let datas = await Helper.GetDetailDocument(url);
        } catch (err) {
            
        }
    };
    // const optionKorwil = korwilRef?.map(function (item) {
    //     return item.polda_name;
    // });
    const formatLogCreatedDate = (dateString) => {
        const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];
        const options = { timeZone: "Asia/Jakarta", timeZoneName: "short" };
        const realdate = new Date(dateString);
        const date = new Date(realdate.getTime() + 7 * 60 * 60 * 1000);
        const day = String(date.getDate()).padStart(2, "0");
        const month = selBulan[date.getMonth()]; // Months are 0-indexed
        const year = String(date.getFullYear()); // Get last two digits of year
        const hours = String(date.getHours()).padStart(2, "0");
        const minutes = String(date.getMinutes()).padStart(2, "0");
        return `${day} ${month} ${year}  ${hours}:${minutes}`;
    };

    useEffect(() => {
        if (getData) {
             setLogTitle(getData.log_title);
            setLogCategory(getData.log_category);
            setLogCreateDate(getData.log_created_date);

            setLogObjId(getData.log_id);
            setLogUser(getData.log_by_user);
            setTypeName(getData.log_type_name);

            let desc = `${getData.log_desc} \n\nDate & Time : ${formatLogCreatedDate(getData.log_created_date)} \nPath : ${getData.log_obj_path ? getData.log_obj_path : ""}`; // + "\n\nDate & Time : " + getData.log_created_date + "\nPath : " + getData.log_obj_path;
            setLogDesc(desc);
            loadDetailObject();
        }
    }, [getData]);

     return (
        <div>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />

            <form>
                {/* <h5 class="card-title mb-3">
                    {t("titlle_al")} : {""}
                </h5> */}

                <div class="row ">
                    <div class="col-6">
                        {/* <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("log_title_lo")}</label>
                            </div>
                            <div class="col-8">
                                <input disabled value={logTitle} type="text " class="form-control" />
                            </div>
                        </div> */}
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("log_object_lo")}</label>
                            </div>
                            <div class="col-8">
                                <input disabled value={logTypeName} type="text " class="form-control" />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("log_action_lo")}</label>
                            </div>
                            <div class="col-8">
                                <input disabled value={logTitle} type="text " class="form-control" />
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-4 fw-medium">
                                <label>{t("log_user_lo")}</label>
                            </div>
                            <div class="col-8">
                                <input disabled value={logUser} type="text " class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-2 fw-medium">
                        <label>{t("description_um")}</label>
                    </div>
                    <div class="col-10">
                        <textarea rows="4" disabled value={logDesc} type="text" class="form-control" />
                    </div>
                </div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    <button class="btn btn-danger col-3 mx-3 " data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}>
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default ViewLogActivity;
