// import React, { useEffect, useState } from "react";
// import path from "path";

// const Updates = () => {
//   const [files, setFiles] = useState([]);

//   useEffect(() => {
//     const updatesPath = path.join(__dirname, "updates");
//     console.log("Updates folder path:", updatesPath);

//     // Fetch file names from the updates directory (using IPC if needed)
//     window.electronAPI.ipcRenderer.invoke("get-files", updatesPath).then((fileList) => {
//       setFiles(fileList);
//     });

//     // Get files from the updates directory
// window.electronAPI.getFiles("D:/baru/src/pages/Updates")
// .then((files) => console.log("Files:", files))
// .catch((err) => console.error("Error fetching files:", err));

// // Handle update events
// window.electronAPI.onUpdateAvailable(() => {
// console.log("An update is available!");
// });

// window.electronAPI.onUpdateDownloaded(() => {
// console.log("Update downloaded. Ready to install.");
// window.electronAPI.installUpdate();
// });

// // Run OCR verification
// window.electronAPI.runOcrVerification("sample-arg");

// window.electronAPI.onOcrStdout((data) => {
// console.log("OCR stdout:", data);
// });

// window.electronAPI.onOcrDone((code) => {
// console.log("OCR process finished with code:", code);
// });
//   }, []);

//   return (
//     <div>
//       <h1>Updates</h1>
//       <ul>
//         {files.map((file, index) => (
//           <li key={index}>{file}</li>
//         ))}
//       </ul>
//     </div>
//   );
// };

// export default Updates;
import React, { useEffect, useState } from "react";

const Updates = () => {
  const [files, setFiles] = useState([]);

  useEffect(() => {
    const updatesPath = window.electronAPI.resolvePath(__dirname, "updates");
    console.log("Updates folder path:", updatesPath);

    // Fetch file names from the updates directory
    window.electronAPI
      .getFiles(updatesPath)
      .then((fileList) => setFiles(fileList))
      .catch((err) => console.error("Error fetching files:", err));

    // Handle update events
    window.electronAPI.onUpdateAvailable(() => {
      console.log("An update is available!");
    });

    window.electronAPI.onUpdateDownloaded(() => {
      console.log("Update downloaded. Ready to install.");
      window.electronAPI.installUpdate();
    });

    // Run OCR verification
    window.electronAPI.runOcrVerification("sample-arg");

    window.electronAPI.onOcrStdout((data) => {
      console.log("OCR stdout:", data);
    });

    window.electronAPI.onOcrDone((code) => {
      console.log("OCR process finished with code:", code);
    });
  }, []);

  return (
    <div>
      <h1>Updates</h1>
      <ul>
        {files.map((file, index) => (
          <li key={index}>{file}</li>
        ))}
      </ul>
    </div>
  );
};

export default Updates;
