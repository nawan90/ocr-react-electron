// import React, { useState, useEffect } from 'react';
// import DynamicProgressBar from '../../../components/Progress';
// import axios from "axios";
// import { ToastContainer, toast } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";
// import { useTranslation } from 'react-i18next';

// function DownloadPdf(props) {
//     const { getData, startDownload, setDownloadStarted } = props;
//     const name = getData?.doc_name;
//     const polda = getData?.doc_polda_name;
//     const linkUrl = getData.doc_url?.original_file_path;
//     let base_url = process.env.REACT_APP_API_URL;
//     const [loadingComplete, setLoadingComplete] = useState(false);
//     const { t } = useTranslation();

//     const notify = () => toast.success('Download Data Berhasil', {
//         icon: '👏',
//     });

//     useEffect(() => {
//         if (startDownload) {
//             getDownload();
//         }
//     }, [startDownload]);

//     const getDownload = async () => {
//         let get_data = {
//             headers: {
//                 Authorization: "Basic " + btoa("root:root"),
//             },
//             responseType: "blob",
//         };

//         const fileName = polda + " - " + name;
//         const pdfUrl = base_url + "/" + linkUrl;
//         axios
//             .get(pdfUrl, get_data) // Set responseType to 'blob' to handle binary data
//             .then((response) => {
//                 const blob = new Blob([response.data], { type: "application/pdf" });
//                 const downloadLink = document.createElement("a");

//                 downloadLink.href = window.URL.createObjectURL(blob);
//                 downloadLink.setAttribute("download", fileName);
//                 document.body.appendChild(downloadLink);
//                 downloadLink.click();
//                 document.body.removeChild(downloadLink);
                
//                 // Notify the user after the download
//                 notify();

//                 setDownloadStarted(false);
//                 setLoadingComplete(false);
//             })
//             .catch((error) => {
//                 console.error(t('error_um'), error);
//             });
//     }

//     return (
//         <div>
//             {startDownload && (
//                 <div className="card shadow mb-4 bg-warning">
//                     <div className="card-body p-3">
//                         <h6>{t('proc_down_ts')}</h6>
//                         <p className="font-italic text-muted">{t('please_ts')}</p>
//                         <div style={{ height: '4px' }} className="progress">
//                             <DynamicProgressBar startDownload={startDownload} setLoadingComplete={setLoadingComplete} />
//                         </div>
//                     </div>
//                 </div>
//             )}
//             <ToastContainer />
//         </div>
//     );
// }

// export default DownloadPdf;
import React, { useState, useEffect } from 'react';
import DynamicProgressBar from '../../../components/Progress';
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useTranslation } from 'react-i18next';

import { PDFDocument, rgb,degrees  } from 'pdf-lib';

import LocalStorageHelper from '../../../api/localStorageHelper';
import LoadSpinner from '../../../components/Load/load';
function DownloadPdf(props) {
    const { getData, startDownload, setDownloadStarted } = props;
    const name = getData?.doc_name;
    const polda = getData?.doc_polda_name;
    const linkUrl = getData.doc_url?.original_file_path;
    const base_url = process.env.REACT_APP_API_URL;
    const userData = LocalStorageHelper.getUserData();
    const [loadingComplete, setLoadingComplete] = useState(false);
    const { t } = useTranslation();
    const [loading, setLoading] = useState(false)

    const notify = () => {
        toast.success('Download has started. Please select a location to save the file.', {
            icon: '👏',
        });
    };

    useEffect(() => {
        if (startDownload) {
            getDownload();
        }
    }, [startDownload]);

    const getDownload = async () => {
        try {
            setLoading(true);
    
            const get_data = {
                headers: {
                    Authorization: `Bearer ${userData.token}`,
                },
                responseType: "blob",
            };
    
            const response = await axios.get(`${base_url}${linkUrl}`, get_data);
    
            const pdfBlob = new Blob([response.data], { type: "application/pdf" });
            const pdfDoc = await PDFDocument.load(await pdfBlob.arrayBuffer());
            const pages = pdfDoc.getPages();
    
            const watermarkLine1 = "Confidential";
            const watermarkLine2 = "Do Not Distribute";
    
            pages.forEach((page) => {
                const { width, height } = page.getSize();
    
                const fontSize = 30; // Size of watermark text
                const gapX = 200; // Horizontal gap between watermarks
                const gapY = 150; // Vertical gap between watermarks
                const rotation = degrees(45); // Watermark rotation angle
    
                // Tile watermarks across the page
                for (let y = 0; y < height; y += gapY) {
                    for (let x = 0; x < width; x += gapX) {
                        // Add first watermark line
                        page.drawText(watermarkLine1, {
                            x,
                            y,
                            size: fontSize,
                            color: rgb(0.8, 0.8, 0.8), // Light gray color
                            opacity: 0.3,
                            rotate: rotation,
                        });
    
                        // Add second watermark line slightly below
                        page.drawText(watermarkLine2, {
                            x,
                            y: y - fontSize, // Adjust y position for the second line
                            size: fontSize,
                            color: rgb(0.8, 0.8, 0.8), // Light gray color
                            opacity: 0.3,
                            rotate: rotation,
                        });
                    }
                }
            });
    
            // Save and download the watermarked PDF
            const modifiedPdfBytes = await pdfDoc.save();
            const modifiedPdfBlob = new Blob([modifiedPdfBytes], { type: "application/pdf" });
    
            const downloadLink = document.createElement("a");
            downloadLink.href = window.URL.createObjectURL(modifiedPdfBlob);
            downloadLink.setAttribute("download", `${polda} - ${name}.pdf`);
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
            setDownloadStarted(false)
            
            setLoading(false);
        } catch (error) {
            console.error("Error while downloading the file: ", error);
            setLoading(false);
        }
    };
    
    return (
        <div>
            {
                loading?(<LoadSpinner/>):("")
            }
            <ToastContainer />
        </div>
    );
}

export default DownloadPdf;
