import React, { useState } from "react";
import { useTranslation } from "react-i18next";

function WordSearchCell({ row, searchtext }) {
  const [expandedIndex, setExpandedIndex] = useState(null); // Track which entry is expanded
  const { t } = useTranslation();

  // Function to highlight the search text
  const highlightText = (text) => {
    const regex = new RegExp(`(${searchtext})`, "gi");
    return text.split(regex).map((part, index) => {
      if (part.toLowerCase() === searchtext.toLowerCase()) {
        return (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {part}
          </span>
        );
      } else {
        return part;
      }
    });
  };

  // Toggle to expand/collapse text
  const handleToggle = (index) => {
    setExpandedIndex((prevIndex) => (prevIndex === index ? null : index));
  };

  // Function to find the first highlighted text and return the truncated version of the text
  const getTruncatedText = (entryText) => {
    const firstHighlightIndex = entryText.toLowerCase().indexOf(searchtext.toLowerCase());
    if (firstHighlightIndex === -1) return entryText; // If searchtext isn't found, return full text
    
    // Truncate text after the first highlighted search term
    return entryText.slice(0, firstHighlightIndex + searchtext.length + 1) + "...";
  };

  const wordSearched = row.original.word_searched;

  // Function to return the full or truncated text with the Read More button
  const getTextWithButton = (entryText, index) => {
    const highlightedText = highlightText(entryText); // Highlight the full text
    const truncatedText = getTruncatedText(entryText); // Truncate text after the search term

    // Check if there is more text after the highlighted text
    const remainingText = entryText.slice(entryText.indexOf(searchtext) + searchtext.length);

    // If there's more text after the first highlighted portion, show the Read More button
    const readMoreButton =
      remainingText.length > 1 && (
        <button
          style={{
            background: "none",
            border: "none",
            color: "blue",
            textDecoration: "underline",
            cursor: "pointer",
            paddingLeft: "10px",
          }}
          onClick={() => handleToggle(index)}
        >
          {expandedIndex === index ? `${t("baca_dikit_um")}` : `${t("baca_banyak_um")}`}
        </button>
      );

    return (
      <span>
        {expandedIndex === index ? highlightedText : highlightText(truncatedText)} {/* Apply highlightText on both truncated and expanded text */}
        {readMoreButton}
      </span>
    );
  };

  return (
    <td
      className="px-0 py-2"
      style={{
        width: "100%",
        backgroundColor: row.original.someCondition ? "#f5f5f5" : "transparent",
        border: row.original.someCondition ? "2px solid red" : "none",
        maxWidth: "800px",
        wordWrap: "break-word",
        overflow: "hidden",
        whiteSpace: "normal",
      }}
    >
      <div style={{ padding: 0, width: "100%" }}>
        {wordSearched?.map((entry, index) => (
          <div key={index} style={{ padding: 0, width: "100%" }}>
            <ul style={{ padding: 0, margin: 0, width: "100%" }}>
              <li
                style={{
                  width: "100%",
                  marginBottom: "15px",
                  paddingLeft: "5px",
                  backgroundColor: row.original.someCondition ? "#D3D3D3" : "transparent",
                }}
              >
                {getTextWithButton(entry.text, index)}
              </li>
            </ul>
          </div>
        ))}
      </div>
    </td>
  );
}

export default WordSearchCell;
