import React, { useState, useEffect } from "react";
import axios from "axios";
import LoadSpinner from "../../../components/Load/load";
import { useTranslation } from "react-i18next";
import LocalStorageHelper from "../../../api/localStorageHelper";
import { PDFDocument, rgb, degrees } from "pdf-lib";
import { Document, Page, pdfjs } from "react-pdf";

import { Worker, Viewer } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
// import { toolbarPlugin, ToolbarSlot } from "@react-pdf-viewer/toolbar";
// import "@react-pdf-viewer/toolbar/lib/styles/index.css";
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
import "../../../assets/css/current-page.css";

function ShowPdf(props) {
    const { closeModal, link, getData, isDownload } = props;
    const [pdfUrl, setPdfUrl] = useState("");
    const [pageNumber, setPageNumber] = useState(1);
    const [totalPages, setTotalPages] = useState(null);
    // const toolbar = toolbarPlugin();
    // const { Toolbar, CurrentPageInput, ZoomIn, ZoomOut, OpenFile, ResetZoom } = toolbarPlugin();
    // const toolbarPluginInstance = toolbarPlugin();
    // const { Toolbar } = toolbarPluginInstance;

    const renderToolbar = (Toolbar: (props: ToolbarProps) => ReactElement) => (
        <Toolbar>
            {(slots: ToolbarSlot) => {
                const { CurrentPageInput, Download, EnterFullScreen, GoToNextPage, GoToPreviousPage, NumberOfPages, Print, ShowSearchPopover, Zoom, ZoomIn, ZoomOut } = slots;
                return (
                    <div
                        style={{
                            alignItems: "center",
                            display: "flex",
                            width: "100%",
                        }}
                    >
                        {/* <div style={{ padding: "0px 2px" }}>
                            <ShowSearchPopover />
                        </div> */}
                        <div style={{ padding: "0px 2px" }}>
                            <ZoomOut />
                        </div>
                        <div style={{ padding: "0px 2px" }}>
                            <Zoom />
                        </div>
                        <div style={{ padding: "0px 2px" }}>
                            <ZoomIn />
                        </div>
                        <div style={{ padding: "0px 2px", marginLeft: "auto" }}>
                            <GoToPreviousPage />
                        </div>
                        <div style={{ padding: "0px 2px" }}>
                            <CurrentPageInput
                                style={{
                                    width: "5rem",
                                }}
                            />{" "}
                            / <NumberOfPages />
                        </div>
                        <div style={{ padding: "0px 2px" }}>
                            <GoToNextPage />
                        </div>
                        <div style={{ padding: "0px 2px", marginLeft: "auto" }}>
                            <EnterFullScreen />
                        </div>
                        {isDownload && isDownload === true ? (
                            <div style={{ padding: "0px 2px" }}>
                                <Download />
                            </div>
                        ) : null}
                        {/* <div style={{ padding: "0px 2px" }}>
                            <Print />
                        </div> */}
                    </div>
                );
            }}
        </Toolbar>
    );

    const defaultLayoutPluginInstance = defaultLayoutPlugin({
        renderToolbar,
    });

    let base_url = process.env.REACT_APP_API_URL;
    const { t } = useTranslation();
    const userData = LocalStorageHelper.getUserData();
    const workerUrl = "pdf.worker.min.js"; //`./node_modules/pdfjs-dist/build/pdf.worker.min.js`;

    // Menentukan worker PDF.js yang akan digunakan
    // pdfjs.GlobalWorkerOptions.workerSrc = workerUrl;
    // pdfjs.GlobalWorkerOptions.workerSrc = require("pdfjs-dist/build/pdf.worker.min.js");
    useEffect(() => {
        const fetchPDF = async () => {
            try {
                let watermarkText = `${userData.nip}:${userData.fullname} \n ${getDate()}`;
                // "100030120031:User 1 Polda Metro Jaya\nKEPOLISIAN DAERAH METRO JAYA:DIT RESNARKOBA POLDA METRO JAYA\n31 Oktober 2024 16:04:59";
                const watermarkLines = watermarkText.split("\n");
                let authentication_data = {
                    headers: {
                        Authorization: `Bearer ${userData.token}`,
                    },
                    responseType: "blob",
                };
                // const pdfUrl = base_url + link;
                if (!link) {
                    return;
                }
                const pdfUrl = api + "/db/dmsbackend/" + link;

                const response = await axios.get(pdfUrl, authentication_data);
                const existingPdfBytes = await response.data.arrayBuffer();

                const fontSize = 14; // Ukuran font watermark
                const spacing = 50; // Jarak antara watermark
                const textWidth = fontSize * 10; // Estimasi lebar teks per watermark
                const textHeight = fontSize * 1.2; // Estimasi tinggi teks per watermark

                // Buat PDF baru
                const pdfDoc = await PDFDocument.load(existingPdfBytes);
                const pages = pdfDoc.getPages();

                // Tambahkan watermark ke setiap halaman
                pages.forEach((page) => {
                    const { width, height } = page.getSize();
                    const textWidth = page.getWidth();
                    for (let x = 0; x < width; x += textWidth / 4) {
                        for (let y = 0; y < height; y += fontSize * 6) {
                            page.drawText(watermarkText, {
                                x: x,
                                y: y,
                                size: fontSize,
                                color: rgb(0.75, 0.75, 0.75), // Light grey color
                                opacity: 0.5,
                                rotate: degrees(45), // Rotate if needed
                            });
                        }
                    }
                });

                // Simpan PDF dengan watermark
                const pdfBytes = await pdfDoc.save();
                const pdfBlob = new Blob([pdfBytes], { type: "application/pdf" });
                // const pdfBlob = new Blob([response.data], { type: "application/pdf" });
                const url = URL.createObjectURL(pdfBlob);
                setPdfUrl(url);
            } catch (error) {
                console.error(`${t("error_um")}`, error);
            }
        };
        const api = userData.myip;

        if (api && api != null && api != "null") {
            fetchPDF();
        }

        return () => {
            if (pdfUrl) {
                URL.revokeObjectURL(pdfUrl);
            }
        };
    }, [props]);

    const goToPage = (page) => {
        if (page > 0 && page <= totalPages) {
            setPageNumber(page);
        }
    };

    const onLoadSuccess = ({ numPages }) => {
        setTotalPages(numPages);
    };

    const getDate = () => {
        // const monthNames = selectBulan();
        const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];
        const cd = new Date();

        let data = cd.getDate() + " " + selBulan[cd.getMonth()] + " " + cd.getFullYear() + " " + String(cd.getHours()).padStart(2, "0") + ":" + String(cd.getMinutes()).padStart(2, "0") + ":" + String(cd.getSeconds()).padStart(2, "0");
        return data;
    };

    return (
        <div>
            {pdfUrl ? (
                <div
                    style={{
                        height: "45rem",
                    }}
                >
                    <Worker workerUrl={workerUrl}>
                        <Viewer fileUrl={pdfUrl} plugins={[defaultLayoutPluginInstance]} />
                    </Worker>
                </div>
            ) : (
                <div>
                    {/* <LoadSpinner /> */}
                    <p>Loading PDF...</p>
                </div>
            )}
        </div>
    );
}
export default ShowPdf;
