import React, { useState, useEffect } from "react";
import axios from "axios";
import LoadSpinner from "../../../components/Load/load";
import { useTranslation } from "react-i18next";
import LocalStorageHelper from "../../../api/localStorageHelper";
function ShowPdf(props) {
  const { closeModal, link, getData } = props;
  const [pdfUrl, setPdfUrl] = useState("");
  let base_url = process.env.REACT_APP_API_URL;
  const { t } = useTranslation();
  const userData = LocalStorageHelper.getUserData();
  useEffect(() => {
    const fetchPDF = async () => {
       try {
        let authentication_data = {
          headers: {
            Authorization: `Bearer ${userData.token}`,
          },
          responseType: "blob",
        };
        const pdfUrl = base_url + link;
        const response = await axios.get(pdfUrl, authentication_data);
        const pdfBlob = new Blob([response.data], { type: "application/pdf" });
        const url = URL.createObjectURL(pdfBlob);
        setPdfUrl(url);
      } catch (error) {
        console.error(`${t('error_um')}`, error);
      }
    };
    const api = userData.myip
    
    if(api && api != null && api != "null"){
      fetchPDF();
     }
   
    return () => {
      if (pdfUrl) {
        URL.revokeObjectURL(pdfUrl);
      }
    };
  }, [props]);

  return (
    <div>
      {pdfUrl ? (
        <iframe
          title="View Document"
          src={pdfUrl}
          width="100%"
          height="800px"
          frameBorder="0"
        />
      ) : (
        <div>
          <LoadSpinner />
        </div>
      )}
    </div>
  );
}
export default ShowPdf;

