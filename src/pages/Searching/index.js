import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import Mark from "mark.js";
import {
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import Paginations from "../../components/Table/pagination";
import IpNotValidPage from "../../components/Page/IpNotValid";
import Modal2 from "../../components/Modal2";
import DownloadPdf from "./Download";
import Down from "../../assets/images/icon/icon_down.png";
import lihat from "../../assets/images/icon/icon_viewer.png";
import srch from "../../assets/images/icon/search.png";
import add from "../../assets/images/icon/add.png";
import clr from "../../assets/images/icon/clear.png";
import excel from "../../assets/images/icon/icon_excel.png";
import map from "../../assets/images/icon/icon_maps.png";
import { useTranslation } from "react-i18next";
import LoadSpinner from "../../components/Load/load";
import LocalStorageHelper from "../../api/localStorageHelper";
import moment from "moment";
import "moment/locale/id";
import WordSearchCell from "./WordSearchCell";
import ExcelJS from "exceljs";
import ModalPdfView from "../../components/Modal/modalPdfView";
import ModalOtp from "../../components/Modal/modalOtp";
import CryptoJS from "crypto-js";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import DummySearch from "../../assets/dummy/search.json"; // Import your JSON data
import axios from "axios";
import { PDFDocument, rgb, degrees } from "pdf-lib";
import Swal from "sweetalert2";
import Mode from "../../../package.json";

import helper from "../../api/helper";

function s2ab(str) {
  const buf = new ArrayBuffer(str.length);
  const view = new Uint8Array(buf);
  for (let i = 0; i < str.length; i++) {
    view[i] = str.charCodeAt(i) & 0xff;
  }
  return buf;
}
// const jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJha3NlcyI6IiIsImNyZWF0ZWQiOjE3MzE5MDg3NTA4MzAsImV4cCI6MTczMjUxMzU1MCwiaW5pdGlhbF9kZXZpY2VfZGlzcGxheV9uYW1lIjoiU3VwZXIgQWRtaW4iLCJpc19hZG1pbiI6ZmFsc2UsImlzX3N1cGVyYWRtaW4iOnRydWUsImtlcG9saXNpYW5fbGV2ZWwiOiIiLCJrb2RlX2tvcndpbCI6IiIsImtvZGVfc2F0a2VyIjoiIiwibmFtYV9rb3J3aWwiOiIiLCJuYW1hX3NhdGtlciI6IiIsIm5hbWUiOiJTdXBlciBBZG1pbiIsIm5ycCI6Ijg4ODg4ODg4Iiwicm9sZXMiOm51bGwsInNwcG0iOiIiLCJzdWIiOiJzdXBlcmFkbWluIiwidXNlcl9uYW1lIjoic3VwZXJhZG1pbiIsInVzZXJfdXVpZCI6IjM5MDRmMzdjLTcwNjItNDM4Yy04NmE5LWE2N2E2ZGQxNTJlNSJ9.U5otKqa4lSfvq6brhpZX6u81tmbcr5148CLLfzWsGTs"
function Searching() {
  const userData = LocalStorageHelper.getUserData();
  let base_url = process.env.REACT_APP_API_URL;
  let poldaID = "PD1";
  const efaskon_url = process.env.REACT_APP_API_EFASKON;
  // console.log("DummySearch", DummySearch);

  const [loading, setLoading] = useState(false);
  const [searchtext, setSearchtext] = useState("");
  const [isProcess, setIsProcess] = useState(false);
  const [data, setData] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [getData, setGetData] = useState([]);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [originalFile, setOriginalFile] = useState("");
  const [downloadStarted, setDownloadStarted] = useState(false);
  const [downloadDocName, setDownloadDocName] = useState("");
  const [downLoadUrl, setDownloadUrl] = useState(false);
  const [otpCode, setOtpCode] = useState("");
  const [timer, setTimer] = useState(2);
  const [downloadData, setDownloadData] = useState({});
  const { t } = useTranslation();
  const [documentTypeId, setDocumentTypeId] = useState("all");
  const [documentTypeCode, setDocumentTypeCode] = useState("");
  const [documentTypeName, setDocumentTypeName] = useState("");
  const [docTypeRef, setDocTypeRef] = useState(null);
  const [docTypeOptionsHtml, setDocTypeOptionsHtml] = useState([]);

  // console.log("data", data);
  const [isOpen, setIsOpen] = useState(false);
  const getSession = () => {
    const sessionKey = "6b59740a-ab96-4da8-90b0-12576a42f5e1";
    // const SECRET_KEY_DEV = "80b61d85-dec6-407b-84d2-ca158e41576b";
    // const secret_prod = process.env.REACT_APP_SECRET_KEY
    // const datesnya = Date.now().toString()
    // //atau datenya yg dari url convert ke curentimilis di cba mat
    // // const date = getparam("dates")
    // // const hmacHash = CryptoJS.HmacSHA256(date, SECRET_KEY).toString(CryptoJS.enc.Hex);
    // const hash = CryptoJS.HmacSHA256(`${datesnya}`, `${SECRET_KEY_DEV}`);
    // const hmacHash = hash.toString(CryptoJS.enc.Hex);
    const datesnya = Date.now().toString();
    const hmacHash = helper.getHmac(datesnya);

    const url = `${efaskon_url}/v1/session/restore/${sessionKey}`;
    axios
      .get(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${hmacHash}_`,
          Dates: datesnya,
        },
      })
      .then((response) => {
        console.log("Responya : ", response);
        if (response.status == 200) {
          const jwt = `${response.data.data}`;
          sendRequestOTP(hmacHash, jwt, datesnya);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const modifyData = (data) => {
    if (data && data.length > 0) {
      return data.sort((a, b) => a.doc_type_index_no - b.doc_type_index_no);
    } else {
      return [];
    }
  };

  const loadDocType = async () => {
    try {
      let url = userData.myip + "/db/dmsbackend/doctype/@list_doctype";

      let options = [];
      options.push(
        <option selected value="all">
          Semua
        </option>
      );
      let res = await helper.GetListDocType(url);
      if ((res && res.status == 200) || 201 || 202 || 204) {
        let datas = modifyData(res.data); //res.data;
        setDocTypeRef(datas);

        datas.map(function (data, index) {
          options.push(
            <option
              value={data.doc_type_id}
            >{`${data.doc_type_title} - ${data.doc_type_code}`}</option>
          );
        });

        setDocTypeOptionsHtml(options);
      }
    } catch (err) {}
  };
  const sendRequestOTP = async () => {
    setLoading(true);
    setOtpCode("");
    resetTimer();
    const jwt = userData.jwt_efaskon;
    const datesnya = Date.now().toString();
    const hmacHash = helper.getHmac(datesnya);
    const bearer = `${hmacHash}_${jwt}`;

    const url = `${efaskon_url}/v1/otp/request`;
    try {
      axios
        .post(
          url,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${bearer}`,
              Dates: datesnya,
            },
          }
        )
        .then((response) => {
          console.log("response Req OTP : ", response);
          if (response.status == 200) {
            Swal.fire("OTP Sukses", "Silahkan Cek Email Anda!", "success");
          }
        })
        .catch((err) => {
          Swal.fire("OTP Gagal", "Kode OTP tidak cocok!", "error");
        })
        .finally(() => {
          setLoading(false);
        });
    } catch (e) {
      setLoading(false);
      Swal.fire("OTP Gagal", "Kode OTP tidak cocok!", "error");
    }
  };

  const sendValidateOTP = async () => {
    setLoading(true);
    const jwt = userData.jwt_efaskon;
    const datesnya = Date.now().toString();
    const hmacHash = helper.getHmac(datesnya);
    const bearer = `${hmacHash}_${jwt}`;
    const url = `${efaskon_url}/v1/otp/validate`;
    try {
      axios
        .post(
          url,
          { otp: otpCode },
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${bearer}`,
              Dates: datesnya,
            },
          }
        )
        .then((response) => {
          if (response.status == 200) {
            getDownload(downloadData);
            setOtpCode("");
            setIsOpen(false);
          }
        })
        .catch((err) => {
          Swal.fire("OTP Gagal", "Kode OTP tidak cocok!", "error");
        })
        .finally(() => {
          // getDownload(downloadData)
          // setIsOpen(false)
          // setOtpCode("")
          setLoading(false);
        });
    } catch (e) {
      setLoading(false);
      Swal.fire("OTP Gagal", "Kode OTP tidak cocok!", "error");
    }
  };

  const onEnterOTPCode = (e) => {
    const value = e.target.value;

    // Memastikan hanya angka yang dapat dimasukkan
    if (/^\d*$/.test(value)) {
      setOtpCode(value); // Update OTP hanya jika input berupa angka
    }
  };

  const getDownload = async (data) => {
    const name = data?.doc_name;
    const polda = data?.doc_polda_name;
    const linkUrl = data.doc_url?.original_file_path;

    try {
      setLoading(true);

      const get_data = {
        headers: {
          Authorization: `Bearer ${userData.token}`,
        },
        responseType: "blob",
      };

      const response = await axios.get(
        `${userData.myip}/db/dmsbackend${linkUrl}`,
        get_data
      );

      const pdfBlob = new Blob([response.data], { type: "application/pdf" });
      const pdfDoc = await PDFDocument.load(await pdfBlob.arrayBuffer());
      const pages = pdfDoc.getPages();
      const date = new Intl.DateTimeFormat("id-ID", {
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        timeZone: "Asia/Jakarta",
      }).format(new Date());
      const watermarkText = `${userData.nip}:${userData.fullname} \n ${date}`;
      const watermarkLine2 = date;
      const fontSize = 14;
      pages.forEach((page) => {
        const { width, height } = page.getSize();
        const textWidth = page.getWidth();
        for (let x = 0; x < width; x += textWidth / 4) {
          for (let y = 0; y < height; y += fontSize * 6) {
            page.drawText(watermarkText, {
              x: x,
              y: y,
              size: fontSize,
              color: rgb(0.75, 0.75, 0.75), // Light grey color
              opacity: 0.5,
              rotate: degrees(45), // Rotate if needed
            });
          }
        }
      });

      // Save and download the watermarked PDF
      const modifiedPdfBytes = await pdfDoc.save();
      const modifiedPdfBlob = new Blob([modifiedPdfBytes], {
        type: "application/pdf",
      });

      const downloadLink = document.createElement("a");
      downloadLink.href = window.URL.createObjectURL(modifiedPdfBlob);
      downloadLink.setAttribute("download", `${polda} - ${name}.pdf`);
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
      setDownloadStarted(false);

      setLoading(false);
    } catch (error) {
      console.error("Error while downloading the file: ", error);
      setLoading(false);
    }
  };
  useEffect(() => {
    loadDocType();
  }, []);
  useEffect(() => {
    let interval;
    if (timer > 0) {
      interval = setInterval(() => {
        setTimer((prev) => prev - 1);
      }, 1000); // Decrease timer every second
    } else if (timer === 0) {
      clearInterval(interval); // Stop the timer when it reaches 0
    }

    return () => clearInterval(interval); // Cleanup interval on unmount
  }, [timer]);

  const resetTimer = () => {
    setTimer(5 * 60); // Reset to 5 minutes
  };

  // Format time to MM:SS
  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  };

  const openModal = (data) => {
    console.log("data download : ", data);
    setDownloadData(data);
    setIsOpen(true);
  };

  const closeModal2 = () => {
    setOtpCode("");
    setIsOpen(false);
  };
  //dokumen sellect
  const changeDocumentType = (e) => {
    // if (docTypeRef) {
    //   let id = e.target.value;

    //   let obj = docTypeRef.find((o) => o.doc_type_id === id);
    //   if (obj && obj.doc_type_id) {

    //   }
    // }
    setDocumentTypeId(e.target.value);
    // setDocumentTypeCode(obj.doc_type_code);
    // setDocumentTypeName(obj.doc_type_title);
  };

  const handleExcel = async (pilih) => {
    // Check if 'pilih' has 'doc_metadata_by_template' and it's an array with data
    if (
      !pilih?.doc_metadata_by_template ||
      pilih.doc_metadata_by_template.length === 0
    ) {
      console.error("doc_metadata_by_template is missing or empty", pilih);
      return; // exit if it's not an array or it's empty
    }

    // Access the first document object (assumed only one document for this example)
    const doc = pilih;

    // Log the entire doc_metadata_by_template to check its structure
    console.log("doc_metadata_by_template:", doc.doc_metadata_by_template);

    // Prepare headers for the Excel file
    const headers = [
      "No Sertifikat",
      "Desa / Kelurahan",
      "Kecamatan",
      "Kabupaten",
      "Provinsi",
      "Suratukur Tanggal",
      "Suratukur No",
      "Suratukur Luas",
      "Penerbitan Sertifikat",
    ];

    // Map metadata items to their column names
    const metadataMap = {
      shp_no_sertifikat: "No Sertifikat",
      shp_alamat_desa_kelurahan: "Desa / Kelurahan",
      shp_alamat_kecamatan: "Kecamatan",
      shp_alamat_kabupaten: "Kabupaten",
      shp_alamat_provinsi: "Provinsi",
      shp_suratukur_tanggal: "Suratukur Tanggal",
      shp_suratukur_no: "Suratukur No",
      shp_suratukur_luas: "Suratukur Luas",
      shp_penerbitan_sertifikat: "Penerbitan Sertifikat",
    };

    // Prepare sheet data
    const sheetData = [];

    // Loop through headers and fill in the corresponding metadata
    const rowData = headers.map((header) => {
      const metadataField = Object.keys(metadataMap).find(
        (key) => metadataMap[key] === header
      );
      const item = doc.doc_metadata_by_template.find(
        (item) => item.metadata_tmp_name === metadataField
      );
      return item ? item.metadata_tmp_value || "N/A" : "N/A";
    });

    // Push the row data to the sheet data array
    sheetData.push(rowData);

    // Create worksheet from the data
    const ws = XLSX.utils.aoa_to_sheet([headers, ...sheetData]);

    // Auto-adjust column widths based on the content
    const colWidths = [];
    headers.forEach((header, colIndex) => {
      // Find the longest value in the column (header + data)
      let maxLength = header.length;
      sheetData.forEach((row) => {
        if (row[colIndex] && row[colIndex].toString().length > maxLength) {
          maxLength = row[colIndex].toString().length;
        }
      });
      // Set column width (we add a bit of padding, e.g., 2 extra units)
      colWidths.push({ wpx: maxLength * 10 }); // wpx: width in pixels (multiply length by 10 for padding)
    });

    // Apply the calculated column widths
    ws["!cols"] = colWidths;

    // Create a new workbook and append the worksheet
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // Generate Excel file as an array (use 'array' type instead of 'blob')
    const excelData = XLSX.write(wb, { bookType: "xlsx", type: "array" });

    // Create a Blob from the array (this is the preferred way to handle Excel files)
    const excelBlob = new Blob([excelData], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });

    // Generate file name
    const doc_name = doc?.doc_polda_name || "Unknown";

    // Log and check if 'shp_no_sertifikat' exists in the metadata
    const metadataItems = doc?.doc_metadata_by_template.filter(
      (item) => item.metadata_tmp_name === "shp_no_sertifikat"
    );
    console.log(
      "Filtered metadata items for 'shp_no_sertifikat':",
      metadataItems
    );

    // Check if metadata was found
    const no_sertifikat =
      metadataItems.length > 0 &&
      metadataItems[0].metadata_tmp_value &&
      metadataItems[0].metadata_tmp_value !== "N/A"
        ? metadataItems[0].metadata_tmp_value
        : "Unknown";

    // Log the final file name and check if it's correct
    const fileName = `${doc_name}_${no_sertifikat}.xlsx`;
    console.log("Generated File Name:", fileName);

    // Trigger the file download
    saveAs(excelBlob, fileName);
  };

  const handleViewPdf = async (link) => {
    setModalIsOpen(true);
    setOriginalFile(link.doc_url?.original_file_path);
    setGetData(link);
  };

  const handleMaps = (link) => {
    console.log("link", link);
  
    // Check if doc_metadata_by_template exists
    if (link.doc_metadata_by_template) {
      if (link.doc_metadata_by_template_verification === null) {
        const provinsiData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_provinsi")?.metadata_tmp_value;
        const kabupatenData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_kabupaten")?.metadata_tmp_value;
        const kecamatanData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_kecamatan")?.metadata_tmp_value;
        const kelurahanData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_desa_kelurahan")?.metadata_tmp_value;
  
        const mapUrl = `https://www.google.com/maps?q=${provinsiData}+${kabupatenData}+${kecamatanData}+${kelurahanData}`;
        
        // Check if it's Electron
        if (window.electronAPI?.isElectron) {
          window.electronAPI.openBrowserWindow(mapUrl, "_blank");  // Open map in Electron window
        } else {
          window.open(mapUrl, "_blank");  // Open map in a new browser tab
        }
      } else {
        const provinsiData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_provinsi")?.metadata_tmp_value;
        const kabupatenData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_kabupaten")?.metadata_tmp_value;
        const kecamatanData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_kecamatan")?.metadata_tmp_value;
        const kelurahanData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_desa_kelurahan")?.metadata_tmp_value;
        
        const mapUrl = `https://www.google.com/maps?q=${provinsiData}+${kabupatenData}+${kecamatanData}+${kelurahanData}`;
        
        // Check if it's Electron
        if (window.electronAPI?.isElectron) {
          window.electronAPI.openBrowserWindow(mapUrl, "_blank");  // Open map in Electron window
        } else {
          window.open(mapUrl, "_blank");  // Open map in a new browser tab
        }
      }
    } else {
      console.log("sudah verifikasi");
      const defaultMapUrl = "https://www.google.com/maps?q=Indonesia";
      window.electronAPI.openBrowserWindow(defaultMapUrl, "_blank");  // Open default map in Electron window

      // Check if it's Electron
      if (window.electronAPI?.isElectron) {
        window.electronAPI.openBrowserWindow(defaultMapUrl, "_blank");  // Open default map in Electron window
      } else {
        window.open(defaultMapUrl, "_blank");  // Open default map in a new browser tab
      }
    }
  };

  const handleDownload = () => {
    let idmodal = document.getElementById("btn_open_download");
    idmodal.click();
  };

  const downloadPdf = (url) => {
    const pdfUrl = "https://example.com/path/to/your-file.pdf"; // URL PDF Anda
    const link = document.createElement("a");
    link.href = pdfUrl;
    link.download = "your-file.pdf"; // Nama file yang akan diunduh
    link.click(); // Simulasikan klik untuk mengunduh
  };

  const handleDownloadPdf = async (link) => {
    setDownloadUrl(null);
    const linkUrl = link.doc_url?.original_file_path;
    // setGetData(link);
    // if (linkUrl) {
    //     // setDownloadStarted(true);
    //     console.log("handleDownloadPdf", linkUrl);
    //     // downloadPdf(linkUrl);
    // }
    console.log("handleDownloadPdf", link);
    // const linkUrl = link.doc_url && link.doc_url.length > 0 ? link.doc_url[0].original_file_path : null;
    setDownloadDocName(link.doc_name);

    setGetData(link);
    console.log(linkUrl);
    if (linkUrl) {
      setDownloadUrl(linkUrl);
      setDownloadStarted(true);
    }
  };

  const addMark = () => {
    const markInstance = new Mark(document.querySelector("#bodyTable"));
    markInstance.unmark({
      done: () => {
        markInstance.mark(searchtext);
      },
    });
  };

  const searchByKeyword = async () => {
    console.log("documentTypeId : ", documentTypeId);
    if (searchtext == "") {
      Swal.fire("Error!", "Keyword Tidak Boleh Kosong!", "error");
      return;
    }
    setLoading(true);
    try {
      let search = searchtext;
      // let url =
      //   userData.myip +
      //   "/db/dmsbackend/@search_doc_byocrall_mod?keywords=" +
      //   search;
      let url =
        userData.myip +
        "/db/dmsbackend/@search_doc_byocrall?keywords=" +
        search;
      if (documentTypeId !== "all") {
        url = `${userData.myip}/db/dmsbackend/@search_doc_byocrall?keywords=${search}&img_doc_type_id=${documentTypeId}`;
        // url = `${userData.myip}/db/dmsbackend/@search_doc_byocrall_mod?keywords=${search}&img_doc_type_id=${documentTypeId}`;
      }

      //
      let get_data = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userData.token}`,
        },
      };
      const response = await fetch(url, get_data);
      if (response.status == 200) {
        const res = await response.json();
        setData(res);
        setLoading(false);
      } else {
        setIsProcess(false);
        setLoading(false);
      }
    } catch (err) {
      setIsProcess(false);
      setLoading(false);
    }
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };

  const columns = [
    {
      accessorKey: "No",
      header: () => <div style={{ textAlign: "center" }}>No</div>,
      enableColumnFilter: true,
      filterFn: "includesString",
      cell: (props) => {
        return (
          props?.table?.getSortedRowModel()?.flatRows?.indexOf(props?.row) + 1
        );
      },
    },
    {
      accessorKey: "doc_name",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("doc_name_td")}</div>
      ),
      enableColumnFilter: true,
      size: 120,
      cell: (info) => (
        <div
          style={{
            maxWidth: "200px",
            wordWrap: "break-word", // Allows text to break and wrap within the container
            overflow: "hidden",
            whiteSpace: "normal", // Enables text wrapping to a new line if needed
          }}
        >
          {info.getValue()}
        </div>
      ),
    },
    {
      accessorKey: "doc_created_date",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("create_date_um")}</div>
      ),
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
      enableColumnFilter: true,
    },
    {
      accessorKey: "doc_type_name",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("doc_type_name_dt")}</div>
      ),
      enableColumnFilter: true,
    },
    {
      accessorKey: "doc_polda_name",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("pemilik_um")}</div>
      ),
      enableColumnFilter: true,
    },
    {
      accessorKey: "word_searched",
      header: () => <div style={{ textAlign: "center" }}>{t("res_um")}</div>,
      enableColumnFilter: true,
      cell: ({ row }) => <WordSearchCell row={row} searchtext={searchtext} />,
    },
    {
      accessorKey: "doc_metadata_by_template_verification",
      header: () => <div style={{ textAlign: "center" }}>Hasil Verifikasi</div>,
      enableColumnFilter: true,
      cell: ({ getValue }) => {
        const data = getValue()|| [];
        
        // Function to format metadata_tmp_name
        const formatName = (name) => {
          return name
            .replace(/shp_/gi, "") // Remove "shp" (case insensitive)
            .replace(/shp/gi, "") // Remove "shp" (case insensitive)
            .replace(/shm_/gi, "") // Remove "shp" (case insensitive)
            .replace(/se_/gi, "") // Remove "shp" (case insensitive)
            .replace(/letterc_/gi, "") // Remove "shp" (case insensitive)
            .replace(/eigendom_/gi, "") // Remove "shp" (case insensitive)
            .replace(/shgu_/gi, "") // Remove "shp" (case insensitive)
            .replace(/shgb_/gi, "") // Remove "shp" (case insensitive)
            .replace(/_/g, " ") // Replace underscores with spaces
            .replace(/\b\w/g, (char) => char.toUpperCase()); // Capitalize first letter of each word
        };
    
        return (
          <div>
            {data.map((item, index) => (
              <div key={index}>
                <strong>{formatName(item.metadata_tmp_name)} = </strong> {item.metadata_tmp_value}
              </div>
            ))}
          </div>
        );
      },
    },
    {
      id: "actions",
      accessorKey: "doc_id",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("actions_um")}</div>
      ),
      cell: function render({ row }) {
        return (
          <div style={{ display: "flex", gap: "10px" }}>
            {/* <button className="btn btn-sm" title="Lihat" onClick={() => handleExcel(row.original)}>
                            <img src={excel} width={"38px"} />
                        </button> */}
            <button
              className="btn btn-sm"
              title="Lihat"
              onClick={() => handleViewPdf(row.original)}
            >
              <img src={lihat} width={"38px"} />
            </button>
            {/* <button className="btn btn-sm" title="Download" data-bs-toggle="modal" data-bs-target="#modalOTP" onClick={() => handleDownloadPdf(row.original)}>
                            <img src={Down} width={"40px"} />
                        </button> */}
            <button
              className="btn btn-sm"
              title="Download"
              onClick={() => openModal(row.original)}
            >
              <img src={Down} width={"40px"} />
            </button>
            {/* <button
              className="btn btn-sm"
              title="Maps"
              onClick={() => handleMaps(row.original)}
            >
              <img src={map} width={"45px"} />
            </button> */}
          </div>
        );
      },
    },
  ];
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        const id = Math.floor(Math.random() * 10000);
        const newRow = {
          id,
          studentNumber: id,
          name: "",
          dateOfBirth: "",
          major: "",
        };
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  });
  const showMode = () => {
    const isLatest = /latest/.test(Mode.version);

    if (isLatest) {
      console.log('Latest version detected');
    } else {
      console.log('Not the latest version');
      return (
        <h4 style={{ background: "#5CFF5C", color: "red", fontWeight: "bold" }}>
          DEVELOPMENT MODE
        </h4>
      )
    }
  }
  return (
    <>
      {loading ? <LoadSpinner /> : ""}

      <div
        class="container-fluid pt-3 "
        style={{
          minHeight: "84vh",
          marginTop: "65px",
        }}
      >
        <button
          id="btn_open_download"
          style={{ display: "none" }}
          type="button"
          class="btn btn-warning col-2 mx-1"
          data-bs-toggle="modal"
          data-bs-target="#ModalPdfView"
        ></button>
        <ModalOtp readyToDownload={handleDownload} />
        {downLoadUrl && downLoadUrl !== "" ? (
          <ModalPdfView
            docName={downloadDocName}
            link={downLoadUrl}
            isDownload={true}
          ></ModalPdfView>
        ) : null}
        <div class="row">
          <div class="col">
            <div class="card my-4">
              <h5 class="card-title mb-3 px-3 pt-3 textfamily">
                {t("titlle_ts")}
              </h5>
              {showMode()}
              <div class="card-header">
                <div className="mt-3">
                  <div class="row mb-3">
                    <div class="col-6">
                      <div class="input-group">
                        <input
                          type="text"
                          className="form-control w-50 "
                          id="searchInput"
                          placeholder="Kata Kunci"
                          onChange={(event) =>
                            setSearchtext(event.target.value)
                          }
                          value={searchtext}
                        />

                        <select
                          id="docType"
                          value={documentTypeId}
                          class="form-select "
                          aria-label="Default select example"
                          onChange={changeDocumentType}
                        >
                          {docTypeOptionsHtml}
                        </select>
                      </div>
                    </div>
                    <div class="col-2">
                      {/* <button className="btn btn-primary"  onClick={cekHmac}>cekHmac</button> */}

                      <button
                        className="btn btn-primary"
                        color="info"
                        onClick={searchByKeyword}
                      >
                        {!isProcess ? (
                          <div>
                            <i className="bx bx-play-circle align-bottom me-1"></i>{" "}
                            {t("process_um")}
                          </div>
                        ) : (
                          <span className="d-flex align-items-center">
                            <div
                              class="spinner-border flex-shrink-0"
                              role="status"
                              size="sm"
                            >
                              {" "}
                            </div>
                            <span className="flex-grow-1 ms-2">
                              {t("loading_um")}...
                            </span>
                          </span>
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="live-preview">
                  <div class="row mt-2">
                    <table className="table table-bordered table-striped table-hover">
                      <thead className="table-primary">
                        {table.getHeaderGroups().map((headerGroup) => (
                          <tr key={headerGroup.id} className="  uppercase">
                            {headerGroup.headers.map((header) => (
                              <th
                                key={header.id}
                                className="px-4 pr-2 py-3 font-medium text-left"
                              >
                                {header.isPlaceholder
                                  ? null
                                  : flexRender(
                                      header.column.columnDef.header,
                                      header.getContext()
                                    )}
                              </th>
                            ))}
                          </tr>
                        ))}
                      </thead>
                      {data === undefined || data.length == 0 ? (
                        <tbody>
                          <tr>
                            <td
                              colSpan="7"
                              style={{ textAlign: "center", margin: "0 auto" }}
                            >
                              <span
                                style={{
                                  color: "red",
                                  fontWeight: "bold",
                                  fontSize: "16px",
                                }}
                              >
                                <IpNotValidPage />
                              </span>
                            </td>
                          </tr>
                        </tbody>
                      ) : (
                        <tbody>
                          {table.getRowModel().rows.map((row) => (
                            <tr
                              className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50"
                              key={row.id}
                            >
                              {row.getVisibleCells().map((cell) => (
                                <td className="px-4 py-2" key={cell.id}>
                                  {flexRender(
                                    cell.column.columnDef.cell,
                                    cell.getContext()
                                  )}
                                </td>
                              ))}
                            </tr>
                          ))}
                        </tbody>
                      )}
                    </table>
                    <Paginations table={table} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {modalIsOpen && (
          <Modal2
            getData={getData}
            closeModal={closeModal}
            link={originalFile}
          />
        )}
        <DownloadPdf
          getData={getData}
          startDownload={downloadStarted}
          setDownloadStarted={setDownloadStarted}
        />

        {isOpen && (
          <div
            class="modal show"
            tabIndex="-1"
            role="dialog"
            style={{ display: "inline-block", width: "100%" }}
          >
            <div class="modal-dialog modal-xs ">
              <div class="modal-content" style={{ minHeight: "300px" }}>
                <div class="modal-header justify-content-center align-items-center">
                  <h5 class="modal-title ">Verifikasi OTP</h5>
                </div>
                <div class="modal-body">
                  <div class="d-flex flex-column justify-content-center align-items-center mt-4 mb-2">
                    {timer <= 0 ? (
                      <button
                        className="btn btn-warning mx-2"
                        onClick={() => {
                          sendRequestOTP();
                        }}
                      >
                        Kirim Permintaan OTP ke Email {userData.myemail}
                      </button>
                    ) : (
                      <button
                        className="btn btn-warning mx-2 mb-2 pt-2 gap-5"
                        disabled
                      >
                        {" "}
                        Kirim ulang OTP dalam {formatTime(timer)}
                      </button>
                    )}
                    <input
                      value={otpCode}
                      type="text"
                      placeholder="Kode OTP"
                      class="form-control w-25 mt-2 mb-4"
                      maxLength="6"
                      onChange={onEnterOTPCode}
                    />
                  </div>
                </div>
                <div className="modal-footer justify-content-center align-items-center">
                  <button
                    type="button"
                    className="btn btn-primary"
                    data-bs-dismiss="modal"
                    disabled={otpCode.length !== 6}
                    onClick={() => {
                      sendValidateOTP();
                    }}
                  >
                    Konfirmasi Kode OTP
                  </button>
                  <button
                    type="button"
                    class="btn   btn-danger"
                    onClick={closeModal2}
                  >
                    Batal
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default Searching;
