import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import Mark from "mark.js";
import {
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import Paginations from "../../components/Table/pagination";
import IpNotValidPage from "../../components/Page/IpNotValid";
import Modal2 from "../../components/Modal2";
import DownloadPdf from "./Download";
import Down from "../../assets/images/icon/icon_down.png";
import lihat from "../../assets/images/icon/icon_viewer.png";
import srch from "../../assets/images/icon/search.png";
import add from "../../assets/images/icon/add.png";
import clr from "../../assets/images/icon/clear.png";
import excel from "../../assets/images/icon/icon_excel.png";
import map from "../../assets/images/icon/icon_maps.png";
import { useTranslation } from "react-i18next";
import LoadSpinner from "../../components/Load/load";
import LocalStorageHelper from "../../api/localStorageHelper";
import moment from "moment";
import "moment/locale/id";
import WordSearchCell from "./WordSearchCell";
import ExcelJS from "exceljs";

import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import DummySearch from '../../assets/dummy/search.json';  // Import your JSON data

function s2ab(str) {
  const buf = new ArrayBuffer(str.length);
  const view = new Uint8Array(buf);
  for (let i = 0; i < str.length; i++) {
    view[i] = str.charCodeAt(i) & 0xFF;
  }
  return buf;
}

function Searching() {
  const userData = LocalStorageHelper.getUserData();
  let base_url = process.env.REACT_APP_API_URL;
  let poldaID = "PD1";


  const [filteredParagraphs, setFilteredParagraphs] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchtext, setSearchtext] = useState("");
  const [isProcess, setIsProcess] = useState(false);
  const [data, setData] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [getData, setGetData] = useState([]);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [originalFile, setOriginalFile] = useState("");
  const [downloadStarted, setDownloadStarted] = useState(false);
  const { t } = useTranslation();


   
  const handleExcel = async (pilih) => {
    // Check if 'pilih' has 'doc_metadata_by_template' and it's an array with data
    if (!pilih?.doc_metadata_by_template || pilih.doc_metadata_by_template.length === 0) {
      console.error("doc_metadata_by_template is missing or empty", pilih);
      return; // exit if it's not an array or it's empty
    }
  
    // Access the first document object (assumed only one document for this example)
    const doc = pilih;
  
    // Log the entire doc_metadata_by_template to check its structure
   
    // Prepare headers for the Excel file
    const headers = [
      'No Sertifikat', 'Desa / Kelurahan', 'Kecamatan', 'Kabupaten', 'Provinsi',
      'Suratukur Tanggal', 'Suratukur No', 'Suratukur Luas', 'Penerbitan Sertifikat'
    ];
  
    // Map metadata items to their column names
    const metadataMap = {
      'shp_no_sertifikat': 'No Sertifikat',
      'shp_alamat_desa_kelurahan': 'Desa / Kelurahan',
      'shp_alamat_kecamatan': 'Kecamatan',
      'shp_alamat_kabupaten': 'Kabupaten',
      'shp_alamat_provinsi': 'Provinsi',
      'shp_suratukur_tanggal': 'Suratukur Tanggal',
      'shp_suratukur_no': 'Suratukur No',
      'shp_suratukur_luas': 'Suratukur Luas',
      'shp_penerbitan_sertifikat': 'Penerbitan Sertifikat'
    };
  
    // Prepare sheet data
    const sheetData = [];
  
    // Loop through headers and fill in the corresponding metadata
    const rowData = headers.map(header => {
      const metadataField = Object.keys(metadataMap).find(key => metadataMap[key] === header);
      const item = doc.doc_metadata_by_template.find(item => item.metadata_tmp_name === metadataField);
      return item ? item.metadata_tmp_value || 'N/A' : 'N/A';
    });
  
    // Push the row data to the sheet data array
    sheetData.push(rowData);
  
    // Create worksheet from the data
    const ws = XLSX.utils.aoa_to_sheet([headers, ...sheetData]);
  
    // Auto-adjust column widths based on the content
    const colWidths = [];
    headers.forEach((header, colIndex) => {
      // Find the longest value in the column (header + data)
      let maxLength = header.length;
      sheetData.forEach(row => {
        if (row[colIndex] && row[colIndex].toString().length > maxLength) {
          maxLength = row[colIndex].toString().length;
        }
      });
      // Set column width (we add a bit of padding, e.g., 2 extra units)
      colWidths.push({ wpx: maxLength * 10 }); // wpx: width in pixels (multiply length by 10 for padding)
    });
  
    // Apply the calculated column widths
    ws['!cols'] = colWidths;
  
    // Create a new workbook and append the worksheet
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  
    // Generate Excel file as an array (use 'array' type instead of 'blob')
    const excelData = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
  
    // Create a Blob from the array (this is the preferred way to handle Excel files)
    const excelBlob = new Blob([excelData], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
  
    // Generate file name
    const doc_name = doc?.doc_polda_name || 'Unknown';
  
    // Log and check if 'shp_no_sertifikat' exists in the metadata
    const metadataItems = doc?.doc_metadata_by_template.filter(item => item.metadata_tmp_name === 'shp_no_sertifikat');
   
    // Check if metadata was found
    const no_sertifikat = metadataItems.length > 0 && metadataItems[0].metadata_tmp_value && metadataItems[0].metadata_tmp_value !== 'N/A'
      ? metadataItems[0].metadata_tmp_value
      : 'Unknown';
  
    // Log the final file name and check if it's correct
    const fileName = `${doc_name}_${no_sertifikat}.xlsx`;
  
  
    // Trigger the file download
    saveAs(excelBlob, fileName);
  };
  
  const handleViewPdf = async (link) => {
    setModalIsOpen(true);
    setOriginalFile(link.doc_url?.original_file_path);
    setGetData(link);
  };

  const handleMaps = (link) => {
    console.log("link", link);
  
    // Check if doc_metadata_by_template exists
    if (link.doc_metadata_by_template) {
      if (link.doc_metadata_by_template_verification === null) {
        const provinsiData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_provinsi")?.metadata_tmp_value;
        const kabupatenData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_kabupaten")?.metadata_tmp_value;
        const kecamatanData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_kecamatan")?.metadata_tmp_value;
        const kelurahanData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_desa_kelurahan")?.metadata_tmp_value;
  
        const mapUrl = `https://www.google.com/maps?q=${provinsiData}+${kabupatenData}+${kecamatanData}+${kelurahanData}`;
        
        // Check if it's Electron
        if (window.electronAPI?.isElectron) {
          window.electronAPI.openBrowserWindow(mapUrl, "_blank");  // Open map in Electron window
        } else {
          window.open(mapUrl, "_blank");  // Open map in a new browser tab
        }
      } else {
        const provinsiData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_provinsi")?.metadata_tmp_value;
        const kabupatenData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_kabupaten")?.metadata_tmp_value;
        const kecamatanData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_kecamatan")?.metadata_tmp_value;
        const kelurahanData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_desa_kelurahan")?.metadata_tmp_value;
        
        const mapUrl = `https://www.google.com/maps?q=${provinsiData}+${kabupatenData}+${kecamatanData}+${kelurahanData}`;
        
        // Check if it's Electron
        if (window.electronAPI?.isElectron) {
          window.electronAPI.openBrowserWindow(mapUrl, "_blank");  // Open map in Electron window
        } else {
          window.open(mapUrl, "_blank");  // Open map in a new browser tab
        }
      }
    } else {
      console.log("sudah verifikasi");
      const defaultMapUrl = "https://www.google.com/maps?q=Indonesia";
      
      // Check if it's Electron
      if (window.electronAPI?.isElectron) {
        window.electronAPI.openBrowserWindow(defaultMapUrl, "_blank");  // Open default map in Electron window
      } else {
        window.open(defaultMapUrl, "_blank");  // Open default map in a new browser tab
      }
    }
  };
  const handleDownloadPdf = async (link) => {
    const linkUrl = link.doc_url?.original_file_path;
    setGetData(link);
    if (linkUrl) {
      setDownloadStarted(true);
    }
  };

  const addMark = () => {
    const markInstance = new Mark(document.querySelector("#bodyTable"));
    markInstance.unmark({
      done: () => {
        markInstance.mark(searchtext);
      },
    });
  };

  const searchByKeyword = async () => {
    setLoading(true);
    try {
      let search = searchtext;
      let url =
        userData.myip +
        "/db/dmsbackend/@search_doc_byocrall?keywords=" +
        search;
      let get_data = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userData.token}`,
        },
      };
      const response = await fetch(url, get_data);
      if (response.status == 200) {
        const res = await response.json();
        // setData(res);
        console.log("res : ",res)
        FilterParagraf(res)
        setLoading(false);
      } else {
        setIsProcess(false);
        setLoading(false);
      }
    } catch (err) {
      setIsProcess(false);
      setLoading(false);
    }
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };
   // Function to extract a sentence around the keyword
  const extractSentence = (text, keyword) => {
    const words = text.split(/\s+/);
    const keywordIndex = words.findIndex(word => word.toLowerCase() === keyword.toLowerCase());

    if (keywordIndex === -1) {
      return 'Keyword not found.';
    }

    // Get 5 words before and after the keyword
    const start = Math.max(0, keywordIndex - 5);
    const end = Math.min(words.length, keywordIndex + 6); // 5 words after + keyword itself

    return words.slice(start, end).join(' ');
  };
  const FilterParagraf = (paragraphs)=>{
    console.loh("searchtext : ",searchtext)
    console.log("paragraphs : ",paragraphs)
    
    const filtered = paragraphs.filter(paragraph => 
      paragraph.text.toLowerCase().includes(searchtext.toLowerCase())
    );
    console.log("filtered : ",filtered)
    // Now, extract sentences for filtered paragraphs
    const updatedFilteredParagraphs = filtered.map(paragraph => {
      const filteredText = extractSentence(paragraph.text, searchtext);
      return { ...paragraph, filteredText };
    });
    console.log("updatedFilteredParagraphs :" ,updatedFilteredParagraphs)
    // Update state with filtered paragraphs
    setData(updatedFilteredParagraphs);

  }
  const columns = [
    {
      accessorKey: "No",
      header: () => <div style={{ textAlign: "center" }}>No</div>,
      enableColumnFilter: true,
      filterFn: "includesString",
      cell: (props) => {
        return (
          props?.table?.getSortedRowModel()?.flatRows?.indexOf(props?.row) + 1
        );
      },
    },
    {
      accessorKey: "doc_name",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("doc_name_td")}</div>
      ),
      enableColumnFilter: true,
      size: 120,
      cell: (info) => (
        <div
          style={{
            maxWidth: "200px",
            wordWrap: "break-word", // Allows text to break and wrap within the container
            overflow: "hidden",
            whiteSpace: "normal", // Enables text wrapping to a new line if needed
          }}
        >
          {info.getValue()}
        </div>
      ),
    },
    {
      accessorKey: "doc_created_date",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("create_date_um")}</div>
      ),
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
      enableColumnFilter: true,
    },
    {
      accessorKey: "doc_type_name",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("doc_type_name_dt")}</div>
      ),
      enableColumnFilter: true,
    },
    {
      accessorKey: "doc_polda_name",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("pemilik_um")}</div>
      ),
      enableColumnFilter: true,
    },
    {
      accessorKey: "word_searched",
      header: () => <div style={{ textAlign: "center" }}>{t("res_um")}</div>,
      enableColumnFilter: true,
      cell: ({ row }) => <WordSearchCell row={row} searchtext={searchtext} />,
    },

    {
      id: "actions",
      accessorKey: "doc_id",
      header: () => (
        <div style={{ textAlign: "center" }}>{t("actions_um")}</div>
      ),
      cell: function render({ row }) {
        return (
          <div style={{ display: "flex", gap: "10px" }}>
            <button
              className="btn btn-sm"
              title="Lihat"
              onClick={() => handleExcel(row.original)}
            >
              <img src={excel} width={"38px"} />
            </button>
            <button
              className="btn btn-sm"
              title="Lihat"
              onClick={() => handleViewPdf(row.original)}
            >
              <img src={lihat} width={"38px"} />
            </button>
            <button
              className="btn btn-sm"
              title="Download"
              onClick={() => handleDownloadPdf(row.original)}
            >
              <img src={Down} width={"40px"} />
            </button>
            <button
              className="btn btn-sm"
              title="Maps"
              onClick={() => handleMaps(row.original)}
            >
              <img src={map} width={"45px"} />
            </button>
          </div>
        );
      },
    },
  ];
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        const id = Math.floor(Math.random() * 10000);
        const newRow = {
          id,
          studentNumber: id,
          name: "",
          dateOfBirth: "",
          major: "",
        };
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  });

  return (
    <>
      {loading ? <LoadSpinner /> : ""}

      <div
        class="container-fluid pt-3 "
        style={{
          minHeight: "84vh",
          marginTop: "65px",
        }}
      >
        <div class="row">
          <div class="col">
            <div class="card my-4">
              <h5 class="card-title mb-3 px-3 pt-3 textfamily">
                {t("titlle_ts")}
              </h5>
              <div class="card-header">
                <div className="mt-3">
                  <div class="row">
                    <div class="col-10">
                      <input
                        type="text"
                        className="form-control "
                        id="searchInput"
                        placeholder="Kata Kunci"
                        onChange={(event) => setSearchtext(event.target.value)}
                        value={searchtext}
                      />
                    </div>
                    <div class="col-2">
                      <button
                        className="btn btn-primary"
                        color="info"
                        onClick={searchByKeyword}
                      >
                        {!isProcess ? (
                          <div>
                            <i className="bx bx-play-circle align-bottom me-1"></i>{" "}
                            {t("process_um")}
                          </div>
                        ) : (
                          <span className="d-flex align-items-center">
                            <div
                              class="spinner-border flex-shrink-0"
                              role="status"
                              size="sm"
                            >
                              {" "}
                            </div>
                            <span className="flex-grow-1 ms-2">
                              {t("loading_um")}...
                            </span>
                          </span>
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="live-preview">
                  <div class="row mt-2">
                    <table className="table table-bordered table-striped table-hover">
                      <thead className="table-primary">
                        {table.getHeaderGroups().map((headerGroup) => (
                          <tr key={headerGroup.id} className="  uppercase">
                            {headerGroup.headers.map((header) => (
                              <th
                                key={header.id}
                                className="px-4 pr-2 py-3 font-medium text-left"
                              >
                                {header.isPlaceholder
                                  ? null
                                  : flexRender(
                                      header.column.columnDef.header,
                                      header.getContext()
                                    )}
                              </th>
                            ))}
                          </tr>
                        ))}
                      </thead>
                      {data === undefined || data.length == 0 ? (
                        <tbody>
                          <tr>
                            <td
                              colSpan="7"
                              style={{ textAlign: "center", margin: "0 auto" }}
                            >
                              <span
                                style={{
                                  color: "red",
                                  fontWeight: "bold",
                                  fontSize: "16px",
                                }}
                              >
                                <IpNotValidPage />
                              </span>
                            </td>
                          </tr>
                        </tbody>
                      ) : (
                        <tbody>
                          {table.getRowModel().rows.map((row) => (
                            <tr
                              className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50"
                              key={row.id}
                            >
                              {row.getVisibleCells().map((cell) => (
                                <td className="px-4 py-2" key={cell.id}>
                                  {flexRender(
                                    cell.column.columnDef.cell,
                                    cell.getContext()
                                  )}
                                </td>
                              ))}
                            </tr>
                          ))}
                        </tbody>
                      )}
                    </table>
                    <Paginations table={table} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {modalIsOpen && (
          <Modal2
            getData={getData}
            closeModal={closeModal}
            link={originalFile}
          />
        )}
        <DownloadPdf
          getData={getData}
          startDownload={downloadStarted}
          setDownloadStarted={setDownloadStarted}
        />
      </div>
    </>
  );
}

export default Searching;
