import React, { useState, useEffect } from 'react';

function MapsLoc() {
  const [results, setResults] = useState(null);
   useEffect(() => {
    if (window.electronAPI && window.electronAPI.getSearchResult) {
      const data = window.electronAPI.getSearchResult();
      setResults(data);
    } else {
     }
  }, []);

  return (
    <div className="container-xxl pt-3" style={{ minHeight: "84vh", marginTop: "65px" }}>
      {results ? <pre>{JSON.stringify(results, null, 2)}</pre> : 'Loading...'}
      Maps
    </div>
  );
}

export default MapsLoc;
