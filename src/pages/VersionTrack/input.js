import React, { useState, useEffect } from "react";
import LoadSpinner from "../../components/Load/load";
import SignIn from "../SignIn";
import LocalStorageHelper from "../../api/localStorageHelper";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import Filters from "../../components/Table/filters";
import Paginations from "../../components/Table/pagination";
import moment from "moment";
import "moment/locale/id";
import { useTranslation } from "react-i18next";
import ModalInput from "../../components/Modal/modalInput";
import ModalHapus from "../../components/Modal/modalHapus";

function InputPC(props) {
  const {handleClose} =props
  // const [macAddresses, setMacAddresses] = useState([]);
  // const [macList, setMacList] = useState([]);
  const userData = LocalStorageHelper.getUserData();
  const [data, setData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [systemInfo, setSystemInfo] = useState(null);
  const [action, setAction] = useState("");
  const { t } = useTranslation();

  let username = localStorage.getItem("user");
  const VERSION_TRACKING_URL = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking';
  const POLDA_SATKER = 'PD1'; // Replace dynamically if needed


  useEffect(() => {
    getListMac()
  }, []);
  const getListMac = async () => {
    let url = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking/@list_versiontrack';
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${userData.token}`);

    const requestOptions = {
      method: "GET",
      headers: myHeaders,
    };
    var res = await fetch(url, requestOptions);
    var datas = await res.json();
    // setMacList(datas);
    setData(datas)

  }


  const sendVersionTrackingData = async () => {
    try {
      // Retrieve system info from Electron
      const systemInfo = await window.electronAPI.getSystemInfo();
      const userData = LocalStorageHelper.getUserData();

      if (!userData || !userData.token) {
        throw new Error('User token is missing.');
      }

      const payload = {
        "@type": "VersionTrack",
        versiontrack_polda_satker: POLDA_SATKER,
        versiontrack_macaddress: systemInfo.macAddress,
        versiontrack_serial_number: systemInfo.serialNumber, // Replace dynamically if needed
        versiontrack_current_version: systemInfo.appVersion,
        versiontrack_current_version_release_date: systemInfo.lastOpened, // Replace dynamically
        versiontrack_update_version: systemInfo.typeConnection, // Replace dynamically
        versiontrack_update_version_release_date: systemInfo.lastOpened, // Replace dynamically
        versiontrack_platform: systemInfo.platform,
        versiontrack_notification_update: false, // Replace dynamically
      };

      console.log('Payload:', payload);

      const response = await fetch(VERSION_TRACKING_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userData.token}`,
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error(`Failed to send data. Status: ${response.status}`);
      }

      const responseData = await response.json();
      console.log('Response from server:', responseData);
      return responseData;
    } catch (error) {
      console.error('Error while sending version tracking data:', error);
      throw error;
    }
  };

  useEffect(() => {
    const fetchSystemInfo = async () => {
      try {
        const info = await window.electronAPI.getSystemInfo();
        console.log('System Info:', info);
        setSystemInfo(info);
      } catch (error) {
        console.error('Error fetching system info:', error);
      }
    };

    fetchSystemInfo();
  }, []);

  const [activeNetwork, setActiveNetwork] = useState(null);
  console.log("data", data)
  console.log("systemInfo", systemInfo)
  // console.log("networkInterfaces",networkInterfaces)
  console.log("activeNetwork", activeNetwork)



  useEffect(() => {
    const fetchActiveNetwork = async () => {
      try {
        const network = await window.electronAPI.getActiveNetworkInterface();
        console.log(activeNetwork); // Active network details
        setActiveNetwork(network);

      } catch (error) {
        console.error('Error fetching network interface:', error);
      }
    };

    fetchActiveNetwork();
  }, []);

  const columns = [
    {
      accessorKey: "versiontrack_notification_update",
      header: "Username",
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_polda_satker",
      header: "Polda Satker",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_macaddress",
      header: "MacAddress",
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_update_version",
      header: "Type Connection",
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_serial_number",
      header: "Serial Number",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },

    {
      accessorKey: "versiontrack_current_version",
      header: "App Version",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_current_version_release_date",
      header: "Release Date",
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_platform",
      header: "Platform",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_update_version_release_date",
      header: "Terkhir Buka Apilikasi",
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY HH:mm:ss"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_platform",
      header: "Status",
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY HH:mm:ss"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },
  ];
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  });
  return (
    <div
    >
 Input PC
    </div>
  );
}

export default InputPC;
