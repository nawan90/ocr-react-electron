import React, { useState, useEffect } from "react";
import LocalStorageHelper from "../../api/localStorageHelper";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import Filters from "../../components/Table/filters";
import Paginations from "../../components/Table/pagination";
import moment from "moment";
import "moment/locale/id";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { useNavigate, useLocation } from "react-router-dom";
 
function VersionTrack() {
  const userData = LocalStorageHelper.getUserData();
  const [data, setData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [systemInfo, setSystemInfo] = useState(null);
  const [action, setAction] = useState("");
  const [activeNetwork, setActiveNetwork] = useState(null);
  const navigate = useNavigate();
  const [lisensi, setLisensi] = useState(() => {
    // Initialize from localStorage
    const storedValue = localStorage.getItem("lisensi");
    return storedValue === "true"; // Convert string to boolean
  });
  const { t } = useTranslation();

  let username = localStorage.getItem("user");

  const [status, setStatus] = useState(() =>
    data.reduce((acc, item) => {
      acc[item.id] = item.versiontrack_license_status ?? false; // Default to `false` if undefined
      return acc;
    }, {})
  );

  useEffect(() => {
    getListMac()
  }, []);
  const getListMac = async () => {
    let url = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking/@list_versiontrack';
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${userData.token}`);

    const requestOptions = {
      method: "GET",
      headers: myHeaders,
    };
    var res = await fetch(url, requestOptions);
    var datas = await res.json();
    // setMacList(datas);
    setData(datas)

  }

  const handleCheckboxChange = async (rowId, row, event) => {
    const isChecked = event.target.checked; // Get the checkbox state
    console.log("rowData", row.original); // Access row data using row.original
    const url_patch = row.original['@id']
    console.log("url_patch", url_patch);
    // Update the local state for the checkbox
    // setStatus((prevStatus) => ({
    //   ...prevStatus,
    //   [rowId]: isChecked,
    // }));

    const payload = {
      "@type": "VersionTrack",
      versiontrack_macaddress: row.original.versiontrack_macaddress, // Use `row.original` for row data
      versiontrack_license_status: isChecked, // Update based on checkbox state
    };
    try {
      const response = await fetch(url_patch, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userData.token}`,
        },
        body: JSON.stringify(payload),
      });

      if (response.status == 200 || response.status == 204) {
        getListMac()

      }
      else {
        console.error("Error with API call:", response.statusText);
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  console.log("data", data)
  const handleDelete = async (rowId, rowData) => {
    console.log("rowId", rowId)
    console.log("rowData", rowData)
    console.log("rowData", rowData['@id'])

    const url_delete = rowData['@id']
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userData.token}`,
      },
    };
    await axios
      .delete(url_delete, config)
      .then((response) => {
        // Swal.fire("Info", ` Telah dihapus`, "success");
        getListMac()
      })
      .catch((error) => {
        console.error("Error deleting resource:", error.response ? error.response.data : error.message);
      });
  }

  useEffect(() => {
    const initialStatus = data.reduce((acc, item) => {
      acc[item.id] = item.versiontrack_license_status ?? false;
      return acc;
    }, {});
    setStatus(initialStatus);
  }, [data]);


  useEffect(() => {
    const fetchSystemInfo = async () => {
      try {
        const info = await window.electronAPI.getSystemInfo();
        console.log('System Info:', info);
        setSystemInfo(info);
      } catch (error) {
        console.error('Error fetching system info:', error);
      }
    };

    fetchSystemInfo();
  }, []);
  useEffect(() => {
    const fetchActiveNetwork = async () => {
      try {
        const network = await window.electronAPI.getActiveNetworkInterface();
        console.log(activeNetwork); // Active network details
        setActiveNetwork(network);

      } catch (error) {
        console.error('Error fetching network interface:', error);
      }
    };

    fetchActiveNetwork();
  }, []);


  const columns = [
    {
      accessorKey: "versiontrack_polda_satker",
      header: "Polda Satker",
      enableColumnFilter: true,
      filterFn: "includesString" 
    },
    {
      accessorKey: "versiontrack_macaddress",
      header: "MacAddress",
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_update_version",
      header:  t("tipe_um")+" "+t("con_ver_track"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_serial_number",
      header: t("no_seri_ver_track"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },

    {
      accessorKey: "versiontrack_current_version",
      header: t("versi_aplikasi_ver_track"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_current_version_release_date",
      header: t("tanggal_rilis_ver_track"),
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_platform",
      header: "Platform",
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "versiontrack_license_status",
      header: t("lisensi_ver_track"),
      cell: ({ row }) => {
        const rowData = row.original; // Access row data
        const rowId = rowData.id; // Ensure each row has a unique ID
        const isChecked = rowData.versiontrack_license_status ?? false; // Use data directly
        setLisensi(isChecked);
        localStorage.setItem("lisensi", isChecked);

        return (
          <div className="form-check form-switch">
            <input
              className="form-check-input"
              type="checkbox"
              id={`switch-${rowId}`}
              checked={isChecked}
              onChange={(event) => handleCheckboxChange(rowId, row, event)}
            />
            <label className="form-check-label" htmlFor={`switch-${rowId}`}>
              {isChecked ? t("active_track") : t("inactive_track")}
            </label>
          </div>
        );
      },
    },
    {
      accessorKey: "",
      header: t("actions_um"),
      cell: ({ row }) => {
        const rowData = row.original; // Access row data
        const rowId = rowData.id; // Ensure each row has a unique ID
        return (
          <button onClick={() => handleDelete(rowId, rowData)}>{t("hapus_um")}</button>
        );
      },
    },
    {
      header: t("detail_um"),
      id: "expand",
      cell: ({ row }) => (
        <button   onClick={() => row.toggleExpanded()}>
          {row.getIsExpanded() ? "⬆️" : "⬇️"}
        </button>
      ),
    },
  ];

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
      },
      removeSelectedRows: (selectedRows) => {
      },
    },
  });

  return (
    <div
      class="container-fluid pt-3 "
      style={{
        minHeight: "84vh",
        marginTop: "65px",
        background: "white"
      }}
    >
        
      <div class="row mt-3 mb-2">
        <div class="col-10">
          <Filters filtering={filtering} setFiltering={setFiltering} />
        </div>
        <div class="col-2">
          <button
            class="btn btn-primary float-end"
            type="button"
            data-bs-toggle="modal"
            data-bs-target="#ModalInput"
            onClick={() => {
              setAction("add");
              // setOnSelected(!onSelected);
            }}
          >
            {" "}
            ✙ {t("pengguna_t1")}
          </button>
        </div>
      </div>
      <div class="row">
      <div
              class="card  mb-3"
              style={{ maxwidth: "18rem" }}
            >
      <table className="multi-line-table  table table-bordered table-striped table-hover">
        <thead className="table-primary">
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id} style={{ textAlign: "center", margin: "0 auto" }}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(header.column.columnDef.header, header.getContext())}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map((row) => (
            <React.Fragment key={row.id}>
              {/* Multi-line Main Row */}
              <tr className="multi-line-row">
                {row.getVisibleCells().map((cell) => (
                  <td key={cell.id} >
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
              </tr>
              {/* Expanded Sub-table */}
              {row.getIsExpanded() && (
                <tr className="sub-table">
                  <td colSpan={columns.length}>
                    <table className=" table-bordered table-striped  ">
                      <thead>
                        <tr >
                          <th style={{padding:"3px"}}>{t("id_pengguna_ver_track")}</th>
                          <th style={{padding:"3px"}}>{t("tanggal_masuk_ver_track")}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {row.original.versiontrack_user_id.map((user, index) => (
                          <tr key={index} >
                            <td style={{padding:"3px"}}>{user}</td>
                            <td style={{padding:"3px"}}>{moment(row.original.versiontrack_login_date[index]).format("DD MMMM YYYY, h:mm:ss a")}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </td>
                </tr>
              )}
            </React.Fragment>
          ))}
        </tbody>
      </table>
      </div>
      <Paginations table={table} />
      </div>
    </div>
  );
}

export default VersionTrack;
