import { useState, useEffect, useRef } from "react";
 import {
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import DataListSingleShareDocument from "../../../fakeData/listSingleShareDocument.json";
import Del from "../../../assets/images/icon/trash-white.svg";
import ModalHapus from "../../../components/Modal/modalHapus";
import toast, { Toaster } from "react-hot-toast";


function ShareSingleDoc() {
  const [data, setData] = useState(DataListSingleShareDocument);

  const [getSingleShare, setGetSingleShare] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
   const columns = [
    {
      accessorKey: "namaDocument",
      header: "Nama Dokumen",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "typeDocument",
      header: "Tipe Dokumen",
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",

    }, 
    {
      id: "actions",
      accessorKey: "id",
      header: "Actions",
      cell: function render({ getValue, state, row, column, instance }) {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              gap: "4px",
            }}
          >
            <button
              type="button"
              className="btn btn-success bg-opacity-70 text-white"
              // onClick={() => onRowView(row.original)}
            >
              Detail
            </button>
            <button
              type="button"
              className="btn btn-danger opacity-70"
              data-bs-toggle="modal"
              data-bs-target="#modalHapus"
              onClick={() =>
                 setGetSingleShare(row.original)
              }
            >
              <img src={Del} width={"35%"} />
            </button>
          </div>
        );
      },
    },
  ];
  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        const id = Math.floor(Math.random() * 10000);
        const newRow = {
          id,
          studentNumber: id,
          name: "",
          dateOfBirth: "",
          major: "",
        };
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  });
  return (
    <div 
    class="container-fluid pt-3 "
    style={{  height: "auto" , marginTop:"65px"}}>
             <ModalHapus getData={getSingleShare} />
 <Toaster position="bottom-left"
            reverseOrder={false}
            gutter={8}
            containerClassName=""
            containerStyle={{}}
            toastOptions={{
              // Define default options
              className: "",
              duration: 5000,
              style: {
                background: "#ffbaba",
                color: "#ff0000 ",
              },

              // Default options for specific types
              success: {
                duration: 3000,
                theme: {
                  primary: "green",
                  secondary: "black",
                },
              },
            }}
          />
    <fieldset>
        <div class="row my-2">
          <div class="col-6">
            <div class="row">
              <div class="col-2">
                <label for="name" class="form-label">
                  Nama
                </label>
              </div>
              <div class="col-10">
                <input type="text" id="name" name="name" class="form-control" />
              </div>
            </div>
          </div>
          <div class="col-6"></div>
        </div>
        <div class="row my-2">
          <div class="col-6">
            <div class="row"></div>
            <div class="row my-2">
              <div class="col-2">
                <label for="name" class="form-label">
                  Dokumen
                </label>
              </div>
              <div class="col-6 share-single">
                <select class="form-select" aria-label="Default select example">
                  <option selected>Dokumen</option>
                  <option value="1">Sertifikat</option>
                  <option value="2">Surat Keputusan</option>
                  <option value="3">Kwitansi</option>
                </select>
              </div>
              <div class="col-4">
                <button type="submit" class="btn btn-primary mb-3 float-end">
                  Add Document
                </button>
              </div>
            </div>
            <table className="table table-bordered table-striped table-hover">
            <thead className="table-primary">
              {table.getHeaderGroups().map((headerGroup) => (
                <tr key={headerGroup.id} className="  uppercase">
                  {headerGroup.headers.map((header) => (
                    <th
                      key={header.id}
                      className="px-4 pr-2 py-3 font-medium text-left"
                    >
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>

            <tbody>
              {table.getRowModel().rows.map((row) => (
                <tr
                  className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50"
                  key={row.id}
                >
                  {row.getVisibleCells().map((cell) => (
                    <td className="px-4 py-2" key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
          </div>
          <div class="col-6">
            <div class="row my-2">
              <div class="col-2">
                <label for="name" class="form-label">
                  Share To
                </label>
              </div>
              <div class="col-7 share-single">
                <select class="form-select" aria-label="Default select example">
                  <option selected>User</option>
                  <option value="1">Ahmad</option>
                  <option value="2">Muhammad</option>
                  <option value="3">Andi</option>
                </select>
              </div>
              <div class="col-3">
                <button type="submit" class="btn btn-primary mb-3 float-end">
                  Add User
                </button>
              </div>
            </div>
            <table className="table table-bordered table-striped table-hover">
            <thead className="table-primary">
              {table.getHeaderGroups().map((headerGroup) => (
                <tr key={headerGroup.id} className="  uppercase">
                  {headerGroup.headers.map((header) => (
                    <th
                      key={header.id}
                      className="px-4 pr-2 py-3 font-medium text-left"
                    >
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>

            <tbody>
              {table.getRowModel().rows.map((row) => (
                <tr
                  className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50"
                  key={row.id}
                >
                  {row.getVisibleCells().map((cell) => (
                    <td className="px-4 py-2" key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
          </div>
        </div>
        <button type="submit" class="btn btn-primary mb-3 float-end">
          Save
        </button>
      </fieldset>
    </div>
  );
}

export default ShareSingleDoc;
