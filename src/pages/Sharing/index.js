import React, { useState } from "react";
// import "../../assets/css/ocr.css";
// import "../../assets/css/table-list.css";
import { useNavigate, useLocation } from "react-router-dom";
 import icSingle from "../../assets/images/icon/doc_single.png";
import icMulti from "../../assets/images/icon/multi_doc.png";

function Sharing() {
  const navigate = useNavigate();

  const toSingleDocument = () => {
    navigate("sharing/single-doc");
  };

  const toMultiDocument = () => {
    navigate("multi-doc");
  };
 
  return (
    <div class="container-fluid"  style={{ height: "auto", padding:"80px 10px" }}>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-3" onClick={toSingleDocument}>
          <div class="card text-bg-primary mb-3" style={{ maxwidth: "18rem" }}>
            <div class="card-header ">
              <h5 class="float-start">Single Document</h5>
            </div>
            <div class="card-body">
              <div class="float-end">
                <img src={icSingle} width={"60%"} />
              </div>
            </div>
          </div>
        </div>

        <div class="col-3 " onClick={toMultiDocument}>
          <div class="card text-bg-info mb-3" style={{ maxwidth: "18rem" }}>
            <div class="card-header ">
              <h5 class="float-start">Multi Document</h5>
            </div>
            <div class="card-body">
              <div class="float-end">
                <img src={icMulti} width={"80%"} />
              </div>
            </div>
          </div>
        </div>
        <div class="col-3"></div>
      </div>
    </div>
  );
}

export default Sharing;
