import React, { useState, useEffect } from "react";
import { useNavigate, useOutletContext, Link } from "react-router-dom";
import axios from "axios";
import Mode from "../../../../package.json";
import LoadSpinner from "../../../components/Load/load";
import CarouselDoc from "../../../components/Carousel";
import ImageCarousel from "../../../components/ImageCarousel";
import { useTranslation } from "react-i18next";
import Swal from "sweetalert2";
import ListDocument from "../ListDocument";
import helper from "../../../api/helper";
import LocalView from "../../Document/ViewDocument/localView";
import Select from "react-select";
// import * as XLSX from "xlsx";
import Load2 from "../../../components/Load2/LoadingProgress.js";
import refresh from "../../../assets/images/icon/icons8-refresh-96.png";

function CreateDocument() {
    const pdfPath = "../../../../public/pdf/HAK PAKAI NO.00062 - 9h.pdf";

    const navigate = useNavigate();
    const { t } = useTranslation();
    const [file, setFile] = useState(null);
    const [image, setImage] = useState({
        image: { src: null },
        width: 0,
        height: 0,
    });
    const [templateID, setTemplateID] = useState("");
    const [templateOptionsHtml, setTemplateOptionsHtml] = useState([]);
    const [loading, setLoading] = useState(false);
    const [docTypeOptionsHtml, setDocTypeOptionsHtml] = useState([]);
    const [poldaOptionsHtml, setPoldaOptionsHtml] = useState([]);
    const [satkerOptionsHtml, setSatkerOptionsHtml] = useState([]);
    const [documentID, setDocumentID] = useState("");
    const [docTypeRef, setDocTypeRef] = useState(null);
    const [poldaRef, setPoldaRef] = useState(null);
    const [satkerRef, setSatkerRef] = useState(null);
    const [docMuncul, setDocMuncul] = useState(false);
    const [addMetaName, setAddMetaName] = useState("");
    const [addMetaValue, setAddMetaValue] = useState("");
    const [tag, setTag] = useState([]);
    const [tagHtml, setTaghtml] = useState([]);
    const [metaPolda, setMetaPolda] = useState([]);
    const [metaPoldaHtml, setMetaPoldahtml] = useState([]);
    const [metadataList, setMetadataList] = useState({});
    const [metadataAdditionalList, setMetadataAdditionalList] = useState([]);
    const [metOptHtlm, setMetOptHtlm] = useState([]);
    const [isCreate, setIsCreate] = useState(true);
    const [imgsOcr, setImgsOcr] = useState([]);
    const [iHtml, setIHtml] = useState([]);
    const [linkPdf, setLinkPdf] = useState(null);
    const [isProcessOcr, setIsProcessOcr] = useState(false);
    const [isMetadataShow, setIsMetadataShow] = useState(false);
    const [dataOCR, setDataOCR] = useState(null);
    const [docVerifiedStatus, setDocVerifiedStatus] = useState(0);
    const [urlShowDocument, setUrlShowDocument] = useState(null);

    const [isSatker, setIsSatker] = useState(false);
    const [isPolda, setIsPolda] = useState(false);
    const [isAdmin, setIsAdmin] = useState(false);
    const [token, setToken] = useState("");
    const [poldaID, setPoldaID] = useState("");
    const [userID, setUserID] = useState("");
    const [poldaName, setPoldaName] = useState("");
    const [satkerID, setSatkerID] = useState("");
    const [satkerName, setSatkerName] = useState("");
    const [nip, setNip] = useState("");
    const [arrImages, setArrImages] = useState([]);
    const [documentTypeId, setDocumentTypeId] = useState("");
    const [documentTypeCode, setDocumentTypeCode] = useState("");
    const [documentTypeName, setDocumentTypeName] = useState("");
    const [stateUpload, setStateUpload] = useState(0);
    const [stateOcrStatus, setStateOcrStatus] = useState(0);
    const [stateOcrProgress, setStateOcrProgress] = useState(0);
    const [stateOcrFinish, setStateOcrFinish] = useState(0);

    let base_url = process.env.REACT_APP_API_CREATE_DOCUMENT;

    const ipServer = localStorage.getItem("ipAddress");

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            // showTable();
            loadDocType();
            loadPolda();
            loadSatker(poldaID);
            // loadMetadata();
        }
    }, [dataIp]);

    useEffect(() => {
        // loadDocType();
        // loadMetadata();
        let userid = localStorage.getItem("user");
        let token = localStorage.getItem("mytoken");
        let poldaid = localStorage.getItem("mypoldaid");
        let poldaname = localStorage.getItem("mypoldaname");
        let satkerid = localStorage.getItem("mysatkerid");
        let satkername = localStorage.getItem("mysatkername");
        let nip = localStorage.getItem("mynip");
        let role = localStorage.getItem("myrole");
        let isAdmin = JSON.parse(localStorage.getItem("myisadmin"));
        setIsAdmin(isAdmin);
        // let token = localStorage.getItem("mytoken");

        const arrayRole = JSON.parse(role);

        // const searchPatternSatker = "SATKER"; // Example pattern
        // const regexSatker = new RegExp(searchPatternSatker, "i"); // 'i' for case insensitive
        // const sakterFound = arrayRole.some((item) => regexSatker.test(item));
        // // const satker = array.filter(item => regexSatker.test(arrayRole));
        // setIsSatker(sakterFound);

        const searchPatternPolda = "POLDA"; // Example pattern
        const regexPolda = new RegExp(searchPatternPolda, "i"); // 'i' for case insensitive
        const sakterPolda = arrayRole.some((item) => regexPolda.test(item));
        setIsPolda(sakterPolda);

        //=======set State=======
        setUserID(userid);
        setToken(token);
        setPoldaID(poldaid);
        setPoldaName(poldaname);
        setSatkerID(satkerid);
        setSatkerName(satkername);
        setNip(nip);
        loadSatker(poldaid);
        if (satkerid && satkerid != "" && satkerid != "null" && satkerid.length > 0) {
            console.log("usereffect - satkerid", satkerid);
            setIsSatker(true);
        }
        console.log("usereffect", poldaid, "-", satkerid, "-", isSatker);
        // loadSatker(poldaid);
    }, []);

    useEffect(() => {
        if (poldaID) {
            loadSatker(poldaID);
        }
    }, [poldaID]);

    ///loading
    const sweetAlert = (body) => {
        Swal.fire({
            title: "Error!",
            text: "" + body,
            icon: "error",
            confirmButtonText: "OK",
        });
    };

    const onChangeTemplate = (event) => {
        const value = event.target.value;
        setTemplateID(value);
    };
    const onChangeAddMeta = (event) => {
        const value = event.target.value;
        setAddMetaName(value);
    };
    /// on btn create doc click
    const createDoc = async () => {
        setDocMuncul(false);
        setIsMetadataShow(false);
        setDocVerifiedStatus(0);
        setTaghtml([]);
        setStateUpload(0);
        setStateOcrStatus(0);
        setLinkPdf(null);
        setStateOcrFinish(0);
        setDataOCR(null);
        setUrlShowDocument(null);
        // console.log("createDoc", documentTypeId, documentTypeCode);

        if (!file) {
            sweetAlert("File harus di pilih");
            return;
        }
        if (!documentTypeId) {
            sweetAlert("Jenis Dokumen harus di pilih");
            return;
        }
        if (!(poldaID && poldaID != "null" && poldaID != "")) {
            console.log("createDoc - poldaID", poldaID);
            sweetAlert("Data Korwil harus di pilih");
            return;
        }
        if (!(poldaID && poldaID != "null" && poldaID != "")) {
            console.log("createDoc - poldaID", poldaID);
            sweetAlert("Data Korwil harus di pilih");
            return;
        }
        let url = "";

        // if (isSatker) {
        //     url = base_url + "polridocument/" + poldaID + "/satker/" + satkerID + "/document";
        // } else if (isPolda) {
        //     url = base_url + "polridocument/" + poldaID + "/document";
        // } else {
        //     sweetAlert("Tidak Memiliki Akses");
        //     return;
        // }

        if (poldaID) {
            url = base_url + "polridocument/" + poldaID + "/document";

            if (satkerID && satkerID != null && satkerID != "null") {
                url = base_url + "polridocument/" + poldaID + "/satker/" + satkerID + "/document";
            }
        } else {
            sweetAlert("Pilih Korwil");
            return;
        }

        // return;
        try {
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            // myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
            myHeaders.append("Authorization", `Bearer ${token}`);

            const body_data = JSON.stringify({
                "@type": "Document",
                doc_type_name: documentTypeName,
                doc_type_id: documentTypeId,
                doc_using_local_ocr: 1,
            });

            const requestOptions = {
                method: "POST",
                headers: myHeaders,
                body: body_data,
            };

            setLoading(true);
            const response = await fetch(url, requestOptions);
            const data = await response.json();

            if (response.status == 201 || 200) {
                console.log('createDoc - data["@id"]', data["@id"]);
                uploadFile(data["@id"]);
                setDocumentID(data["@id"]);
            } else if (response.status == 409) {
                setLoading(false);
                setIsCreate(true);
                sweetAlert("Nama Dokumen Tidak boleh sama");
            } else {
                setLoading(false);
                setIsCreate(true);

                // sweetAlert("Service Tidak Aktif")
            }
        } catch (err) {
            setLoading(false);
            setIsCreate(true);

            // sweetAlert("Service Tidak Aktif")
        }
        // showTagMetadata();
    };

    const uploadFile = async (url) => {
        console.log("uploadFile", url);
        try {
            setLoading(true);
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/pdf");
            // myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
            myHeaders.append("Authorization", `Bearer ${token}`);
            const requestOptions = {
                method: "PATCH",
                headers: myHeaders,
                body: file,
            };
            console.log("uploadFile", requestOptions);
            await fetch(url + "/@upload/doc_file", requestOptions)
                .then((response) => {
                    console.log(response);
                    if (response.status == 200) {
                        setTimeout(() => {
                            showDocument(url);
                            // setLoading(false);
                        }, 6000);
                    } else if (response.status >= 400) {
                        Swal.fire("Upload Gagal, E-" + response.status, "Terjadi kesalahan, harap untuk mencoba kembali", "error");
                        setStateUpload(1);
                        setStateOcrStatus(1);
                        setIsProcessOcr(false);
                        setLoading(false);
                    }
                })
                // .then((result) => console.log(result))
                .catch((error) => console.error(error));
        } catch (err) {
            console.log("err : ", err);
            if (err.status >= 400) {
                Swal.fire("Upload Gagal, E-" + err.status, "Terjadi kesalahan, harap untuk mencoba kembali", "error");
                setStateUpload(1);
                setStateOcrStatus(1);
                setIsProcessOcr(false);
                setLoading(false);
            }
            setLoading(false);
            setIsCreate(true);
        }
    };

    const showDocument = async (url) => {
        console.log("showDocument", url, token, stateOcrFinish);

        console.log("showDocument", url, stateOcrFinish);
        setUrlShowDocument(url);
        setStateUpload(1);
        setStateOcrStatus(0);
        setStateOcrProgress(0);
        try {
            setLinkPdf(url + "/@download/doc_file");

            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/pdf");
            // myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
            myHeaders.append("Authorization", `Bearer ${token}`);
            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            setIsProcessOcr(true);
            console.log("showDocument", url, requestOptions);
            const req = await fetch(url, requestOptions);
            const data = await req.json();
            setDataOCR(data);
            setDocMuncul(true);
            setDocVerifiedStatus(0);
            if (req.status == 200) {
                setStateOcrProgress(data.doc_ocr_progress_status);
                if (data.doc_ocr_status == 0) {
                    setTimeout(() => {
                        showDocument(url);
                    }, 6000);
                } else if (data.doc_ocr_status == 400) {
                    // Swal("Error", "Terjadi kesalahan. Mohon coba upload kembali", "error");
                    Swal.fire("Gagal OCR", "Terjadi kesalahan, harap untuk upload kembali", "error");
                    setLoading(false);
                    onFinishOcr();
                } else if (req.status == 4221) {
                    // deleteDoc(url);
                    Swal("Error", "Template Tidak Sesuai", "error");
                    setLoading(false);
                    onFinishOcr();
                } else if (data.doc_ocr_status == 1) {
                    setUrlShowDocument(url);
                    setStateUpload(1);
                    setStateOcrStatus(1);
                    setStateOcrProgress(100);
                    const metadata_template = data.doc_metadata_by_template;

                    showTagMetadata(metadata_template);

                    setIsProcessOcr(false);
                    setLoading(false);
                } else {
                    setUrlShowDocument(url);
                    setStateUpload(1);
                    setStateOcrStatus(1);
                    setStateOcrProgress(100);
                    const metadata_template = data.doc_metadata_by_template;

                    showTagMetadata(metadata_template);

                    setIsProcessOcr(false);
                    setLoading(false);
                }
            } else if (req.status >= 400) {
                Swal.fire("Upload Gagal, E-" + req.status, "Terjadi kesalahan, harap untuk mencoba kembali", "error");
                setStateUpload(1);
                setStateOcrStatus(1);
                setIsProcessOcr(false);
                setLoading(false);
            }
        } catch (err) {
            if (err.status >= 400) {
                Swal.fire("Upload Gagal, E-" + err.status, "Terjadi kesalahan, harap untuk mencoba kembali", "error");
                setStateUpload(1);
                setStateOcrStatus(1);
                setIsProcessOcr(false);
                setLoading(false);
            }
            setIsProcessOcr(false);
            setLoading(false);
            setIsCreate(true);
            console.log("err : ", err);
        }
    };
    ///Delete Document
    const deleteDoc = async (url) => {
        setLoading(true);

        const config = {
            headers: {
                Authorization: "Basic " + btoa("root:Adm1nDMS2024"), // Add token from userData
            },
        };
        await axios
            .delete(url, config)
            .then(async (response) => {})
            .catch((error) => {
                console.log(error);
                setLoading(false);
            });
    };

    /// show image documnet ocr
    const showImages = async (images_ocr) => {
        setImgsOcr([]);
        const arr_len = images_ocr.length - 1;
        let arr_img = "";
        images_ocr.forEach((data, index) => {
            if (arr_len != index) {
                return;
            }
            arr_img = data.ocr_file_path;
        });

        arr_img.forEach((link, index) => {
            imgTohtml(link);
        });
        setLoading(false);
    };
    const imgTohtml = async (link) => {
        let authentication_data = {
            headers: {
                Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
            },
            responseType: "arraybuffer",
        };
        let arr_img_temp = imgsOcr;

        await axios
            .get(base_url + "/" + link, authentication_data)
            .then(async (response) => {
                const imgsrc = _imageEncode(response.data);

                arr_img_temp.push(imgsrc);

                if (imgsOcr.length == 0) {
                    setImgsOcr([imgsrc]);
                } else {
                    setImgsOcr([...imgsOcr, imgsrc]);
                }
            })
            .catch((error) => {
                setLoading(false);
            });
    };

    function _imageEncode(arrayBuffer) {
        let b64encoded = btoa(
            [].reduce.call(
                new Uint8Array(arrayBuffer),
                function (p, c) {
                    return p + String.fromCharCode(c);
                },
                ""
            )
        );

        let mimetype = "image/jpeg";
        let base64 = "data:" + mimetype + ";base64," + b64encoded;

        let mImage = new Image();
        mImage.src = base64;
        mImage.onload = function (evt) {};

        return base64;
    }
    //show metadata Tagging
    const showTagMetadata = (metadata_template) => {
        let listTagTemp = [];
        setIsMetadataShow(true);
        metadata_template.map(function (data, idx) {
            let metadata_name = data.metadata_tmp_name
                .split("_")
                .slice(1)
                .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                .join(" ");
            listTagTemp.push(
                // <div class="row py-1">
                //     <div class="col">
                //         <label>{data.metadata_tmp_name}</label>
                //     </div>
                //     <div class="col">
                //         <textarea readonly="readonly" class="form-control form-control-sm">
                //             {data.metadata_tmp_value}
                //         </textarea>
                //     </div>
                // </div>
                <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={idx}>
                    <td className="px-4 py-2">{metadata_name}</td>
                    <td className="px-4 py-2"> {data.metadata_tmp_value} </td>
                </tr>
            );
        });
        setTaghtml(listTagTemp);
    };
    //show metadataByPolda
    const showPoldaMetadata = (metadata_polda) => {
        let listPolTemp = [];
        metadata_polda.map(function (data, idx) {
            listPolTemp.push(
                <div class="row py-1">
                    <div class="col">
                        <label>{data.metadata_pol_name}</label>
                    </div>
                    <div class="col">
                        <textarea readonly="readonly" class="form-control form-control-sm">
                            {data.metadata_pol_value}
                        </textarea>
                    </div>
                </div>
            );
        });

        setMetaPoldahtml(listPolTemp);
    };

    const handleFileChange = (e) => {
        let selectedFile = e.target.files[0];
        setFile(selectedFile);
    };

    const checkDocumentType = (name) => {
        if (name) {
            let type = name.split(/[\s._\-]+/)[0];
            if (type) {
                let obj = docTypeRef.find((o) => o.doc_type_code === type);
                if (obj) {
                    setDocumentTypeId(obj.doc_type_id);
                    setDocumentTypeCode(obj.doc_type_code);
                    setDocumentTypeName(obj.doc_type_title);
                } else {
                    setDocumentTypeId("");
                    setDocumentTypeCode("");
                    setDocumentTypeName("");
                }
            } else {
                setDocumentTypeId("");
                setDocumentTypeCode("");
                setDocumentTypeName("");
            }
        }
    };

    const changePolda = (e) => {
        if (poldaRef) {
            // let id = e.target.value;
            // let obj = poldaRef.find((o) => o.polda_id === id);
            // if (obj && obj.polda_id) {
            //     setPoldaID(obj.polda_id);
            //     setPoldaName(obj.polda_name);
            // }
            if (e) {
                const { value, label } = e;
                setSatkerID(null);
                setSatkerName(null);
                setPoldaID(value);
                setPoldaName(label);
                loadSatker(value);
            } else {
                setPoldaID(null);
                setPoldaName(null);
            }
        }
    };

    const changeSatker = (e) => {
        if (poldaRef) {
            if (e) {
                const { value, label } = e;
                setSatkerID(value);
                setSatkerName(label);
            } else {
                setSatkerID(null);
                setSatkerName(null);
            }
        }
    };

    const changeDocumentType = (e) => {
        if (docTypeRef) {
            let id = e.target.value;

            let obj = docTypeRef.find((o) => o.doc_type_id === id);
            if (obj && obj.doc_type_id) {
                setDocumentTypeId(obj.doc_type_id);
                setDocumentTypeCode(obj.doc_type_code);
                setDocumentTypeName(obj.doc_type_title);
            }
        }
    };

    const modifyData = (data) => {
        if (data && data.length > 0) {
            return data.sort((a, b) => a.doc_type_index_no - b.doc_type_index_no);
        } else {
            return [];
        }
    };

    const loadDocType = async () => {
        try {
            let url = dataIp + "/doctype/@list_doctype";

            let options = [];
            if (dataIp && dataIp != "") {
                let res = await helper.GetListDocType(url);
                if ((res && res.status == 200) || 201 || 202 || 204) {
                    let datas = modifyData(res.data); //res.data;
                    setDocTypeRef(datas);

                    datas.map(function (data, index) {
                        options.push(<option value={data.doc_type_id}>{`${data.doc_type_title} - ${data.doc_type_code}`}</option>);
                    });

                    setDocTypeOptionsHtml(options);
                }
            }
        } catch (err) {}
    };

    const loadPolda = async () => {
        try {
            // let url = dataIp + "/doctype/@list_doctype";

            let options = [];
            if (dataIp && dataIp != "") {
                // let res = await helper.GetListDocType(url);
                let res = await helper.GetListPolda(dataIp + "/polridocument/@list_polda");
                if (res && (res.status == 200 || 201 || 202 || 204)) {
                    let datas = res.data;
                    setPoldaRef(res);

                    res.map(function (data, index) {
                        // options.push(<option value={data.polda_id}>{`${data.polda_name} `}</option>);
                        options.push({ value: data.polda_id, label: `${data.polda_name}` });
                    });

                    setPoldaOptionsHtml(options);
                }
            }
        } catch (err) {
            console.log("loadPolda", err);
        }
    };

    const loadSatker = async (poldaId) => {
        console.log("loadSatker", poldaId);
        try {
            // let url = dataIp + "/doctype/@list_doctype";

            let options = [];
            if (dataIp && dataIp != "") {
                // let res = await helper.GetListDocType(url);
                let res = await helper.GetListSatker(dataIp + "/polridocument/" + poldaId + "/@list_satker");
                if (res && (res.status == 200 || 201 || 202 || 204)) {
                    let datas = res.data;
                    setSatkerRef(res);

                    res.map(function (data, index) {
                        // options.push(<option value={data.polda_id}>{`${data.polda_name} `}</option>);
                        options.push({ value: data.satker_id, label: `${data.satker_name}` });
                    });

                    setSatkerOptionsHtml(options);
                }
            }
        } catch (err) {
            console.log("loadPolda", err);
        }
    };

    const exportToExcel = (data) => {};

    const onDownloadResultOCR = () => {
        if (dataOCR && dataOCR.doc_metadata_by_template && dataOCR.doc_metadata_by_template.length > 0) {
            exportToExcel(dataOCR.doc_metadata_by_template);
        }
    };

    const onRefreshResultOCR = async () => {
        console.log("onRefreshResultOCR", urlShowDocument);
        if (urlShowDocument) {
            try {
                const myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/pdf");
                myHeaders.append("Authorization", `Bearer ${token}`);
                const requestOptions = {
                    method: "GET",
                    headers: myHeaders,
                };
                const req = await fetch(urlShowDocument, requestOptions);
                const data = await req.json();
                setDataOCR(data);
                // setDocMuncul(true);
                // setDocVerifiedStatus(0);
                if (req.status == 200) {
                    if (data.doc_ocr_status == 1) {
                        const metadata_template = data.doc_metadata_by_template;
                        showTagMetadata(metadata_template);
                    }
                    // setStateOcrProgress(data.doc_ocr_progress_status);
                    // if (data.doc_ocr_status == 0) {
                    //     setTimeout(() => {
                    //         showDocument(url);
                    //     }, 6000);
                    // } else if (req.status == 4221) {
                    //     // deleteDoc(url);
                    //     Swal("Error", "Template Tidak Sesuai", "error");
                    //     setLoading(false);
                    // } else {
                    //     setUrlShowDocument(url)
                    //     setStateUpload(1);
                    //     setStateOcrStatus(1);
                    //     setStateOcrProgress(100);
                    //     const metadata_template = data.doc_metadata_by_template;

                    //     showTagMetadata(metadata_template);

                    //     setIsProcessOcr(false);
                    //     setLoading(false);
                    // }
                }
            } catch (err) {
                console.log("onRefreshResultOCR", err);
            }
        }
    };

    const btnCreate = () => (
        <button
            id="btnCreate"
            type="submit"
            disabled={loading && !docMuncul}
            onClick={(e) => {
                createDoc();
            }}
            class="btn btn-primary btn-md"
        >
            {t("process_um")}
        </button>
    );

    const onFinishOcr = () => {
        setDataOCR(null);
        setDocMuncul(false);
        setLoading(false);
        setLinkPdf(null);
        setStateUpload(0);
        setStateOcrStatus(0);
        setStateOcrFinish(1);
    };
    // console.log("isSatker", isSatker);
    const showMode = () => {
        const isLatest = /latest/.test(Mode.version);

        if (isLatest) {
            console.log("Latest version detected");
        } else {
            console.log("Not the latest version");
            return <h4 style={{ background: "#5CFF5C", color: "red", fontWeight: "bold" }}>DEVELOPMENT MODE</h4>;
        }
    };
    return (
        <div
            class="container-fluid"
            style={{
                height: "auto",
                padding: "100px 10px",
            }}
        >
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title textfamily">
                        {t("create_um")} {t("dokumen_t1")}
                    </h5>
                    {showMode()}
                    <div class="row"></div>
                    <div class="row mt-3">
                        <div class="col-1" />
                        <div class="col-2">
                            <div class="row" style={{ paddingBottom: "10px" }}>
                                <div class="col-12">
                                    <select id="docType" value={documentTypeId} class="form-select" aria-label="Default select example" onChange={changeDocumentType}>
                                        <option selected>
                                            {t("select_um")} {t("titlle_dt")}
                                        </option>
                                        {docTypeOptionsHtml}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <input type="file" class="form-control form-control" accept="application/pdf" onChange={handleFileChange} />
                        </div>
                        <div class="col-3">
                            <div class="row" style={{ paddingBottom: "10px" }}>
                                <div class="col-12">
                                    <Select
                                        id="SelectPolda"
                                        isDisabled={!isAdmin}
                                        value={poldaOptionsHtml.find((option) => option.value === poldaID) || null}
                                        onChange={changePolda}
                                        options={poldaOptionsHtml}
                                        placeholder={`${t("select_um")} ${t("titlle_lp")}`}
                                        className=""
                                        classNamePrefix="select"
                                    />
                                </div>
                                {isAdmin ? (
                                    poldaID && poldaID !== "" ? (
                                        <div class="col-12 mt-2">
                                            <Select
                                                id="SelectSatker"
                                                // isDisabled={!isAdmin}
                                                value={satkerOptionsHtml.find((option) => option.value === satkerID) || null}
                                                onChange={changeSatker}
                                                options={satkerOptionsHtml}
                                                placeholder={`${t("select_um")} ${t("titlle_ls")}`}
                                                className=""
                                                classNamePrefix="select"
                                            />
                                        </div>
                                    ) : null
                                ) : poldaID && poldaID !== "" ? (
                                    <div class="col-12 mt-2">
                                        <Select
                                            id="SelectSatker"
                                            isDisabled={isSatker}
                                            value={satkerOptionsHtml.find((option) => option.value === satkerID) || null}
                                            onChange={changeSatker}
                                            options={satkerOptionsHtml}
                                            placeholder={`${t("select_um")} ${t("titlle_ls")}`}
                                            className=""
                                            classNamePrefix="select"
                                        />
                                    </div>
                                ) : null}
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="row" style={{ paddingBottom: "10px" }}>
                                <div class="col-12">{isCreate ? btnCreate() : ""}</div>
                            </div>
                        </div>
                        {/* <div class="col-1">{isCreate ? btnCancel() : btnFinish()}</div> */}
                    </div>

                    {/* {loading && !docMuncul ? (
                        <div class="row">
                            <div class="col-10">
                                <Load2 stateUpload={stateUpload} stateOcr={stateOcr} />
                            </div>
                        </div>
                    ) : null} */}
                    {loading && stateOcrStatus == 0 ? (
                        <div class="row">
                            <div class="col-1" />
                            <div class="col-10">
                                <Load2 stateUpload={stateUpload} stateOcr={stateOcrStatus} stateOcrProgress={stateOcrProgress} />
                            </div>
                        </div>
                    ) : null}

                    {/* <Load2 stateUpload={stateUpload} stateOcr={stateOcr} /> */}
                    {/* <div class="row mt-3 bg-light">{arrImages.length === 0 ? <div></div> : <ImageCarousel imgApiUrls={arrImages} />}</div> */}
                    {/* {loading && !docMuncul ? (
                        <div
                            class="mt-5"
                            style={{
                                display: "10px",
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                            }}
                        >
                            <div class="spinner-border text-primary" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    ) : null} */}
                    <div class="row mt-3 bg-light">{arrImages.length === 0 ? <div></div> : <ImageCarousel imgApiUrls={arrImages} />}</div>
                    <div>
                        {docMuncul && (
                            <div>
                                <div class="row mt-3">
                                    {documentTypeCode == "SHP" || documentTypeCode == "SHM" || documentTypeCode == "SE" || documentTypeCode == "eigendom" || documentTypeCode == "LetterC" || documentTypeCode == "SHGU" || documentTypeCode == "SHGB" ? (
                                        <div class="col-6">
                                            <LocalView link={linkPdf} token={token}></LocalView>
                                        </div>
                                    ) : (
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-3" />
                                                <div class="col-6">
                                                    <LocalView link={linkPdf} token={token}></LocalView>
                                                </div>
                                            </div>
                                            <div class="row d-flex justify-content-end">
                                                <div class="col-3 d-flex justify-content-end mt-3">
                                                    <button type="button" onClick={() => onFinishOcr()}>
                                                        {t("fin_um")}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                    {documentTypeCode == "SHP" || documentTypeCode == "SHM" || documentTypeCode == "SE" || documentTypeCode == "eigendom" || documentTypeCode == "LetterC" || documentTypeCode == "SHGU" || documentTypeCode == "SHGB" ? (
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-9">
                                                    <h5 class="card-title">Hasil OCR</h5>{" "}
                                                </div>
                                                {/* <div class="col-3 d-flex justify-content-end mb-3">
                                                    <button type="button" onClick={() => onDownloadResultOCR()}>
                                                        {t("download_um")}
                                                    </button>
                                                </div> */}
                                                <div class="col-3 d-flex justify-content-end mb-3">
                                                    <button type="button" onClick={() => onRefreshResultOCR()}>
                                                        {/* {t("download_um")} */}
                                                        <img src={refresh} width={"30px"} />
                                                    </button>
                                                </div>
                                            </div>
                                            <table className="table table-bordered table-striped table-hover">
                                                <thead className="table-primary">
                                                    <tr className="  uppercase">
                                                        <th className="px-4 pr-2 py-3 font-medium text-left">{t("tag_um")}</th>
                                                        <th className="px-4 pr-2 py-3 font-medium text-left">{t("res_um")}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>{tagHtml && tagHtml.length > 0 ? tagHtml : null}</tbody>
                                            </table>

                                            <div class="row d-flex justify-content-end">
                                                <div class="col-3 d-flex justify-content-end mt-3">
                                                    <button type="button" onClick={() => onFinishOcr()}>
                                                        {t("fin_um")}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null}
                                </div>
                            </div>
                        )}
                    </div>
                    {/* {docMuncul ? null : (
                        <div class="mt-4">
                            <hr></hr>
                            <ListDocument></ListDocument>
                        </div>
                    )} */}
                </div>
            </div>

            {docMuncul ? null : (
                <div class="card mt-2">
                    <div class="card-body">
                        <ListDocument></ListDocument>
                    </div>
                </div>
            )}
        </div>
    );
}

export default CreateDocument;
