import React, { useState, useEffect } from "react";
import { useNavigate, useLocation, useOutletContext, Link } from "react-router-dom";
import axios from "axios";
import LoadSpinner from "../../../components/Load/load";
//Dynamic
import CarouselDoc from "../../../components/Carousel";
//Static
// import CarouselDoc from "../../../components/Carousel/statis";
import Close from "../../../assets/images/icon/close.png";
import ShowPdf from "../../Searching/View";
import { useTranslation } from "react-i18next";
import LocalStorageHelper from "../../../api/localStorageHelper";
import { Button } from "reactstrap";
import verifyDone from "../../../assets/images/icon/verify_done.png";
import verifyUndone from "../../../assets/images/icon/verify_undone.png";

function Approval() {
    let location = useLocation();
    const navigate = useNavigate();
    let base_url = process.env.REACT_APP_API_URL;
    const userData = LocalStorageHelper.getUserData();
    const { t } = useTranslation();

    const { state } = location;
    const [viewData, setViewData] = useState(state);
    const [loading, setLoading] = useState(false);
    const [document, setDocument] = useState(null);
    const [documentVerify, setDocumentVerify] = useState(null);
    const [isDownload, setIsDownload] = useState(false);

    const [tagHtml, setTaghtml] = useState([]);
    const [docType, setDocType] = useState(null);
    const [template, setTemplate] = useState(null);
    const [linkPdf, setLinkPdf] = useState(null);
    const [poldaName, setPoldaName] = useState(null);
    const [isOpen, setIsOpen] = useState(true);
    const [isVisible, setIsVisible] = useState(true);

    useEffect(() => {
        if (viewData) {
            const api = userData.myip;

            if (api && api != null && api != "null") {
                loadState();
            }
        } else {
            toListDocument();
        }
    }, [viewData]);

    const loadState = () => {
        let data = viewData.document;
        let version = data.doc_active_version;
        let url_data = data.doc_url;
        let isDownload = viewData.hasOwnProperty("isDownload") ? viewData.isDownload : false;
        setIsDownload(isDownload);

        let filterVersion = "";
        let url_file = "";
        if (url_data) {
            filterVersion = url_data.filter((url) => {
                return url.version === version;
            });
            url_file = filterVersion[0].original_file_path;
        }

        setTaghtml([]);
        setDocument(data);
        setPoldaName(data ? data.doc_polda_name : "");
        setDocType(data ? data.doc_type_name : "");
        setLinkPdf(url_file);

        onLoadData(data);

        if (data && data.doc_verified_status > 0) {
            showTagMetadata(data.doc_metadata_by_template_verification)
        }
        else {
            showTagMetadata(data.doc_metadata_by_template)
        }
        
    };

    const onLoadData = async (data) => {
        console.log("onLoadData", data);

        if (data && data.hasOwnProperty("path")) {
            try {
                let url = userData.myip + "/db/dmsbackend/" + data.path;
                const myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Authorization", `Bearer ${userData.token}`);

                const requestOptions = {
                    method: "GET",
                    headers: myHeaders,
                };
                var res = await fetch(url, requestOptions);
                var datas = await res.json();
                console.log("onLoadData datas", datas);
                setDocument(datas);
            } catch (err) {
                alert(`${t("error_um")}`);
                setLoading(false);
            }
        }
    };

    const showTagMetadata = (metadata_template) => {
        if (metadata_template) {
            let listTagTemp = [];
            // setIsMetadataShow(true);
            metadata_template.map(function (data, idx) {
                let metadata_name = data.metadata_tmp_name
                    .split("_")
                    .slice(1)
                    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                    .join(" ");
                listTagTemp.push(
                    <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={idx}>
                        <td className="px-4 py-2">{metadata_name}</td>
                        <td className="px-4 py-2"> {data.metadata_tmp_value} </td>
                    </tr>
                );
            });
            setTaghtml(listTagTemp);
        }
    };

    const toListDocument = () => {
        navigate("/document/list-document");
    };

    const loadDocType = async (type, template) => {
        setLoading(true);
        try {
            let url = userData.myip + "/db/dmsbackend/doctype/@list_doctype?doc_type_id=" + type;
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);

            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
            let options = {};

            var doc_type = datas[0].doc_type_title;
            setDocType(doc_type);
        } catch (err) {
            alert(`${t("error_um")}`);
            setLoading(false);
        }
    };

    const loadTemplate = async (template) => {
        try {
            let url = userData.myip + "/db/dmsbackend/templatemaster/@list_template?template_id=" + template;
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);

            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
            let options = {};

            // datas.map(function (data, index) {
            //   options[data.id] = data
            // });
            var doc_template = datas[0].template_name;

            setTemplate(doc_template);
            setLoading(false);
            // loadState()
        } catch (err) {
            setLoading(false);
        }
    };
    const approveDocument = async () => {
        setLoading(true);
        try {
            let url = document["@id"];
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);

            const body_data = JSON.stringify({
                doc_state: 2,
            });

            const requestOptions = {
                method: "PATCH",
                headers: myHeaders,
                body: body_data,
            };
            const response = await fetch(url, requestOptions);
            const data = await response.json();
            if (response.status == 200) {
                setLoading(false);
            } else {
                setLoading(false);
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
            setLoading(false);
        }
    };
    const rejectDocument = async () => {
        setLoading(true);
        try {
            let url = document["@id"];
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);

            const body_data = JSON.stringify({
                doc_state: 3,
            });

            const requestOptions = {
                method: "PATCH",
                headers: myHeaders,
                body: body_data,
            };
            const response = await fetch(url, requestOptions);
            const data = await response.json();
            if (response.status == 200) {
                setLoading(false);
            } else {
                console.log(" status :", response.status);
                setLoading(false);
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
            setLoading(false);
        }
    };
    const reviewDocument = async () => {
        setLoading(true);
        try {
            let url = document["@id"];
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);

            const body_data = JSON.stringify({
                doc_state: 1,
            });

            const requestOptions = {
                method: "PATCH",
                headers: myHeaders,
                body: body_data,
            };
            const response = await fetch(url, requestOptions);
            const data = await response.json();
            if (response.status == 200) {
                console.log("data: ", data);
                setLoading(false);
            } else {
                console.log(" status :", response.status);
                setLoading(false);
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
            setLoading(false);
        }
    };
    const handleClose = () => {
        setIsVisible(false);
    };
    const toDocument = () => {
        navigate("/document/create-document");
    };
    return (
        <div
            class="container-fluid"
            style={{
                height: "auto",
                padding: "100px 10px",
            }}
        >
            {loading ? <LoadSpinner /> : ""}
            {/* tes 2 colom */}
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-6">
                            <h5 class="card-title px-3 pt-3 textfamily">
                                {t("lihat_um")} {t("dokumen_t1")}{" "}
                            </h5>
                        </div>
                        <div class="col-6">
                            <div class="float-end">
                                {isVisible && (
                                    <button type="button" className="btn btn-sm" title="keluar" onClick={toDocument}>
                                        <img src={Close} width={"30"} />
                                    </button>
                                )}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {document &&
                        (document.doc_type_id == "SHM" ||
                            document.doc_type_id == "SHP" ||
                            document.doc_type_id == "SE" ||
                            document.doc_type_id == "eigendom" ||
                            document.doc_type_id == "LetterC" ||
                            document.doc_type_id == "SHGU" ||
                            document.doc_type_id == "SHGB" ||
                            document.doc_type_id == "DT2" ||
                            document.doc_type_id == "DT1" ||
                            document.doc_type_id == "DT4" ||
                            document.doc_type_id == "DT8" ||
                            document.doc_type_id == "DT9") ? (
                            <div class="col-7">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mt-1 bg-light">
                                            <div class="col-5 ">
                                                <label class="col-form-label fw-bolder">{t("doc_name_td")}</label>
                                                <div class="input-group">
                                                    <label>- {document && document.doc_name}</label>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <label class=" col-form-label fw-bolder">{t("titlle_dt")}</label>
                                                <div class="input-group">
                                                    <label>- {document && docType}</label>
                                                    {/* <label>{document && docType[document.doc_type_id]}</label> */}
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <label class="  col-form-label fw-bolder">{t("titlle_lp")}</label>
                                                <div class="input-group">
                                                    <label>- {document && poldaName}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">{linkPdf && <ShowPdf link={linkPdf} isDownload={isDownload}></ShowPdf>}</div>
                            </div>
                        ) : (
                            <div class="row">
                                <div class="col-2" />
                                <div class="col-8">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row mt-1 bg-light">
                                                <div class="col-5 ">
                                                    <label class="col-form-label fw-bolder">{t("doc_name_td")}</label>
                                                    <div class="input-group">
                                                        <label>- {document && document.doc_name}</label>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <label class=" col-form-label fw-bolder">{t("titlle_dt")}</label>
                                                    <div class="input-group">
                                                        <label>- {document && docType}</label>
                                                        {/* <label>{document && docType[document.doc_type_id]}</label> */}
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <label class="  col-form-label fw-bolder">{t("titlle_lp")}</label>
                                                    <div class="input-group">
                                                        <label>- {document && poldaName}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">{linkPdf && <ShowPdf link={linkPdf} isDownload={isDownload}></ShowPdf>}</div>
                                </div>
                            </div>
                        )}

                        {document &&
                        (document.doc_type_id == "SHM" ||
                            document.doc_type_id == "SHP" ||
                            document.doc_type_id == "SE" ||
                            document.doc_type_id == "eigendom" ||
                            document.doc_type_id == "LetterC" ||
                            document.doc_type_id == "SHGU" ||
                            document.doc_type_id == "SHGB" ||
                            document.doc_type_id == "DT2" ||
                            document.doc_type_id == "DT1" ||
                            document.doc_type_id == "DT4" ||
                            document.doc_type_id == "DT8" ||
                            document.doc_type_id == "DT9") ? (
                            <div class=" col-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mt-1 bg-light">
                                            <div class="col-12 p-3">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <h6 class="card-title pb-2">
                                                            <p class="text-decoration-underline text-primary">
                                                                {t("tag_um")} {t("titlle_md")}
                                                            </p>
                                                        </h6>
                                                    </div>
                                                    {document && document.doc_verified_status > 0 ? (
                                                        <div class="col-4 d-flex flex-row-reverse">
                                                            <button className="btn btn-sm" title="Telah di verifikasi">
                                                                <img src={verifyDone} width={"30px"} />
                                                            </button>
                                                        </div>
                                                    ) : (
                                                        <div class="col-4 d-flex flex-row-reverse">
                                                            <button className="btn btn-sm" title="Belum di verifikasi">
                                                                <img src={verifyUndone} width={"30px"} />
                                                            </button>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                        {/* hasil ocr */}
                                        <div class="row mt-3 bg-light">
                                            <table className="table table-bordered table-striped table-hover">
                                                <thead className="table-primary">
                                                    <tr className="  uppercase">
                                                        <th className="px-4 pr-2 py-3 font-medium text-left">{t("tag_um")}</th>
                                                        <th className="px-4 pr-2 py-3 font-medium text-left">{t("res_um")}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>{tagHtml && tagHtml.length > 0 ? tagHtml : null}</tbody>
                                            </table>
                                            {/* <div class="col-5 ">
                                                <label class="col-form-label fw-bolder">{t("tag_um")}</label>
                                                {document
                                                    ? document.doc_verified_status > 0
                                                        ? document.doc_metadata_by_template_verification &&
                                                          document.doc_metadata_by_template_verification.map((data, index) => {
                                                              let metadata_name = data.metadata_tmp_name
                                                                  .split("_")
                                                                  .slice(1)
                                                                  .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                                                                  .join(" ");
                                                              return (
                                                                  <div class="row py-1 mx-1">
                                                                      <div class="col">{metadata_name}</div>
                                                                  </div>
                                                              );
                                                          })
                                                        : document.doc_metadata_by_template &&
                                                          document.doc_metadata_by_template.map((data, index) => {
                                                              let metadata_name = data.metadata_tmp_name
                                                                  .split("_")
                                                                  .slice(1)
                                                                  .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                                                                  .join(" ");
                                                              return (
                                                                  <div class="row py-1 mx-1">
                                                                      <div class="col">{metadata_name}</div>
                                                                  </div>
                                                              );
                                                          })
                                                    : null}
                                            </div>
                                            <div class="col-7 bg-white">
                                                <label class="  col-form-label fw-bolder ">{t("res_um")}</label>                                           

                                                {document
                                                    ? document.doc_verified_status > 0
                                                        ? document.doc_metadata_by_template_verification &&
                                                          document.doc_metadata_by_template_verification.map((data, index) => (
                                                              <div class="row py-1 mx-1">
                                                                  <div class="col">{data.metadata_tmp_value}</div>
                                                              </div>
                                                          ))
                                                        : document.doc_metadata_by_template &&
                                                          document.doc_metadata_by_template.map((data, index) => (
                                                              <div class="row py-1 mx-1">
                                                                  <div class="col">{data.metadata_tmp_value}</div>
                                                              </div>
                                                          ))
                                                    : null}
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : null}
                    </div>
                </div>
            </div>
            {/* end  */}
        </div>
    );
}

export default Approval;
