 
import React, { useState, useEffect } from "react";
import { useNavigate,useLocation, useOutletContext,Link } from "react-router-dom";
import axios from "axios";
import LoadSpinner from "../../../components/Load/load";
//Dynamic
import CarouselDoc from "../../../components/Carousel";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
import Swal from 'sweetalert2'
import ImageCarousel from "../../../components/ImageCarousel";
import LocalStorageHelper from "../../../api/localStorageHelper";
function AddVersion() {

   let location = useLocation()
  const navigate = useNavigate();
  let base_url = process.env.REACT_APP_API_LOCAL;
  const { t } = useTranslation();
  const userData = LocalStorageHelper.getUserData()
 
  const { state } = location;
  const [viewData, setViewData] = useState(state)
  const [document, setDocument] = useState(null);
  const [docType, setDocType] = useState(null)
  const [templateOptionsHtml, setTemplateOptionsHtml] = useState([]);
  const [file, setFile] = useState(null);
  const [templateID, setTemplateID] = useState("");
  const [imgsOcr, setImgsOcr] = useState([]);
  const [loading, setLoading] = useState(false);
  const [poldaMetadata, setPoldaMetadata] = useState(null)
  const [docMetadata, setDocMetadata] = useState(null)
  const [addiMetadata, setAddiMetadata] = useState(null)
  const [isCreate, setIsCreate] = useState(true);
  const userid = localStorage.getItem("user");
  const token  = localStorage.getItem("mytoken");
  const [arrImages,setArrImages] = useState([])
  
  useEffect(() => {
    if (viewData) {
      loadState()
    }else{
        toListDocument()
    }
  },[viewData]);

  const errorAlert = (body) => {
      Swal.fire({
        title: 'Error!',
        text: ''+body,
        icon: 'error',
        confirmButtonText: 'OK'
      })
    
  }
  const loadState = () => {
    let data = viewData.document
    setDocument(data)
    
    loadDocType(data.doc_type_id,data.doc_polda_id)
  }
  const toListDocument = () => {
    navigate("/document/list-document");
  };

  const onChangeTemplate = (event) => {
    const value = event.target.value;
    setTemplateID(value);
  };

  const uploadFile = async (url) => {
    
    setLoading(true);
    try {
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/pdf");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
      const requestOptions = {
        method: "PATCH",
        headers: myHeaders,
        body: file,
      };
      await fetch(url + "/@upload/doc_file", requestOptions)
        .then((response) => {
          
          if (response.status == 200) {
            
            // setTimeout(() => {
            //   showDocument(url);
            // }, 6000);
          }
          
          loadDocument(url)
        })
        .then((result) => console.log(result))
        .catch((error) => {
          setLoading(false);
          console.error(error)});
    } catch (err) {
      
      setLoading(false);
      setIsCreate(true);
      
    }
  };
  const postDatatoLocal = async () =>{
    if(!templateID || !file){
      errorAlert("File atau Template harus di pilih")
      return
    }
    setLoading(true);
    let url = document["@id"];
    let bodyPost = {};
    const config = helper.GetToken()
    let data = {}
    try{
      
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/pdf");
    myHeaders.append("Authorization", `Bearer ${userData.token}`);
    const requestOptions = {
      method: "GET",
      headers: myHeaders,
    };
      await fetch(url , requestOptions)
        .then(async (response) => {
          data = await response.json()
          
          if (response.status == 200) {
            bodyPost = {
              "@type":"Document",
              "id": data.id,
              "creation_date": data.creation_date,
              "modification_date": data.modification_date,
              "doc_id": data.doc_id,
              "doc_name": data.doc_name,
              "doc_file_size": data.doc_file_size,
              "doc_type_id": data.doc_type_id,
              "doc_type_name": data.doc_type_name,
              "doc_created_date": data.doc_created_date,
              "doc_deleted_date": data.doc_deleted_date,
              "doc_is_deleted": data.doc_is_deleted,
              "is_deleted": data.is_deleted,
              "doc_created_by_user": data.doc_created_by_user,
              "doc_modified_by_user": data.doc_modified_by_user,
              "doc_polda_id": data.doc_polda_id,
              "doc_polda_name": data.doc_polda_name,
              "doc_template_id": templateID,
              // "doc_active_version": data.doc_active_version,
              // "doc_before_version": data.doc_before_version,
              // "doc_last_version": data.doc_last_version,
              doc_active_version: data.doc_last_version+1,
              doc_before_version: data.doc_last_version,
              doc_last_version: data.doc_last_version+1,
              "doc_state": data.doc_state,
              "doc_img_split": data.doc_img_split,
              "doc_url": data.doc_url,
              "doc_polda_metadata": data.doc_polda_metadata,
              "doc_metadata_by_template": data.doc_metadata_by_template,
              "doc_metadata_additional": data.doc_metadata_additional,
              "doc_ocr_status": data.doc_ocr_status,
              "doc_data_location": 0,
              "doc_using_local_ocr": 1,
              "doc_satker_id": data.doc_satker_id,
              "doc_satker_name": data.doc_satker_name,
              "doc_satker_code": data.doc_satker_code,
              "type_name": "Document"
           }
           
          }
          
          
        })
        .then((result) => console.log(result))
        .catch((error) => console.error(error));
    }catch(e){
        console.log(e)
        setLoading(false);
        errorAlert("Terjadi Kesalahan")
    }
    // const urlLocal = url.replace(/^.*(?=\/db)/, "http://localhost:8069");
    const urlLocal = url.replace(/^.*(?=\/db)/, "http://localhost:8069").replace(/\/document\/.*/, '/document');
    const urlUpload= url.replace(/^.*(?=\/db)/, "http://localhost:8069");
    let axios_headers = {
      method: "POST",
      headers: {
          "Content-Type": "application/json",
          Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
      },
    };
    await axios
      
      .post(urlLocal ,bodyPost,axios_headers)
      .then((response) => {
        
        if (response.status == 201) {
          uploadFile(urlUpload)
        }else{
          errorAlert("Terjadi Kesalahan")
          setLoading(false);
        }
        
      })
      .catch((error) => {
        console.log(error);
        errorAlert("Terjadi Kesalahan")
        setLoading(false);
      });
    
    
  }
  const changeStateDocument = async (data) =>{
    
    let url = data["@id"];
    const urlLocal = url.replace(/^.*(?=\/db)/, "http://localhost:8069")
    
    try {
      
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const body_data = JSON.stringify({
          doc_state: 1,
          doc_polda_metadata : poldaMetadata?poldaMetadata:"[]",
          doc_modified_by_user : userid,
          doc_active_version: data.doc_last_version+1,
          doc_before_version: data.doc_last_version,
          doc_last_version: data.doc_last_version+1,
          
      });
    
      
      const requestOptions = {
        method: "PATCH",
        headers: myHeaders,
        body: body_data,
      };
      const response = await fetch(urlLocal, requestOptions);
      
      if (response.status == 200) {
       
        setLoading(false);
      }else{
        
        setLoading(false);
      }
      uploadFile(urlLocal)
    }catch(err){
      
      setLoading(false);
    }
    
  }

  const loadDocType = async (doc_type,doc_polda_id) => {
    setLoading(true)
    try {
      let url = base_url + "doctype/@list_doctype?doc_type_id="+doc_type;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      let options = {};

     
      var doctype = datas[0].doc_type_title
      setDocType(doctype)
      
      loadTemplate(doc_type,doc_polda_id)
    } catch (err) {
      // alert(`${t('error_um')}`);
      errorAlert('Service Tidak Aktif')
      setLoading(false)
    }
  };  

  
  const loadTemplate = async (type,doc_polda_id) => {
    try {
      let url = base_url + "templatemaster/@list_template?template_doc_type_id=" + type;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      let options = [];

      // setProvinsiRef(datas)
      
      datas.map(function (data, index) {
        options.push(
          <option key={"opt-" + index} value={data.id}>
            {data.template_name}
          </option>
        );
      });

      setTemplateOptionsHtml(options);
      loadPoldaMetadata(doc_polda_id)
    } catch (err) {
      
      setLoading(false)
    }
  };

  const loadPoldaMetadata = async (poldaID) => {
    try {
      let url = base_url + "polridocument/" + poldaID;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var data = await res.json();
     
      
      setPoldaMetadata(data.polda_metadata)
      setLoading(false)
    } catch (err) {
      
      setLoading(false)
    }
  };

  const loadDocument = async (url) => {
    setArrImages([])
    try {
      // let url = document["@id"];
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
     
      if (res.status == 200) {
        
        if (datas.doc_ocr_status == 0) {
     
          setTimeout(() => {
            loadDocument(url);
          }, 6000);
        
        } else {
          setPoldaMetadata(datas.doc_polda_metadata)
          setDocMetadata(datas.doc_metadata_by_template)
          let options = {};
          // let additional_metadata = datas.doc_metadata_additional
          // additional_metadata.map(function (data, index) {
          //   options[data.metadata_add_id] = {
          //     "name" : data.metadata_add_name,
          //     "value" : data.metadata_add_value,
          //     "id" : data.metadata_add_id
          //   }
          // });
          // setAddiMetadata(options)
         
       
          const images_ocr = datas.doc_url;
         
          let filterVersion = images_ocr.filter((e) => {
            return e.version === datas.doc_active_version;
          });
          
          setArrImages(filterVersion[0].ocr_file_path)
          // showImages(filterVersion[0].ocr_file_path)
          setLoading(false)
          setIsCreate(false)
        }
      }
      
    } catch (err) {
      
      setLoading(false)
    }
  };
  const showImages = async (images_ocr) => {
    setImgsOcr([]);
    const arr_len = images_ocr.length - 1;
    let arr_img = "";
    images_ocr.forEach((data, index) => {
      if (arr_len != index) {
        return;
      }
      arr_img = data.ocr_file_path;
    });
    

    arr_img.forEach((link, index) => {
      imgTohtml(link);
    });
    setLoading(false);
  };
  const imgTohtml = async (link) => {
    let authentication_data = {
      headers: {
        Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
      },
      responseType: "arraybuffer",
    };
    let arr_img_temp = imgsOcr;
    await axios
      .get(base_url + "" + link, authentication_data)
      .then(async (response) => {
        const imgsrc = _imageEncode(response.data);
  
        arr_img_temp.push(imgsrc);
        
        if (imgsOcr.length == 0) {
          
          setImgsOcr([imgsrc]);
        } else {
          
          setImgsOcr([...imgsOcr, imgsrc]);
        }
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  };

  function _imageEncode(arrayBuffer) {
    let b64encoded = btoa(
      [].reduce.call(
        new Uint8Array(arrayBuffer),
        function (p, c) {
          return p + String.fromCharCode(c);
        },
        ""
      )
    );

    let mimetype = "image/jpeg";
    let base64 = "data:" + mimetype + ";base64," + b64encoded;

    let mImage = new Image();
    mImage.src = base64;
    mImage.onload = function (evt) {
    };

    return base64;
  }
  const addVersion = async () => {
    // changeStateDocument()
    postDatatoLocal()
  }
  const btnCreate = () => (
    <button
      type="submit"
      onClick={() => {
        addVersion();
        
      }}
      class="btn btn-primary btn-md"
    >
      {t('process_um')}
    </button>
  );

  const btnFinish = () => (
    <button
      type="submit"
      onClick={() => {
        finishProccess();
      }}
      class="btn btn-success btn-md"
    >
      {t('fin_um')}
    </button>
  );
  const btnCancel = () => (
    <button
      type="submit"
      onClick={() => {
        finishProccess();
      }}
      class="btn btn-danger btn-md"
    >
      {t('cancel_um')}
    </button>
  );
  const finishProccess = () => {
    navigate("/document/list-document");
  }
    return (
        <div
          class="container-fluid"
          style={{
             height: "auto",
            padding: "100px 10px",
          }}
        >
          {loading ? <LoadSpinner /> : ""}
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{t('add_new_ver_td')}</h5>
              <hr></hr>
              <div class="row"></div>
              <div class="row mt-3">
                <div class="col-3">
                  <label class="col-form-label">{t('doc_name_td')}</label>
                  <div class="input-group">
                    <label>{document && document.doc_name}</label>
                  </div>
                </div>
                <div class="col-3">
                  <label class="  col-form-label">{t('titlle_dt')}</label>
                  <div class="input-group">
                
                    <label>{docType && docType}</label>
                  </div>
                </div>
    
                <div class="col-3">
                  <label class="col-form-label">{t('file_doc_td')}</label>
                  <div class="input-group">
                    <input
                      type="file"
                      class="form-control form-control"
                      accept="application/pdf"
                      onChange={(e) => setFile(e.target.files[0])}
                    />
                  </div>
                </div>
                <div class="col-3">
                  <label class="  col-form-label">{t('templat_t1')}</label>
                  <div class="input-group">
                    <select
                      class="form-select form-control"
                      id="template"
                      onChange={onChangeTemplate}
                    >
                      <option selected>{t('choose _um')} {t('templat_t1')}...</option>
                      {templateOptionsHtml}
                    </select>
                    {isCreate ? btnCreate() : ""}
                    {isCreate ?btnCancel() : btnFinish()}
                  </div>
                </div>
              </div>
              <div class="row mt-3">

                {arrImages.length === 0 ? (
                  <div></div>
                ) : (
                  // <CarouselDoc imgsOcr={imgsOcr} />
                  <ImageCarousel imgApiUrls={arrImages}  />
                )}
              </div>
              {isCreate ? (
                ""
              ) : (
                <div>
                  <div class="row mt-3">
                    <div class="col-12 p-3">
                      <hr></hr>
                      {t('tag_um')} {t('titlle_md')}
                      <hr></hr>
                    </div>
                    <div class="col-12 p-3">
                      <div class="row py-3">
                        <div class="col">
                          <label>{t('tag_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('res_um')}</label>
                        </div>
                        {/* <div class="col">
                          <label>Correction</label>
                        </div> */}
                      </div>
                      {
                        docMetadata && 
                        docMetadata.map((data,index) =>
                          <div class="row py-3">
                            <div class="col"><label>{data.metadata_tmp_name}</label></div>
                            <div class="col"><label>{data.metadata_tmp_value}</label></div>
                           
                          </div>
                        )
                      }
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-12 p-3">
                      <hr></hr>
                      {t('pol_md_td')}
                      <hr></hr>
                    </div>
                    <div class="col-12 p-3">
                      <div class="row py-3">
                        <div class="col">
                          <label>{t('tag_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('res_um')}</label>
                        </div>
                        {/* <div class="col">
                          <label>Correction</label>
                        </div> */}
                      </div>
                      {
                        poldaMetadata && 
                        poldaMetadata.map((data,index) =>
                          <div class="row py-3">
                            <div class="col"><label>{data.metadata_pol_name}</label></div>
                            <div class="col"><label>{data.metadata_pol_value}</label></div>
                           
                          </div>
                        )
                      }
                    </div>
                    
                  </div>
                  {/* <div class="row mt-3">
                    <div class="col-12 p-3">
                      <hr></hr>
                      {t('add_md_td')}
                      <hr></hr>
                    </div>
                    <div class="col-12 p-3">
                      <div class="row py-3">
                        <div class="col">
                          <label>{t('tag_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('val_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('actions_um')}</label>
                        </div>
                      </div> 
                      {
                        addiMetadata && 
                        Object.keys(addiMetadata).map((item, i) => (
                          <div class="row py-3">
                            <div class="col"><label>A{addiMetadata[item].name}</label></div>
                            <div class="col"><label>B{addiMetadata[item].value}</label></div>
                            <div class="col"><label>C{item}</label></div>
                          </div>
                       ))
                        
                      }
                    </div>
                    
                  </div> */}
                </div>
              )}
            </div>
          </div>
        </div>
    );
} 
import React, { useState, useEffect } from "react";
import { useNavigate,useLocation, useOutletContext,Link } from "react-router-dom";
import axios from "axios";
import LoadSpinner from "../../../components/Load/load";
//Dynamic
import CarouselDoc from "../../../components/Carousel";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
import Swal from 'sweetalert2'
import ImageCarousel from "../../../components/ImageCarousel";
import LocalStorageHelper from "../../../api/localStorageHelper";
function AddVersion() {

   let location = useLocation()
  const navigate = useNavigate();
  let base_url = process.env.REACT_APP_API_LOCAL;
  const { t } = useTranslation();
  const userData = LocalStorageHelper.getUserData()
 
  const { state } = location;
  const [viewData, setViewData] = useState(state)
  const [document, setDocument] = useState(null);
  const [docType, setDocType] = useState(null)
  const [templateOptionsHtml, setTemplateOptionsHtml] = useState([]);
  const [file, setFile] = useState(null);
  const [templateID, setTemplateID] = useState("");
  const [imgsOcr, setImgsOcr] = useState([]);
  const [loading, setLoading] = useState(false);
  const [poldaMetadata, setPoldaMetadata] = useState(null)
  const [docMetadata, setDocMetadata] = useState(null)
  const [addiMetadata, setAddiMetadata] = useState(null)
  const [isCreate, setIsCreate] = useState(true);
  const userid = localStorage.getItem("user");
  const token  = localStorage.getItem("mytoken");
  const [arrImages,setArrImages] = useState([])
  
  useEffect(() => {
    if (viewData) {
      loadState()
    }else{
        toListDocument()
    }
  },[viewData]);

  const errorAlert = (body) => {
      Swal.fire({
        title: 'Error!',
        text: ''+body,
        icon: 'error',
        confirmButtonText: 'OK'
      })
    
  }
  const loadState = () => {
    let data = viewData.document
    setDocument(data)
    
    loadDocType(data.doc_type_id,data.doc_polda_id)
  }
  const toListDocument = () => {
    navigate("/document/list-document");
  };

  const onChangeTemplate = (event) => {
    const value = event.target.value;
    setTemplateID(value);
  };

  const uploadFile = async (url) => {
    
    setLoading(true);
    try {
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/pdf");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
      const requestOptions = {
        method: "PATCH",
        headers: myHeaders,
        body: file,
      };
      await fetch(url + "/@upload/doc_file", requestOptions)
        .then((response) => {
          
          if (response.status == 200) {
            
            // setTimeout(() => {
            //   showDocument(url);
            // }, 6000);
          }
          
          loadDocument(url)
        })
        .then((result) => console.log(result))
        .catch((error) => {
          setLoading(false);
          console.error(error)});
    } catch (err) {
      
      setLoading(false);
      setIsCreate(true);
      
    }
  };
  const postDatatoLocal = async () =>{
    if(!templateID || !file){
      errorAlert("File atau Template harus di pilih")
      return
    }
    setLoading(true);
    let url = document["@id"];
    let bodyPost = {};
    const config = helper.GetToken()
    let data = {}
    try{
      
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/pdf");
    myHeaders.append("Authorization", `Bearer ${userData.token}`);
    const requestOptions = {
      method: "GET",
      headers: myHeaders,
    };
      await fetch(url , requestOptions)
        .then(async (response) => {
          data = await response.json()
          
          if (response.status == 200) {
            bodyPost = {
              "@type":"Document",
              "id": data.id,
              "creation_date": data.creation_date,
              "modification_date": data.modification_date,
              "doc_id": data.doc_id,
              "doc_name": data.doc_name,
              "doc_file_size": data.doc_file_size,
              "doc_type_id": data.doc_type_id,
              "doc_type_name": data.doc_type_name,
              "doc_created_date": data.doc_created_date,
              "doc_deleted_date": data.doc_deleted_date,
              "doc_is_deleted": data.doc_is_deleted,
              "is_deleted": data.is_deleted,
              "doc_created_by_user": data.doc_created_by_user,
              "doc_modified_by_user": data.doc_modified_by_user,
              "doc_polda_id": data.doc_polda_id,
              "doc_polda_name": data.doc_polda_name,
              "doc_template_id": templateID,
              // "doc_active_version": data.doc_active_version,
              // "doc_before_version": data.doc_before_version,
              // "doc_last_version": data.doc_last_version,
              doc_active_version: data.doc_last_version+1,
              doc_before_version: data.doc_last_version,
              doc_last_version: data.doc_last_version+1,
              "doc_state": data.doc_state,
              "doc_img_split": data.doc_img_split,
              "doc_url": data.doc_url,
              "doc_polda_metadata": data.doc_polda_metadata,
              "doc_metadata_by_template": data.doc_metadata_by_template,
              "doc_metadata_additional": data.doc_metadata_additional,
              "doc_ocr_status": data.doc_ocr_status,
              "doc_data_location": 0,
              "doc_using_local_ocr": 1,
              "doc_satker_id": data.doc_satker_id,
              "doc_satker_name": data.doc_satker_name,
              "doc_satker_code": data.doc_satker_code,
              "type_name": "Document"
           }
           
          }
          
          
        })
        .then((result) => console.log(result))
        .catch((error) => console.error(error));
    }catch(e){
        console.log(e)
        setLoading(false);
        errorAlert("Terjadi Kesalahan")
    }
    // const urlLocal = url.replace(/^.*(?=\/db)/, "http://localhost:8069");
    const urlLocal = url.replace(/^.*(?=\/db)/, "http://localhost:8069").replace(/\/document\/.*/, '/document');
    const urlUpload= url.replace(/^.*(?=\/db)/, "http://localhost:8069");
    let axios_headers = {
      method: "POST",
      headers: {
          "Content-Type": "application/json",
          Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
      },
    };
    await axios
      
      .post(urlLocal ,bodyPost,axios_headers)
      .then((response) => {
        
        if (response.status == 201) {
          uploadFile(urlUpload)
        }else{
          errorAlert("Terjadi Kesalahan")
          setLoading(false);
        }
        
      })
      .catch((error) => {
        console.log(error);
        errorAlert("Terjadi Kesalahan")
        setLoading(false);
      });
    
    
  }
  const changeStateDocument = async (data) =>{
    
    let url = data["@id"];
    const urlLocal = url.replace(/^.*(?=\/db)/, "http://localhost:8069")
    
    try {
      
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const body_data = JSON.stringify({
          doc_state: 1,
          doc_polda_metadata : poldaMetadata?poldaMetadata:"[]",
          doc_modified_by_user : userid,
          doc_active_version: data.doc_last_version+1,
          doc_before_version: data.doc_last_version,
          doc_last_version: data.doc_last_version+1,
          
      });
    
      
      const requestOptions = {
        method: "PATCH",
        headers: myHeaders,
        body: body_data,
      };
      const response = await fetch(urlLocal, requestOptions);
      
      if (response.status == 200) {
       
        setLoading(false);
      }else{
        
        setLoading(false);
      }
      uploadFile(urlLocal)
    }catch(err){
      
      setLoading(false);
    }
    
  }

  const loadDocType = async (doc_type,doc_polda_id) => {
    setLoading(true)
    try {
      let url = base_url + "doctype/@list_doctype?doc_type_id="+doc_type;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      let options = {};

     
      var doctype = datas[0].doc_type_title
      setDocType(doctype)
      
      loadTemplate(doc_type,doc_polda_id)
    } catch (err) {
      // alert(`${t('error_um')}`);
      errorAlert('Service Tidak Aktif')
      setLoading(false)
    }
  };  

  
  const loadTemplate = async (type,doc_polda_id) => {
    try {
      let url = base_url + "templatemaster/@list_template?template_doc_type_id=" + type;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      let options = [];

      // setProvinsiRef(datas)
      
      datas.map(function (data, index) {
        options.push(
          <option key={"opt-" + index} value={data.id}>
            {data.template_name}
          </option>
        );
      });

      setTemplateOptionsHtml(options);
      loadPoldaMetadata(doc_polda_id)
    } catch (err) {
      
      setLoading(false)
    }
  };

  const loadPoldaMetadata = async (poldaID) => {
    try {
      let url = base_url + "polridocument/" + poldaID;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var data = await res.json();
     
      
      setPoldaMetadata(data.polda_metadata)
      setLoading(false)
    } catch (err) {
      
      setLoading(false)
    }
  };

  const loadDocument = async (url) => {
    setArrImages([])
    try {
      // let url = document["@id"];
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
     
      if (res.status == 200) {
        
        if (datas.doc_ocr_status == 0) {
     
          setTimeout(() => {
            loadDocument(url);
          }, 6000);
        
        } else {
          setPoldaMetadata(datas.doc_polda_metadata)
          setDocMetadata(datas.doc_metadata_by_template)
          let options = {};
          // let additional_metadata = datas.doc_metadata_additional
          // additional_metadata.map(function (data, index) {
          //   options[data.metadata_add_id] = {
          //     "name" : data.metadata_add_name,
          //     "value" : data.metadata_add_value,
          //     "id" : data.metadata_add_id
          //   }
          // });
          // setAddiMetadata(options)
         
       
          const images_ocr = datas.doc_url;
         
          let filterVersion = images_ocr.filter((e) => {
            return e.version === datas.doc_active_version;
          });
          
          setArrImages(filterVersion[0].ocr_file_path)
          // showImages(filterVersion[0].ocr_file_path)
          setLoading(false)
          setIsCreate(false)
        }
      }
      
    } catch (err) {
      
      setLoading(false)
    }
  };
  const showImages = async (images_ocr) => {
    setImgsOcr([]);
    const arr_len = images_ocr.length - 1;
    let arr_img = "";
    images_ocr.forEach((data, index) => {
      if (arr_len != index) {
        return;
      }
      arr_img = data.ocr_file_path;
    });
    

    arr_img.forEach((link, index) => {
      imgTohtml(link);
    });
    setLoading(false);
  };
  const imgTohtml = async (link) => {
    let authentication_data = {
      headers: {
        Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
      },
      responseType: "arraybuffer",
    };
    let arr_img_temp = imgsOcr;
    await axios
      .get(base_url + "" + link, authentication_data)
      .then(async (response) => {
        const imgsrc = _imageEncode(response.data);
  
        arr_img_temp.push(imgsrc);
        
        if (imgsOcr.length == 0) {
          
          setImgsOcr([imgsrc]);
        } else {
          
          setImgsOcr([...imgsOcr, imgsrc]);
        }
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  };

  function _imageEncode(arrayBuffer) {
    let b64encoded = btoa(
      [].reduce.call(
        new Uint8Array(arrayBuffer),
        function (p, c) {
          return p + String.fromCharCode(c);
        },
        ""
      )
    );

    let mimetype = "image/jpeg";
    let base64 = "data:" + mimetype + ";base64," + b64encoded;

    let mImage = new Image();
    mImage.src = base64;
    mImage.onload = function (evt) {
    };

    return base64;
  }
  const addVersion = async () => {
    // changeStateDocument()
    postDatatoLocal()
  }
  const btnCreate = () => (
    <button
      type="submit"
      onClick={() => {
        addVersion();
        
      }}
      class="btn btn-primary btn-md"
    >
      {t('process_um')}
    </button>
  );

  const btnFinish = () => (
    <button
      type="submit"
      onClick={() => {
        finishProccess();
      }}
      class="btn btn-success btn-md"
    >
      {t('fin_um')}
    </button>
  );
  const btnCancel = () => (
    <button
      type="submit"
      onClick={() => {
        finishProccess();
      }}
      class="btn btn-danger btn-md"
    >
      {t('cancel_um')}
    </button>
  );
  const finishProccess = () => {
    navigate("/document/list-document");
  }
    return (
        <div
          class="container-fluid"
          style={{
             height: "auto",
            padding: "100px 10px",
          }}
        >
          {loading ? <LoadSpinner /> : ""}
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{t('add_new_ver_td')}</h5>
              <hr></hr>
              <div class="row"></div>
              <div class="row mt-3">
                <div class="col-3">
                  <label class="col-form-label">{t('doc_name_td')}</label>
                  <div class="input-group">
                    <label>{document && document.doc_name}</label>
                  </div>
                </div>
                <div class="col-3">
                  <label class="  col-form-label">{t('titlle_dt')}</label>
                  <div class="input-group">
                
                    <label>{docType && docType}</label>
                  </div>
                </div>
    
                <div class="col-3">
                  <label class="col-form-label">{t('file_doc_td')}</label>
                  <div class="input-group">
                    <input
                      type="file"
                      class="form-control form-control"
                      accept="application/pdf"
                      onChange={(e) => setFile(e.target.files[0])}
                    />
                  </div>
                </div>
                <div class="col-3">
                  <label class="  col-form-label">{t('templat_t1')}</label>
                  <div class="input-group">
                    <select
                      class="form-select form-control"
                      id="template"
                      onChange={onChangeTemplate}
                    >
                      <option selected>{t('choose _um')} {t('templat_t1')}...</option>
                      {templateOptionsHtml}
                    </select>
                    {isCreate ? btnCreate() : ""}
                    {isCreate ?btnCancel() : btnFinish()}
                  </div>
                </div>
              </div>
              <div class="row mt-3">

                {arrImages.length === 0 ? (
                  <div></div>
                ) : (
                  // <CarouselDoc imgsOcr={imgsOcr} />
                  <ImageCarousel imgApiUrls={arrImages}  />
                )}
              </div>
              {isCreate ? (
                ""
              ) : (
                <div>
                  <div class="row mt-3">
                    <div class="col-12 p-3">
                      <hr></hr>
                      {t('tag_um')} {t('titlle_md')}
                      <hr></hr>
                    </div>
                    <div class="col-12 p-3">
                      <div class="row py-3">
                        <div class="col">
                          <label>{t('tag_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('res_um')}</label>
                        </div>
                        {/* <div class="col">
                          <label>Correction</label>
                        </div> */}
                      </div>
                      {
                        docMetadata && 
                        docMetadata.map((data,index) =>
                          <div class="row py-3">
                            <div class="col"><label>{data.metadata_tmp_name}</label></div>
                            <div class="col"><label>{data.metadata_tmp_value}</label></div>
                           
                          </div>
                        )
                      }
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-12 p-3">
                      <hr></hr>
                      {t('pol_md_td')}
                      <hr></hr>
                    </div>
                    <div class="col-12 p-3">
                      <div class="row py-3">
                        <div class="col">
                          <label>{t('tag_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('res_um')}</label>
                        </div>
                        {/* <div class="col">
                          <label>Correction</label>
                        </div> */}
                      </div>
                      {
                        poldaMetadata && 
                        poldaMetadata.map((data,index) =>
                          <div class="row py-3">
                            <div class="col"><label>{data.metadata_pol_name}</label></div>
                            <div class="col"><label>{data.metadata_pol_value}</label></div>
                           
                          </div>
                        )
                      }
                    </div>
                    
                  </div>
                  {/* <div class="row mt-3">
                    <div class="col-12 p-3">
                      <hr></hr>
                      {t('add_md_td')}
                      <hr></hr>
                    </div>
                    <div class="col-12 p-3">
                      <div class="row py-3">
                        <div class="col">
                          <label>{t('tag_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('val_um')}</label>
                        </div>
                        <div class="col">
                          <label>{t('actions_um')}</label>
                        </div>
                      </div> 
                      {
                        addiMetadata && 
                        Object.keys(addiMetadata).map((item, i) => (
                          <div class="row py-3">
                            <div class="col"><label>A{addiMetadata[item].name}</label></div>
                            <div class="col"><label>B{addiMetadata[item].value}</label></div>
                            <div class="col"><label>C{item}</label></div>
                          </div>
                       ))
                        
                      }
                    </div>
                    
                  </div> */}
                </div>
              )}
            </div>
          </div>
        </div>
    );
}
export default AddVersion;