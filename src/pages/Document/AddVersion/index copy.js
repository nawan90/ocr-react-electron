import { useLocation, useNavigate } from "react-router-dom";
import LocalStorageHelper from "../../../api/localStorageHelper";
import { useEffect, useState } from "react";
import LoadSpinner from "../../../components/Load/load";
import ShowPdf from "../../Searching/View";
import axios from "axios";
import Swal from "sweetalert2";


const AddVersion = () =>{
  // const { t } = useTransition(); 
  
  const userData = LocalStorageHelper.getUserData();
  let location = useLocation();
  const navigate = useNavigate();
  const { state } = location;
  const [viewData, setViewData] = useState(state)
  const [document, setDocument] = useState(null);
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loading2, setLoading2] = useState(false);
  const [linkPdf, setLinkPdf] = useState(null);
  const [versionList, setVersionList] = useState({});
  const [versionActive, setVersionActive] = useState("");
  const [metadata, setMetadata] = useState(null)

  const [statusVersioning, setStatusVersioning] = useState("add")

  const [disableBtn, setDisableBtn] = useState(false);
  const [finish, setFinish] = useState(false);
  const [docUrl, setDocUrl] = useState("");



  const [metadataBaru, setMetadataBaru]= useState(null)
  const [linkPdfBaru, setLinkPdfBaru] = useState(null)
  const [documentBaru, setDocumentBaru] = useState(null)

  const [metadataVersi, setMetadataVersi]= useState(null)
  const [linkPdfVersi, setLinkPdfVersi] = useState(null)
  const [documentVersi, setDocumentVersi] = useState(null)

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.token}`,
    },
  };

  useEffect(() => {
    if (viewData) {
      loadState()
    }else{
        toListDocument()
    }
  },[viewData]);
  const toListDocument = () => {
    navigate("/document/list-document");
  };

  const loadState = () => {
    let data = viewData.document
    console.log("data : ",data)
    setDocument(data)
    let metadataActive = data.doc_metadata_by_template;
    if(data.doc_metadata_by_template_verification !== null ){
      
      metadataActive = data.doc_metadata_by_template_verification
    }
    
    setMetadata(metadataActive)
    let version = data.doc_active_version;
    setVersionActive(version);
    let url_data = data.doc_url;
    let filterVersion = "";
    let url_file = "";
    if (url_data) {
      filterVersion = url_data.filter((url) => {
        return url.version === version;
      });
      url_file = filterVersion[0].original_file_path;
    }
    const filteredData = url_data.filter((item) => item.version !== version);
    const formattedData = filteredData.reduce((acc, item) => {
      // Remove '/@download/file' from the original_file_path
      const cleanedFilePath = item.original_file_path.replace(
        "/@download/file",
        ""
      );

      // Store cleaned data
      acc[item.version] = { ...item, original_file_path: cleanedFilePath };
      return acc;
    }, {});

    // Set the formatted data in state
    setVersionList(formattedData);
    setDocument(data);
    setLinkPdf(url_file);
  }
  // const uploadFile = async (url) => {
    
  //   setLoading(true);
  //   try {
  //     const myHeaders = new Headers();
  //     myHeaders.append("Content-Type", "application/pdf");
  //     myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
  //     const requestOptions = {
  //       method: "PATCH",
  //       headers: myHeaders,
  //       body: file,
  //     };
  //     await fetch(url + "/@upload/doc_file", requestOptions)
  //       .then((response) => {
          
  //         if (response.status == 200) {
            
  //           // setTimeout(() => {
  //           //   showDocument(url);
  //           // }, 6000);
  //         }
          
  //         loadDocument(url)
  //       })
  //       .then((result) => console.log(result))
  //       .catch((error) => {
  //         setLoading(false);
  //         console.error(error)});
  //   } catch (err) {
      //ok
  //     setLoading(false);
  //     setIsCreate(true);
      
  //   }
  // };
  



  //// show document Exist
  const handleSelectVersion = (e) => {
    const value = e.target.value
    setStatusVersioning(value)
    setDocUrl(value);
    if(value !=="add"){
      showDocVersi(value)
    }
  };
  const showDocVersi = async (path) =>{
    const url = `${userData.myip}/db/dmsbackend${path}`;
    setLoading2(true)
    await axios.get(url,config)
            .then((response) => {
              const data = response.data
              setDocumentVersi(data)
              let metadataActive = data.doc_metadata_by_template;
              if(data.doc_metadata_by_template_verification != undefined){
                if(data.doc_metadata_by_template_verification !== null ){
                  metadataActive = data.doc_metadata_by_template_verification
                }
              }
              
              setMetadataVersi(metadataActive)
              const doc_url = `${path}/@download/file`
              setLinkPdfVersi(doc_url)
              setLoading2(false)
            }).catch((error) => {
              setLoading2(false)
            })

  } 
  const prosesVersioning = (e)=>{
    let msg = "Apakah Anda Yakin Ingin Mengganti Versi Dokumen"
    if(statusVersioning == "add"){
      msg = "Apakah Anda Yakin Ingin Menambah  Versi Dokumen"
    }
    Swal.fire({
      title: 'Konfirmasi ',
      text: msg,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Saya Yakin',
      cancelButtonText: 'Tidak, Batalkan!',
    }).then((result) => {
      if (result.isConfirmed) {
        // User clicked "Yes"
        if(statusVersioning == "add"){

        }else{
          changeVersion()
        }
      } 
    });
    
  }
  const AddVersion = async () => {
    if(!file){
      Swal.fire("Error!","Silahkan Pilih File Dokumen!","error")
      return
    }
    Swal.fire({
      title: 'Konfirmasi ',
      text: 'Anda Yakin?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Saya Yakin',
      cancelButtonText: 'Tidak, Batalkan!',
    }).then((result) => {
      if (result.isConfirmed) {
        // User clicked "Yes"
        patchAddVersion()
        
      } 
    });
  }
  const patchAddVersion = async ()=>{
    setLoading2(true)
    setDocumentBaru(null)
    const url = document['@id']
    const body = {
      
      "doc_active_version": document.doc_last_version+1,
      "doc_before_version": document.doc_active_version,
      "doc_last_version": document.doc_last_version+1
    }
    
    await axios.patch(url,body,config)
      .then((response) => {
        console.log(response)
        uploadFile(url)
      }).catch((error) => {
        console.log(error)
        setLoading2(false)
      })
  }
  const uploadFile = async (url) => {
    
    setLoading2(true);
    try {
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/pdf");
      myHeaders.append("Authorization", `Bearer ${userData.token}`);
      const requestOptions = {
        method: "PATCH",
        headers: myHeaders,
        body: file,
      };
      await fetch(url + "/@upload/doc_file", requestOptions)
        .then((response) => {
          
          // if (response.status == 200) {
            
          //   // setTimeout(() => {
          //   //   showDocument(url);
          //   // }, 6000);
          // }
          
          loadDocumentBaru(url)
          
        })
        .catch((error) => {
          setLoading2(false);
          console.error(error)})
     
    } catch (err) {
      
      setLoading2(false);
      // setIsCreate(true);
      
    }
  };

  const loadDocumentBaru = async (url) => {
    setLoading2(true);
    await axios.get(url,config)
      .then((response) => {
        const data = response.data
        if(data.doc_ocr_status == 0){
          setTimeout(() => {
              loadDocumentBaru(url);
          }, 6000);

        }else{
          setDocumentBaru(data)
          setMetadataBaru(data.doc_metadata_by_template)
          const doc_url = data.doc_url
          console.log("data Baru : ",data)
          const version1Object = doc_url.find(item => item.version === data.doc_active_version);
          console.log(" version1Object: ", version1Object)
          setLinkPdfBaru(version1Object.original_file_path)
          
          console.log(" setLinkPdfBaru : ",version1Object.original_file_path)
          setLoading2(false)
          setFinish(true)
        }
      }).catch((error) => {
        setLoading2(false)
      })
  }
  const changeVersion = async () => {
    setLoading(true);
    
    const getVersionByOriginalPath = (data, filternya) => {
      const result = Object.values(data).find(
        (item) => item.original_file_path === filternya
      );
      return result ? result.version : null; // Returns the version or null if not found
    };
    const url = document["@id"];
    // Example usage
    const version = getVersionByOriginalPath(versionList, docUrl);
   
    const body = {
      doc_active_version: version,
      doc_before_version: document.doc_active_version,
    };
   
    await axios
      .patch(url, body, config)
      .then((response) => {
        
        setLoading(false);
        Swal.fire("info", "Versi Sukses diganti", "success");
        setFinish(true);
      })
      .catch((error) => {
        setLoading(false);
        Swal.fire("info", "Terjadi Kesalahan", "error");
      });
  };
  const finishProses = (e) => {
    navigate("/document/list-document");
  };


  return (
    <div  class="container-fluid"
      style={{
        height: "auto",
        padding: "100px 10px",
        width: "100%",
      }}>
        {loading ? <LoadSpinner /> : ""}
        <div class="card p-2">
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <h5 class="card-title  textfamily">Versi Dokumen</h5>
              </div>
            </div>
            
            <div class="row ">
              <div class="col-6">
                <div class="card mt-2">
                  <div class="card-body">
                    <h6 class="card-title my-4 ">
                      <p class="textfamily">
                        Dokumen Versi {document && document.doc_active_version}
                      </p>
                    </h6>

                    <div class="row mt-1 bg-light">
                      <div class="col-3 ">
                        <label class="col-form-label fw-bolder">Nama Dokumen</label>
                        <div class="input-group">
                            <label>- {document && document.doc_name}</label>
                        </div>
                      </div>
                      <div class="col-3">
                        <label class=" col-form-label fw-bolder">Tipe Dokumen</label>
                        <div class="input-group">
                            <label>- {document && document.doc_type_name}</label>
                            {/* <label>{document && docType[document.doc_type_id]}</label> */}
                        </div>
                      </div>
                      <div class="col-3">
                        <label class="  col-form-label fw-bolder">Korwil</label>
                        <div class="input-group">
                            <label>- {document && document.doc_polda_name}</label>
                        </div>
                      </div>
                      <div class="col-3">
                        <label class="  col-form-label fw-bolder">Satker</label>
                        <div class="input-group">
                            <label>- {document && document.doc_satker_name}</label>
                        </div>
                      </div>
                    </div>
                    <hr></hr>
                    <div class="row mt-3 ">
                      <div class="col-12 px-2">
                        <table class="table table-striped table-hover">
                          <thead className="thead-dark">
                            <tr>
                              <th>Tagging</th>
                              <th>Result</th>
                            </tr>
                          </thead>
                          <tbody>
                            {metadata && metadata.map((item, index) => (
                              <tr key={index}>
                                <td>{item.metadata_tmp_name}</td>
                                <td>{item.metadata_tmp_value || "N/A"}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                      
                    </div>
                    <hr></hr>
                    <div class="row mt-3">
                      
                      {linkPdf && <ShowPdf link={linkPdf}></ShowPdf>}
                      
                    </div>

                  

                
                  
                  </div>
                </div>
              </div>
              <div class="col-6">
              <div class="row ">
                <div class="card mt-2">
                  <div class="card-body" style={{ height: "60px;" }}>
                    <div class="row">
                      <div class="col-4">
                        {/* <label class="label">Pilih Versi</label> */}
                        <select onChange={handleSelectVersion} class="me-2 form-control">
                          <option value="add" key="add" selected>Tambah Versi</option>
                          {Object.keys(versionList).map((version) => (
                            <option
                              key={version}
                              value={versionList[version].original_file_path}
                            >
                              Versi {version}
                            </option>
                          ))}
                        </select>
                      </div>
                      <div class="col-2">
                        {
                          (statusVersioning !=="add" && !finish) ?
                          (<button
                            class="btn btn-primary btn-md me-2 form-control "
                            disabled={disableBtn}
                            onClick={(e) => {
                              prosesVersioning(e);
                            }}
                          >
                            Proses
                          </button>):("")
                        }
                        {finish && (
                          <button
                            class="btn btn-success btn-sm "
                            onClick={(e) => {
                              finishProses(e);
                            }}
                          >
                            Selesai
                          </button>
                        )}
                      </div>
                      <div class="col-4"></div>
                      <div class="col-2">
                        <button class="btn btn-md btn-danger" 
                        onClick={(e) => {
                          finishProses(e);
                        }}>Keluar</button>
                      </div>
                    </div>
                   
                    
                   
                    
                  </div>
                </div>
              </div>
              {
                statusVersioning == "add"?
                (<div class="card mt-2">
                  {
                    loading2 ? (<LoadSpinner/>):("")
                  }
                  <div class="card-body">
                   
                  {
                    !finish?
                    (<div class="row mt-1 bg-light">
                      <div class="col-5 ">
                        <label class="col-form-label fw-bolder">Pilih File Dokument</label>
                        <div class="input-group">
                            <input type="file" 
                               accept="application/pdf"
                               onChange={(e) => setFile(e.target.files[0])}
                               />
                        </div>
                      </div>
                    
                      <div class="col-3">
                      <label class="col-form-label fw-bolder">  </label>
                        <div class="input-group">
                          {
                            !finish? 
                            (<button class="btn btn-success btn-md" onClick={() => {AddVersion()}}>Tambah Versi</button>) :("")
                          }
                          
                        </div>
                        
                      </div>
                    </div>):
                    (<div class="row mt-1 bg-light">
                      <div class="col-3 ">
                        <label class="col-form-label fw-bolder">Nama Dokumen</label>
                        <div class="input-group">
                            <label>- {documentBaru && documentBaru.doc_name}</label>
                        </div>
                      </div>
                      <div class="col-3">
                        <label class=" col-form-label fw-bolder">Tipe Dokumen</label>
                        <div class="input-group">
                            <label>- {documentBaru && documentBaru.doc_type_name}</label>
                            {/* <label>{document && docType[document.doc_type_id]}</label> */}
                        </div>
                      </div>
                      <div class="col-3">
                        <label class="  col-form-label fw-bolder">Korwil</label>
                        <div class="input-group">
                            <label>- {documentBaru && documentBaru.doc_polda_name}</label>
                        </div>
                      </div>
                      <div class="col-3">
                        <label class="  col-form-label fw-bolder">Satker</label>
                        <div class="input-group">
                            <label>- {documentBaru && documentBaru.doc_satker_name}</label>
                        </div>
                      </div>
                    </div>)
                  }
                    
                    
                    <hr></hr>
                    <div class="row mt-3 ">
                      <div class="col-12 px-2">
                        <table class="table table-striped table-hover">
                          <thead className="thead-dark">
                            <tr>
                              <th>Tagging</th>
                              <th>Result</th>
                            </tr>
                          </thead>
                          <tbody>
                            {metadataBaru && metadataBaru.map((item, index) => (
                              <tr key={index}>
                                <td>{item.metadata_tmp_name}</td>
                                <td>{item.metadata_tmp_value || "N/A"}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>                      
                    </div>
                    <hr></hr>
                    <div class="row mt-3">
                      
                      {linkPdfBaru && <ShowPdf link={linkPdfBaru}></ShowPdf>}
                      
                    </div>

                  

                
                  
                  </div>
                </div>):
                (<div class="card mt-2">
                  <div class="card-body">
                   

                    
                  <div class="row mt-1 bg-light">
                      <div class="col-3 ">
                        <label class="col-form-label fw-bolder">Nama Dokumen</label>
                        <div class="input-group">
                            <label>- {documentVersi && documentVersi.doc_name}</label>
                        </div>
                      </div>
                      <div class="col-3">
                        <label class=" col-form-label fw-bolder">Tipe Dokumen</label>
                        <div class="input-group">
                            <label>- {documentVersi && documentVersi.doc_type_name}</label>
                            {/* <label>{document && docType[document.doc_type_id]}</label> */}
                        </div>
                      </div>
                      <div class="col-3">
                        <label class="  col-form-label fw-bolder">Korwil</label>
                        <div class="input-group">
                            <label>- {documentVersi && documentVersi.doc_polda_name}</label>
                        </div>
                      </div>
                      <div class="col-3">
                        <label class="  col-form-label fw-bolder">Satker</label>
                        <div class="input-group">
                            <label>- {documentVersi && documentVersi.doc_satker_name}</label>
                        </div>
                      </div>
                    </div>
                    <hr></hr>
                    <div class="row mt-3 ">
                      <div class="col-12 px-2">
                        <table class="table table-striped table-hover">
                          <thead className="thead-dark">
                            <tr>
                              <th>Tagging</th>
                              <th>Result</th>
                            </tr>
                          </thead>
                          <tbody>
                            {metadataVersi && metadataVersi.map((item, index) => (
                              <tr key={index}>
                                <td>{item.metadata_tmp_name}</td>
                                <td>{item.metadata_tmp_value || "N/A"}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                      
                    </div>
                    <hr></hr>
                    <div class="row mt-3">
                      
                      {linkPdfVersi && <ShowPdf link={linkPdfVersi}></ShowPdf>}
                      
                    </div>

                  

                
                  
                  </div>
                </div>)
              }
              
            </div>
            </div>
          </div>
        </div>
    </div>
  )
}
export default AddVersion;