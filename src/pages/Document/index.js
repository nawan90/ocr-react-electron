import React, { useState } from "react";
//import "../../assets/css/ocr.css";
//import "../../assets/css/table-list.css";
import { useNavigate, useLocation } from "react-router-dom";
 import icList from "../../assets/images/list_doc.png";
import icCreate from "../../assets/images/icon/doc_single.png";
import { useTranslation } from "react-i18next";

function Document() {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const toCreateDocument = () => {
    navigate("create-document");
  };

  const toListDocument = () => {
    navigate("list-document");
  };
 
  return (
    <div id="layoutAuthentication">
      <div
        class="container-xxl"
        style={{
           height: "90vh",
          padding: "300px 0px",
        }}
      >
        <div class="row ">
          <div class="col-3"></div>
          <div class="col-3" onClick={toListDocument}>
            <div
              class="card bgcard2 card-animate mb-3"
              style={{ maxwidth: "18rem" }}
            >
              <div class="card-header ">
                <h5 class="float-start text-white">
                  {t("list_um")} {t("dokumen_t1")}
                </h5>
              </div>
              <div class="card-body">
                <div class="float-end">
                  <img src={icList} width={"80%"} />
                </div>
              </div>
            </div> 
          </div>

          <div class="col-3 " onClick={toCreateDocument}>
            <div
              class="card bgcard2 card-animate mb-3"
              style={{ maxwidth: "18rem" }}
            >
              <div class="card-header ">
                <h5 class="float-start text-white">
                  {t("create_um")} {t("dokumen_t1")}
                </h5>
              </div>
              <div class="card-body">
                <div class="float-end">
                  <img src={icCreate} width={"80%"} />
                </div>
              </div>
            </div>
          </div>
          <div class="col-3"></div>
        </div>
      </div>
    </div>
  );
}

export default Document;
