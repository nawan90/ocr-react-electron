import React, { useState, useEffect } from "react";
import {
  useNavigate,
  useLocation,
  useOutletContext,
  Link,
} from "react-router-dom";
import axios from "axios";
import LoadSpinner from "../../../components/Load/load";
//Dynamic
import CarouselDoc from "../../../components/Carousel";
//Static
// import CarouselDoc from "../../../components/Carousel/statis";
import ShowPdf from "../../Searching/View";
import { useTranslation } from "react-i18next";
import { Card, CardBody, CardHeader } from "reactstrap";
import ShowDocument from "../../../components/ImageCarousel/ShowDocument";
import helper from "../../../api/helper";
import Swal from "sweetalert2";
import LocalStorageHelper from "../../../api/localStorageHelper";
function Compare() {
   let location = useLocation();
  const navigate = useNavigate();
  let base_url = process.env.REACT_APP_API_URL;
  const { t } = useTranslation();
  const userData = LocalStorageHelper.getUserData()
  const { state } = location;
  const [viewData, setViewData] = useState(state);
  const [loading, setLoading] = useState(false);

  const [disableBtn, setDisableBtn] = useState(false);
  const [document, setDocument] = useState(null);

  const [docType, setDocType] = useState(null);
  const [template, setTemplate] = useState(null);
  const [linkPdf, setLinkPdf] = useState(null);
  const [docUrl, setDocUrl] = useState("");
  const [versionList, setVersionList] = useState({});
  const [versionActive, setVersionActive] = useState("");
  const [finish, setFinish] = useState(false);

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.token}`,
    },
  };
  useEffect(() => {
    if (viewData) {
      loadState();
    } else {
      toListDocument();
    }
  }, [viewData]);

  const loadState = () => {
    let data = viewData.document;
    let version = data.doc_active_version;
    setVersionActive(version);
    let url_data = data.doc_url;
 
    let filterVersion = "";
    let url_file = "";
    if (url_data) {
      filterVersion = url_data.filter((url) => {
        return url.version === version;
      });
      url_file = filterVersion[0].original_file_path;
    }
    const filteredData = url_data.filter((item) => item.version !== version);
    const formattedData = filteredData.reduce((acc, item) => {
      // Remove '/@download/file' from the original_file_path
      const cleanedFilePath = item.original_file_path.replace(
        "/@download/file",
        ""
      );

      // Store cleaned data
      acc[item.version] = { ...item, original_file_path: cleanedFilePath };
      return acc;
    }, {});

    // Set the formatted data in state
    setVersionList(formattedData);
    setDocument(data);
    setLinkPdf(url_file);
    loadDocType(data.doc_type_id, data.doc_template_id);
  };
  const toListDocument = () => {
    navigate("/document/list-document");
  };
  const handleSelectVersion = (e) => {
    setDocUrl(e.target.value);
  };
  const changeVersion = (e) => {
    setLoading(true);
    
    const getVersionByOriginalPath = (data, filternya) => {
      const result = Object.values(data).find(
        (item) => item.original_file_path === filternya
      );
      return result ? result.version : null; // Returns the version or null if not found
    };
    const url = document["@id"];
    // Example usage
    const version = getVersionByOriginalPath(versionList, docUrl);
   
    const body = {
      doc_active_version: version,
      doc_before_version: document.doc_active_version,
    };
   
    axios
      .patch(url, body, config)
      .then((response) => {
        
        setLoading(false);
        Swal.fire("info", "Versi Sukses diganti", "success");
        setFinish(true);
      })
      .catch((error) => {
        setLoading(false);
        Swal.fire("info", "Terjadi Kesalahan", "error");
        console.error(
          "Error updating resource:",
          error.response ? error.response.data : error.message
        );
      });
  };
  const finishProses = (e) => {
    navigate("/document/list-document");
  };

  const loadDocType = async (type, template) => {
    setLoading(true);
    try {
      let url = userData.myip + "/db/dmsbackend/doctype/@list_doctype?doc_type_id=" + type;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", `Bearer ${userData.token}`);

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      let options = {};

      var doc_type = datas[0].doc_type_title;
      setDocType(doc_type);
      loadTemplate(template);
    } catch (err) {
      alert(`${t("error_um")}`);
      setLoading(false);
    }
  };

  const loadTemplate = async (template) => {
    try {
      let url = userData.myip + "/db/dmsbackend/templatemaster/@list_template?template_id=" + template;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", `Bearer ${userData.token}`);

      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      let options = {};

      // datas.map(function (data, index) {
      //   options[data.id] = data
      // });
      var doc_template = datas[0].template_name;

      setTemplate(doc_template);
      setLoading(false);
      // loadState()
    } catch (err) {
      setLoading(false);
      console.log(`${t("error_um")}`, err);
    }
  };

  return (
    <div
      class="container-fluid"
      style={{
         height: "auto",
        padding: "100px 10px",
        width: "100%",
      }}
    >
      {loading ? <LoadSpinner /> : ""}
      <div class="card p-2">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <h5 class="card-title  textfamily">Bandingkan Dokument</h5>
            </div>
          </div>

          <div class="row ">
            <div class="col-6">
              <div class="card mt-2">
                <div class="card-body">
                  <h6 class="card-title my-4 ">
                    <p class="textfamily">
                      {t("dokumen_t1")} Versi {versionActive}
                    </p>
                  </h6>

                  <div class="row mt-1 bg-light">
                    <div class="col-4 ">
                      <label class="col-form-label fw-bolder">
                        {t("doc_name_td")}
                      </label>
                      <div class="input-group">
                        <label>- {document && document.doc_name}</label>
                      </div>
                    </div>
                    <div class="col-4">
                      <label class="  col-form-label fw-bolder">
                        {t("titlle_dt")}
                      </label>
                      <div class="input-group">
                        <label>- {document && docType}</label>
                        {/* <label>{document && docType[document.doc_type_id]}</label> */}
                      </div>
                    </div>
                    <div class="col-4">
                      <label class="  col-form-label fw-bolder">
                        {t("templat_t1")}
                      </label>
                      <div class="input-group">
                        <label>- {document && template}</label>
                      </div>
                    </div>
                  </div>
                  <hr></hr>
                  <div class="row mt-3">
                    {linkPdf && <ShowPdf link={linkPdf}></ShowPdf>}
                  </div>

                  <div class="row mt-1">
                    <div class="col-12 p-3">
                      <hr></hr>
                      <h6 class="card-title ">
                        <p class="text-decoration-underline text-primary">
                          {t("tag_um")} {t("titlle_md")}
                        </p>
                      </h6>
                    </div>
                  </div>
                  {/* WILL */}
                  <div class="row mt-1 bg-light">
                    <div class="col-6 ">
                      <label class="col-form-label fw-bolder">
                        {t("tag_um")}
                      </label>
                      {document &&
                        document.doc_metadata_by_template.map((data, index) => (
                          <div class="row py-1">
                            <div class="col">{data.metadata_tmp_name}</div>
                          </div>
                        ))}
                    </div>
                    <div class="col-6">
                      <label class="  col-form-label fw-bolder">
                        {t("res_um")}
                      </label>
                      {document &&
                        document.doc_metadata_by_template.map((data, index) => (
                          <div class="row py-1">
                            <div class="col">{data.metadata_tmp_value}</div>
                          </div>
                        ))}
                    </div>
                  </div>

                  {/* END */}
                  <div class="row mt-1">
                    <div class="col-12 p-3">
                      <hr></hr>
                      <h6 class="card-title ">
                        <p class="text-decoration-underline text-primary">
                          {t("pol_md_td")}
                        </p>
                      </h6>
                    </div>
                  </div>
                  {/* WILL */}
                  <div class="row mt-1 bg-light">
                    <div class="col-6 ">
                      <label class="col-form-label fw-bolder">
                        {t("tag_um")}
                      </label>
                      {document &&
                        document.doc_polda_metadata.map((data, index) => (
                          <div class="row py-3">
                            <div class="col">{data.metadata_pol_name}</div>
                          </div>
                        ))}
                    </div>
                    <div class="col-6">
                      <label class="  col-form-label fw-bolder">
                        {t("res_um")}
                      </label>
                      {document &&
                        document.doc_polda_metadata.map((data, index) => (
                          <div class="row py-3">
                            <div class="col">{data.metadata_pol_value}</div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* kolom ke 2 */}

            <div class="col-6">
              <div class="row ">
                <div class="card mt-2">
                  <div class="card-body" style={{ height: "60px;" }}>
                    <label>Pilih Versi</label>
                    <select onChange={handleSelectVersion} class="me-2">
                      <option value="">Select a version</option>
                      {Object.keys(versionList).map((version) => (
                        <option
                          key={version}
                          value={versionList[version].original_file_path}
                        >
                          Versi {version}
                        </option>
                      ))}
                    </select>
                    <button
                      class="btn btn-primary btn-sm me-2 "
                      disabled={disableBtn}
                      onClick={(e) => {
                        changeVersion(e);
                      }}
                    >
                      Gunakan Versi ini
                    </button>
                    {finish && (
                      <button
                        class="btn btn-success btn-sm "
                        onClick={(e) => {
                          finishProses(e);
                        }}
                      >
                        Selesai
                      </button>
                    )}
                  </div>
                </div>
              </div>
              {docUrl && <ShowDocument docUrl={docUrl} />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Compare;
