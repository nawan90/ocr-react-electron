import { useState, useEffect, useRef } from "react";
import DataListDocument from "../../../fakeData/listDocument.json";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
// import "../../../assets/css/table.css";
import Del from "../../../assets/images/icon/delete.png";
import View from "../../../assets/images/icon/info.png";
import Versi from "../../../assets/images/icon/versi.png";
import aksi from "../../../assets/images/icon/parameter.png";
import Compare from "../../../assets/images/icon/compare.png";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import { EditCell } from "../../../components/Table/editCell";
import Modal from "../../../components/Modal";
import ModalHapus from "../../../components/Modal/modalHapus";
import ModalOtp from "../../../components/Modal/modalOtp";
import { useNavigate, Link } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import IpNotValidPage from "../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import Swal from "sweetalert2";
import helper from "../../../api/helper";
import axios from "axios";
import Down from "../../../assets/images/icon/icon_down.png";
import lihat from "../../../assets/images/icon/icon_viewer.png";
import verifyDone from "../../../assets/images/icon/verify_done.png";
import verifyUndone from "../../../assets/images/icon/verify_undone.png";

import srch from "../../../assets/images/icon/search.png";
import add from "../../../assets/images/icon/add.png";
import clr from "../../../assets/images/icon/clear.png";
import excel from "../../../assets/images/icon/icon_excel.png";
import map from "../../../assets/images/icon/icon_maps.png";
import "../../../assets/css/modal.css";
import moment from "moment";
import "moment/locale/id";
import { use } from "i18next";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
// import ShowPdf from "../../Searching/View";
import ModalPdfView from "../../../components/Modal/modalPdfView";
import ButtonWithSelect from "../../../components/ButtonSelect/ButtonWithSelect";

import LocalStorageHelper from "../../../api/localStorageHelper";

function ListDocument() {
  const ip = localStorage.getItem("ipAddress");
  const mytoken = localStorage.getItem("mytoken");

  let base_url = process.env.REACT_APP_API_URL;
  const navigate = useNavigate();
  const [dataIp, setDataIp] = useState("");
  const [dataToken, setDataToken] = useState("");
  const [data, setData] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [mounted, setMounted] = useState(true);
  const [getData, setGetData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [permission, setPermission] = useState({
    permissionAdd: false,
    permissionModify: false,
    permissionView: false,
    permissionList: false,
    permissionVersion: false,
  });
  const [isAdmin, setIsAdmin] = useState(false);
  const [downloadStarted, setDownloadStarted] = useState(false);
  const [downloadDocName, setDownloadDocName] = useState("");
  const [downLoadUrl, setDownloadUrl] = useState(false);
  // const [permissionAdd, setPermissionAdd] = useState(false)
  // const [permissionModify, setPermissionModify] = useState(false)
  // const [permissionView, setPermissionView] = useState(false)
  // const [permissionList, setPermissionList] = useState(false)
  // const [permissionVersion, setPermissionVersion] = useState(false)
  const [isPolda, setIsPolda] = useState("");
  const [isSatker, setIsSatker] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  // const [accessAdd, setAccessAdd] = useState(false ? false :LocalStorageHelper.getUserData(["dms.document.add"])  )

  const { t } = useTranslation();
  const userData = LocalStorageHelper.getUserData();
  const toCreateDocument = (value) => {
    navigate("/document/create-document");
  };

  const toViewDocument = (value) => {
    navigate("/document/view-document", {
      state: {
        state: "view",
        document: value,
        isDownload: false,
        sourcepath: "/document/list-document",
      },
    });
  };
  const toAddVersionDocument = (value) => {
    navigate("/document/add-version-document", {
      state: {
        state: "view",
        document: value,
        sourcepath: "/document/list-document",
      },
    });
  };

  const toCompareDocument = (value) => {
    navigate("/document/compare-document", {
      state: {
        state: "view",
        document: value,
        sourcepath: "/document/list-document",
      },
    });
  };

  useEffect(() => {
    const accessAdd = JSON.parse(userData["dms.document.add"]);
    const accessModify = JSON.parse(userData["dms.document.modify"]);
    const accessView = JSON.parse(userData["dms.document.view"]);
    const accessVersion = JSON.parse(userData["dms.document.version"]);
    const accessList = JSON.parse(userData["dms.document.list"]);
    const satker = userData.satkerid;
    const polda = userData.poldaid;
    const isAdmin = userData.isAdmin;

    setIsAdmin(isAdmin);
    let permissionTemp = {
      permissionAdd: false,
      permissionModify: false,
      permissionView: false,
      permissionList: false,
      permissionVersion: false,
    };

    if (accessList) {
      permissionTemp.permissionList = true;
    }
    if (accessView) {
      permissionTemp.permissionView = true;
    }

    if ((satker && satker != null && satker != "null") || (polda && polda != null && polda != "null")) {
      if (accessAdd) {
        permissionTemp.permissionAdd = true;
      }

      if (accessVersion) {
        permissionTemp.permissionVersion = true;
      }

      if (accessModify) {
        permissionTemp.permissionModify = true;
      }

      setPermission(permissionTemp);
    }

    if (accessList) {
      showTable();
    }
  }, []);

  const handlerDelete = async (data) => {
    const url = data["@id"];
    const name = data["doc_name"];

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userData.token}`,
      },
    };

    await axios
      .delete(url, config)
      .then((response) => {
        Swal.fire("Info", `Document ${name} Telah dihapus`, "success");
        setTimeout(() => {
          showTable();
        }, 3000);
      })
      .catch((error) => {
        console.error("Error deleting resource:", error.response ? error.response.data : error.message);
      });
  };

  const showTable = async () => {
    const url = userData.myip + "/db/dmsbackend/polridocument/@list_document";
    try {
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", `Bearer ${userData.token}`);
      const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      // setData(datas);
      let modif = modifyData(datas);
      if (modif && modif.length > 0) {
        setData(modif);
      }
    } catch (err) {
      console.log(`${t("error_um")}`);
    }
  };

  const modifyData = (datas) => {
    if (datas) {
      const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];

      datas?.forEach((doc) => {
        const cd = new Date(doc.doc_created_date);
        doc.doc_created_date = cd.getDate() + " " + selBulan[cd.getMonth()] + " " + cd.getFullYear() + " " + cd.getHours() + ":" + cd.getMinutes();
      });

      // Sorting data berdasarkan modification_date secara descending (data terbaru di atas)
      return datas.sort((a, b) => new Date(b.doc_created_date) - new Date(a.doc_created_date));
    } else {
      return null;
    }
  };

  const handleExcel = (pilih) => {
    if (!pilih?.doc_metadata_by_template || pilih.doc_metadata_by_template.length === 0) {
      console.error("doc_metadata_by_template is missing or empty", pilih);
      return; // exit if it's not an array or it's empty
    }

    // Access the first document object (assumed only one document for this example)
    const doc = pilih;

    // Log the entire doc_metadata_by_template to check its structure

    // Prepare headers for the Excel file
    const headers = ["No Sertifikat", "Desa / Kelurahan", "Kecamatan", "Kabupaten", "Provinsi", "Suratukur Tanggal", "Suratukur No", "Suratukur Luas", "Penerbitan Sertifikat"];

    // Map metadata items to their column names
    const metadataMap = {
      shp_no_sertifikat: "No Sertifikat",
      shp_alamat_desa_kelurahan: "Desa / Kelurahan",
      shp_alamat_kecamatan: "Kecamatan",
      shp_alamat_kabupaten: "Kabupaten",
      shp_alamat_provinsi: "Provinsi",
      shp_suratukur_tanggal: "Suratukur Tanggal",
      shp_suratukur_no: "Suratukur No",
      shp_suratukur_luas: "Suratukur Luas",
      shp_penerbitan_sertifikat: "Penerbitan Sertifikat",
    };

    // Prepare sheet data
    const sheetData = [];

    // Loop through headers and fill in the corresponding metadata
    const rowData = headers.map((header) => {
      const metadataField = Object.keys(metadataMap).find((key) => metadataMap[key] === header);
      const item = doc.doc_metadata_by_template.find((item) => item.metadata_tmp_name === metadataField);
      return item ? item.metadata_tmp_value || "N/A" : "N/A";
    });

    // Push the row data to the sheet data array
    sheetData.push(rowData);

    // Create worksheet from the data
    const ws = XLSX.utils.aoa_to_sheet([headers, ...sheetData]);

    // Auto-adjust column widths based on the content
    const colWidths = [];
    headers.forEach((header, colIndex) => {
      // Find the longest value in the column (header + data)
      let maxLength = header.length;
      sheetData.forEach((row) => {
        if (row[colIndex] && row[colIndex].toString().length > maxLength) {
          maxLength = row[colIndex].toString().length;
        }
      });
      // Set column width (we add a bit of padding, e.g., 2 extra units)
      colWidths.push({ wpx: maxLength * 10 }); // wpx: width in pixels (multiply length by 10 for padding)
    });

    // Apply the calculated column widths
    ws["!cols"] = colWidths;

    // Create a new workbook and append the worksheet
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // Generate Excel file as an array (use 'array' type instead of 'blob')
    const excelData = XLSX.write(wb, { bookType: "xlsx", type: "array" });

    // Create a Blob from the array (this is the preferred way to handle Excel files)
    const excelBlob = new Blob([excelData], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

    // Generate file name
    const doc_name = doc?.doc_polda_name || "Unknown";

    // Log and check if 'shp_no_sertifikat' exists in the metadata
    const metadataItems = doc?.doc_metadata_by_template.filter((item) => item.metadata_tmp_name === "shp_no_sertifikat");

    // Check if metadata was found
    const no_sertifikat = metadataItems.length > 0 && metadataItems[0].metadata_tmp_value && metadataItems[0].metadata_tmp_value !== "N/A" ? metadataItems[0].metadata_tmp_value : "Unknown";

    // Log the final file name and check if it's correct
    const fileName = `${doc_name}_${no_sertifikat}.xlsx`;

    // Trigger the file download
    saveAs(excelBlob, fileName);
  };

  const handleViewPdf = (link) => {
    // setModalIsOpen(true);
    // setOriginalFile(link.doc_url?.original_file_path);
    // setGetData(link);
  };

  const onSetupOtp = (link) => {
    console.log("onSetupOtp", link);
    // handleDownloadPdf(link);
    const linkUrl = link.doc_url && link.doc_url.length > 0 ? link.doc_url[0].original_file_path : null;
    setDownloadDocName(link.doc_name);

    setGetData(link);
    if (linkUrl) {
      setDownloadUrl(linkUrl);
      setDownloadStarted(true);
    }
  };

  const handleDownload = (link) => {
    let idmodal = document.getElementById("btn_open_download");
    idmodal.click();
  };

  //** Backup */
  const handleDownloadPdf = (link) => {
    const linkUrl = link.doc_url && link.doc_url.length > 0 ? link.doc_url[0].original_file_path : null;
    setDownloadDocName(link.doc_name);

    setGetData(link);
    if (linkUrl) {
      setDownloadUrl(linkUrl);
      setDownloadStarted(true);
    }
  };

  const handleMaps = (link) => {
    console.log("link", link);
  
    // Check if doc_metadata_by_template exists
    if (link.doc_metadata_by_template) {
      if (link.doc_metadata_by_template_verification === null) {
        const provinsiData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_provinsi")?.metadata_tmp_value;
        const kabupatenData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_kabupaten")?.metadata_tmp_value;
        const kecamatanData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_kecamatan")?.metadata_tmp_value;
        const kelurahanData = link.doc_metadata_by_template.find((item) => item.metadata_tmp_name === "shp_alamat_desa_kelurahan")?.metadata_tmp_value;
  
        const mapUrl = `https://www.google.com/maps?q=${provinsiData}+${kabupatenData}+${kecamatanData}+${kelurahanData}`;
        
        // Check if it's Electron
        if (window.electronAPI?.isElectron) {
          window.electronAPI.openBrowserWindow(mapUrl, "_blank");  // Open map in Electron window
        } else {
          window.open(mapUrl, "_blank");  // Open map in a new browser tab
        }
      } else {
        const provinsiData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_provinsi")?.metadata_tmp_value;
        const kabupatenData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_kabupaten")?.metadata_tmp_value;
        const kecamatanData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_kecamatan")?.metadata_tmp_value;
        const kelurahanData = link.doc_metadata_by_template_verification.find((item) => item.metadata_tmp_name === "shp_alamat_desa_kelurahan")?.metadata_tmp_value;
        
        const mapUrl = `https://www.google.com/maps?q=${provinsiData}+${kabupatenData}+${kecamatanData}+${kelurahanData}`;
        
        // Check if it's Electron
        if (window.electronAPI?.isElectron) {
          window.electronAPI.openBrowserWindow(mapUrl, "_blank");  // Open map in Electron window
        } else {
          window.open(mapUrl, "_blank");  // Open map in a new browser tab
        }
      }
    } else {
      console.log("sudah verifikasi");
      const defaultMapUrl = "https://www.google.com/maps?q=Indonesia";
      
      // Check if it's Electron
      if (window.electronAPI?.isElectron) {
        window.electronAPI.openBrowserWindow(defaultMapUrl, "_blank");  // Open default map in Electron window
      } else {
        window.open(defaultMapUrl, "_blank");  // Open default map in a new browser tab
      }
    }
  };
  const runOCRVerification = (link) => {
    if (!link || !link.doc_url || link.doc_url.length === 0) {
        console.error("Invalid link or missing doc_url");
        return;
    }
// const args = `--${fileName} --${mytoken}`;
    const fileName = link.doc_url[0]?.filename_sent_to_mirableOCR;
    if (!fileName) {
        console.error("Filename not found in doc_url");
        return;
    }

    const args = `OCRVerificationCommand --${fileName} --${mytoken}`;
    console.log(`Executing OCR verification with args: ${args}`);

    if (window.electronAPI) {
        window.electronAPI.sendOCRVerificationCommand(args);

        // Listen for process feedback
        window.electronAPI.onOCRProcessOutput((data) => console.log(`Output: ${data}`));
        window.electronAPI.onOCRProcessError((error) => console.error(`Error: ${error}`));
        window.electronAPI.onOCRProcessClose((message) => console.log(`Process ended: ${message}`));
    } else {
        console.error("Electron API not available");
    }
};
 
  const columns = [
    {
      accessorKey: "doc_name",
      header: t("doc_name_td"),
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
      cell: function render({ getValue, renderValue, state, row, column, instance }) {
        return (
          <div
            style={{
              width: "100%",
              maxWidth: "600px",
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
            }}
          >
            {row.original.doc_name}
          </div>
        );
      },
    },
    {
      accessorKey: "doc_type_name",
      header: t("titlle_dt"),
      // cell: EditableCell,
      enableColumnFilter: true,
      filterFn: "includesString",
    },
    {
      accessorKey: "creation_date",
      header: t("create_date_um"),
      // cell: EditableCell,
      cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
      enableColumnFilter: true,
    },
    {
      accessorKey: "doc_created_by_user",
      header: t("pengguna_t1"),
      // cell: EditableCell,
      enableColumnFilter: true,
    },
    {
      accessorKey: "doc_polda_name",
      header: t("titlle_lp"),
      // cell: EditableCell,
      enableColumnFilter: true,
    },
    {
      id: "actions",
      accessorKey: "id",
      header: t("actions_um"),
      cell: function render({ getValue, state, row, column, instance }) {
        return (
          // <div
          //     style={{
          //         display: "flex",
          //         flexDirection: "row",
          //         gap: "4px",
          //         width: "150px",
          //     }}
          // >
          //     {(isAdmin || permission.permissionVersion) && (

          //     )}
          //     {(isAdmin || permission.permissionView) && (
          //         <button type="button" className="btn" title="Lihat" onClick={() => toViewDocument(row.original)}>
          //             <img src={View} width={"20"} />
          //         </button>
          //     )}
          //     {(isAdmin || permission.permissionVersion) && (

          //     )}
          //     {(isAdmin || permission.permissionModify) && (
          //         <button
          //             type="button"
          //             className="btn"
          //             title="Hapus"
          //             data-bs-toggle="modal"
          //             data-bs-target="#modalHapus"
          //             onClick={() =>
          //                 setGetData(row.original)
          //             }
          //         >
          //             <img src={Del} width={"20"} />
          //         </button>
          //     )}
          // </div>
          <div style={{ display: "flex", gap: "10px" }}>
            <button className="btn btn-sm" title="Verifikasi" onClick={() => runOCRVerification(row.original)}>
              <img src={verifyUndone} width={"30px"} />
            </button>
            <nav className="navbar navbar-expand-lg navbar-light ">
              <div className="container">
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav ms-auto">
                    {/* Dropdown Menu */}
                    <li className="nav-item dropdown">
                      <a className="nav-link dropdown-toggle d-flex align-items-center" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src={aksi} width={"25px"} />
                        &nbsp; Action
                      </a>
                      <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                        {/* <li>
                          <a className="dropdown-item">
                            <button className="btn btn-sm" title="Export to Xls" onClick={() => handleExcel(row.original)}>
                              <img src={excel} width={"30px"} />
                            </button>
                            Export
                          </a>
                        </li> */}
                        <li>
                          <a className="dropdown-item">
                            <button className="btn btn-sm" title="Export to Xls" onClick={() => handleExcel(row.original)}>
                              <img src={Versi} width={"25px"} />
                              &nbsp; version
                            </button>
                          </a>
                        </li>
                        <li>
                          <a className="dropdown-item">
                            <button className="btn btn-sm" title="Lihat" onClick={() => toViewDocument(row.original)}>
                              <img src={lihat} width={"30px"} />
                              &nbsp; View
                            </button>{" "}
                          </a>
                        </li>
                        <li>
                          <a className="dropdown-item">
                            <button type="button" className="btn btn-sm" title="Download" data-bs-toggle="modal" data-bs-target="#modalOTP" onClick={() => onSetupOtp(row.original)}>
                              <img src={Down} width={"32px"} />
                              &nbsp; Download
                            </button>
                          </a>
                        </li>
                        <li>
                          <a className="dropdown-item">
                            <button className="btn btn-sm" title="Maps" onClick={() => handleMaps(row.original)}>
                              <img src={map} width={"37px"} />
                              &nbsp; Maps
                            </button>{" "}
                          </a>
                        </li>
                        <li>
                          <a className="dropdown-item">
                            {(isAdmin || permission.permissionModify) && (
                              <button type="button" className="btn" title="Hapus" data-bs-toggle="modal" data-bs-target="#modalHapus" onClick={() => setGetData(row.original)}>
                                <img src={Del} width={"20"} />
                                &nbsp; Delete
                              </button>
                            )}
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            {/* <button type="button" className="btn" title="Compare" onClick={() => toCompareDocument(row.original)}>
                            <img src={Compare} width={"20"} />
                        </button>
                        <button type="button" title="Versi" className="btn" onClick={() => toAddVersionDocument(row.original)}>
                            <img src={Versi} width={"20"} />
                        </button> */}
            {/* buttonasli *
            <button className="btn btn-sm" title="Export to Xls"  onClick={() => runOCRVerification(row.original)}>
            <img src={verifyUndone} width={"30px"} />
            </button>
            <button className="btn btn-sm" title="Export to Xls" onClick={() => handleExcel(row.original)}>
              <img src={excel} width={"30px"} />
            </button>

            <button className="btn btn-sm" title="Lihat" onClick={() => toViewDocument(row.original)}>
              <img src={lihat} width={"30px"} />
            </button>
            <button type="button" className="btn btn-sm" title="Download" data-bs-toggle="modal" data-bs-target="#modalOTP" onClick={() => onSetupOtp(row.original)}>
              <img src={Down} width={"32px"} />
            </button>

            <button className="btn btn-sm" title="Maps" onClick={() => handleMaps(row.original)}>
              <img src={map} width={"37px"} />
            </button>
            {(isAdmin || permission.permissionModify) && (
              <button type="button" className="btn" title="Hapus" data-bs-toggle="modal" data-bs-target="#modalHapus" onClick={() => setGetData(row.original)}>
                <img src={Del} width={"20"} />
              </button>
            )}*/}
          </div>
        );
      },
    },
  ];

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting: sorting,
      globalFilter: filtering,
      columnFilters: columnFilters,
    },
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onGlobalFilterChange: setFiltering,
    onColumnFiltersChange: setColumnFilters,
    meta: {
      editedRows,
      setEditedRows,
      validRows,
      setValidRows,
      revertData: (rowIndex) => {
        // setData((old) =>
        //   old.map((row, index) =>
        //     index === rowIndex ? originalData[rowIndex] : row
        //   )
        // );
      },
      updateRow: (rowIndex) => {
        // updateRow(data[rowIndex].id, data[rowIndex]);
      },
      updateData: (rowIndex, columnId, value, isValid) => {
        setData((old) =>
          old.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...old[rowIndex],
                [columnId]: value,
              };
            }
            return row;
          })
        );
        setValidRows((old) => ({
          ...old,
          [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
        }));
      },
      addRow: () => {
        const id = Math.floor(Math.random() * 10000);
        const newRow = {
          id,
          studentNumber: id,
          name: "",
          dateOfBirth: "",
          major: "",
        };
        // addRow(newRow);
      },
      removeRow: (rowIndex) => {
        // deleteRow(data[rowIndex].id);
      },
      removeSelectedRows: (selectedRows) => {
        // selectedRows.forEach((rowIndex) => {
        //   deleteRow(data[rowIndex].id);
        // });
      },
    },
  });

  return (
    // <div class="container-fluid pt-3 " style={{ height: " auto", marginTop: "75px" }}>
    <div>
      {/* <h5 class="textfamily">
                {t("list_um")} {t("dokumen_t1")}
            </h5> */}
      <div>
        <div class="row">
          <div class="col-4">
            <Filters filtering={filtering} setFiltering={setFiltering} />
          </div>
          <div class="col-8">
            <div className="float-end">
              <ButtonWithSelect />
            </div>
          </div>
        </div>
        <button id="btn_open_download" style={{ display: "none" }} type="button" class="btn btn-warning col-2 mx-1" data-bs-toggle="modal" data-bs-target="#ModalPdfView"></button>
        <ModalOtp readyToDownload={handleDownload} />
        <ModalHapus handlerDelete={handlerDelete} getData={getData} keyboard="false" backdrop="static" />
        <Toaster
          position="bottom-left"
          reverseOrder={false}
          gutter={8}
          containerClassName=""
          containerStyle={{}}
          toastOptions={{
            // Define default options
            className: "",
            duration: 5000,
            style: {
              background: "#ffbaba",
              color: "#ff0000 ",
            },

            // Default options for specific types
            success: {
              duration: 3000,
              theme: {
                primary: "green",
                secondary: "black",
              },
            },
          }}
        />
        <ModalPdfView docName={downloadDocName} link={downLoadUrl} isDownload={true}></ModalPdfView>
        {/* {downloadStarted ? (<ModalPdfView link={downLoadUrl} isDownload={true}></ModalPdfView>) : null} */}
        {/* {downloadStarted ? (
                    // <div class="modal show " id="modalDownload"  aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
                    //     <div className="modal-dialog">
                    //         <div className="modal-content">
                    //             <div className="modal-header">

                    //                 <button type="button" id="closeModalDownload" className="btn-close" data-bs-dismiss="modal" ></button>
                    //             </div>
                    //             <div className="modal-body">
                    //                 <ShowPdf link={downLoadUrl} isDownload={true}></ShowPdf>
                    //             </div>
                    //         </div>
                    //     </div>
                    // </div>   
                    <ModalPdfView link={downLoadUrl} isDownload={true}></ModalPdfView>
                ) : null} */}

        <table className="table table-bordered table-striped table-hover">
          <thead className="table-primary">
            {table.getHeaderGroups().map((headerGroup) => (
              <tr key={headerGroup.id} className="  uppercase">
                {headerGroup.headers.map((header) => (
                  <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                    {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          {data === undefined || data.length == 0 ? (
            <tbody>
              <tr>
                <td colSpan="5" style={{ textAlign: "center", margin: "0 auto" }}>
                  <span
                    style={{
                      color: "red",
                      fontWeight: "bold",
                      fontSize: "16px",
                    }}
                  >
                    <IpNotValidPage />
                  </span>
                </td>
              </tr>
            </tbody>
          ) : (
            <tbody>
              {table.getRowModel().rows.map((row) => (
                <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                  {row.getVisibleCells().map((cell) => (
                    <td className="px-4 py-2" key={cell.id}>
                      {flexRender(cell.column.columnDef.cell, cell.getContext())}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          )}
        </table>
        <Paginations table={table} />
      </div>
    </div>
    // </div>
  );
}

export default ListDocument;

