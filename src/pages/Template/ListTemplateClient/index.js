import { useState, useEffect, useRef } from "react";
import DataListDocument from "../../../fakeData/listDocument.json";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
// import "../../../assets/css/table.css";
import Del from "../../../assets/images/icon/trash-white.svg";
import Dlt from "../../../assets/images/icon/delete.png";
import Detail from "../../../assets/images/icon/info.png";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import { EditCell } from "../../../components/Table/editCell";
import Modal from "../../../components/Modal";
import ModalHapusTemplate from "../../../components/Modal/modalHapusTemplate";
import { useNavigate, Link, createSearchParams } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import { useTranslation } from "react-i18next";
import Helper from "../../../api/helper";

function ListTemplate() {
     const language = localStorage.getItem("bahasa");

    let base_url = `${process.env.REACT_APP_API_URL + "/"}`; //"http://116.90.165.46:8069/db/dmsbackend/";
    const navigate = useNavigate();
    const { t } = useTranslation();

    const [data, setData] = useState([]);

    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [mounted, setMounted] = useState(true);
    const [getData, setGetData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isSynchronize, setIsSynchronize] = useState(false);
    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    const [sorting, setSorting] = useState([]);

    useEffect(() => {
         const ipServer = `http://localhost:8069/db/dmsbackend`; //ip ? `http://localhost:8069/db/dmsbackend` : "";

        setDataIp(ipServer);
        // const urlValid = () => {
        //     if (ip === "localhost") {
        //         setDataIp(base_url + "/@list_template"); // Assuming base_url is defined elsewhere
        //     } else {
        //         setDataIp(ipServer + "/@list_template");
        //     }
        // };

        // urlValid();
    }, [ip]);

    useEffect(() => {
         if (dataIp && !dataIp.includes("undefined")) {
            requestListTemplate();
        }
    }, [dataIp]);

    const toCreateTemplate = () => {
        // navigate({
        //   pathname: "/template/create-template",
        //   search: createSearchParams({ state: "new" }).toString(),
        // });
        navigate(`/template/create-template`, {
            state: {
                state: "new",
                template: null,
                sourcepath: "/template/list-template",
            },
        });
    };

    const columns = [
        {
            accessorKey: "template_name",
            header: t("templat_t1"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "template_doc_type_name",
            header: `${t("tipe_um")} ${t("templat_t1")}`,
            enableColumnFilter: true,
        },
        {
            accessorKey: "creation_date",
            header: t("create_date_um"),
            enableColumnFilter: true,
        },
        {
            accessorKey: "modification_date",
            header: t("modif_date_tt"),
            enableColumnFilter: true,
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            gap: "4px",
                            width: "100px",
                        }}
                    >
                        <button type="button" title="Detail" className="btn" onClick={() => onRowView(row.original)}>
                            <img src={Detail} width={"30%"} />
                        </button>
                        {/* <button
                            type="button"
                            title="Hapus"
                            className="btn"
                            data-bs-toggle="modal"
                            data-bs-target="#modalHapus"
                            onClick={() =>
                                 setGetData(row.original)
                            }
                        >
                            <img src={Dlt} width={"80%"} />
                        </button> */}
                    </div>
                );
            },
        },
    ];

    const onRowView = (value) => {
        navigate(`/template/create-template`, {
            state: {
                state: "edit",
                template: value,
                sourcepath: "/template/list-template",
            },
        });
    };

    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });

    useEffect(() => {
        // requestListTemplate();
        // showTable();
    }, []);

    const onHandleRefresh = async () => {
        requestListTemplate();
    };

    const requestListTemplate = async () => {
        try {
            setIsLoading(true);
 
            let datas = await Helper.GetListTemplateClient(dataIp + "/templatemaster/@list_template");
             if (datas.status == 200 || 201 || 203 || 204) {
                var modifyData = modifyDataListTemplate(datas.data);
                setData(modifyData);
            }
            setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
         }
    };

    const onSynchronize = async () => {
         try {
            setIsSynchronize(true);
            let datas = await Helper.SynchronizeTemplate(dataIp + "/@template_sync");
             setIsSynchronize(false);
            if (datas.status == 200 || 201 || 203 || 204) {
                requestListTemplate();
                // var modifyData = modifyDataListTemplate(datas.data);
                // setData(modifyData);
            }
        } catch (err) {
            setIsSynchronize(false);
         }
    };

    useEffect(() => {
        // requestListTemplate();
    }, [language]);

    const modifyDataListTemplate = (data) => {
        // const monthNames = selectBulan();
        const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];

        data?.forEach((tmpl) => {
            const cd = new Date(tmpl.creation_date);
            const md = new Date(tmpl.modification_date);

            tmpl.creation_date = cd.getDate() + " " + selBulan[cd.getMonth()] + " " + cd.getFullYear();
            tmpl.modification_date = md.getDate() + " " + selBulan[md.getMonth()] + " " + md.getFullYear();
        });
        return data;
    };

    return (
        <div class="container-fluid pt-3 " style={{  height: " auto", marginTop: "75px" }}>
            <div class="card">
                <h5 class="card-title px-3 pt-3 textfamily">
                    {t("list_um")} {t("templat_t1")}
                </h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-10">
                            <Filters filtering={filtering} setFiltering={setFiltering} />
                        </div>
                        <div class="col-2">
                            {isSynchronize ? (
                                <button type="button" className="btn btn-primary bg-opacity-70 text-white float-end mx-1">
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true">
                                        {" "}
                                    </span>
                                    <span> </span> {t("synchronize_tt")}...
                                </button>
                            ) : (
                                <button type="button" className="btn btn-primary bg-opacity-70 text-white float-end mx-1" onClick={onSynchronize}>
                                    🔄 {t("synchronize_tt")}
                                </button>
                            )}

                            {isLoading ? (
                                <button class="btn btn-primary float-end mx-1" type="button">
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true">
                                        {" "}
                                    </span>
                                    <span> </span>
                                    Loading...
                                </button>
                            ) : (
                                <button class="btn btn-primary float-end mx-1" type="button" onClick={onHandleRefresh}>
                                    ⟳ Refresh
                                </button>
                            )}
                        </div>
                        {/* <div class="col-1">
                            {isSynchronize ? (
                                <button type="button" className="btn btn-primary bg-opacity-70 text-white float-end">
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true">
                                        {" "}
                                    </span>
                                    <span> </span> {t("synchronize_tt")}...
                                </button>
                            ) : (
                                <button type="button" className="btn btn-primary bg-opacity-70 text-white float-end" onClick={() => onSynchronize()}>
                                    🔄 {t("synchronize_tt")}
                                </button>
                            )}
                        </div> */}
                    </div>
                    {/* <ModalHapusTemplate getData={getData} onConfirm={onConfirmHapus} /> */}
                    <Toaster
                        position="bottom-left"
                        reverseOrder={false}
                        gutter={8}
                        containerClassName=""
                        containerStyle={{}}
                        toastOptions={{
                            // Define default options
                            className: "",
                            duration: 5000,
                            style: {
                                background: "#ffbaba",
                                color: "#ff0000 ",
                            },

                            // Default options for specific types
                            success: {
                                duration: 3000,
                                theme: {
                                    primary: "green",
                                    secondary: "black",
                                },
                            },
                        }}
                    />
                    <table className="table table-bordered table-striped table-hover">
                        <thead className="table-primary">
                            {table.getHeaderGroups().map((headerGroup) => (
                                <tr key={headerGroup.id} className="  uppercase">
                                    {headerGroup.headers.map((header) => (
                                        <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                            {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>

                        <tbody>
                             {table.getRowModel().rows.map((row) => (
                                <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                    {row.getVisibleCells().map((cell) => (
                                        <td className="px-4 py-2" key={cell.id}>
                                            {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                        </td>
                                    ))}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <Paginations table={table} />
                </div>
            </div>
        </div>
    );
}

export default ListTemplate;
