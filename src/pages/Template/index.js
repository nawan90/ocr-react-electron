import React, { useState, useEffect } from "react";
// import "../../assets/css/ocr.css";
// import "../../assets/css/table-list.css";
import { useNavigate, useLocation, Link } from "react-router-dom";
import Maxtor from "../../assets/images/Maxtor_Blue.png";
import icList from "../../assets/images/list_template.png";
import icCreate from "../../assets/images/create_template.png";
import { useTranslation } from "react-i18next";

function Template() {
    const navigate = useNavigate();
    const { t } = useTranslation();
    const [arrRole, setArrRole] = useState(null);
    const [isAdmin, setIsAdmin] = useState(false);

    useEffect(() => {
        let username = localStorage.getItem("user");
        let role = localStorage.getItem("myrole");

        if (username && username.length > 0) {
            if (username == "root") {
                setIsAdmin(true);
            }

            if (role && role != "") {
                let roleArr = JSON.parse(role);
                if (roleArr && roleArr.length > 0) {
                    setArrRole(roleArr);
                    const subUser = roleArr.filter((str) => {
                        return str.includes("SATKER") || str.includes("POLDA");
                    });
                    if (subUser && subUser.length > 0) {
                        setIsAdmin(false);
                    }

                    const subAdmin = roleArr.filter((str) => {
                        return str.includes("superadmin");
                    });
                    if (subAdmin && subAdmin.length > 0) {
                        setIsAdmin(true);
                    }
                }
            }
        }
    }, []);

    const toCreateTemplate = () => {
        navigate("create-template");
    };

    const toListTemplate = () => {
        if (isAdmin) {
            navigate("list-template");
        } else {
            navigate("list-template-client");
        }
    };
 
    return (
        <div id="layoutAuthentication">
            <div
                class="container-xxl "
                style={{
                     height: "90vh",
                    padding: "300px 10px",
                }}
            >
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-3"></div>
                    {isAdmin ? null : <div class="col-1"></div>}
                    <div class="col-3" onClick={toListTemplate}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h5 class="float-start text-white">
                                    {t("list_um")} {t("templat_t1")}
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={icList} width={"80%"} />
                                </div>
                            </div>
                        </div>
                        {/* <div className=" " style={{ backgroundColor: "#acb40e" }}>
            <div>List User</div>
            <img src={icList} width={"10%"} />
          </div> */}
                    </div>

                    {isAdmin ? (
                        <div class="col-3 " onClick={toCreateTemplate}>
                            <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                                <div class="card-header ">
                                    <h5 class="float-start text-white">
                                        {t("create_um")} {t("templat_t1")}
                                    </h5>
                                </div>
                                <div class="card-body">
                                    <div class="float-end">
                                        <img src={icCreate} width={"80%"} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : null}
                    <div class="col-3"></div>
                </div>
            </div>
        </div>
    );
}

export default Template;
