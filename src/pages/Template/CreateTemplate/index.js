import React, { useState, useEffect } from "react";
import { Button, Card, CardBody, CardHeader, Col, Container, Form, Modal, ModalHeader, ModalBody, ModalFooter, Nav, NavItem, NavLink, Row, TabContent, TabPane, UncontrolledTooltip, Input, Spinner } from "reactstrap";
import classnames from "classnames";
//import '../../../assets/css/ocr.css';
// Import Content
// import ImageOcr from "./ImageOcr";
// import PdfOcr from "./PdfOcr";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import axios from "axios";
import { OcrTabContent } from "./OcrTabContent";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
import utils from "../../../utils";
// import React, { useState } from 'react';
// import { Card, CardBody, Col, Container, Nav, NavItem, NavLink, Row, TabContent, TabPane, UncontrolledTooltip } from "reactstrap";
// import { Link } from 'react-router-dom';
// import classnames from "classnames";
// import BreadCrumb from "../../../../Components/Common/BreadCrumb";

// // Import Content
// import UiContent from '../../../../Components/Common/UiContent';
// import ImageOcr from './ImageOcr';
// import PdfOcr from './PdfOcr';

const CreateTemplate = () => {
    let base_url = `${process.env.REACT_APP_API_URL + "/"}`; //"http://116.90.165.46:8069/db/dmsbackend/";
    const navigate = useNavigate();
    const { t } = useTranslation();

    // Arrow Nav tabs
    const [arrowNavTab, setarrowNavTab] = useState("1");
    const [templateId, setTemplateId] = useState("");
    const [templateName, setTemplateName] = useState("");
    const [documentTypeId, setDocumentTypeId] = useState("");
    const [documentTypeName, setDocumentTypeName] = useState("");
    // const [poldaId, setPoldaId] = useState("");
    // const [poldaName, setPoldaName] = useState("");
    const [docTypeOptionsHtml, setDocTypeOptionsHtml] = useState([]);
    const [poldaOptionsHtml, setPoldaOptionsHtml] = useState([]);
    const [poldaRef, setPoldaRef] = useState(null);
    const [docTypeRef, setDocTypeRef] = useState(null);
    const [searchparams] = useSearchParams();
    const [stateTemplate, setStateTemplate] = useState(null);
    const [pathTemplate, setPathTemplate] = useState();

    const [image, setImage] = useState({
        image: { src: null },
        width: 0,
        height: 0,
    });
    const [dataFile, setDataFile] = useState(null);
    const [resultFileApi, setResultFileApi] = useState(null);
    const [candId, setCandId] = useState("");
    const [resultOcr, setResultOcr] = useState(null);
    const [annotations, setAnnotations] = useState([]);
    const [newAnnotation, setNewAnnotation] = useState([]);
    const [taggingData, setTaggingData] = useState([]);
    const [message_succes, setmessage_success] = useState(false);
    const [modal_success, setmodal_success] = useState(false);
    const [isUploadProcess, setIsUploadProcess] = useState(false);
    const [isSplitResult, setIsSplitResult] = useState(false);
    const [isOcrProcess, setIsOcrProcess] = useState(false);
    const [activeArrowTab, setactiveArrowTab] = useState(1);
    const [passedarrowSteps, setPassedarrowSteps] = useState([1]);
    const [titleOcrProcess, setTitleOcrProcess] = useState("Status OCR");
    const [stateProcessOcr, setStateProcessOcr] = useState(0);
    const [stateLoadTag, setStateLoadTag] = useState(0);
    const [templateUrl, setTemplateUrl] = useState("");
    const [isPreprocessingOcr, setIsPreprocessingOcr] = useState(false);
    const [isOcrFinished, setIsOcrFinished] = useState(false);
    const [metadataRef, setMetadataRef] = useState(null);

    const changeTemplateName = (e) => setTemplateName(e.target.value);

    const [dataPdf, setDataPdf] = useState([]);

    let location = useLocation();
    const { state } = location;
    const [editData, setEditData] = useState(state);
    const [sourcePath, setSourcePath] = useState(null);
    const [myToken, setMyToken] = useState("");
    const [dataIp, setDataIp] = useState("");
    const token = localStorage.getItem("mytoken");
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
         // let token = await localStorage.getItem("mytoken");
        setMyToken(token);
        const ipServer = ip ? `${ip}/db/dmsbackend` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip, token]);

    useEffect(() => {
         if (dataIp && !dataIp.includes("undefined")) {
            loadDocumentType();
            loadMetadata();
            if (editData) {
                 loadState();
            }
        }
    }, [dataIp, editData]);

    // useEffect(() => {
     //     // loadDocumentType();
    //     // loadMetadata();
    //     if (editData) {
    //         loadState();
    //     }
    // }, [editData]);

    const loadState = () => {
         setStateTemplate(editData.state);
        if (editData.template) {
            setPathTemplate(editData.template.path);
            // loadTemplate(editData.template.path)
            setTemplateUrl(dataIp + editData.template.path);
            setTemplateId(editData.template.template_id);
            setTemplateName(editData.template.template_name);
            setDocumentTypeId(editData.template.template_doc_type_id);
            setDocumentTypeName(editData.template.template_doc_type_name);
            getPageSplit(dataIp + editData.template.path);

            if (editData.hasOwnProperty("sourcepath")) {
                setSourcePath(editData.sourcepath);
            }
        }
    };

    function toggleArrowTab(tab) {
        if (activeArrowTab !== tab) {
            var modifiedSteps = [...passedarrowSteps, tab];

            if (tab >= 1 && tab <= 30) {
                setactiveArrowTab(tab);
                setPassedarrowSteps(modifiedSteps);
            }
        }
    }

    function tog_success() {
        setmodal_success(!modal_success);
    }

    const [modal_xlarge, setmodal_xlarge] = useState(false);
    function tog_xlarge() {
        setmodal_xlarge(!modal_xlarge);
    }

    const changeDocumentType = (e) => {
        if (docTypeRef) {
            let id = e.target.value;
            let obj = docTypeRef.find((o) => o.doc_type_id === id);
            setDocumentTypeId(obj.doc_type_id);
            setDocumentTypeName(obj.doc_type_title);
        }
    };

    const onHandleChange = async (e) => {
        let files = e.target.files[0];
        setDataFile(files);
    };

    const onChangeInput = (e, id) => {
        const { name, value } = e.target;
        const editData = taggingData.map((item) => (item.id === id && name ? { ...item, [name]: value } : item));
        setTaggingData(editData);
    };

    const onReset = () => {
        setAnnotations([]);
        setTaggingData([]);
        setCandId(null);
        setResultOcr(null);
    };

    const onPrepocessing = async () => {
        setIsUploadProcess(true);
        setIsSplitResult(false);
        setTitleOcrProcess(`${t("check_ocr_tt") + "..."} `);

        saveFilePdf();
    };

    const saveFilePdf = async () => {
        // let url = data.data["@id"];
         let url = templateUrl + "/@upload/template_file";
        // let config = //helper.GetToken();
        let patch_data = {
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/pdf",
            },
        };
 
        await axios
            .patch(url, dataFile, patch_data)
            .then((response) => {
                 setIsPreprocessingOcr(true);
                getPageSplit(templateUrl);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const getPageSplit = async (url) => {
        // let config = helper.GetHeaderOnlyAuth();
        // let token = await localStorage.getItem("mytoken");
        let authentication_data = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };
        // {
        //     headers: {
        //         Authorization: "Basic " ,
        //     },
        // };
         await axios
            .get(url, authentication_data)
            .then((response) => {
 
                let page_split = response.data.template_file_page;
                if (page_split) {
                    setIsPreprocessingOcr(true);
                     setTitleOcrProcess(`${t("res_um") + "OCR"} `);
                    let listpage = [];
                    let n = 1;
                    page_split.forEach((eurl) => {
                        listpage.push({
                            id: n++,
                            imageObject: url + "/" + eurl,
                            imageUrl: url + "/" + eurl + "/@download/image",
                            isSample: false,
                        });
                    });
                     setIsUploadProcess(false);
                    setIsSplitResult(true);
                    setDataPdf(listpage);
                    setIsOcrFinished(true);
                    if (editData) {
                        setStateLoadTag(randomNumberInRange(1, 9998));
                    }
                }
            })
            .catch((error) => {
                setIsUploadProcess(false);
                console.log(error);
            });
    };

    const randomNumberInRange = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    const onOCRProcess = () => {
        setStateProcessOcr(randomNumberInRange(1, 9998));
        setIsOcrProcess(true);
    };

    const onStateOcrProcess = (data) => {
         setIsOcrProcess(data);
    };

    const loadMetadata = async () => {
        try {
            let url = dataIp + "/metadatamaster/@list_metadata";
            // let config = helper.GetToken();
            // const myHeaders = new Headers();
            // myHeaders.append("Content-Type", "application/json");
            // myHeaders.append("Authorization", config.headers.Authorization);
            // // myHeaders.append("Authorization", "");

            // const requestOptions = {
            //     method: "GET",
            //     headers: myHeaders,
            // };
            // var res = await fetch(url, requestOptions);
            // var datas = await res.json();
            // let options = [];
            if (dataIp && dataIp != "") {
                let res = await helper.GetListMetadata(url);
                if ((res && res.status == 200) || 201 || 202 || 204) {
                    setMetadataRef(res.data);
                }
            }

            // datas.map(function (data, index) {
            //     options.push(<option value={data.metadata_id}>{data.metadata_name}</option>);
            // });

            // setMetadataOptionsHtml(options);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const loadDocumentType = async () => {
        try {
            let url = dataIp + "/doctype/@list_doctype";
            // let config = helper.GetToken();
            // const myHeaders = new Headers();
            // myHeaders.append("Content-Type", "application/json");
            // myHeaders.append("Authorization", config.headers.Authorization);
            // // myHeaders.append("Authorization", "");

            // const requestOptions = {
            //     method: "GET",
            //     headers: myHeaders,
            // };
            // var res = await fetch(url, requestOptions);
            // var datas = await res.json();
            let options = [];
            if (dataIp && dataIp != "") {
                let res = await helper.GetListDocType(url);
                if ((res && res.status == 200) || 201 || 202 || 204) {
                    let datas = res.data;
                    setDocTypeRef(datas);

                    datas.map(function (data, index) {
                        options.push(<option value={data.doc_type_id}>{data.doc_type_title}</option>);
                    });

                    setDocTypeOptionsHtml(options);
                }
            }
        } catch (err) {
           
        }
    };

    
    const arrowNavToggle = (tab) => {
        if (arrowNavTab !== tab) {
            setarrowNavTab(tab);
        }
    };

    const onSubmitTemplate = async () => {
        if (documentTypeId.trim() == "") {
            // alert(`${t("empty_um")}`);
            return;
        }
        try {
            let url = dataIp + "/templatemaster";

            const body_data = JSON.stringify({
                "@type": "Template_Master",
                template_name: templateName,
                // "template_polda_id": poldaId,
                // "template_polda_name": poldaName,
                template_doc_type_id: documentTypeId,
                template_doc_type_name: documentTypeName,
            });

            let headers =
                
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`,
                    },
                };

            await axios
                .post(url, body_data, headers)
                .then((response) => {
                     setTemplateUrl(response.data["@id"]);
                })
                .catch((error) => {
                    console.log(error);
                });
        } catch (err) {
            // alert(`${t("error_um")}`);
            
        }
    };

    const onUpdateTemplate = async () => {

        if (documentTypeId.trim() == "") {
            // alert(`${t("empty_um")}`);
            utils.SwalError(`${t("empty_um")}`);
            return;
        }
        try {
            let url = dataIp + "/" + pathTemplate; //base_url + "templatemaster";

            const body_data = JSON.stringify({
                "@type": "Template_Master",
                template_name: templateName,
                // "template_polda_id": poldaId,
                // "template_polda_name": poldaName,
                template_doc_type_id: documentTypeId,
                template_doc_type_name: documentTypeName,
            });

            let headers =
                //helper.GetToken();
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`,
                    },
                };

            await axios
                .patch(url, body_data, headers)
                .then((response) => {
                     // setTemplateUrl(response.data["@id"]);
                })
                .catch((error) => {
                    console.log(error);
                });
        } catch (err) {
            // alert(`${t("error_um")}`);
           
        }
    };

    const onOcrFinished = () => {
         if (sourcePath) {
            navigate(sourcePath);
        } else {
            navigate("/home");
        }
    };

 
    return (
        <React.Fragment>
            <div class="container-fluid pt-3 pb-5 mb-5 " style={{  height: "auto", marginTop: "75px" }}>
                <div class="card mb-2">
                    <div class="card-body">
                        <h5 class="card-title textfamily">{stateTemplate && stateTemplate == "edit" ? "Template" : "Create New Template"}</h5>
                        <hr></hr>
                        <Row>
                            <Col lg={5}>
                                <div class="row">
                                    <label for="templateName" class="col-sm-3 col-form-label fw-medium">
                                        {t("name_temp_tt")}
                                    </label>
                                    <div class="col-8">
                                        <input type="text" class="form-control" id="templateName" value={templateName} onChange={changeTemplateName} />
                                    </div>
                                </div>
                            </Col>
                            <Col lg={5}>
                                <div class="row" style={{ paddingBottom: "10px" }}>
                                    <label for="docType" class="col-sm-3 col-form-label fw-medium">
                                        {t("titlle_dt")}{" "}
                                    </label>
                                    <div class="col-8">
                                        <select id="docType" value={documentTypeId} class="form-select" aria-label="Default select example" onChange={changeDocumentType}>
                                            <option selected>
                                                {t("select_um")} {t("titlle_dt")}
                                            </option>
                                            {docTypeOptionsHtml}
                                        </select>
                                    </div>
                                </div>
                            </Col>
                            {/* <div class="col-3">
                <div class="row" style={{ paddingBottom: "10px" }}>
                  <label for="docType" class="col-sm-3 col-form-label">POLDA </label>
                  <div class="col-9">
                    <select id="docType" value={poldaId} class="form-select" aria-label="Default select example" onChange={changePolda}>
                      <option selected>Pilih Polda</option>
                      {poldaOptionsHtml}
                    </select>
                  </div>                    
                </div>
              </div> */}
                            <Col lg={1}>
                                {stateTemplate && stateTemplate == "edit" ? (
                                    <button class="btn btn-primary w-100" onClick={onUpdateTemplate}>
                                        {" "}
                                        {t("edit_um")}{" "}
                                    </button>
                                ) : (
                                    <button class="btn btn-primary w-100" onClick={onSubmitTemplate}>
                                        {" "}
                                        {t("submit_um")}{" "}
                                    </button>
                                )}
                            </Col>
                            <Col lg={1}>
                                {isOcrFinished ? (
                                    <button class="btn btn-success w-100" onClick={onOcrFinished}>
                                        {" "}
                                        {t("fin_um")}{" "}
                                    </button>
                                ) : (
                                    <div />
                                )}
                            </Col>
                        </Row>
                    </div>
                </div>
                {templateUrl && templateUrl != "" ? (
                    <Card className="mb-2">
                        <div className="p-3 rounded mb-0">
                            <Row className="g-2">
                                <Col lg={5}>
                                    <Input className="form-control" id="formSizeDefault" type="file" accept="application/pdf" onChange={onHandleChange} disabled={isPreprocessingOcr} />
                                </Col>
                                <Col lg={6}></Col>
                                <Col lg={1}>
                                    <Button color="success" onClick={onPrepocessing} className="float-end w-100" disabled={!dataFile || isPreprocessingOcr}>
                                        {!isUploadProcess ? (
                                            <div>
                                                <i className="bx bx-play-circle align-bottom me-1"></i> {t("upload_um")} {t("file_um")}
                                            </div>
                                        ) : (
                                            <span className="d-flex align-items-center">
                                                <Spinner size="sm" className="flex-shrink-0">
                                                    {" "}
                                                    {t("loading_um")}...{" "}
                                                </Spinner>
                                                <span className="flex-grow-1 ms-2">{t("loading_um")}...</span>
                                            </span>
                                        )}
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Card>
                ) : (
                    <div />
                )}

                {isSplitResult ? (
                    <Card>
                        <CardHeader className="align-items-center d-flex">
                            <h4 className="card-title mb-0 flex-grow-1">{titleOcrProcess}</h4>
                            <div className="flex-shrink-0">
                                <div className="form-check form-switch form-switch-right form-switch-md">
                                    <Button color="success w-100" onClick={() => onOCRProcess()}>
                                        {!isOcrProcess ? (
                                            <div>
                                                <i className="bx bx-play-circle align-bottom me-1"></i> {t("process_um")} OCR
                                            </div>
                                        ) : (
                                            <span className="d-flex align-items-center">
                                                <Spinner size="sm" className="flex-shrink-0">
                                                    {" "}
                                                    {t("loading_um")}...{" "}
                                                </Spinner>
                                                <span className="flex-grow-1 ms-2"> {t("loading_um")}...</span>
                                            </span>
                                        )}
                                    </Button>
                                </div>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <Form className="form-steps">
                                <div className="step-arrow-nav mb-4">
                                    <Nav className="nav-pills custom-nav nav-justified" role="tablist">
                                        {dataPdf.map(({ id, imageUrl }) => (
                                            <NavItem>
                                                <NavLink
                                                    href="#"
                                                    id="steparrow-gen-info-tab"
                                                    className={classnames({
                                                        active: activeArrowTab === id,
                                                        done: activeArrowTab < id && activeArrowTab > id,
                                                    })}
                                                    onClick={() => {
                                                        toggleArrowTab(id);
                                                    }}
                                                >
                                                    {t("page_table")} {id}
                                                </NavLink>
                                            </NavItem>
                                        ))}
                                    </Nav>
                                </div>

                                <TabContent activeTab={activeArrowTab}>
                                    {dataPdf.map(({ id, imageUrl, imageObject, isSample }) => {
                                        return (
                                            <OcrTabContent
                                                onOcrCallBack={onStateOcrProcess}
                                                tabid={id}
                                                tabImageUrl={imageUrl}
                                                tabImageObject={imageObject}
                                                tabIsSample={isSample}
                                                processOcr={stateProcessOcr}
                                                loadTags={stateLoadTag}
                                                tabMetadata={metadataRef}
                                            ></OcrTabContent>
                                        );
                                    })}
                                </TabContent>
                            </Form>
                        </CardBody>
                    </Card>
                ) : (
                    <div />
                )}
            </div>
            <Modal
                isOpen={modal_success}
                toggle={() => {
                    tog_success();
                }}
                centered
            >
                <ModalHeader className="modal-title" />

                <ModalBody className="text-center p-5">
                    <div className="mt-4">
                        <h4 className="mb-3">{message_succes}</h4>
                        <p className="text-muted mb-4"> </p>
                        <div className="hstack gap-2 justify-content-center">
                            <Button color="success" onClick={() => setmodal_success(false)}>
                                OK
                            </Button>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </React.Fragment>
    );
};

export default CreateTemplate;
