import React, { useEffect, useState, useCallback } from "react";
import { Alert, Col, Container, Row, TabPane, Card, CardBody, CardHeader } from "reactstrap";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import { Stage, Layer, Rect } from "react-konva";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
//import '../../../assets/css/ocr.css';
// import "../../../assets/css/table-template.css";
import axios from "axios";
import { Link } from "react-router-dom";
import IpNotValidPage from "../../../components/Page/IpNotValid";
// import {Buffer} from 'buffer';
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";

const OcrTabContent = (props) => {
    let base_url = `${process.env.REACT_APP_API_URL + "/"}`;
    const [annotations, setAnnotations] = useState([]);
    const [newAnnotation, setNewAnnotation] = useState([]);
    const [taggingData, setTaggingData] = useState([]);
    const [urlPhoto, setUrlPhoto] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState("");
    const [numLoadOcr, setNumLoadOcr] = useState(0);
    const [sumLoadOcr, setSumLoadOcr] = useState(0);
    const [isLoadOcr, setIsLoadOcr] = useState(0);
    const [templateId, setTemplateId] = useState("");
    const [templateImage, setTemplateImage] = useState("");
    const [isLoadOcrResult, setIsLoadOcrResult] = useState(false);
    const [, updateState] = useState();
    const { t } = useTranslation();

    const forceUpdate = useCallback(() => updateState({}), []);

    //** for tagging result */
    const [data, setData] = useState([]);
    const [sorting, setSorting] = useState([]);
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});

    const [validRows, setValidRows] = useState({});

    const [myToken, setMyToken] = useState("");
    const [dataIp, setDataIp] = useState("");
    const token = localStorage.getItem("mytoken");
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
         // let token = await localStorage.getItem("mytoken");
        setMyToken(token);
        const ipServer = ip ? `${ip}/db/dmsbackend` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip, token]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            loadMetadataType();
        }
    }, [dataIp]);

    useEffect(() => {
         if (props.tabMetadata) {
            setMetadataRef(props.tabMetadata);
            initListMetadata(props.tabMetadata);
        }
    }, [props.tabMetadata]);

    useEffect(() => {
         setUrlPhoto(props.tabImageObject);
        getPhoto(props.tabImageUrl);
        loadMetadataType();
    }, [props.tabImageUrl, props.tabImageObject, props.tabIsSample]);

    useEffect(() => {
        loadMetadataType();
        if (props.processOcr != 0 && props.processOcr != 9999) {
             // setNumLoadOcr(props.processOcr)
            //** disable dl - jangan lupa diaktifkan */
            // onSave(); disable dl - jangan lupa diaktifkan
        }
    }, [props.processOcr]);

    useEffect(() => {
        loadMetadataType();
        if (props.loadTags != 0) {
             setNumLoadOcr(props.loadTags);
            setSumLoadOcr(props.loadTags);
            setIsLoadOcr(props.loadTags);
            onLoadCoordinate();
        }
    }, [props.loadTags]);

    useEffect(() => {
        loadMetadataType();
        if (taggingData && templateId && templateImage) {
             if (numLoadOcr != 0 && numLoadOcr != 9999) {
                let sum = sumLoadOcr;
                if (sum == numLoadOcr) {
                    getResultOcr(templateId, templateImage);
                    sum++;
                    setSumLoadOcr(sum);
                }
            }
        }
    }, [taggingData, templateId, templateImage]);

    const [imgWidth, setImgWidth] = useState(1033);
    const [imgHeight, setImgHeight] = useState(1461);
    const [imgData, setImgData] = useState(null);
    const [metadataRef, setMetadataRef] = useState(null);
    const [metadataOptionsHtml, setMetadataOptionsHtml] = useState([]);
    const [metadataTypeOptionsHtml, setMetadataTypeOptionsHtml] = useState([]);

    function _imageEncode(arrayBuffer) {
        let u8 = new Uint8Array(arrayBuffer);
        let b64encoded = btoa(
            [].reduce.call(
                new Uint8Array(arrayBuffer),
                function (p, c) {
                    return p + String.fromCharCode(c);
                },
                ""
            )
        );

        let mimetype = "image/jpeg";
        let base64 = "data:" + mimetype + ";base64," + b64encoded;

        let mImage = new Image();
        mImage.src = base64;
        mImage.onload = function (evt) {
             setImgWidth(mImage.width);
            setImgHeight(mImage.height);
        };

        return base64;
    }

    const onLoadCoordinate = async () => {
        let url = urlPhoto ? urlPhoto : props.tabImageObject;
        let headers =
            //helper.GetToken();
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
            };
         await axios
            .get(url, headers)
            .then((response) => {
                 let data = response.data;
                if (data.template_metadata_tag && data.template_metadata_tag.length > 0) {
                    let _taggingData = [];
                    data.template_metadata_tag.forEach((mt_tag, index) => {
                         let tagData = { id: index + 1, tag_id: mt_tag.tag_metadata_id, fix_tag: mt_tag.tag_metadata_name, name: mt_tag.tag_metadata_name, tag_type: mt_tag.tag_metadata_type, coordinate: mt_tag.tag_coor, result: "" };
                        _taggingData.push(tagData);

                        const annotationToAdd = {
                            x: mt_tag.tag_coor[0][0],
                            y: mt_tag.tag_coor[0][1],
                            width: mt_tag.tag_coor[1][0] - mt_tag.tag_coor[0][0],
                            height: mt_tag.tag_coor[2][1] - mt_tag.tag_coor[0][1],
                            key: index,
                        };
                        annotations.push(annotationToAdd);
                        setNewAnnotation([]);
                        setAnnotations(annotations);

                        // getResultOcr(data.template_id, data['@name']);
                    });
                    setTemplateId(data.template_id);
                    setTemplateImage(data["@name"]);
                    setTaggingData(_taggingData);

                    // onLoadResultOcr(data);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const onLoadResultOcr = (data) => {
        const timeout = setTimeout(() => {
             getResultOcr(data.template_id, data["@name"]);
        }, 3000);
    };

    const initListMetadata = async (datas) => {
         if (datas && datas.length > 0) {
            let options = [];
            datas.map(function (data, index) {
                options.push(<option value={data.metadata_id}>{data.metadata_name}</option>);
            });

            setMetadataOptionsHtml(options);
        }
    };

    

    const loadMetadataType = async () => {
        try {
            let options = [];

            options.push(<option value={0}>{t("default_tt")}</option>);
            // options.push(<option value={1}>{t("table_tt")}</option>);
            options.push(<option value={2}>{t("handwriting_tt")}</option>);

            setMetadataTypeOptionsHtml(options);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const getPhoto = async (URL) => {
        let authentication_data = {
            headers: {
                Authorization: `Bearer ${token}`,
            },
            responseType: "arraybuffer",
        };
        await axios
            .get(URL, authentication_data)
            .then(async (response) => {
 
                
                setImgData(_imageEncode(response.data));
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const handleMouseDown = (event) => {
        if (newAnnotation.length === 0) {
            const { x, y } = event.target.getStage().getPointerPosition();
            setNewAnnotation([{ x, y, width: 0, height: 0, key: "0" }]);
         }
    };

    const handleMouseUp = (event) => {
         if (newAnnotation.length === 1) {
            const sx = newAnnotation[0].x;
            const sy = newAnnotation[0].y;
            const { x, y } = event.target.getStage().getPointerPosition();
            const annotationToAdd = {
                x: sx,
                y: sy,
                width: x - sx,
                height: y - sy,
                key: annotations.length + 1,
            };

            let titik1 = [annotationToAdd.x, annotationToAdd.y];
            let titik2 = [annotationToAdd.x + annotationToAdd.width, annotationToAdd.y];
            let titik3 = [annotationToAdd.x, annotationToAdd.y + annotationToAdd.height];
            let titik4 = [annotationToAdd.x + annotationToAdd.width, annotationToAdd.y + annotationToAdd.height];

            if (annotationToAdd.width > 30 && annotationToAdd.height > 25) {
                let tagData = {
                    id: annotationToAdd.key,
                    tag_id: "tag_id_" + annotationToAdd.key,
                    fix_tag: "tag_name_" + annotationToAdd.key,
                    tag_type: 0, //"tag_type_" + annotationToAdd.key,
                    name: "tag_name_" + annotationToAdd.key,
                    coordinate: [],
                    result: "",
                };
                tagData.coordinate.push(titik1);
                tagData.coordinate.push(titik2);
                tagData.coordinate.push(titik3);
                tagData.coordinate.push(titik4);
                taggingData.push(tagData);
                setTaggingData(taggingData);

                annotations.push(annotationToAdd);
                setNewAnnotation([]);
                setAnnotations(annotations);
             } else {
                setAlertMessage("Area tagging terlalu kecil");
                setShowAlert(true);
                window.setTimeout(() => {
                    setShowAlert(false);
                }, 1500);
                setNewAnnotation([]);
            }
        }
    };

    const handleMouseMove = (event) => {
        if (newAnnotation.length === 1) {
            const sx = newAnnotation[0].x;
            const sy = newAnnotation[0].y;
            const { x, y } = event.target.getStage().getPointerPosition();
            setNewAnnotation([
                {
                    x: sx,
                    y: sy,
                    width: x - sx,
                    height: y - sy,
                    key: "0",
                },
            ]);
        }
    };

    const onHandleChangeTagging = (tagging, value) => {
 
        let filter = metadataRef.filter((obj) => {
            return obj.metadata_id == value;
        });
        tagging["tag_id"] = filter[0].metadata_id;
        tagging["fix_tag"] = filter[0].metadata_name;
        tagging["name"] = filter[0].metadata_name;
        let _taggingData = taggingData;
        // setTaggingData([])
        setTaggingData(_taggingData);
         forceUpdate();
        // this.forceUpdate()
    };

    const onHandleRemoveTag = (tag) => {
 
        let _tags = taggingData.filter((mytag) => mytag.tag_id != tag.tag_id);
        setTaggingData(_tags);

        let _annotations = annotations.filter((ant) => ant.x != tag.coordinate[0][0] && ant.y != tag.coordinate[0][1]);
        setAnnotations(_annotations);
    };

    const onHandleChangeTaggingType = (tagging, value) => {
        tagging["tag_type"] = value;
        let _taggingData = taggingData;
        // setTaggingData([])
        setTaggingData(_taggingData);
         forceUpdate();
    };

    const onChangeInput = (e, id) => {
        const { name, value } = e.target;
        const editData = taggingData.map((item) => (item.id === id && name ? { ...item, [name]: value } : item));
        setTaggingData(editData);
    };

    const onSave = async () => {
         let data = {
            template_metadata_tag: [],
            // tagging: ["nama", "alamat"],
            // tagging_cor: [
            //   [
            //     [1, 1],
            //     [4, 1],
            //     [1, 5],
            //     [4, 5],
            //   ],
            //   [
            //     [2, 2],
            //     [5, 2],
            //     [2, 6],
            //     [5, 6],
            //   ],
            // ],
        };

        // let tagging = [];
        // let tagging_cor = [];
        let template_metadata_tag = [];
        taggingData.map(({ name, coordinate, fix_tag, tag_id, tag_type }) => {
             // tagging.push(name);
            // tagging_cor.push(coordinate);
            template_metadata_tag.push({
                tag_metadata_id: tag_id,
                tag_metadata_name: fix_tag,
                tag_metadata_type: tag_type ? tag_type : 0,
                tag_coor: coordinate,
            });
        });
        let statusAction = "Edit";
        let method = statusAction == "Edit" ? "PATCH" : "POST";
        try {
            data["template_metadata_tag"] = template_metadata_tag;
            let config =
                //helper.GetToken();
                {
                    // method: method,
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`,
                    },
                    // body: JSON.stringify(data)
                };
 
            let url = urlPhoto ? urlPhoto : props.tabImageObject;
             await axios
                .patch(url, data, config)
                .then((response) => {
                     let pathUrl = url.split("/");
                    getResultOcr(pathUrl[pathUrl.length - 2], pathUrl[pathUrl.length - 1]);
                })
                .catch((error) => {
                    console.log(`${t("error_um")}`);
                });
        } catch (err) {
            console.log(err.message);
        }
    };

    

    const getResultOcr = async (templateId, imageId) => {
         // let url = `${process.env.REACT_APP_API_URL + "/@ocr_ezocr?id=" + id}`;
        // let url = `${process.env.REACT_APP_API_URL + "/@ocr_template?id=" + templateId + "&idpage=" + imageId}`;
        let url = `${dataIp + "/@ocr_template?id=" + templateId + "&idpage=" + imageId}`;
         // let config = helper.GetToken();
        // let token = await localStorage.getItem("mytoken");
        let authentication_data = {
            // method: "PATCH",
            headers: {
                Authorization: `Bearer ${token}`,
                // 'Content-Type': 'multipart/form-data'
            },
            // body: dataFile
        };
        // {
        //     // method: "PATCH",
        //     headers: {
        //         Authorization:
        //         // 'Content-Type': 'multipart/form-data'
        //     },
        //     // body: dataFile
        // };
         setIsLoadOcrResult(true);
        await axios
            .patch(url, null, authentication_data)
            .then((response) => {
                setIsLoadOcrResult(false);
                 props.onOcrCallBack(false);
                if (response) {
                    //&& response.length > 1 ) {
                    let res = response.data; //["result"]; //response[response.length-1]
                    let editData = taggingData.map((item) => {
                         // if (res.hasOwnProperty(item.tag_metadata_name)) {
                         //   item.result = res.result//res[item.tag_metadata_name];
                        // }
                        let object = res.filter((obj) => {
                            return obj.tag_metadata_id == item.tag_id;
                        });
                         item.result = object[0].result;
                        return item;
                    });
                     setTaggingData(editData);
                }
            })
            .catch((error) => {
                setIsLoadOcrResult(false);
                 
            });
    };

    const columns = [
        {
            accessorKey: "name",
            header: t("name_tag_tt"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "tag_type",
            header: t("type_tag_tt"),
            enableColumnFilter: true,
        },
        {
            accessorKey: "result",
            header: t("res_um") + " OCR",
            enableColumnFilter: true,
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            gap: "4px",
                        }}
                    >
                        <button
                            onClick={() => {
                                console.log("reset tag");
                            }}
                        >
                            ❌
                        </button>
                    </div>
                );
            },
        },
    ];
    const table = useReactTable({
        taggingData,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });

    const annotationsToDraw = [...annotations, ...newAnnotation];
    return (
        <TabPane id="pills-experience" tabId={props.tabid}>
            <Alert color="danger" isOpen={showAlert} toggle={() => setShowAlert(false)}>
                {alertMessage}
            </Alert>
            <div class="row my-3 mb-5">
                <div class="col-7">
                    <div>
                        {/* TESTS {props.tabid} */}
                        <Stage
                            onMouseDown={handleMouseDown}
                            onMouseUp={handleMouseUp}
                            onMouseMove={handleMouseMove}
                            // width={img.width}
                            // height={img.height}
                            width={imgWidth}
                            height={imgHeight}
                            style={{ border: "2px solid #ced4da", borderRadius: "6px", position: "absolute" }}
                        >
                            <Layer>
                                {annotationsToDraw.map((value) => {
                                     return <Rect x={value.x} y={value.y} width={value.width} height={value.height} fill="transparent" stroke="red" />;
                                })}
                            </Layer>
                        </Stage>
                        <img id={"imagehal" + props.tabid} src={imgData} width={imgWidth} height={imgHeight} className="figure-img img-fluid rounded" alt="..." />
                    </div>
                </div>
                <div class="col-5  ">
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <CardHeader>
                                    <div className="">
                                        <h4 className="card-title mb-0">{t("list_tag_tt")}</h4>
                                    </div>
                                </CardHeader>

                                <CardBody className="p-4">
                                    <table className="mt-1 table table-bordered table-striped table-hover">
                                        <thead className="table-primary">
                                            <tr>
                                                {/* <th id="cl1"></th> */}
                                                <th id="cl2">
                                                    {" "}
                                                    <span class="ms-2">{t("name_tag_tt")}</span>
                                                </th>
                                                <th id="cl3">
                                                    {" "}
                                                    <span class="ms-2">{t("type_tag_tt")}</span>
                                                </th>
                                                <th id="cl4">
                                                    <span class="ms-2">{t("res_um")} OCR</span>
                                                </th>
                                                <th id="cl5">
                                                    <span class="ms-2"> {t("actions_um")}</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {taggingData.map((tagging) => (
                                                <tr key={tagging.id}>
                                                    {/* <td>
                                                        <div className="hstack gap-3 fs-15 justify-content-center">
                                                            <Link to="#" className="link-danger">
                                                                <i className="ri-close-circle-fill"></i>
                                                            </Link>
                                                        </div>
                                                    </td> */}
                                                    <td>
                                                        <select id={"select" + tagging.id} class="form-select" value={tagging.tag_id} aria-label="Default select example" onChange={(e) => onHandleChangeTagging(tagging, e.target.value)}>
                                                            <option selected>
                                                                {t("select_um")} {t("titlle_md")}
                                                            </option>
                                                            {metadataOptionsHtml}
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id={"selecttype" + tagging.id} class="form-select" value={tagging.tag_type} aria-label="Default select example" onChange={(e) => onHandleChangeTaggingType(tagging, e.target.value)}>
                                                            <option selected>
                                                                {t("select_um")} {t("tipe_um")}
                                                            </option>
                                                            {metadataTypeOptionsHtml}
                                                        </select>
                                                    </td>
                                                    <td>
                                                        {tagging && !isLoadOcrResult ? (
                                                            <label>{tagging.result}</label>
                                                        ) : (
                                                            <div class="spinner-border text-primary" role="status">
                                                                <span class="visually-hidden">{t("loading_um")}...</span>
                                                            </div>
                                                        )}
                                                        {/* <label>{tagging.result}</label>
                                                        <div class="spinner-border text-primary" role="status">
                                                            <span class="visually-hidden">Loading...</span>
                                                        </div> */}
                                                    </td>
                                                    <td>
                                                        <button
                                                            class="btn"
                                                            onClick={() => {
                                                                onHandleRemoveTag(tagging);
                                                            }}
                                                            data-bs-toggle="tooltip"
                                                            data-bs-html="true"
                                                            data-bs-title="<em>Tooltip</em> <u>with</u> <b>HTML</b>"
                                                        >
                                                            ❌
                                                        </button>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        </TabPane>
    );
};

export { OcrTabContent };
