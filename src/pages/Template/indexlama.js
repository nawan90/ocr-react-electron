import React, { useState } from 'react';
import { Card, CardBody, Col, Container, Nav, NavItem, NavLink, Row, TabContent, TabPane, UncontrolledTooltip } from "reactstrap";
import { Link } from 'react-router-dom';
import classnames from "classnames";
import BreadCrumb from "../../../../Components/Common/BreadCrumb";

// Import Content
 import ImageOcr from './ImageOcr';
import PdfOcr from './PdfOcr';

const DigitalisasiDokumen = () => {

    // Arrow Nav tabs
    const [arrowNavTab, setarrowNavTab] = useState("1");
    const arrowNavToggle = (tab) => {
        if (arrowNavTab !== tab) {
            setarrowNavTab(tab);
        }
    };

 
    return (
        <React.Fragment>
           <div className="page-content">
            <BreadCrumb title="OCR" pageTitle="Digitalisasi Dokumen" />
            <Nav pills className="nav nav-pills arrow-navtabs nav-success bg-light mb-3">
                <NavItem>
                    <NavLink
                        style={{ cursor: "pointer" }}
                        className={classnames({
                            active: arrowNavTab === "1",
                        })}
                        onClick={() => {
                            arrowNavToggle("1");
                        }}
                    >
                        IMAGE OCR
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        style={{ cursor: "pointer" }}
                        className={classnames({
                            active: arrowNavTab === "2",
                        })}
                        onClick={() => {
                            arrowNavToggle("2");
                        }}
                    >
                        PDF OCR
                    </NavLink>
                </NavItem>

            </Nav>

            <TabContent
                activeTab={arrowNavTab}
                className="text-muted"
            >
                <TabPane tabId="1" id="arrow-overview">

                    <ImageOcr/>

                </TabPane>
                <TabPane tabId="2" id="arrow-profile">

                    <PdfOcr/>

                </TabPane>
                
            </TabContent>
          </div>
        </React.Fragment>
    );
};

export default DigitalisasiDokumen;