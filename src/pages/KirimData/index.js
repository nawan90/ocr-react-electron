import React, { useState } from "react";

function KirimData() { 
  const [text, setText] = useState("");

  const onChangeText = (e) => {
    e.preventDefault();
    setText(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const data = {
      message: text,
      timestamp: new Date().toISOString(),
    };

    // Send data to Electron main process via ipcRenderer
    window.electronAPI.sendData(data);
  };

  return (
    <div className="container-fluid" style={{ height: "100vh", padding: "100px 10px" }}>
      <div className="row mt-5">
        <form onSubmit={handleSubmit}>
          <label htmlFor="exampleEmail">Kirim Data</label>
          <input
            id="text"
            name="text"
            placeholder="Text"
            type="text"
            value={text}
            className="form-control"
            onChange={onChangeText}
          />
          <button style={{ marginTop: "30px" }} className="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  );
}

export default KirimData;
