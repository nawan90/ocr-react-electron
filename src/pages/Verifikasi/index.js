import { useState, useEffect, useRef } from "react";
 import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import Del from "../../assets/images/icon/delete.png";
import View from "../../assets/images/icon/info.png";

import Filters from "../../components/Table/filters";
import Paginations from "../../components/Table/pagination";
 import ModalHapus from "../../components/Modal/modalHapus";
import { useNavigate, Link } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import IpNotValidPage from "../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import Swal from "sweetalert2";
import axios from "axios";

import moment from "moment";
import "moment/locale/id";
import { use } from "i18next";

import LocalStorageHelper from "../../api/localStorageHelper";


function Verifikasi() {
  const navigate = useNavigate();
  const [dataIp, setDataIp] = useState("");
  const [dataToken, setDataToken] = useState("");
  const [data, setData] = useState([]);
  const [filtering, setFiltering] = useState("");
  const [columnFilters, setColumnFilters] = useState([]);
  const [editedRows, setEditedRows] = useState({});
  const [validRows, setValidRows] = useState({});
  const [mounted, setMounted] = useState(true);
  const [getData, setGetData] = useState([]);
  const [sorting, setSorting] = useState([]);
  const [permission, setPermission] = useState({
      permissionAdd: false,
      permissionModify: false,
      permissionView: false,
      permissionList: false,
      permissionVersion: false,
  });
  // const [permissionAdd, setPermissionAdd] = useState(false)
  // const [permissionModify, setPermissionModify] = useState(false)
  // const [permissionView, setPermissionView] = useState(false)
  // const [permissionList, setPermissionList] = useState(false)
  // const [permissionVersion, setPermissionVersion] = useState(false)
  const [isPolda, setIsPolda] = useState("");
  const [isSatker, setIsSatker] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  // const [accessAdd, setAccessAdd] = useState(false ? false :LocalStorageHelper.getUserData(["dms.document.add"])  )

  const { t } = useTranslation();
  const userData = LocalStorageHelper.getUserData();
  // const toCreateDocument = (value) => {
  //     navigate("/verifikasi/create-document");
  // };

  const toViewVerification = (value) => {
      
      navigate("/verifikasi/view-verifikasi", {
          state: {
              state: "view",
              document: value,
              sourcepath: "/verifikasi/list-verifikasi",
          },
      });
  };
  // toViewVerification

 
  useEffect(() => {
      const accessAdd = JSON.parse(userData["dms.document.add"]);
      const accessModify = JSON.parse(userData["dms.document.modify"]);
      const accessView = JSON.parse(userData["dms.document.view"]);
      const accessVersion = JSON.parse(userData["dms.document.version"]);
      const accessList = JSON.parse(userData["dms.document.list"]);
      const satker = userData.satkerid;
      const polda = userData.poldaid;
      let permissionTemp = {
          permissionAdd: false,
          permissionModify: false,
          permissionView: false,
          permissionList: false,
          permissionVersion: false,
      };

      if (accessList) {
          permissionTemp.permissionList = true;
      }
      if (accessView) {
          permissionTemp.permissionView = true;
      }

      if ((satker && satker != null && satker != "null") || (polda && polda != null && polda != "null")) {
          if (accessAdd) {
              permissionTemp.permissionAdd = true;
          }

          if (accessVersion) {
              permissionTemp.permissionVersion = true;
          }

          if (accessModify) {
              permissionTemp.permissionModify = true;
          }

          setPermission(permissionTemp);
      }

      if (accessList) {
          showTable();
      }
  }, []);

  const handlerDelete = async (data) => {
      
      const url = data["@id"];
      const name = data["doc_name"];
     
      const config = {
          headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${userData.token}`,
          },
      };
      
      await axios
          .delete(url, config)
          .then((response) => {
              
              Swal.fire("Info", `Document ${name} Telah dihapus`, "success");
              setTimeout(() => {
                  showTable();
              }, 3000);
          })
          .catch((error) => {
              console.error("Error deleting resource:", error.response ? error.response.data : error.message);
          });
  };

  const showTable = async () => {
      const url = userData.myip + "/db/dmsbackend/polridocument/@list_document";
      try {
          const myHeaders = new Headers();
          myHeaders.append("Content-Type", "application/json");
          myHeaders.append("Authorization", `Bearer ${userData.token}`);
          const requestOptions = {
              method: "GET",
              headers: myHeaders,
          };
          var res = await fetch(url, requestOptions);
          var datas = await res.json();
          setData(datas);
      } catch (err) {
          console.log(`${t("error_um")}`);
      }
  };

  const columns = [
      {
          accessorKey: "doc_name",
          header: t("doc_name_td"),
          // cell: EditableCell,
          enableColumnFilter: true,
          filterFn: "includesString",
          cell: function render({ getValue, renderValue, state, row, column, instance }) {
              return (
              <div style={{
                  width: '100%',
                  maxWidth: '600px',
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap'
              }}>
                  {row.original.doc_name}
              </div>
          );} 
      },
      {
          accessorKey: "doc_type_name",
          header: t("titlle_dt"),
          // cell: EditableCell,
          enableColumnFilter: true,
          filterFn: "includesString",
      },
      {
          accessorKey: "creation_date",
          header: t("create_date_um"),
          // cell: EditableCell,
          cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
          enableColumnFilter: true,
      },
      {
          accessorKey: "doc_created_by_user",
          header: t("pengguna_t1"),
          // cell: EditableCell,
          enableColumnFilter: true,
      },
      {
          accessorKey: "doc_polda_name",
          header: t("titlle_lp"),
          // cell: EditableCell,
          enableColumnFilter: true,
      },
      {
          id: "actions",
          accessorKey: "id",
          header: t("actions_um"),
          cell: function render({ getValue, state, row, column, instance }) {
              return (
                  <div
                      style={{
                          display: "flex",
                          flexDirection: "row",
                          gap: "4px",
                          width: "150px",
                      }}
                  > 
                             <button type="button" className="btn" title="Lihat" onClick={() => toViewVerification(row.original)}>
                                <img src={View} width={"20"} />
                            </button>
                      
                           
                  </div>
              );
          },
      },
  ];

  const table = useReactTable({
      data,
      columns,
      state: {
          sorting: sorting,
          globalFilter: filtering,
          columnFilters: columnFilters,
      },
      getCoreRowModel: getCoreRowModel(),
      getPaginationRowModel: getPaginationRowModel(),
      getSortedRowModel: getSortedRowModel(),
      getFilteredRowModel: getFilteredRowModel(),
      onSortingChange: setSorting,
      onGlobalFilterChange: setFiltering,
      onColumnFiltersChange: setColumnFilters,
      meta: {
          editedRows,
          setEditedRows,
          validRows,
          setValidRows,
          revertData: (rowIndex) => {
              // setData((old) =>
              //   old.map((row, index) =>
              //     index === rowIndex ? originalData[rowIndex] : row
              //   )
              // );
          },
          updateRow: (rowIndex) => {
              // updateRow(data[rowIndex].id, data[rowIndex]);
          },
          updateData: (rowIndex, columnId, value, isValid) => {
              setData((old) =>
                  old.map((row, index) => {
                      if (index === rowIndex) {
                          return {
                              ...old[rowIndex],
                              [columnId]: value,
                          };
                      }
                      return row;
                  })
              );
              setValidRows((old) => ({
                  ...old,
                  [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
              }));
          },
          addRow: () => {
              const id = Math.floor(Math.random() * 10000);
              const newRow = {
                  id,
                  studentNumber: id,
                  name: "",
                  dateOfBirth: "",
                  major: "",
              };
              // addRow(newRow);
          },
          removeRow: (rowIndex) => {
              // deleteRow(data[rowIndex].id);
          },
          removeSelectedRows: (selectedRows) => {
              // selectedRows.forEach((rowIndex) => {
              //   deleteRow(data[rowIndex].id);
              // });
          },
      },
  });

 
  return (
    <div class="container-fluid pt-3 " style={{ height: " auto", marginTop: "85px" }}> 
            <div>
                <div class="row">
                    <div class="col-4">
                        <Filters filtering={filtering} setFiltering={setFiltering} />
                    </div> 
                </div>
                <ModalHapus handlerDelete={handlerDelete} getData={getData} keyboard="false" backdrop="static" />
                <Toaster
                    position="bottom-left"
                    reverseOrder={false}
                    gutter={8}
                    containerClassName=""
                    containerStyle={{}}
                    toastOptions={{
                        // Define default options
                        className: "",
                        duration: 5000,
                        style: {
                            background: "#ffbaba",
                            color: "#ff0000 ",
                        },

                        // Default options for specific types
                        success: {
                            duration: 3000,
                            theme: {
                                primary: "green",
                                secondary: "black",
                            },
                        },
                    }}
                />


        <table className="table table-bordered table-striped table-hover">
            <thead className="table-primary">
                {table.getHeaderGroups().map((headerGroup) => (
                    <tr key={headerGroup.id} className="  uppercase">
                        {headerGroup.headers.map((header) => (
                            <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                            </th>
                        ))}
                    </tr>
                ))}
            </thead>
            {data === undefined || data.length == 0 ? (
                <tbody>
                    <tr>
                        <td colSpan="5" style={{ textAlign: "center", margin: "0 auto" }}>
                            <span
                                style={{
                                    color: "red",
                                    fontWeight: "bold",
                                    fontSize: "16px",
                                }}
                            >
                                <IpNotValidPage />
                            </span>
                        </td>
                    </tr>
                </tbody>
            ) : (
                <tbody>
                    {table.getRowModel().rows.map((row) => (
                        <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                            {row.getVisibleCells().map((cell) => (
                                <td className="px-4 py-2" key={cell.id}>
                                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            )}
        </table>
        <Paginations table={table} />
    </div>
</div>
  );
}

export default Verifikasi;
