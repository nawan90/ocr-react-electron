import React, { useState, useEffect } from "react";  // Import useState and useEffect from React
import axios from "axios";  // Import axios for making HTTP requests
import LoadSpinner from "../../../components/Load/load";  // Import custom LoadSpinner component
import { useTranslation } from "react-i18next";  // Import useTranslation hook for localization
import LocalStorageHelper from "../../../api/localStorageHelper";  // Import your LocalStorageHelper

function ShowPdf(props) {
  const { closeModal, link, getData } = props;
  const [pdfUrl, setPdfUrl] = useState("");  // State for storing the generated PDF URL
  const [prevLink, setPrevLink] = useState("");  // State to track the previous link to avoid unnecessary reloads
  const base_url = process.env.REACT_APP_API_URL;  // Get the base URL from environment variables
  const { t } = useTranslation();  // Initialize translation hook
  const userData = LocalStorageHelper.getUserData();  // Get user data from LocalStorageHelper

  // Fetch PDF whenever the `link` prop changes
  useEffect(() => {
    const fetchPDF = async () => {
      if (!link || link === "null") {
        console.error("Invalid link passed: ", link);
        return;  // Don't proceed if the link is invalid
      }

      if (!userData || !userData.token) {
        console.error("No user token found");
        return;  // Don't proceed if user data or token is missing
      }

      // Set up authentication headers
      let authentication_data = {
        headers: {
          Authorization: `Bearer ${userData.token}`,
        },
        responseType: "blob",  // Expecting binary data (PDF)
      };

      const isFullUrl = link.startsWith("http://") || link.startsWith("https://");
      const pdfUrl = isFullUrl ? link : base_url + link;  // If it's a full URL, use it directly

      

      // Make the request to fetch the PDF
      try {
        const response = await axios.get(pdfUrl, authentication_data);
        const pdfBlob = new Blob([response.data], { type: "application/pdf" });
        const url = URL.createObjectURL(pdfBlob);  // Create a blob URL

        // Update the state with the new blob URL
        setPdfUrl(url);
        setPrevLink(link);  // Update the previous link to the current one
      } catch (error) {
        console.error(`${t('error_um')}`, error);
      }
    };

    // Only fetch the PDF if the link has changed (avoid unnecessary refetches)
    if (link && link !== prevLink) {
      fetchPDF();  // Fetch the PDF only when the link has changed
    }

    // Clean up the blob URL when the component unmounts or the link changes
    return () => {
      if (pdfUrl) {
        URL.revokeObjectURL(pdfUrl);  // Revoke the old blob URL to avoid memory leaks
      }
    };
  }, [link, prevLink, userData, base_url, t, pdfUrl]);  // Use the link prop as a dependency to trigger re-fetch

  return (
    <div>
      {pdfUrl ? (
        <iframe
          title="View Document"
          src={pdfUrl}  // Set the source of the iframe to the generated PDF URL
          width="100%"
          height="800px"
          frameBorder="0"
        />
      ) : (
        <div>
          <LoadSpinner />  {/* Show a loading spinner if the PDF URL is not ready */}
        </div>
      )}
    </div>
  );
}

export default ShowPdf;  // Export the component
