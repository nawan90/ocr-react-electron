import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import { useTranslation } from "react-i18next";
import Close from "../../../assets/images/icon/close.png";
import ShowPdf from "./showPdf";
import LocalStorageHelper from "../../../api/localStorageHelper";
import ImageCarousel from "../../../components/ImageCarousel";

function ViewVerification({ props }) {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { state } = useLocation();
    const [viewData, setViewData] = useState(state);
    const [tagHtml, setTaghtml] = useState([]);
    const [editedValue, setEditedValue] = useState("");
    const [editingMetadata, setEditingMetadata] = useState(null);
    const [loading, setLoading] = useState(false);
    const [linkPdf, setLinkPdf] = useState(null);
    const userData = LocalStorageHelper.getUserData();
    const [isVisible, setIsVisible] = useState(true);
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
        if (viewData && viewData.document) {
            showDocument(viewData.document["@id"]);
        } else {
            navigate("/list-verifikasi");
        }
    }, [viewData]);

    const showDocument = async (url) => {
        try {
            setLinkPdf(url + "/@download/doc_file");

            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/pdf");
            myHeaders.append("Authorization", `Bearer ${userData.token}`);

            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };

            const req = await fetch(url, requestOptions);
            const data = await req.json();

            if (req.status === 200) {
                const metadata_template = data.doc_metadata_by_template;
                showTagMetadata(metadata_template);
            }
        } catch (err) {
            setLoading(false);
            console.error("Error loading document:", err);
        }
    };

    const showTagMetadata = (metadata_template) => {
        let listTagTemp = [];
        metadata_template.forEach((data, idx) => {
            let metadata_name = data.metadata_tmp_name
                .split("_")
                .slice(1)
                .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                .join(" ");
            
            listTagTemp.push(
                <tr key={idx}>
                    <td className="px-4 py-2">{metadata_name}</td>


                    <td className="px-4 py-2">
                        {editingMetadata && editingMetadata.metadata_tmp_name === data.metadata_tmp_name ? (
                            <input
                                type="text"
                                value={editedValue}
                                onChange={(e) => setEditedValue(e.target.value)}
                                className="form-control"
                            />
                        ) : (
                            data.metadata_tmp_value
                        )}
                    </td>
                    <td className="px-4 py-2">
                        {editingMetadata && editingMetadata.metadata_tmp_name === data.metadata_tmp_name ? (
                            <button
                                className="btn btn-sm btn-success"
                                onClick={() => handleSave(data)}
                            >
                                Save
                            </button>
                        ) : (
                            <button
                                className="btn btn-sm btn-primary"
                                onClick={() => handleEdit(data)}
                            >
                                Edit
                            </button>
                        )}
                    </td>
                </tr>
            );
        });
        setTaghtml(listTagTemp);
    };

    const handleEdit = (data) => {
        setEditingMetadata(data);
        setEditedValue(data.metadata_tmp_value);
    };

    const handleSave = async (data) => {
        if (!editedValue.trim()) {
            Swal.fire("Error!", "Metadata value cannot be empty.", "error");
            return;
        }

        try {
            const updatedMetadata = { ...data, metadata_tmp_value: editedValue };
            // Optionally make an API request to save the updated data
            // await axios.put('/api/update-metadata', updatedMetadata);

            const updatedMetadataTemplate = tagHtml.map(item =>
                item.metadata_tmp_name === updatedMetadata.metadata_tmp_name ? updatedMetadata : item
            );
            setTaghtml(updatedMetadataTemplate);
            setEditingMetadata(null);
            setEditedValue("");
        } catch (error) {
            console.error("Error saving metadata:", error);
            Swal.fire("Error!", "Failed to save the metadata value.", "error");
        }
    };
    const toDocument = () => {
        navigate("/verifikasi/list-verifikasi");
      };
    return (
        <div className="container-fluid" style={{ height: " auto", marginTop: "100px" }}>
        <div className="card">
          <div className="card-body">
            <div class="row mb-2">
              <div class="col-6">
                <h5 class="card-title px-3 pt-3 textfamily">
                  {t("lihat_um")} {t("dokumen_t1")}{" "}
                </h5>
              </div>
              <div class="col-6">
                <div class="float-end">
                  {isVisible && (
                    <button type="button" className="btn btn-sm" title="keluar" onClick={toDocument}>
                      <img src={Close} width={"35"} />
                    </button>
                  )}
                </div>
              </div>
            </div>
                    {loading && (
                        <div className="mt-5" style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                            <div className="spinner-border text-primary" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    )}
                    {viewData && (
                        <div className="row mt-3">
                            <div className="col-6">
                                <ShowPdf link={linkPdf}></ShowPdf>
                            </div>
                            <div className="col-6">
                                <h5 className="card-title">Hasil OCR</h5>
                                <table className="table table-bordered table-striped table-hover">
                                    <thead className="table-primary">
                                        <tr className="uppercase">
                                            <th className="px-4 pr-2 py-3 font-medium text-left">{t("tag_um")}</th>
                                            <th className="px-4 pr-2 py-3 font-medium text-left">{t("res_um")}</th>
                                            <th className="px-4 pr-2 py-3 font-medium text-left">{t("edit_um")}</th>
                                        </tr>
                                    </thead>
                                    <tbody>{tagHtml && tagHtml.length > 0 ? tagHtml : null}</tbody>
                                </table>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}

export default ViewVerification;
