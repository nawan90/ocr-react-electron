import React, { useState, useEffect } from "react";

function TerimaData() {
  let dataWeb = localStorage.getItem("data web");
  var retrievedObject = JSON.parse(dataWeb);
   const formatDateTime = (date) => {
    const options = {
      day: "2-digit",
      month: "long",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    };
    return new Intl.DateTimeFormat("en-GB", options)
      .format(date)
      .replace(",", "");
  }; 

 
  return (
    <div
      class="container-fluid"
      style={{
         height: "100vh",
        padding: "100px 10px",
      }}
    >
      <h4>Terima Data</h4> <br/>
      <h5>Message :</h5>
      <h6>{retrievedObject.message}</h6>
      <br/>
      <h5>Timestamp :</h5>
      <h6>{formatDateTime(new Date(retrievedObject.timestamp))}</h6>
    </div>
  );
}

export default TerimaData;
