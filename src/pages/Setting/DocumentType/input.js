import { useState, useEffect, useCallback } from "react";
import toast, { Toaster } from "react-hot-toast";
import "../../../assets/css/addcss.css";
import { useTranslation } from "react-i18next";
import { useRef } from "react";
import helper from "../../../api/helper";

const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};
function InputDocType(props) {
    const {
        showTable,
        getData,
        action,
        onSelected,
        getAllTable,
        closeButton,
        handleClose,
        disButton,
        setDisButton,
        setCloseButton,
        // setValidDocCode, validDocCode, validDocText, setValidDocText  , validDocDesc  , setValidDocDesc
    } = props;
    const [documentTypeCode, setDocumentTypeCode] = useState(action ? getData.doc_type_code : "");
    const [documentTypeText, setDocumentTypeText] = useState(action ? getData.doc_type_title : "");
    const [documentTypeDesc, setDocumentTypeDesc] = useState(action ? getData.doc_type_desc : "");
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const [codeTouched, setCodeTouched] = useState(false);

    const [validDocCode, setValidDocCode] = useState("");
    const [validDocText, setValidDocText] = useState("");
    const [validDocDesc, setValidDocDesc] = useState("");
    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    const { t } = useTranslation();

    let base_url = process.env.REACT_APP_API_URL;
    let id_update = getData ? getData.id : "";
    // let urlInput = base_url + "doctype";
    // let urlEdit = base_url + "doctype/" + id_update;
    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });
    const checkDocCode = getAllTable.some((xy) => xy.doc_type_code?.toLowerCase() === documentTypeCode.toLowerCase());
    const checkDocText = getAllTable.some((xy) => xy.doc_type_title?.toLowerCase() === documentTypeText.toLowerCase());
    const changeDocumentTypeCode = (e) => {
        e.preventDefault();
        setCodeTouched(true); // Mark the field as "touched"
        setDocumentTypeCode(e.target.value);
    };

    const changeDocumentTypeText = (e) => {
        e.preventDefault();
        setDocumentTypeText(e.target.value);
    };

    const changeDocumentTypeDesc = (e) => {
        e.preventDefault();
        setDocumentTypeDesc(e.target.value);
    };
    const validationDocCode = () => {
        if (documentTypeCode === "") {
            setValidDocCode(`❌ ${t("empty_um")}`);
        } else if (checkDocCode == true) {
            setValidDocCode(`❌ ${t("doc_code_exists_dt")}`);
        } else if (documentTypeCode !== "" && checkDocCode == false) {
            setValidDocCode("");
        }
    };

    const validationDocText = () => {
        if (documentTypeText === "") {
            setValidDocText(`❌ ${t("empty_um")}`);
        } else if (checkDocText == true) {
            setValidDocText(`❌ ${t("doc_type_exists_dt")}`);
        } else if (documentTypeText !== "" && checkDocText == false) {
            setValidDocText("");
        }
    };
    const validationDocTypeDesc = () => {
        if (documentTypeDesc === "") {
            setValidDocDesc(`❌ ${t("empty_um")}`);
        } else if (documentTypeDesc !== "") {
            setValidDocDesc("");
        }
    };

    const validation = () => {
        if (checkDocCode == false && checkDocText == false && documentTypeDesc !== "") {
            setDisButton(false);
        } else {
            setDisButton(true);
        }
    };

   

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (codeTouched) {
            validationDocCode(); // Only validate after the field has been touched
        }
    }, [documentTypeCode]);

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.doctype.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.doctype.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.doctype.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.doctype.List"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);

    useEffect(() => {
        if (closeButton) {
            setValidDocCode("");
            setValidDocText("");
            setValidDocDesc("");
            // Also reset the input fields if necessary
            // setDocumentTypeCode("");
            // setDocumentTypeText("");
            // setDocumentTypeDesc("");
        }
    }, [closeButton]);

    // Validate documentTypeText only when it changes
    useEffect(() => {
        validationDocText();
    }, [documentTypeText]);

    // Validate documentTypeDesc only when it changes
    useEffect(() => {
        validationDocTypeDesc();
    }, [documentTypeDesc]);

    useEffect(() => {
        validation(); // Check if the form is valid and enable/disable the button
    }, [documentTypeCode, documentTypeText, documentTypeDesc]);

    useEffect(() => {
        if (action === "edit") {
            setDocumentTypeCode(getData.doc_type_code);
            setDocumentTypeText(getData.doc_type_title);
            setDocumentTypeDesc(getData.doc_type_desc);
        } else if (action === "add") {
            setDocumentTypeCode("");
            setValidDocCode("");
            setValidDocText("");
            setDocumentTypeText("");
            setDocumentTypeDesc("");
            setValidDocDesc("");
        }
    }, [onSelected]);

    const handleSubmit = async (event) => {
        event.preventDefault();
       

        const body_data = JSON.stringify({
            "@type": "Doc_Type",
            doc_type_title: documentTypeText,
            doc_type_code: documentTypeCode,
            doc_type_desc: documentTypeDesc,
        });

        try {
            // let config = helper.GetToken();
            // const response = await fetch(
            //     `${action === "edit" ? urlEdit : urlInput}`,
            //     // urlInput,
            //     {
            //         method: `${action === "edit" ? "PATCH" : "POST"}`,
            //         // method:   "POST",
            //         headers: {
            //             "Content-Type": "application/json",
            //             Authorization: config.headers.Authorization,
            //         },
            //         body: body_data,
            //     }
            // );
            let urlInput = dataIp + "doctype";
            let urlEdit = dataIp + "doctype/" + id_update;
            let url = `${action === "edit" ? urlEdit : urlInput}`;
            let response = await helper.SubmitDocType(url, body_data, action);
            if (response.status == 201 || 200) {
                // const modalElement = document.getElementById('exampleModal');
                // const modalInstance = new bootstrap.Modal(modalElement);  // Now bootstrap is defined
                // modalInstance.hide();  // Close the modal
                setDocumentTypeText("");
                setDocumentTypeCode("");
                setDocumentTypeDesc("");
                notify();
                showTable();
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    return (
        <div>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <form onSubmit={handleSubmit}>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="documentTypeCode" class=" ">
                            {t("doc_type_code_dt")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" required class={validDocCode !== "" ? "text-field-with-error" : "form-control"} id="documentTypeCode" value={documentTypeCode} name="documentTypeCode" onChange={changeDocumentTypeCode} />
                        <p class="pt-3">{validDocCode}</p>
                    </div>
                </div>
                <div class="mb-3 row">
                    {" "}
                    <div class="col-4">
                        <label for="documentTypeCode" class=" ">
                            {t("doc_type_name_dt")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validDocText !== "" ? "text-field-with-error" : "form-control"} id="documentTypeText" value={documentTypeText} name="documentTypeText" onChange={changeDocumentTypeText} />
                        <p class="pt-3"> {validDocText}</p>
                    </div>
                </div>
                <div class="mb-3 row" style={{ paddingBottom: "10px" }}>
                    <div class="col-4">
                        <label for="documentTypeCode" class=" ">
                            {t("doc_type_desc_dt")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validDocDesc !== "" ? "text-field-with-error" : "form-control"} id="documentTypeDesc" name="documentTypeDesc" value={documentTypeDesc} onChange={changeDocumentTypeDesc} />
                        <p class="pt-3"> {validDocDesc}</p>
                    </div>
                </div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    {havePermissionAccess.haveAccessAdd && (
                        <button disabled={disButton} class={action === "add" ? "btn btn-primary col-3" : "btn btn-success col-3"} data-bs-dismiss="modal" id="save-btn" type="submit">
                            {action === "add" ? t("add_um") : t("edit_um")}
                        </button>
                    )}

                    <button class="btn btn-danger col-3 mx-3 " data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}>
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputDocType;
