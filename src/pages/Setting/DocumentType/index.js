import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import Del from "../../../assets/images/icon/delete.png";
import Edit from "../../../assets/images/icon/edit.png";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import toast, { Toaster } from "react-hot-toast";
import ModalHapus from "../../../components/Modal/modalHapus";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import ModalInput from "../../../components/Modal/modalInput";
import IpNotValidPage from "../../../components/Page/IpNotValid";
import moment from "moment";
import "moment/locale/id";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
import utils from "../../../api/utils";
const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};
function DocumentType() {
    let base_url = helper.GetBaseUrl(); //process.env.REACT_APP_API_URL;
    const { t } = useTranslation();

    const [data, setData] = useState([]);
    const [action, setAction] = useState("");
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [getData, setGetData] = useState([]);
    const [sorting, setSorting] = useState([]);
    const [onSelected, setOnSelected] = useState(false);
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const [closeButtonDocType, setCloseButtonDocType] = useState(false);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            showTable();
        }
    }, [dataIp]);

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.doctype.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.doctype.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.doctype.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.doctype.List"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
        // showTable();
    }, []);

    const handlerDelete = async (id) => {
        try {
            // let config = helper.GetToken();
            let id_doctype = getData ? getData.id : "";
            if (getData && id_doctype && id_doctype != "") {
                let url = dataIp + "doctype/" + id_doctype;

                let response = await helper.DeleteDocType(url);
                if ((response && response.status == 200) || 201 || 202 || 203 || 204) {
                    utils.sweetAlertSuccess("Hapus data 'Document Type' berhasil!");
                    showTable();
                }
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
        }
    };

    const modifyData = (data) => {
        if (data && data.length > 0) {
            // const selBulan = [t("januari_um"), t("februari_um"), t("maret_um"), t("april_um"), t("mei_um"), t("juni_um"), t("juli_um"), t("agustus_um"), t("september_um"), t("oktober_um"), t("november_um"), t("desember_um")];
            // data?.forEach((doc) => {
            //     const cd = new Date(doc.doc_created_date);
            //     doc.doc_created_date = cd.getDate() + " " + selBulan[cd.getMonth()] + " " + cd.getFullYear() + " " + cd.getHours() + ":" + cd.getMinutes();
            // });

            // Sorting data berdasarkan modification_date secara descending (data terbaru di atas)
            return data.sort((a, b) => a.doc_type_index_no - b.doc_type_index_no);
        } else {
            return [];
        }
    };

    const showTable = async () => {
        try {
            let url = dataIp + "/doctype/@list_doctype";
            let res = await helper.GetListDocType(url);
            if ((res && res.status == 200) || 201 || 202 || 204) {
                let mydata = modifyData(res.data);
                setData(res.data);
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
        }
    };

    const columns = [
        {
            accessorKey: "doc_type_code",
            header: t("doc_kode_pr"),
            enableColumnFilter: true,
        },
        {
            accessorKey: "doc_type_title",
            header: t("doc_name_td"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "doc_type_created_date",
            header: t("create_date_um"),
            cell: (info) => moment(info.getValue()).format("DD MMMM YYYY"),
            enableColumnFilter: true,
        },
        {
            accessorKey: "doc_type_desc",
            header: t("description_um"),
            enableColumnFilter: true,
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            width: "100px",
                            gap: "4px",
                        }}
                    >
                        {havePermissionAccess.haveAccessModify && (
                            <button
                                class="btn"
                                title="Edit"
                                type="button"
                                data-bs-toggle="modal"
                                data-bs-target="#ModalInput"
                                onClick={() => {
                                    setAction("edit");
                                    setCloseButtonDocType(false);
                                    setGetData(row.original);
                                    setOnSelected(!onSelected);
                                }}
                            >
                                <img src={Edit} width={"80%"} />
                            </button>
                        )}
                        {havePermissionAccess.haveAccessModify && (
                            <button
                                type="button"
                                title="Hapus"
                                className="btn"
                                data-bs-toggle="modal"
                                data-bs-target="#modalHapus"
                                onClick={() => {
                                    setAction("hapus");
                                    setGetData(row.original);
                                }}
                            >
                                <img src={Del} width={"80%"} />
                            </button>
                        )}
                    </div>
                );
            },
        },
    ];

    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });

    return (
        <div class="container-fluid pt-3 " style={{ height: "auto", marginTop: "75px" }}>
            <ModalInput
                getAllTable={data}
                keyboard="false"
                backdrop="static"
                onSelected={onSelected}
                action={action}
                getData={getData ? getData : ""}
                showTable={showTable}
                // setCloseButtonDocType = {setCloseButtonDocType}
                // closeButtonDocType = {closeButtonDocType}
                // setGetData={setGetData}
                // validDocCode ={validDocCode}
                // setValidDocCode={setValidDocCode}
                // validDocText ={validDocText}
                // setValidDocText ={setValidDocText}
                // validDocDesc ={validDocDesc}
                // setValidDocDesc = {setValidDocDesc}
            />
            <ModalHapus handlerDelete={handlerDelete} getData={getData} keyboard="false" backdrop="static" />
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "#ffbaba",
                        color: "#ff0000 ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-3 textfamily">{t("titlle_dt")}</h5>
                    <div class="row ">
                        <div class="col-10">
                            <Filters filtering={filtering} setFiltering={setFiltering} />
                        </div>
                        {havePermissionAccess.haveAccessAdd && (
                            <div class="col-2">
                                <button
                                    class="btn btn-primary float-end"
                                    type="button"
                                    onClick={() => {
                                        setAction("add");
                                        setCloseButtonDocType(false);
                                        setOnSelected(!onSelected);
                                    }}
                                    data-bs-toggle="modal"
                                    title="Tambah"
                                    data-bs-target="#ModalInput"
                                >
                                    ✙ {t("titlle_dt")}
                                </button>
                            </div>
                        )}
                    </div>
                    {/* <div class="row mt-2"> */}
                    <table className="table table-bordered table-striped table-hover">
                        <thead className="table-primary">
                            {table.getHeaderGroups().map((headerGroup) => (
                                <tr key={headerGroup.id} className="  uppercase">
                                    {headerGroup.headers.map((header) => (
                                        <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                            {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        {data === undefined || data.length == 0 ? (
                            <tbody>
                                <tr>
                                    <td colSpan="5" style={{ textAlign: "center", margin: "0 auto" }}>
                                        <span
                                            style={{
                                                color: "red",
                                                fontWeight: "bold",
                                                fontSize: "16px",
                                            }}
                                        >
                                            <IpNotValidPage />
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        ) : (
                            <tbody>
                                {table.getRowModel().rows.map((row) => (
                                    <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                        {row.getVisibleCells().map((cell) => (
                                            <td className="px-4 py-2" key={cell.id}>
                                                {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        )}
                    </table>
                    <Paginations table={table} />
                    {/* </div> */}
                </div>
            </div>
        </div>
    );
}

export default DocumentType;
