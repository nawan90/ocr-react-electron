import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Del from "../../../assets/images/icon/delete.png";
import Edit from "../../../assets/images/icon/edit.png";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import toast, { Toaster } from "react-hot-toast";
import ModalHapus from "../../../components/Modal/modalHapus";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import LoadSpinner from "../../../components/Load/load";
import ModalInput from "../../../components/Modal/modalInput";
import IpNotValidPage from "../../../components/Page/IpNotValid";
import Helper from "../../../api/helper";
import { useTranslation } from "react-i18next";
import utils from "../../../api/utils";

const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};

function Polda() {
    const navigate = useNavigate();
     let base_url = process.env.REACT_APP_API_URL;
    const { t } = useTranslation();

    const [action, setAction] = useState("");
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [getData, setGetData] = useState([]);
    const [sorting, setSorting] = useState([]);
    const [onSelected, setOnSelected] = useState(false);
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const [data, setData] = useState([]);
    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
           
            showTable();
        }
    }, [dataIp]);

    useEffect(() => {
        
        // showTable();
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.polda.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.polda.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.polda.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.polda.list"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);

    const showTable = async () => {
        try {
            let datas = await Helper.GetListPolda(dataIp + "/polridocument/@list_polda");
            setData(datas);
        } catch (err) {
            console.log(`${t("error_um")}`, err);
            if (err && err.hasOwnProperty("status")) {
                if (err.status == 401) {
                    toast(`${t("session_exp_um")}`, { icon: "👏" });
                    navigate("/home");
                }
            }
        }
    };

    const handlerDelete = async (id) => {
        try {
            let id_polda = getData ? getData.id : "";
            if (getData && id_polda && id_polda != "") {
                let url = dataIp + "/polridocument/" + id_polda;

                let response = await Helper.DeletePolda(url);
                // setData(datas);
                if (response.status == 201 || 200 || 203 || 204 || 202) {
                    utils.sweetAlertSuccess("Hapus data 'Korwil' berhasil!");
                    showTable();
                }
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
            if (err && err.hasOwnProperty("status")) {
                if (err.status == 401) {
                    toast(`${t("session_exp_um")}`, { icon: "👏" });
                    navigate("/home");
                }
            }
        }
    };
    const columns = [
        {
            accessorKey: "polda_code",
            header: t("polda_code_lp"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "polda_name",
            header: t("polda_name_lp"),
            enableColumnFilter: true,
        },
        {
            accessorKey: "polda_propinsi_name",
            header: t("titlle_pr"),
            // cell: EditableCell,
            enableColumnFilter: true,
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",
                            width: "100px",
                            gap: "4px",
                        }}
                    >
                        {havePermissionAccess.haveAccessModify && (
                            <button
                                class="btn"
                                type="button"
                                data-bs-toggle="modal"
                                data-bs-target="#ModalInput"
                                onClick={() => {
                                    setAction("edit");
                                    setGetData(row.original);
                                    setOnSelected(!onSelected);
                                }}
                            >
                                <img src={Edit} width={"80%"} />
                            </button>
                        )}
                        {havePermissionAccess.haveAccessModify && (
                            <button
                                type="button"
                                className="btn"
                                title="Hapus"
                                data-bs-toggle="modal"
                                data-bs-target="#modalHapus"
                                onClick={() => {
                                    setAction("hapus");
                                    setGetData(row.original);
                                }}
                            >
                                <img src={Del} width={"80%"} />
                            </button>
                        )}
                    </div>
                );
            },
        },
    ];
    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });
    return (
        <div class="container-fluid pt-3 " style={{  height: "auto", marginTop: "75px" }}>
            {/* {callLoading()} */}
            <ModalInput getAllTable={data} keyboard="false" backdrop="static" onSelected={onSelected} action={action} getData={getData ? getData : ""} showTable={showTable} />
            <ModalHapus getData={getData} keyboard="false" backdrop="static" handlerDelete={handlerDelete} />
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "#ffbaba",
                        color: "#ff0000 ",
                    },
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            {/* LEFT SIDE */}

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-3 textfamily">{t("titlle_lp")}</h5>
                    <div class="row">
                        <div class="col-10">
                            <Filters filtering={filtering} setFiltering={setFiltering} />
                        </div>
                        {havePermissionAccess.haveAccessAdd && (
                            <div class="col-2">
                                <button
                                    class="btn btn-primary float-end"
                                    type="button"
                                    onClick={() => {
                                        setAction("add");
                                        setOnSelected(!onSelected);
                                    }}
                                    data-bs-toggle="modal"
                                    title="Tambah"
                                    data-bs-target="#ModalInput"
                                >
                                    ✙ {t("titlle_lp")}
                                </button>
                            </div>
                        )}
                    </div>
                    {/* <div class="row mt-2"> */}
                    <table className="table table-bordered table-striped table-hover">
                        <thead className="table-primary">
                            {table.getHeaderGroups().map((headerGroup) => (
                                <tr key={headerGroup.id} className="  uppercase">
                                    {headerGroup.headers.map((header) => (
                                        <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                            {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        {data === undefined || data.length == 0 ? (
                            <tbody>
                                <tr>
                                    <td colSpan="5" style={{ textAlign: "center", margin: "0 auto" }}>
                                        <span
                                            style={{
                                                color: "red",
                                                fontWeight: "bold",
                                                fontSize: "16px",
                                            }}
                                        >
                                            <IpNotValidPage />
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        ) : (
                            <tbody>
                                {table.getRowModel().rows.map((row) => (
                                    <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                        {row.getVisibleCells().map((cell) => (
                                            <td className="px-4 py-2" key={cell.id}>
                                                {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        )}
                    </table>
                    <Paginations table={table} />
                    {/* </div> */}
                </div>
            </div>
        </div>
    );
}

export default Polda;
