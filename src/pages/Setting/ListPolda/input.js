import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import LoadSpinner from "../../../components/Load/load";
import "../../../assets/css/addcss.css";
import Helper from "../../../api/helper";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";

const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};

function InputPolda(props) {
    const { showTable, getData, action, onSelected, getAllTable } = props;
    const { t } = useTranslation();
    const [poldaName, setPoldaName] = useState("");
    const [poldaCode, setPoldaCode] = useState("");
    const [provinsiId, setProvinsiId] = useState("");
    const [provinsiName, setProvinsiName] = useState("");
    const [metadataRef, setMetadataRef] = useState(null);
    const [metadata, setMetadata] = useState([]);
    const [provinsiRef, setProvinsiRef] = useState(null);
    const [provinsiOptionsHtml, setProvinsiOptionsHtml] = useState([]);
    const [loadSpin, setLoadSpinner] = useState(false);

    const [newItemMetadata, setNewItemMetadata] = useState("");
    const [newContentItemMetadata, setNewContentItemMetadata] = useState("");
    const [poldaMetadataList, setPoldaMetadataList] = useState([]);
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const [disButton, setDisButton] = useState(true);
    const [validPoldaCode, setValidPoldaCode] = useState("");
    const [validPoldaName, setValidPoldaName] = useState("");
    const [validProvinsi, setValidProvinsi] = useState("");

    const [newRow, setNewRow] = useState({
        polda_metadata_id: null,
        polda_metadata_name: "",
        polda_metadata_value: "",
    });
    const initialRows = [{ polda_metadata_id: 1, polda_metadata_name: "", polda_metadata_value: "" }];
    const [rows, setRows] = useState(initialRows);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    let base_url = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    let id_update = getData ? getData.id : "";

    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });
    const checkPoldaCode = getAllTable.some((xy) => xy.polda_code?.toLowerCase() === poldaCode.toLowerCase());
    const checkPoldaName = getAllTable.some((xy) => xy.polda_name?.toLowerCase() === poldaName.toLowerCase());
    const provMap = getAllTable.map((xy) => xy.polda_propinsi_name?.toLowerCase());

    const changePoldaCode = (e) => {
        e.preventDefault();
        setPoldaCode(e.target.value);
    };

    const changePoldaName = (e) => {
        e.preventDefault();
        setPoldaName(e.target.value);
    };
    const changeProvinsi = (e) => {
        setProvinsiName(e.target.value);
    };

    const cekDouble = poldaCode !== getData.polda_code && poldaName === getData.polda_name;

    const cekButtonAdd = poldaCode !== "" && checkPoldaCode == false && poldaName !== "" && checkPoldaName == false && provinsiName !== "" && provinsiName !== "Pilih Provinsi";

    const cekButtonEdit = (poldaCode !== "") & (poldaName !== "") && provinsiName !== "" && provinsiName !== "Pilih Provinsi";

    const validationEdit = () => {
        if (poldaCode === "") {
            setValidPoldaCode(`❌ ${t("empty_um")}`);
            setDisButton(true);
        } else if (poldaCode !== "") {
            setValidPoldaCode("");
        }

        if (poldaName === "") {
            setValidPoldaName(`❌ ${t("empty_um")}`);
            setDisButton(true);
        } else if (poldaName !== "") {
            setValidPoldaName("");
        }

        if (provinsiName === "" || provinsiName === "Pilih Provinsi") {
            setDisButton(true);
            setValidProvinsi(`❌ ${t("empty_um")}`);
        } else if (provinsiName !== "" || provinsiName !== "Pilih Provinsi") {
            setValidProvinsi("");
        }
        if (cekDouble == true) {
            setValidPoldaName(`❌ ${t("polda_avail_lp")}`);
            setValidPoldaCode(`❌ ${t("polda_avail_lp")}`);
            setDisButton(true);
        }
        if (cekButtonEdit == true && cekDouble == false) {
            setDisButton(false);
        }
    };

    const validationAdd = () => {
        if (poldaCode === "") {
            setValidPoldaCode(`❌ ${t("empty_um")}`);
            setDisButton(true);
        } else if (checkPoldaCode == true) {
            setValidPoldaCode(`❌ ${t("polda_code_avail_lp")}`);
            setDisButton(true);
        } else if (poldaCode !== "" && checkPoldaCode == false) {
            setValidPoldaCode("");
        }

        if (poldaName === "") {
            setValidPoldaName(`❌ ${t("empty_um")}`);
            setDisButton(true);
        } else if (checkPoldaName == true) {
            setValidPoldaName(`❌ ${t("polda_name_avail_lp")}`);
            setDisButton(true);
        } else if (poldaName !== "" && checkPoldaName == false) {
            setValidPoldaName("");
        }

        if (provinsiName === "" || provinsiName === "Pilih Provinsi") {
            setDisButton(true);
            setValidProvinsi(`❌ ${t("empty_um")}`);
        } else if (provinsiName !== "" || provinsiName !== "Pilih Provinsi") {
            setValidProvinsi("");
        }
        if (cekButtonAdd == true) {
            setDisButton(false);
        }
    };

    useEffect(
        () => {
            if (action === "add") {
                validationAdd();
            } else if (action === "edit") {
                validationEdit();
            }
        },
        [action][changePoldaCode],
        [changePoldaName],
        [changeProvinsi],
        [cekButtonAdd]
    );

    const changeContentPoldaMetadata = (e) => setNewContentItemMetadata(e.target.value);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            loadDataProvinsi();
            loadMetadata();
        }
    }, [dataIp]);

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.polda.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.polda.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.polda.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.polda.list"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);

    useEffect(() => {
        setTimeout(() => setLoadSpinner(false), 3300);
    }, [loadSpin]);

    const callLoading = () => {
        if (loadSpin == true) {
            return <LoadSpinner />;
        } else {
            return <div></div>;
        }
    };

    const onHandlerAddMetadata = async () => {
        let metadata = {
            metadata_name: newItemMetadata,
            metadata_value: newContentItemMetadata,
        };
        poldaMetadataList.push(metadata);
        setNewItemMetadata("");
        setNewContentItemMetadata("");
    };

    const loadDataProvinsi = async () => {
        try {
            let url = dataIp + "/provinsimaster/@list_provinsi";
            let res = await helper.GetListProvinsi(url);
            if ((res && res.status == 201) || 202 || 200 || 204) {
                setProvinsiRef(res.data);
            }

            // let { data } = await Helper.GetListProvinsi(url);
            // setProvinsiRef(data);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };
    const optionProvinsi = provinsiRef?.map(function (item) {
        return item.provinsi_name;
    });
    const loadMetadata = async () => {
        try {
            let url = dataIp + "/metadatamaster/@list_metadata";

            let res = await helper.GetListMetadata(url);
            if ((res && res.status == 200) || 201 || 202 || 204) {
                setMetadataRef(res.data);
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };
    const optionMetadata = metadataRef?.map(function (item) {
        return item.metadata_name;
    });

    useEffect(() => {
        if (action === "edit") {
            setPoldaName(getData.polda_name);
            setPoldaCode(getData.polda_code);

            setProvinsiName(getData.polda_propinsi_name);
            setPoldaMetadataList(getData?.polda_metadata);
        } else if (action === "add") {
            setPoldaName("");
            setPoldaCode("");
            setProvinsiId("");
            setProvinsiName("");
            setPoldaMetadataList([]);
        }
    }, [onSelected]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const body_data = JSON.stringify({
            "@type": "Polda_Master",
            polda_name: poldaName,
            polda_code: poldaCode,
            polda_propinsi_id: provinsiId,
            polda_propinsi_name: provinsiName,
            polda_metadata: rows,
        });
        try {
            let urlInput = dataIp + "/polridocument";
            let urlEdit = dataIp + "/polridocument/" + id_update;

            let url = `${action === "edit" ? urlEdit : urlInput}`;
            let res = helper.SubmitPolda(url, body_data, action);
            if ((res && res.status == 200) || 201 || 202 || 204) {
                setPoldaCode("");
                setPoldaName("");
                setProvinsiId("");
                setProvinsiName("");
                setPoldaMetadataList([]);
                notify();
                showTable();
            }
        } catch (err) {
            console.log(`${t("error_um")}`, err);
        }
    };
    const onHandlerRemoveMetadata = async (data) => {
        setPoldaMetadataList(
            poldaMetadataList?.filter(function (obj) {
                return obj.metadata_name !== data.metadata_name;
            })
        );
        setNewItemMetadata("");
        setNewContentItemMetadata("");
    };
    const definePoldaMetadata = () => {
        let listMetadata = [];
        poldaMetadataList.map(function (data, index) {
            let metadata = {
                polda_metadata_id: "",
                polda_metadata_name: "",
                polda_metadata_value: "",
            };
            let ref = metadataRef.find((element) => element.metadata_id == data.metadata_name);
            metadata["polda_metadata_id"] = data.metadata_name;
            metadata["polda_metadata_name"] = ref.metadata_name;
            metadata["polda_metadata_value"] = data.metadata_value;
            listMetadata.push(metadata);
        });

        return setMetadata(listMetadata);
    };

    function checkArrayIndex0(arr) {
        if (arr && arr.length > 0 && arr[0] !== undefined) {
            return true;
        } else {
            return false;
        }
    }

    // Function to handle changes in select dropdown
    const handleSelectChange = (e, index) => {
        const { value } = e.target;
        setRows((prevRows) => {
            const updatedRows = [...prevRows];
            updatedRows[index] = {
                ...updatedRows[index],
                polda_metadata_name: value,
                polda_metadata_value: "", // Optionally reset value if needed
            };
            return updatedRows;
        });
    };

    const handleChangeMetadataVal = (event, index) => {
        const { value } = event.target;
        setRows((prevRows) => {
            const updatedRows = [...prevRows];
            updatedRows[index] = {
                ...updatedRows[index],
                polda_metadata_value: value,
            };
            return updatedRows;
        });
    };

    // Function to add a new row
    const handleAddRow = () => {
        const newRow = {
            id: rows.length > 0 ? rows[rows.length - 1].id + 1 : 1,
            polda_metadata_name: "",
            polda_metadata_value: "",
        };
        setRows([...rows, newRow]);
    };

    // Function to remove a row by index
    const handleRemoveRow = (index) => {
        const updatedRows = [...rows];
        updatedRows.splice(index, 1);
        setRows(updatedRows);
    };

    useEffect(() => {
        if (checkArrayIndex0(poldaMetadataList) == false) {
            setRows(initialRows);
        } else {
            setRows(poldaMetadataList);
        }
    }, [poldaMetadataList]);

    return (
        <div>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <form onSubmit={handleSubmit}>
                <h5 class="card-title mb-3">{t("titlle_lp")}</h5>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="poldaCode" class="col-sm-6 col-form-label">
                            {t("code_um")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validPoldaCode !== "" ? "text-field-with-error" : "form-control"} id="poldaCode" value={poldaCode} name="poldaCode" onChange={changePoldaCode} />
                        <p class="pt-3">{validPoldaCode}</p>
                    </div>
                </div>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="poldaName" class="col-sm-6 col-form-label">
                            {t("polda_name_lp")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validPoldaName !== "" ? "text-field-with-error" : "form-control"} id="poldaName" value={poldaName} name="poldaName" onChange={changePoldaName} />
                        <p class="pt-3">{validPoldaName}</p>
                    </div>
                </div>
                <div class="mb-3 row" style={{ paddingBottom: "20px" }}>
                    <div class="col-4">
                        <label for="provinsiName" class="col-sm-3 col-form-label">
                            {t("titlle_pr")}
                        </label>
                    </div>
                    <div class="col-8">
                        <select options={provMap} onChange={(option) => changeProvinsi(option)} id="selectProvinsi" value={provinsiName} class="form-select" aria-label="Default select example">
                            <option selected>
                                {t("select_um")} {t("titlle_pr")}
                            </option>
                            {getAllTable?.map((item, i) => (
                                <option>{item.polda_propinsi_name}</option>
                            ))}
                            <option disabled>──────</option>
                            {optionProvinsi?.map((option, idx) => (
                                <option key={idx} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>
                        <p class="pt-3">{validProvinsi}</p>
                    </div>
                </div>

                {/* <h5 class="card-title mb-2">{t("titlle_md")}</h5>
                {checkArrayIndex0(poldaMetadataList) == false ? (
                    <div>
                        {rows.map((row, index) => (
                            <div className="mb-3 row" key={row.polda_metadata_id}>
                                <div class="col-4">
                                    <select class="form-select" value={row.polda_metadata_name} onChange={(e) => handleSelectChange(e, index)}>
                                        <option value="">
                                            {t("select_um")} {t("titlle_md")}
                                        </option>
                                        {optionMetadata?.map((option, idx) => (
                                            <option key={idx} value={option}>
                                                {option}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div class="col-6">
                                    <input type="text" name="polda_metadata_value" value={row.polda_metadata_value} class="form-control" id={`poldaMetadataCode${row.polda_metadata_id}`} onChange={(e) => handleChangeMetadataVal(e, index)} />
                                </div>
                                <div className="col-2">
                                    {index === rows.length - 1 ? (
                                        <button type="button" className="btn btn-primary" onClick={handleAddRow}>
                                            +
                                        </button>
                                    ) : (
                                        <button type="button" className="btn btn-danger" onClick={() => handleRemoveRow(index)}>
                                            -
                                        </button>
                                    )}
                                </div>
                            </div>
                        ))}
                    </div>
                ) : (
                    <div>
                        {poldaMetadataList.length > 0 ? (
                            rows.map((item, index, row) => (
                                <div class="mb-3 row" key={item.polda_metadata_id}>
                                    <div class="col-4">
                                        <select className="form-select" value={item?.polda_metadata_name} id={`poldaMetadataName${item.polda_metadata_id}`} onChange={(e) => handleSelectChange(e, index)}>
                                            <option value="">
                                                {t("select_um")} {t("titlle_md")}
                                            </option>
                                            <option value={item?.polda_metadata_name}>{item?.polda_metadata_name}</option>
                                            <option disabled>──────</option>
                                            {optionMetadata.map((option, idx) => (
                                                <option key={idx} value={option}>
                                                    {option}
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <input type="text " value={item.polda_metadata_value} class="form-control" name="polda_metadata_value" id={`poldaMetadataCode${item.polda_metadata_id}`} onChange={(e) => handleChangeMetadataVal(e, index)} />
                                    </div>
                                    <div className="col-2">
                                        {index === rows.length - 1 ? (
                                            <button type="button" className="btn btn-primary" onClick={handleAddRow}>
                                                +
                                            </button>
                                        ) : (
                                            <button type="button" className="btn btn-danger" onClick={() => handleRemoveRow(index)}>
                                                -
                                            </button>
                                        )}
                                    </div>
                                </div>
                            ))
                        ) : (
                            <p>{t("avail_metada_lp")}</p>
                        )}
                    </div>
                )} */}
                <div class="mb-4 row" style={{ paddingBottom: "10px" }}></div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    {havePermissionAccess.haveAccessAdd || havePermissionAccess.haveAccessModify ? (
                        <button disabled={disButton} class={action === "add" ? "btn btn-primary col-3" : "btn btn-success col-3"} id="btnSave" data-bs-dismiss="modal" type="submit">
                            {action === "add" ? t("add_um") : t("edit_um")}
                        </button>
                    ) : null}
                    <button class="btn btn-danger col-3 mx-3 " data-bs-dismiss="modal" aria-label="Close" onClick={(e) => e.preventDefault()}>
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputPolda;
