import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import Del from "../../../assets/images/icon/delete.png";
import Edit from "../../../assets/images/icon/edit.png";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import toast, { Toaster } from "react-hot-toast";
import ModalHapus from "../../../components/Modal/modalHapus";
import ModalInput from "../../../components/Modal/modalInput";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import IpNotValidPage from "../../../components/Page/IpNotValid";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
import utils from "../../../api/utils";

const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};

function Provinsi() {
     let base_url = helper.GetBaseUrl(); //process.env.REACT_APP_API_URL;
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [action, setAction] = useState("");
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [getData, setGetData] = useState([]);
    const [sorting, setSorting] = useState([]);
    const [onSelected, setOnSelected] = useState(false);

    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";
        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };
        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            
            showTable();
        }
    }, [dataIp]);

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.province.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.province.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.province.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.province.List"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
        // showTable();
    }, []);

    const handlerDelete = async (id) => {
        try {
            let id_provinsi = getData ? getData.id : "";
            if (getData && id_provinsi && id_provinsi != "") {
                let url = dataIp + "provinsimaster/" + id_provinsi;

                let response = await helper.DeleteProvinsi(url);
                if ((response && response.status == 200) || 201 || 202 || 203 || 204) {
                    utils.sweetAlertSuccess("Hapus data provinsi berhasil!");
                    showTable();
                }
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    const showTable = async () => {
        try {
            let url = dataIp + "/provinsimaster/@list_provinsi";

            let response = await helper.GetListProvinsi(url);
            if (response.status == 200 || 201 || 202 || 203 || 204) {
                setData(response.data);
            }
        } catch (err) {
            if (err.status == 401) {
                utils.sweetAlertError(`${t("session_exp_um")}`);
                navigate("/home");
            }
            console.log(`${t("error_um")}`);
        }
    };

    const columns = [
        {
            accessorKey: "provinsi_code",
            header: t("prov_kode_pr"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            accessorKey: "provinsi_name",
            header: t("prov_name_pr"),
            enableColumnFilter: true,
        },
        {
            accessorKey: "provinsi_desc",
            header: t("description_um"),
            enableColumnFilter: true,
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div
                        style={{
                            display: "flex",
                            flexDirection: "row",

                            gap: "4px",
                            width: "100px",
                        }}
                    >
                        {havePermissionAccess.haveAccessModify && (
                            <button
                                class="btn"
                                type="button"
                                title="Edit"
                                data-bs-toggle="modal"
                                data-bs-target="#ModalInput"
                                onClick={() => {
                                    setAction("edit");
                                    setGetData(row.original);
                                    setOnSelected(!onSelected);
                                }}
                            >
                                <img src={Edit} width={"80%"} />
                            </button>
                        )}
                        {havePermissionAccess.haveAccessModify && (
                            <button type="button" className="btn" title="Hapus" data-bs-toggle="modal" data-bs-target="#modalHapus" onClick={() => setGetData(row.original)}>
                                <img src={Del} width={"80%"} />
                            </button>
                        )}
                    </div>
                );
            },
        },
    ];

    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });

    return (
        <div class="container-fluid pt-3 " style={{  height: " auto", marginTop: "75px" }}>
            <ModalInput getAllTable={data} keyboard="false" backdrop="static" onSelected={onSelected} action={action} getData={getData ? getData : ""} showTable={showTable} />
            <ModalHapus handlerDelete={handlerDelete} getData={getData} keyboard="false" backdrop="static" />
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "#ffbaba",
                        color: "#ff0000 ",
                    },

                    // Default options for specific types
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />

            {/* <div class="row"> */}
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-3 textfamily">{t("titlle_pr")}</h5>

                    <div class="row ">
                        <div class="col-11">
                            <Filters filtering={filtering} setFiltering={setFiltering} />
                        </div>
                        {havePermissionAccess.haveAccessAdd && (
                            <div class="col-1">
                                <button
                                    class="btn btn-primary float-end"
                                    type="button"
                                    onClick={() => {
                                        setAction("add");
                                        setOnSelected(!onSelected);
                                    }}
                                    data-bs-toggle="modal"
                                    title="Tambah"
                                    data-bs-target="#ModalInput"
                                >
                                    ✙ {t("titlle_pr")}
                                </button>
                            </div>
                        )}
                    </div>
                    {/* <div class="row mt-2"> */}
                    <table className="table table-bordered table-striped table-hover">
                        <thead className="table-primary">
                            {table.getHeaderGroups().map((headerGroup) => (
                                <tr key={headerGroup.id} className="  uppercase">
                                    {headerGroup.headers.map((header) => (
                                        <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                            {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        {data === undefined || data.length == 0 ? (
                            <tbody>
                                <tr>
                                    <td colSpan="5" style={{ textAlign: "center", margin: "0 auto" }}>
                                        <span
                                            style={{
                                                color: "red",
                                                fontWeight: "bold",
                                                fontSize: "16px",
                                            }}
                                        >
                                            <IpNotValidPage />
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        ) : (
                            <tbody>
                                {table.getRowModel().rows.map((row) => (
                                    <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                        {row.getVisibleCells().map((cell) => (
                                            <td className="px-4 py-2" key={cell.id}>
                                                {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        )}
                    </table>
                    <Paginations table={table} />
                    {/* </div> */}
                </div>
            </div>
            {/* </div> */}
        </div>
    );
}

export default Provinsi;
