import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};

function InputProvinsi(props) {
    const { showTable, getData, action, onSelected, getAllTable, handleClose, disButton, setDisButton } = props;
    const { t } = useTranslation();

    const [provinsiText, setProvinsiText] = useState(action ? getData.provinsi_name : "");
    const [provinsiCode, setProvinsiCode] = useState(action ? getData.provinsi_code : "");
    const [provinsiDesc, setProvinsiDesc] = useState(action ? getData.provinsi_desc : "");
    const [validProvCode, setValidProvCode] = useState("");
    const [validProvText, setValidProvText] = useState("");
    const [validProvDesc, setValidProvDesc] = useState("");
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    let base_url = helper.GetBaseUrl(); //process.env.REACT_APP_API_URL;
    let id_update = getData ? getData.id : "";

    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });
    const checkProvCode = getAllTable.some((xy) => xy.provinsi_code?.toLowerCase() === provinsiCode.toLowerCase());
    const checkProvText = getAllTable.some((xy) => xy.provinsi_name?.toLowerCase() === provinsiText.toLowerCase());

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.province.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.province.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.province.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.province.List"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);
    const validationProvCode = () => {
        if (provinsiCode === "") {
            setValidProvCode(`❌ ${t("empty_um")}`);
        } else if (checkProvCode == true) {
            setValidProvCode(`❌ ${t("doc_kode_avail_pr")}`);
        } else if (provinsiCode !== "" && checkProvCode == false) {
            setValidProvCode("");
        }
    };

    const validationProvText = () => {
        if (provinsiText === "") {
            setValidProvText(`❌ ${t("empty_um")}`);
        } else if (checkProvText == true) {
            setValidProvText(`❌ ${t("prov_avail_pr")}`);
        } else if (provinsiText !== "" && checkProvText == false) {
            setValidProvText("");
        }
    };
    const validationProvDesc = () => {
        if (provinsiDesc === "") {
            setValidProvDesc(`❌ ${t("empty_um")}`);
        } else if (provinsiDesc !== "") {
            setValidProvDesc("");
        }
    };

    const changeprovinsiCode = (e) => {
        e.preventDefault();
        setProvinsiCode(e.target.value);
    };

    const changeprovinsiText = (e) => {
        e.preventDefault();
        setProvinsiText(e.target.value);
    };

    const changeprovinsiDesc = (e) => {
        e.preventDefault();
        setProvinsiDesc(e.target.value);
    };

    useEffect(() => {
        validationProvCode();
    }, [changeprovinsiCode]);
    useEffect(() => {
        validationProvText();
    }, [changeprovinsiText]);
    useEffect(() => {
        validationProvDesc();
    }, [changeprovinsiDesc]);

    const validation = () => {
        if (checkProvCode == false && checkProvText == false && provinsiDesc !== "") {
            setDisButton(false);
        } else {
            setDisButton(true);
        }
    };

    useEffect(
        () => {
            validation();
        },
        [changeprovinsiCode],
        [changeprovinsiText],
        [changeprovinsiDesc]
    );

    useEffect(() => {
        if (action === "edit") {
            setProvinsiText(getData.provinsi_name);
            setProvinsiCode(getData.provinsi_code);
            setProvinsiDesc(getData.provinsi_desc);
        } else if (action === "add") {
            setProvinsiText("");
            setProvinsiCode("");
            setProvinsiDesc("");
        }
    }, [onSelected]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const body_data = JSON.stringify({
            "@type": "Provinsi_Master",
            provinsi_name: provinsiText,
            provinsi_code: provinsiCode,
            provinsi_desc: provinsiDesc,
        });

        try {
            let urlInput = dataIp + "provinsimaster";
            let urlEdit = dataIp + "provinsimaster/" + id_update;
            let url = `${action === "edit" ? urlEdit : urlInput}`;
            let response = await helper.SubmitProvinsi(url, body_data, action);
            if (response.status == 201 || 200 || 204) {
                setProvinsiText("");
                setProvinsiCode("");
                setProvinsiDesc("");
                notify();
                showTable();
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    return (
        <div>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <form onSubmit={handleSubmit}>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="provinsiCode" class=" ">
                            {t("prov_kode_pr")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validProvCode !== "" ? "text-field-with-error" : "form-control"} id="provinsiCode" value={provinsiCode} name="provinsiCode" onChange={changeprovinsiCode} />
                        <p class="pt-3">{validProvCode}</p>
                    </div>
                </div>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="provinsiText" class=" ">
                            {t("prov_name_pr")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validProvText !== "" ? "text-field-with-error" : "form-control"} id="provinsiText" value={provinsiText} name="provinsiText" onChange={changeprovinsiText} />
                        <p class="pt-3">{validProvText} </p>
                    </div>
                </div>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="provinsiDesc" class="">
                            {t("description_um")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validProvDesc !== "" ? "text-field-with-error" : "form-control"} id="provinsiDesc" value={provinsiDesc} name="provinsiDesc" onChange={changeprovinsiDesc} />
                        <p class="pt-3">{validProvDesc}</p>
                    </div>
                </div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    {havePermissionAccess.haveAccessAdd || havePermissionAccess.haveAccessModify ? (
                        <button disabled={disButton} class={action === "add" ? "btn btn-primary col-3" : "btn btn-success col-3"} id="btnSave" data-bs-dismiss="modal" type="submit">
                            {action === "add" ? t("add_um") : t("edit_um")}
                        </button>
                    ) : null}
                    <button class="btn btn-danger col-3 mx-3 " data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}>
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputProvinsi;
