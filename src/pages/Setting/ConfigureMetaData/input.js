import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import "../../../assets/css/addcss.css";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";
const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};
function InputMetaData(props) {
    const { showTable, getData, action, onSelected, getAllTable } = props;
    const [metadataText, setMetadataText] = useState(action ? getData.metadata_name : "");
    const [disButton, setDisButton] = useState(true);
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const [valid, setValid] = useState("");
    const { t } = useTranslation();

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    let base_url = process.env.REACT_APP_API_URL;
    let id_update = getData ? getData.id : "";
    // let urlInput = base_url + "metadatamaster";
    // let urlEdit = base_url + "metadatamaster/" + id_update;
    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });

    useEffect(() => {
        
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    const checkMetadata = getAllTable.some((xy) => xy.metadata_name?.toLowerCase() === metadataText.toLowerCase());
    const validation = () => {
        if (metadataText === "") {
            setValid(`❌ ${t("empty_um")}`);
        } else if (metadataText !== "" && checkMetadata == false) {
            setValid("");
        }
    };

    const changeMetadataText = (e) => {
        e.preventDefault();
        setMetadataText(e.target.value);
        validation();
    };

    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.doctype.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.doctype.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.doctype.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.doctype.List"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);

    useEffect(() => {
        if (checkMetadata == true) {
            setValid(`❌ ${t("meta_avail_md")}`);
        }
    }, [checkMetadata]);

    useEffect(() => {
        if (checkMetadata == false && checkMetadata !== "") {
            setDisButton(false);
        }
    }, [metadataText]);

    useEffect(() => {
        if (action === "edit") {
            setMetadataText(getData.metadata_name);
        } else if (action === "add") {
            setMetadataText("");
        }
    }, [onSelected]);

    const handleSubmit = async (e) => {
        const body_data = JSON.stringify({
            "@type": "Metadata_Master",
            metadata_name: metadataText,
        });

        try {
            // let config = helper.GetToken();
            // const response = await fetch(`${action === "edit" ? urlEdit : urlInput}`, {
            //     method: `${action === "edit" ? "PATCH" : "POST"}`,
            //     headers: {
            //         "Content-Type": "application/json",
            //         Authorization: config.headers.Authorization,
            //     },
            //     body: body_data,
            // });
            // if (response.status == 201 || 200) {
            //     setMetadataText("");
            //     notify();
            //     showTable();
            // }
            let urlInput = dataIp + "metadatamaster";
            let urlEdit = dataIp + "metadatamaster/" + id_update;
            let url = `${action === "edit" ? urlEdit : urlInput}`;
            let response = await helper.SubmitMetadata(url, body_data, action);
            if (response.status == 201 || 200 || 202 || 204) {
                setMetadataText("");
                notify();
                showTable();
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };
    return (
        <div>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />
            <form onSubmit={handleSubmit}>
                <div class="row my-2">
                    <div class="col-3">
                        <label>{t("titlle_md")}</label>
                    </div>
                    <div class="col-9">
                        <input type="text" id="metadataText" value={metadataText} onChange={changeMetadataText} class={valid !== "" ? "text-field-with-error" : "form-control"} />
                        <p class="pt-3"> {valid}</p>
                    </div>
                </div>

                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    {havePermissionAccess.haveAccessAdd && (
                        <button disabled={disButton} class={action === "add" ? "btn btn-primary col-3" : "btn btn-success col-3"} id="btnSave" data-bs-dismiss="modal" type="submit">
                            {action === "add" ? t("add_um") : t("edit_um")}
                        </button>
                    )}

                    <button class="btn btn-danger col-3 mx-3 " data-bs-dismiss="modal" aria-label="Close" onClick={(e) => e.preventDefault()}>
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputMetaData;
