import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import LoadSpinner from "../../../components/Load/load";
import "../../../assets/css/addcss.css";
import { useTranslation } from "react-i18next";
import Helper from "../../../api/helper";

const statePermission = {
    haveAccessAdd: false,
    haveAccessModify: false,
    haveAccessView: false,
    haveAccessList: false,
};

function InputSatker(props) {
    const {
        showTable,
        getData,
        action,
        onSelected,
        getAllTable,
        closeButton,
        setCloseButton,
        handleClose,
        disButton,
        setDisButton,
        // validSatkerCode, setValidSatkerCode,validSatkerName, setValidSatkerName, validSatkerPoldaName, setValidSatkerPoldaName
    } = props;
    const { t } = useTranslation();
    const [satkerName, setSatkerName] = useState("");
    const [satkerCode, setSatkerCode] = useState("");
    const [satkerPoldaId, setSatkerPoldaId] = useState("");
    const [satkerPoldaName, setSatkerPoldaName] = useState("");
    const [korwilRef, setKorwilRef] = useState(null);
    const [korwilOptionsHtml, setKorwilOptionsHtml] = useState([]);
    const [provinsiRef, setProvinsiRef] = useState(null);
    const [loadSpin, setLoadSpinner] = useState(false);
    const [newItemMetadata, setNewItemMetadata] = useState("");
    const [newContentItemMetadata, setNewContentItemMetadata] = useState("");
    const [poldaMetadataList, setPoldaMetadataList] = useState([]);

    const [validSatkerCode, setValidSatkerCode] = useState("");
    const [validSatkerName, setValidSatkerName] = useState("");
    const [validPoldaName, setValidPoldaName] = useState("");
    const [havePermissionAccess, setHavePermissionAccess] = useState(statePermission);
    const initialRows = [{ polda_metadata_id: 1, polda_metadata_name: "", polda_metadata_value: "" }];
    const [rows, setRows] = useState(initialRows);

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");

    let base_url = process.env.REACT_APP_API_URL;
    let id_update = getData ? getData.path : "";

    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
           
            loadKorwil();
        }
    }, [dataIp]);

    const checkSatkerCode = getAllTable.some((xy) => xy.satker_code?.toLowerCase() === satkerCode.toLowerCase());
    const checkSatkerName = getAllTable.some((xy) => xy.satker_name?.toLowerCase() === satkerName.toLowerCase());
    const checkPoldaId = satkerPoldaId && satkerPoldaId != undefined ? true : false;
    const poldaMap = getAllTable.map((xy) => xy.satker_polda_name?.toLowerCase());

    const changeSatkerCode = (e) => {
        e.preventDefault();
        setSatkerCode(e.target.value);
    };

    const changeSatkerName = (e) => {
        e.preventDefault();
        setSatkerName(e.target.value);
    };

    const changeSatkerPolda = (e) => {
        
        e.preventDefault();
        setSatkerPoldaId(e.target.value);
        let poldas = korwilRef.filter((polda) => polda.polda_id == e.target.value);
        let polda = poldas ? poldas[0] : "";
        
        setSatkerPoldaName(polda ? polda.polda_name : "");
    };

    const changeSatkerPoldaName = (e) => {
        e.preventDefault();
        setSatkerPoldaName(e.target.value);
    };

    const cekDouble = satkerCode !== getData.satker_code && satkerName === getData.satker_name;

    const cekButtonAdd = satkerCode !== "" && checkSatkerCode == false && satkerName !== "" && checkSatkerName == false && satkerPoldaId !== "" && checkPoldaId;

    const cekButtonEdit = satkerCode !== "" && satkerName !== "";

    const validationEdit = () => {
        if (satkerCode === "") {
            setValidSatkerCode(`❌ ${t("empty_um")}`);
            setDisButton(true);
           
        } else if (satkerCode !== "") {
            setValidSatkerCode("");
            
        }

        if (satkerName === "") {
            setValidSatkerName(`❌ ${t("empty_um")}`);
            setDisButton(true);
           
        } else if (satkerName !== "") {
            setValidSatkerName("");
            
        }

        
        if (satkerPoldaName && satkerPoldaName !== undefined && satkerPoldaName !== "") {
            setValidPoldaName("");
            
        } else {
            setValidPoldaName(`❌ ${t("empty_um")}`);
            setDisButton(true);
            
        }

        if (cekDouble == true) {
            setValidSatkerName(`❌ ${t("satker_avail_lps")}`);
            setValidSatkerCode(`❌ ${t("satker_avail_ls")}`);
            setDisButton(true);
            
        }
        if (cekButtonEdit == true && cekDouble == false) {
            setDisButton(false);
            
        }
    };

    const validationAdd = () => {
        if (satkerCode === "") {
            setValidSatkerCode(`❌ ${t("empty_um")}`);
            setDisButton(true);
            
        } else if (checkSatkerCode == true) {
            setValidSatkerCode(`❌ ${t("satker_code_avail_ls")}`);
            setDisButton(true);
            
        } else if (satkerCode !== "" && checkSatkerCode == false) {
            setValidSatkerCode("");
            
        }

        if (satkerName === "") {
            setValidSatkerName(`❌ ${t("empty_um")}`);
            setDisButton(true);
            
        } else if (checkSatkerName == true) {
            setValidSatkerName(`❌ ${t("satker_name_avail_ls")}`);
            setDisButton(true);
            
        } else if (satkerName !== "" && checkSatkerName == false) {
            setValidSatkerName("");
            
        }

        if (satkerPoldaName && satkerPoldaName !== undefined && satkerPoldaName !== "") {
            setValidPoldaName("");
            
        } else {
            setValidPoldaName(`❌ ${t("empty_um")}`);
            setDisButton(true);
            
        }

        if (cekButtonAdd == true) {
            setDisButton(false);
            
        }
    };

    useEffect(
        () => {
            if (action === "add") {
                validationAdd();
            } else if (action === "edit") {
                validationEdit();
            }
        },
        [action][changeSatkerCode],
        [changeSatkerName],
        [changeSatkerPolda],
        [cekButtonAdd]
    );

    const changeContentPoldaMetadata = (e) => setNewContentItemMetadata(e.target.value);
    useEffect(() => {
        let haveAccessAdd = JSON.parse(localStorage.getItem("dms.satker.add"));
        let haveAccessModify = JSON.parse(localStorage.getItem("dms.satker.modify"));
        let haveAccessView = JSON.parse(localStorage.getItem("dms.satker.view"));
        let haveAccessList = JSON.parse(localStorage.getItem("dms.satker.List"));
        setHavePermissionAccess({
            haveAccessAdd: haveAccessAdd,
            haveAccessModify: haveAccessModify,
            haveAccessView: haveAccessView,
            haveAccessList: haveAccessList,
        });
    }, []);

    useEffect(() => {
        setTimeout(() => setLoadSpinner(false), 3300);
    }, [loadSpin]);

    const loadKorwil = async () => {
        try {
            let url = dataIp + "/polridocument/@list_polda";

            let datas = await Helper.GetListPolda(url);
            let options = [];
            setKorwilRef(datas);

            datas.map(function (data, index) {
                options.push(<option value={data.polda_id}>{data.polda_name}</option>);
            });

            setKorwilOptionsHtml(options);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };
    const optionKorwil = korwilRef?.map(function (item) {
        return item.polda_name;
    });

    useEffect(() => {
        if (action === "edit") {
            
            setSatkerName(getData.satker_name);
            setSatkerCode(getData.satker_code);
            setSatkerPoldaName(getData.satker_polda_name);
            setSatkerPoldaId(getData.satker_polda_id);
        } else if (action === "add") {
            setSatkerName("");
            setSatkerCode("");
            setSatkerPoldaName("");
            setSatkerPoldaId("");
            setSatkerPoldaName("");
        }
    }, [onSelected]);
    
    const handleFormSubmit = async (e) => {
        e.preventDefault(); // Prevents the form from submitting the traditional way

        let body_data = JSON.stringify({
            "@type": "Satker_Master",
            satker_name: satkerName,
            satker_code: satkerCode,
        });
        
        try {
            let urlInput = base_url + "polridocument";
            let urlEdit = base_url + id_update;
            let method = action === "edit" ? "edit" : "add";
            let url = action === "edit" ? urlEdit : urlInput + "/" + satkerPoldaId + "/satker";
            let datas = await Helper.SubmitSatker(url, body_data, method);
            
            if (datas.status == 201 || 200 || 203 || 204) {
                setSatkerCode("");
                setSatkerName("");
                setSatkerPoldaId("");
                setSatkerPoldaName("");
                notify();
                // showTable();

                // const lastPageData = getAllTable[getAllTable.length - 1];
                const data = datas; //response.json();
                // Assuming you are updating the table with this data
                // setAllTable(data); // Assuming setAllTable updates the state holding all table data
                await showTable();
                // Optional: Scroll to the last page or last data
                if (data.length > 0) {
                    const lastData = data[data.length - 1];
                   
                    // Additional logic to handle displaying the last data in the UI
                }
            }
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };
    const onHandlerRemoveMetadata = async (data) => {
        

        setPoldaMetadataList(
            poldaMetadataList?.filter(function (obj) {
                return obj.metadata_name !== data.metadata_name;
            })
        );
        setNewItemMetadata("");
        setNewContentItemMetadata("");
    };
    function checkArrayIndex0(arr) {
        if (arr && arr.length > 0 && arr[0] !== undefined) {
            return true;
        } else {
            return false;
        }
    }

    // Function to handle changes in select dropdown
    const handleSelectChange = (e, index) => {
        const { value } = e.target;
        setRows((prevRows) => {
            const updatedRows = [...prevRows];
            updatedRows[index] = {
                ...updatedRows[index],
                polda_metadata_name: value,
                polda_metadata_value: "",
            };
            return updatedRows;
        });
    };

    const handleChangeMetadataVal = (event, index) => {
        const { value } = event.target;
        setRows((prevRows) => {
            const updatedRows = [...prevRows];
            updatedRows[index] = {
                ...updatedRows[index],
                polda_metadata_value: value,
            };
            return updatedRows;
        });
    };

    // Function to add a new row
    const handleAddRow = () => {
        const newRow = {
            id: rows.length > 0 ? rows[rows.length - 1].id + 1 : 1,
            polda_metadata_name: "",
            polda_metadata_value: "",
        };
        setRows([...rows, newRow]);
    };

    // Function to remove a row by index
    const handleRemoveRow = (index) => {
        const updatedRows = [...rows];
        updatedRows.splice(index, 1);
        setRows(updatedRows);
    };

    useEffect(() => {
        if (checkArrayIndex0(poldaMetadataList) == false) {
            setRows(initialRows);
        } else {
            setRows(poldaMetadataList);
        }
    }, [poldaMetadataList]);

   
    return (
        <div>
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: "",
                    duration: 5000,
                    style: {
                        background: "green",
                        color: "white ",
                    },

                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />

            <form onSubmit={handleFormSubmit}>
                <h5 class="card-title mb-3">{t("satuan_kerja_lsk")}</h5>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="satkerCode" class="col-sm-12 col-form-label">
                            {t("code_um")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validSatkerCode !== "" ? "text-field-with-error" : "form-control"} id="satkerCode" value={satkerCode} name="satkerCode" onChange={changeSatkerCode} />
                        <p class="pt-3">{validSatkerCode}</p>
                    </div>
                </div>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="satkerName" class="col-sm-12 col-form-label">
                            {t("satker_name_ls")}
                        </label>
                    </div>
                    <div class="col-8">
                        <input type="text" class={validSatkerName !== "" ? "text-field-with-error" : "form-control"} id="satkerName" value={satkerName} name="poldaName" onChange={changeSatkerName} />
                        <p class="pt-3">{validSatkerName}</p>
                    </div>
                </div>
                <div class="mb-3 row">
                    <div class="col-4">
                        <label for="satkerPoldaName" class="col-sm-12 col-form-label">
                            {t("polda_name_lp")}
                        </label>
                    </div>
                    <div class="col-5">
                        <select disabled={action === "edit"} onChange={(option) => changeSatkerPolda(option)} id="selectPolda" value={satkerPoldaId} class="form-select" aria-label="Default select example">
                            <option value="" selected>
                                {t("select_one_ts")} {t("titlle_lp")}
                            </option>
                            {korwilOptionsHtml}
                            {/* {getAllTable?.map((item, i) => (
                                    <option>{item.polda_name}</option>
                                ))} */}
                            {/* <option disabled>──────</option>
                                {optionKorwil?.map((option, idx) => (
                                    <option key={idx} value={option}>
                                        {option}
                                    </option>
                                ))} */}
                        </select>
                        <p class="pt-3">{validPoldaName}</p>
                    </div>
                </div>
                {/* {action === "add" ? (
                    <div class="mb-3 row">
                        <div class="col-4">
                            <label for="satkerPoldaName" class="col-sm-12 col-form-label">
                                {t("polda_name_lp")}
                            </label>
                        </div>
                        <div class="col-5">
                            <select options={poldaMap} onChange={(option) => changeSatkerPolda(option)} id="selectPolda" value={satkerPoldaId} class="form-select" aria-label="Default select example">
                                <option selected>{t("polda_name_lp")}</option>
                                {korwilOptionsHtml}
                            </select>
                            <p class="pt-3">{validSatkerPoldaName}</p>
                        </div>
                    </div>
                ) : (
                    <div class="mb-3 row">
                        <div class="col-4">
                            <label for="satkerPoldaName" class="col-sm-12 col-form-label">
                                {t("polda_name_lp")}
                            </label>
                        </div>
                        <div class="col-8">
                            <input
                                type="text"
                                class={validSatkerName !== "" ? "text-field-with-error" : "form-control"}
                                id="satkerPoldaName"
                                value={satkerPoldaName}
                                name="satkerPoldaName"
                                onChange={changeSatkerPoldaName}
                                disabled={action === "edit"}
                            />
                            <p class="pt-3">{validSatkerPoldaName}</p>
                        </div>
                    </div>
                )} */}

                <div class="mb-4 row" style={{ paddingBottom: "10px" }}></div>
                <div class="row my-2 mt-1 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    {havePermissionAccess.haveAccessAdd || havePermissionAccess.haveAccessModify ? (
                        <button disabled={disButton} class={action === "add" ? "btn btn-primary col-3" : "btn btn-success col-3"} id="save-btn" data-bs-dismiss="modal" type="submit">
                            {action === "add" ? t("add_um") : t("edit_um")}
                        </button>
                    ) : null}
                    <button class="btn btn-danger col-3 mx-3 " data-bs-dismiss="modal" aria-label="Close" onClick={handleClose}>
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputSatker;
