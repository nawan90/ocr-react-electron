import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import ictheme from "../../assets/images/theme.png";
import icroleuser from "../../assets/images/role_user.png";
import icDoc from "../../assets/images/icon/doc_single.png";
import icMetadata from "../../assets/images/metadata.png";
import icPolda from "../../assets/images/list_doc.png";
import icProv from "../../assets/images/list_doc.png";
import { useTranslation } from "react-i18next";

function Setting() {
    const navigate = useNavigate();
    const { t } = useTranslation();

    const [isAdmin, setIsAdmin] = useState(false);
    const [isRoot, setIsRoot] = useState(false);
    const [permission, setPermission] = useState({ isRoot: false, isAdmin: false, isRoleAccess: false });

    useEffect(() => {
        let isAdmin = JSON.parse(localStorage.getItem("myisadmin"));
        let username = localStorage.getItem("user");
        let role = JSON.parse(localStorage.getItem("dms.role.access"));
       
        let isRoot = username === "root";
        // if (username === "root") {
        //     setIsRoot(true);
        // }
        setPermission({ isRoot: isRoot, isAdmin: isAdmin, isRoleAccess: role });
    }, []);

    const toTheme = () => {
        navigate("theme");
    };
    const toRoleUser = () => {
        navigate("role-permission");
    };
    const toDocType = () => {
        navigate("doctype");
    };
    const toProvinsi = () => {
        navigate("provinsi");
    };
    const toListPolda = () => {
        navigate("list-polda");
    };
    const toListSatker = () => {
        navigate("list-satker");
    };
    const toConfigureMetaData = () => {
        navigate("configure-meta-data");
    };
    const toConnectionServer = () => {
        navigate("connection-server");
    };
 
    return (
        <div id="layoutAuthentication">
            <div
                class="container-xl"
                style={{
                     height: "90vh",
                    padding: "300px 10px",
                }}
            >
                <div class="row d-flex justify-content-center align-items-center">
                    {/* <div class="col-3" onClick={toTheme}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h6 class="float-start text-white">{t("theme_nav")}</h6>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={ictheme} width={"70%"} />
                                </div>
                            </div>
                        </div>
                    </div> */}
                    {permission.isAdmin && (
                        <div class="col-2" onClick={toRoleUser}>
                            <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                                <div class="card-header ">
                                    <h6 class="float-start text-white">{t("titlle_rp")}</h6>
                                </div>
                                <div class="card-body">
                                    <div class="float-end">
                                        <img src={icroleuser} width={"150%"} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                    <div class="col-2" onClick={toDocType}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h6 class="float-start text-white">{t("titlle_dt")}</h6>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={icDoc} width={"50%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2" onClick={toProvinsi}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h6 class="float-start text-white">
                                    {t("list_um")} {t("titlle_pr")}
                                </h6>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={icProv} width={"60%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* </div>
                <div class="row"> */}
                    <div class="col-2" onClick={toListPolda}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h6 class="float-start text-white">
                                    {t("list_um")} {t("titlle_lp")}
                                </h6>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={icPolda} width={"60%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2" onClick={toListSatker}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h6 class="float-start text-white">
                                    {t("list_um")} {t("satuan_kerja_lsk")}
                                </h6>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={icPolda} width={"60%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2" onClick={toConfigureMetaData}>
                        <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                            <div class="card-header ">
                                <h6 class="float-start text-white">{t("titlle_md")}</h6>
                            </div>
                            <div class="card-body">
                                <div class="float-end">
                                    <img src={icMetadata} width={"140%"} />
                                </div>
                            </div>
                        </div>
                    </div>
                    {permission.isRoot && (
                        <div class="col-2" onClick={toConnectionServer}>
                            <div class="card bgcard2 card-animate mb-3" style={{ maxwidth: "18rem" }}>
                                <div class="card-header ">
                                    <h6 class="float-start text-white">{t("con_nav")}</h6>
                                </div>
                                <div class="card-body">
                                    <div class="float-end">
                                        <img src={icroleuser} width={"150%"} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}

                    {/* <div class="col flip-card " onClick={toTheme}>
          <div className="flip-card-inner ">
            <div className="flip-card-front p-4 " style={{ backgroundColor: "#acb50e" }}>
              <div
                style={{
                  width: "100%",
                  textAlign: "left",
                  top: "5px",
                  left: "10px",
                  position: "absolute",
                  color: "white",
                  fontWeight: "bold",
                }}
              >
                Theme
              </div>
              <img src={icList} width={"20%"} style={{ marginTop: "15px" }} />
            </div>
            <div className="flip-card-back bg-back">
              
              <img src={Efaskon} width={"35%"} style={{ marginTop: "5px" }} />
              <div style={{ marginTop: "10px", fontSize: "14px" }}>E-Faskon</div>
            </div>
          </div>
        </div> */}
                </div>
            </div>
        </div>
    );
}
export default Setting;
