import { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { flexRender, getCoreRowModel, getFilteredRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import { FormGroup, Input } from "reactstrap";
import DataListRole from "../../../fakeData/listRole.json";
import Del from "../../../assets/images/icon/delete.png";
import Edit from "../../../assets/images/icon/edit.png";
import ModalHapus from "../../../components/Modal/modalHapus";
import toast, { Toaster } from "react-hot-toast";
import Filters from "../../../components/Table/filters";
import Paginations from "../../../components/Table/pagination";
import ModalInput from "../../../components/Modal/modalInput";
import helper from "../../../api/helper";
import { useTranslation } from "react-i18next";
import Swal from "sweetalert2";

function RoleUser() {
    const navigate = useNavigate();
    var mystate = {
        polda: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        satker: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        metadata: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        doctype: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        template: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        province: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        document: {
            add: false,
            modify: false,
            view: false,
            list: false,
            version: false,
            approve: false,
        },
        theme: {
            access: false,
        },
        user: {
            add: false,
            modify: false,
            view: false,
            list: false,
        },
        search: {
            access: false,
        },
        log: {
            access: false,
        },
        role: {
            access: false,
        },
    };
     let base_url = process.env.REACT_APP_API_URL;
    const ip = localStorage.getItem("ipAddress");

    var permissionMap = {
        "dms.korwil.add": "",
        "dms.korwil.modify": "",
        "dms.korwil.view": "",
        "dms.korwil.list": "",
    };
    const { t } = useTranslation();

    const [data, setData] = useState(DataListRole);
    const [getSingleShare, setGetSingleShare] = useState([]);
    const [sorting, setSorting] = useState([]);
    const [filtering, setFiltering] = useState("");
    const [columnFilters, setColumnFilters] = useState([]);
    const [editedRows, setEditedRows] = useState({});
    const [validRows, setValidRows] = useState({});
    const [status, setStatus] = useState(mystate);
    const [state, setState] = useState(true);
    const [rowSelected, setRowSelected] = useState(null);
    const [viewDisabled, setViewDisabled] = useState(false);
    const [unselectedRole, setUnselectedRole] = useState(false);
    const [isSelected, setIsSelected] = useState(false);
    const [isRoleAdmin, setIsRoleAdmin] = useState(false);
    const [permission, setPermission] = useState({ isRoleAdmin: false });
    const [dataIp, setDataIp] = useState("");

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            showTable();
        }
    }, [dataIp]);

    useEffect(() => {
        if (!rowSelected) {
            setIsSelected(false);
        } else {
            setIsSelected(true);
            
            if (rowSelected.rolename == "Admin") {
                // setIsRoleAdmin(true);
                setPermission({ isRoleAdmin: true });
            } else {
                setPermission({ isRoleAdmin: false });
            }
        }
    }, [rowSelected]);

    const columns = [
        {
            accessorKey: "rolename",
            header: t("nama_izin_rp"),
            enableColumnFilter: true,
            filterFn: "includesString",
        },
        {
            id: "actions",
            accessorKey: "id",
            header: t("actions_um"),
            cell: function render({ getValue, state, row, column, instance }) {
                return (
                    <div>
                        {row.original["rolename"] == "Admin" || row.original["rolename"] == "SuperAdmin" || row.original["rolename"] == "POLDA" || row.original["rolename"] == "SATKER" ? (
                            <div
                                style={{
                                    display: "flow",
                                    flexDirection: "row",
                                    width: "100px",
                                    gap: "4px",
                                }}
                            >
                                <button
                                    type="button"
                                    title="Edit"
                                    className="btn"
                                    onClick={
                                        () => {
                                           
                                            setRowSelected(row.original);
                                            setIsSelected(true);
                                            onRowEdit(row.original);
                                        }
                                        
                                    }
                                >
                                    <img src={Edit} width={"25%"} />
                                </button>
                               
                            </div>
                        ) : (
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    width: "100px",
                                    gap: "4px",
                                }}
                            >
                                <button
                                    type="button"
                                    title="Edit"
                                    className="btn"
                                    onClick={
                                        () => {
                                            
                                            setRowSelected(row.original);
                                            setIsSelected(true);
                                            onRowEdit(row.original);
                                        }
                                        
                                    }
                                >
                                    <img src={Edit} width={"80%"} />
                                </button>
                                <button
                                    type="button"
                                    className="btn"
                                    title="Hapus"
                                    data-bs-toggle="modal"
                                    data-bs-target="#modalHapus"
                                    onClick={() => {
                                        
                                        setRowSelected(row.original);
                                    }}
                                >
                                    <img src={Del} width={"80%"} />
                                </button>
                            </div>
                        )}
                    </div>
                );
            },
        },
    ];
    const table = useReactTable({
        data,
        columns,
        state: {
            sorting: sorting,
            globalFilter: filtering,
            columnFilters: columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        onSortingChange: setSorting,
        onGlobalFilterChange: setFiltering,
        onColumnFiltersChange: setColumnFilters,
        meta: {
            editedRows,
            setEditedRows,
            validRows,
            setValidRows,
            revertData: (rowIndex) => {
                // setData((old) =>
                //   old.map((row, index) =>
                //     index === rowIndex ? originalData[rowIndex] : row
                //   )
                // );
            },
            updateRow: (rowIndex) => {
                // updateRow(data[rowIndex].id, data[rowIndex]);
            },
            updateData: (rowIndex, columnId, value, isValid) => {
                setData((old) =>
                    old.map((row, index) => {
                        if (index === rowIndex) {
                            return {
                                ...old[rowIndex],
                                [columnId]: value,
                            };
                        }
                        return row;
                    })
                );
                setValidRows((old) => ({
                    ...old,
                    [rowIndex]: { ...old[rowIndex], [columnId]: isValid },
                }));
            },
            addRow: () => {
                const id = Math.floor(Math.random() * 10000);
                const newRow = {
                    id,
                    studentNumber: id,
                    name: "",
                    dateOfBirth: "",
                    major: "",
                };
                // addRow(newRow);
            },
            removeRow: (rowIndex) => {
                // deleteRow(data[rowIndex].id);
            },
            removeSelectedRows: (selectedRows) => {
                // selectedRows.forEach((rowIndex) => {
                //   deleteRow(data[rowIndex].id);
                // });
            },
        },
    });

    const onRowEdit = async (data) => {
        

        if (data) {
            try {
                let url = dataIp + "/dmsroleperm/" + data.id;
                let response = await helper.GetDetailRole(url);
                
                if (response.status == 201 || response.status == 204 || response.status == 200 || response.status == 202) {
                    let data = response.data;
                    let listRole = data.dms_role_perm;
                   
                    listRole.forEach((role) => {
                        const [root, module, action] = role.split(".");
                        let _status = mystate;
                        if (_status[module] && _status[module][action] !== undefined) {
                            _status[module][action] = true; // Set value menjadi true
                        }
                        
                        setStatus(_status);
                    });
                }
            } catch (err) {
                
                if (err && err.hasOwnProperty("status")) {
                    if (err.status == 401) {
                        toast(`${t("session_exp_um")}`, { icon: "👏" });
                        navigate("/home");
                    }
                }
            }
        }
    };

    const onConfirmHapus = async (data) => {
        

        if (data) {
            try {
                let url = dataIp + "/dmsroleperm/" + data.id;
                let response = await helper.DeleteRole(url);
                if (response.status == 201 || response.status == 204 || response.status == 200 || response.status == 202) {
                    toast(`${t("delete_sucses_um")}`, { icon: "👏" });
                    setRowSelected(null);
                    refreshTable();
                }
            } catch (err) {
                
                if (err && err.hasOwnProperty("status")) {
                    if (err.status == 401) {
                        toast(`${t("session_exp_um")}`, { icon: "👏" });
                        navigate("/home");
                    }
                }
            }

            // toast("Delete Data Berhasil", {
            //     icon: "👏",
            // });
        }
    };

    const refreshTable = () => {
        showTable();
    };

    const showTable = async () => {
        
        try {
            let url = dataIp + "/dmsroleperm/@search?type_name=DMSRole&_metadata=id,rolename&_size=1000";
            let datas = await helper.GetListRole(url);
            
            if (datas.status == 201 || 200 || 203 || 204) {
                let filterdata = datas.data.items.map((item) => ({
                    ...item, // Keep other properties unchanged
                    rolename: item.rolename.split(".")[1], // Modify the 'nama' property
                }));
                setData(filterdata);
            }
        } catch (err) {
            
        }
    };

    const getRolePermissions = () => {
        let roleperms = [];
        for (const key in status) {
            if (status.hasOwnProperty(key)) {
                
                for (const permission in status[key]) {
                    if (status[key].hasOwnProperty(permission)) {
                        
                        if (status[key][permission]) {
                            let property = `dms.${key}.${permission}`;
                            let data = {
                                op: "append",
                                value: property,
                            };
                            roleperms.push(data);
                        }
                    }
                }
            }
        }

        return roleperms;
    };

    const sweetAlertError = (body) => {
        
        Swal.fire({
            title: "Error!",
            text: "" + body,
            icon: "error",
            confirmButtonText: "OK",
        });
    };

    
    const onCancelRoleUpdate = async () => {
        setStatus(mystate);
        setRowSelected(null);
    };

    const onUpdateRole = async () => {
        let idRole = rowSelected.id;
        let roleperms1 = [
            {
                op: "clear",
            },
        ];

        let roleperms2 = getRolePermissions();
        let roleperms = [...roleperms1, ...roleperms2];
        // roleperms.push({
        //     op: "clear",
        // });
        let post_data = {
            "@type": "DMSRole",
            dms_role_perm: {
                op: "multi",
                value: roleperms,
            },
        };
        // if (roleperms && roleperms.length > 0) {
        //     post_data["dms_role_perm"] = {
        //         op: "multi",
        //         value: roleperms,
        //     };
        // }

        

        try {
            let url = dataIp + "/dmsroleperm/" + idRole;
            let datas = await helper.ModifyRole(url, post_data);
            
            if (datas.status == 201 || 200 || 203 || 204) {
                toast(`${t("modify_sucses_um")}`, { icon: "👏" });
            }
        } catch (err) {
            if (err.status == 409) {
                sweetAlertError("Nama role ini sudah terdaftar!");
            }
           
        }
    };

    return (
        <div class="container-fluid pt-3 " style={{  height: "auto", marginTop: "75px" }}>
            <ModalHapus getData={rowSelected} handlerDelete={onConfirmHapus} />
            <ModalInput showTable={refreshTable} action={"add"} />
            <Toaster
                position="bottom-left"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    // Define default options
                    className: "",
                    duration: 5000,
                    style: {
                        background: "#ffbaba",
                        color: "#ff0000 ",
                    },

                    // Default options for specific types
                    success: {
                        duration: 3000,
                        theme: {
                            primary: "green",
                            secondary: "black",
                        },
                    },
                }}
            />

            <div class="card ">
                <div class="card-body">
                    <div class="col">
                        <h5 class="card-title mb-3 textfamily">{t("titlle_rp")}</h5>
                        <div class="row">
                            <div class="col-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title mb-3">
                                            {t("list_um")} {t("role_tu")}
                                        </h5>
                                        <div class="row">
                                            <div class="col-8">
                                                <Filters filtering={filtering} setFiltering={setFiltering} />
                                            </div>
                                            <div class="col-4">
                                                <button class="btn btn-primary float-end" type="button" data-bs-toggle="modal" data-bs-target="#ModalInput" title="Tambah">
                                                    {" "}
                                                    ✙ {t("role_tu")}
                                                </button>
                                            </div>
                                        </div>
                                        {/* <div class="row"> */}
                                        <table className="table table-bordered table-striped table-hover">
                                            <thead className="table-primary">
                                                {table.getHeaderGroups().map((headerGroup) => (
                                                    <tr key={headerGroup.id} className="  uppercase">
                                                        {headerGroup.headers.map((header) => (
                                                            <th key={header.id} className="px-4 pr-2 py-3 font-medium text-left">
                                                                {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                                                            </th>
                                                        ))}
                                                    </tr>
                                                ))}
                                            </thead>

                                            <tbody>
                                                {table.getRowModel().rows.map((row) => (
                                                    <tr className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50" key={row.id}>
                                                        {row.getVisibleCells().map((cell) => (
                                                            <td className="px-4 py-2" key={cell.id}>
                                                                {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                                            </td>
                                                        ))}
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                        <Paginations table={table} />
                                        {/* </div> */}
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title mb-3">
                                            {t("list_um")} {t("sub_titlle_rp")} : {rowSelected ? `"${rowSelected.rolename}"` : ""}
                                        </h5>
                                        <div class="row">
                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("titlle_lp")}</label>
                                                        <div class="form-check form-switch">
                                                            <input
                                                                disabled={!rowSelected}
                                                                class="form-check-input"
                                                                type="checkbox"
                                                                id="switchCreatePolda"
                                                                checked={status.polda.add}
                                                                onChange={() => {
                                                                    let _status = status;
                                                                    _status.polda.add = !status.polda.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label class="form-check-label" disabled={true} style={{ display: "contents" }} for="switchCreatePolda">
                                                                {t("create_um")} {t("titlle_lp")}
                                                            </label>
                                                        </div>
                                                        <div class="form-check form-switch">
                                                            <input
                                                                disabled={!rowSelected}
                                                                class="form-check-input"
                                                                type="checkbox"
                                                                id="switchEditPolda"
                                                                checked={status.polda.modify}
                                                                onChange={() => {
                                                                    let _status = status;
                                                                    _status.polda.modify = !status.polda.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label class="form-check-label" style={{ display: "contents" }} for="switchEditPolda">
                                                                {t("edit_um")} {t("titlle_lp")}
                                                            </label>
                                                        </div>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.polda.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.polda.view = !status.polda.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("titlle_lp")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.polda.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.polda.list = !status.polda.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("titlle_lp")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("titlle_ls")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.satker.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.satker.add = !status.satker.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("titlle_ls")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.satker.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.satker.modify = !status.satker.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("titlle_ls")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.satker.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.satker.view = !status.satker.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("titlle_ls")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.satker.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.satker.list = !status.satker.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("titlle_ls")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("titlle_pr")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.province.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.province.add = !status.province.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("titlle_pr")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.province.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.province.modify = !status.province.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("titlle_pr")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.province.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.province.view = !status.province.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("titlle_pr")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.province.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.province.list = !status.province.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("titlle_pr")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("titlle_md")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.metadata.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.metadata.add = !status.metadata.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("titlle_md")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.metadata.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.metadata.modify = !status.metadata.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("titlle_md")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.metadata.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.metadata.view = !status.metadata.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("titlle_md")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.metadata.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.metadata.list = !status.metadata.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("titlle_md")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("titlle_dt")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.doctype.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.doctype.add = !status.doctype.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("titlle_dt")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.doctype.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.doctype.modify = !status.doctype.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("titlle_dt")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.doctype.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.doctype.view = !status.doctype.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("titlle_dt")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.doctype.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.doctype.list = !status.doctype.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("titlle_dt")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("templat_t1")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.template.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.template.add = !status.template.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("templat_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.template.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.template.modify = !status.template.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("templat_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.template.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.template.view = !status.template.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("templat_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.template.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.template.list = !status.template.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("templat_t1")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("dokumen_t1")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.document.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.document.add = !status.document.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("dokumen_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.document.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.document.modify = !status.document.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("dokumen_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.document.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.document.view = !status.document.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("dokumen_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.document.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.document.list = !status.document.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("dokumen_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.document.version}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.document.version = !status.document.version;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("acc_ver_doc_rp")}
                                                            </label>
                                                        </FormGroup>
                                                        {/* <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.document.approve}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.document.approve = !status.document.approve;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("apr_um")} {t("dokumen_t1")}
                                                            </label>
                                                        </FormGroup> */}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("pengguna_t1")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.user.add}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.user.add = !status.user.add;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("create_um")} {t("pengguna_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.user.modify}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.user.modify = !status.user.modify;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("pengguna_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.user.view}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.user.view = !status.user.view;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("lihat_um")} {t("pengguna_t1")}
                                                            </label>
                                                        </FormGroup>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.user.list}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.user.list = !status.user.list;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("list_um")} {t("pengguna_t1")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("pencarian_t1")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.search.access}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.search.access = !status.search.access;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("acc_src_rp")}
                                                            </label>
                                                        </FormGroup>
                                                        <label>{t("log_activity_t1")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.log.access}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.log.access = !status.log.access;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("acc_log_rp")}
                                                            </label>
                                                        </FormGroup>
                                                        <label>{t("role_tu")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.role.access}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.role.access = !status.role.access;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("role_access_tu")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div>

                                            {/* <div class="col-4 mb-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label>{t("tema_title_set")}</label>
                                                        <FormGroup switch>
                                                            <Input
                                                                disabled={!rowSelected}
                                                                type="switch"
                                                                checked={status.theme.access}
                                                                onClick={() => {
                                                                    let _status = status;
                                                                    _status.theme.access = !status.theme.access;
                                                                    setStatus(_status);
                                                                    setState(!state);
                                                                }}
                                                            />
                                                            <label check style={{ display: "contents" }}>
                                                                {t("edit_um")} {t("tema_title_set")}
                                                            </label>
                                                        </FormGroup>
                                                    </div>
                                                </div>
                                            </div> */}
                                        </div>

                                        <div class="row " style={{ margin: "0 auto", textAlign: "center" }}>
                                            <button disabled={!rowSelected } class="btn btn-primary float-end my-1" type="button" onClick={onUpdateRole}>
                                                {" "}
                                                🖫 {t("save_um")}
                                            </button>
                                            <button disabled={!rowSelected} class="btn btn-danger float-end" type="button" onClick={onCancelRoleUpdate}>
                                                {" "}
                                                ☓ {t("cancel_um")}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default RoleUser;
