import { useState, useEffect, useRef } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useTranslation } from "react-i18next";
import { FormGroup, Input } from "reactstrap";
import helper from "../../../api/helper";
import Swal from "sweetalert2";

function InputRolePermission(props) {
    var mystate = {
        polda: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        satker: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        metadata: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        doctype: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        template: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        province: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        document: {
            add: false,
            modify: false,
            view: false,
            list: true,
            version: false,
            approve: false,
        },
        theme: {
            access: true,
        },
        user: {
            add: false,
            modify: false,
            view: false,
            list: true,
        },
        search: {
            access: true,
        },
        log: {
            access: false,
        },
        role: {
            access: false,
        },
    };

    const { showTable, getData, action, onSelected, getAllTable } = props;

    const { t } = useTranslation();
    const [roleName, setRoleName] = useState("");
    const [validRoleName, setValidRoleName] = useState("");
    const [status, setStatus] = useState(mystate);
    const [state, setState] = useState(true);
    let base_url = process.env.REACT_APP_API_URL;

    const [dataIp, setDataIp] = useState("");
    const ip = localStorage.getItem("ipAddress");
    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer);
            }
        };

        urlValid();
    }, [ip]);

    const notify = () =>
        toast(`${t("input_sucses_um")}`, {
            icon: "👏",
        });

    const changeRoleName = (e) => {
        e.preventDefault();
        const value = e.target.value.replace(/\s/g, ""); // Menghapus semua spasi
        setRoleName(value);
    };

    const getRolePermissions = () => {
        let roleperms = [];
        for (const key in status) {
            if (status.hasOwnProperty(key)) {
                
                for (const permission in status[key]) {
                    if (status[key].hasOwnProperty(permission)) {
                        
                        if (status[key][permission]) {
                            let property = `dms.${key}.${permission}`;
                            let data = {
                                op: "append",
                                value: property,
                            };
                            roleperms.push(data);
                        }
                    }
                }
            }
        }

        return roleperms;
    };

    const sweetAlertError = (body) => {
        
        Swal.fire({
            title: "Error!",
            text: "" + body,
            icon: "error",
            confirmButtonText: "OK",
        });
    };

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        if (roleName == "") {
            sweetAlertError("Nama role masih kosong!");
            return;
        }
        let roleperms = getRolePermissions();
        let post_data = {
            "@type": "DMSRole",
            flag_implement: 3,
            dms_rolename: "dms." + roleName,
        };
        if (roleperms && roleperms.length > 0) {
            post_data["dms_role_perm"] = {
                op: "multi",
                value: roleperms,
            };
        }

        

        try {
            let url = dataIp + "/dmsroleperm";
            let datas = await helper.AddNewRole(url, post_data);
            
            if (datas.status == 201 || 200 || 203 || 204) {
                setStatus(mystate);
                setRoleName("");
                notify();
                showTable();
                document.getElementById("closeModal").click();
            }
        } catch (err) {
            if (err.status == 409) {
                sweetAlertError("Nama role ini sudah terdaftar!");
            }
            
        }

       
    };

    return (
        <div>
            <form onSubmit={handleFormSubmit}>
                <div class="row mt-2">
                    <div class="col-3">
                        <label>{t("nama_izin_rp")}</label>
                    </div>
                    <div class="col-9">
                        <input type="text" class={validRoleName !== "" ? "text-field-with-error" : "form-control"} id="roleName" value={roleName} name="roleName" onChange={changeRoleName} />
                        <p class="pt-3">{validRoleName}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("titlle_lp")}</label>
                                                <div class="form-check form-switch">
                                                    <input
                                                        class="form-check-input"
                                                        type="checkbox"
                                                        id="switchCreatePolda"
                                                        checked={status.polda.add}
                                                        onChange={() => {
                                                            let _status = status;
                                                            _status.polda.add = !status.polda.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label class="form-check-label" disabled={true} style={{ display: "contents" }} for="switchCreatePolda">
                                                        {t("create_um")} {t("titlle_lp")}
                                                    </label>
                                                </div>
                                                <div class="form-check form-switch">
                                                    <input
                                                        class="form-check-input"
                                                        type="checkbox"
                                                        id="switchEditPolda"
                                                        checked={status.polda.modify}
                                                        onChange={() => {
                                                            let _status = status;
                                                            _status.polda.modify = !status.polda.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label class="form-check-label" style={{ display: "contents" }} for="switchEditPolda">
                                                        {t("edit_um")} {t("titlle_lp")}
                                                    </label>
                                                </div>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.polda.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.polda.view = !status.polda.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("titlle_lp")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.polda.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.polda.list = !status.polda.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("titlle_lp")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("titlle_ls")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.satker.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.satker.add = !status.satker.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("titlle_ls")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.satker.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.satker.modify = !status.satker.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("titlle_ls")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.satker.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.satker.view = !status.satker.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("titlle_ls")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.satker.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.satker.list = !status.satker.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("titlle_ls")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("titlle_pr")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.province.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.province.add = !status.province.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("titlle_pr")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.province.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.province.modify = !status.province.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("titlle_pr")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.province.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.province.view = !status.province.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("titlle_pr")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.province.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.province.list = !status.province.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("titlle_pr")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("titlle_md")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.metadata.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.metadata.add = !status.metadata.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("titlle_md")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.metadata.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.metadata.modify = !status.metadata.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("titlle_md")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.metadata.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.metadata.view = !status.metadata.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("titlle_md")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.metadata.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.metadata.list = !status.metadata.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("titlle_md")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("titlle_dt")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.doctype.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.doctype.add = !status.doctype.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("titlle_dt")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.doctype.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.doctype.modify = !status.doctype.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("titlle_dt")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.doctype.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.doctype.view = !status.doctype.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("titlle_dt")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.doctype.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.doctype.list = !status.doctype.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("titlle_dt")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("templat_t1")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.template.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.template.add = !status.template.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("templat_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.template.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.template.modify = !status.template.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("templat_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.template.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.template.view = !status.template.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("templat_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.template.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.template.list = !status.template.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("templat_t1")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("dokumen_t1")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.document.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.document.add = !status.document.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("dokumen_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.document.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.document.modify = !status.document.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("dokumen_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.document.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.document.view = !status.document.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("dokumen_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.document.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.document.list = !status.document.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("dokumen_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.document.version}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.document.version = !status.document.version;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("acc_ver_doc_rp")}
                                                    </label>
                                                </FormGroup>
                                                {/* <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.document.approve}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.document.approve = !status.document.approve;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("apr_um")} {t("dokumen_t1")}
                                                    </label>
                                                </FormGroup> */}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("pengguna_t1")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.user.add}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.user.add = !status.user.add;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("create_um")} {t("pengguna_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.user.modify}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.user.modify = !status.user.modify;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("pengguna_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.user.view}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.user.view = !status.user.view;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("lihat_um")} {t("pengguna_t1")}
                                                    </label>
                                                </FormGroup>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.user.list}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.user.list = !status.user.list;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("list_um")} {t("pengguna_t1")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("pencarian_t1")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.search.access}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.search.access = !status.search.access;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("acc_src_rp")}
                                                    </label>
                                                </FormGroup>
                                                <label>{t("log_activity_t1")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.log.access}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.log.access = !status.log.access;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("acc_log_rp")}
                                                    </label>
                                                </FormGroup>
                                                <label>{t("role_tu")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.role.access}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.role.access = !status.role.access;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("role_access_tu")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div>

                                    {/* <div class="col-4 mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <label>{t("tema_title_set")}</label>
                                                <FormGroup switch>
                                                    <Input
                                                        type="switch"
                                                        checked={status.theme.access}
                                                        onClick={() => {
                                                            let _status = status;
                                                            _status.theme.access = !status.theme.access;
                                                            setStatus(_status);
                                                            setState(!state);
                                                        }}
                                                    />
                                                    <label check style={{ display: "contents" }}>
                                                        {t("edit_um")} {t("tema_title_set")}
                                                    </label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2 mt-5 d-flex flex-row-reverse" style={{ margin: "0 auto", textAlign: "center" }}>
                    <button class="btn btn-primary col-3 " type="submit">
                        {t("save_um")}
                    </button>

                    <button type="button" class="btn btn-danger col-3 mx-3" data-bs-dismiss="modal">
                        {t("close_um")}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default InputRolePermission;
