import React, { useState, useEffect } from "react";
import "../../../assets/css/addcss.css";
import InputConnectionServer from "./input";
import { useTranslation } from "react-i18next";

function ConnectionServer() {
   let base_url = process.env.REACT_APP_API_URL;
  let id_ip = "IP20240605040259";
  const [data, setData] = useState([]);
  const dataUser = localStorage.getItem("user");
  const { t } = useTranslation();


  useEffect(() => {
    showIpSetting();
  }, []);

  const showIpSetting = async () => {
    try {
      let url = base_url + "ipsetting/" + id_ip;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
       const requestOptions = {
        method: "GET",
        headers: myHeaders,
      };
      var res = await fetch(url, requestOptions);
      var datas = await res.json();
      setData(datas);
    } catch (err) {
      console.log(`${t("error_um")}`);
    }
  };
  return (
    <div
      class="container-xl py-3 "
      style={{  height: "82vh", marginTop: "65px" }}
    >
      <InputConnectionServer
        showIpSetting={showIpSetting}
        getData={data}
        id={id_ip}
      />
    </div>
  );
}

export default ConnectionServer;
