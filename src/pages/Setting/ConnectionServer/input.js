import React, { useState, useEffect } from "react";
import { useNavigate, useOutletContext } from "react-router-dom";
import "../../../assets/css/addcss.css";
import { useTranslation } from "react-i18next";
import helper from "../../../api/helper";


function InputConnectionServer(props) {
  const { getData, id, showIpSetting } = props;
   const navigate = useNavigate();
  const { t } = useTranslation();

  let base_url = process.env.REACT_APP_API_URL;
  // let ipid = getData ? getData.id : "";
  let urlInput = base_url + "ipsetting";
  let urlEdit = base_url + "ipsetting/" + id;
  //  + ipid;
  const [data, setData] = useState([]);
  const [edit, setEdit] = useState(false);

  const [responStatus, setResponStatus] = useState(false);

  const [ipChoice, setIpChoice] = useState("");
  // const [ipCon, setIpCon] = useOutletContext(ipChoice);
  const status = localStorage.getItem("onlinStatus");

  const [disabled, setDisabled] = useState(true);
  // const [protocol, setProtocol] = useState( "http://");
  const [protocol, setProtocol] = useState(() => {
    // Check if getData and ip_address are defined before using them
    if (getData && getData.ip_address) {
      return getData.ip_address.split('://')[0] + '://'; // Ensure we return with the protocol
    }
    return "http://"; // Default protocol if undefined
  });
  const [disButton, setDisButton] = useState(false);
  const [disInput, setDisInput] = useState(true);
  const [ipAddress, setIpAddress] = useState( "");
  const [ipName, setIpName] = useState(getData ? getData.ip_name : "");
  const [ipStatus, setIpStatus] = useState(getData ? getData.ip_status : "");
  // const [ipAdd, setIpAdd] = useOutletContext(ipAddress);
  const [butActive, setButActive] = useState(false);
  const [errors, setErrors] = useState({});
  const [selected, setSelected] = useOutletContext();

  const onConChange = (e) => {
    setIpChoice(e.target.value);
  };

 

  useEffect(() => {
    const initialIpAddress = getData ? getData.ip_address : "";
    setIpAddress(initialIpAddress?.replace(/^https?:\/\//, ''));
  }, [getData]);


  // const extractedProtocol = inputValue.match(/^(https?:\/\/)/);
  // setProtocol(extractedProtocol ? extractedProtocol[0] : "http://");

  useEffect(() => {
    const initialIpAddress = getData ? getData.ip_address : "";
    setIpAddress(initialIpAddress?.replace(/^https?:\/\//, ''));
  }, [getData]);

  useEffect(() => {
    const initialIpAddress = getData ? getData.ip_address : "";
    setProtocol(initialIpAddress?.split('://')[0] + '://');
  }, [getData]);
 

  const handleIpAddressChange = (event) => {
    // event.preventDefault();
    const inputValue = event.target.value;
    // setIpAddress(inputValue);
    setIpAddress(inputValue.replace(/^https?:\/\//, '')); // Trim the protocol

    setErrors({ ...errors, ipAddress: "" });
    setDisButton(false);
    // const isValidIP = /^(\d{1,3}.){3}\d{1,3}$/.test(inputValue);

    // if (!isValidIP) {
    //   setErrors({ ...errors, ipAddress: t("ip_invalid_con") });
    //   setDisButton(true);
    // }
  };


 
  const handleIpNameChange = (event) => {
     const inputValue = event.target.value;
    setIpName(inputValue);
  };

  const handleIpStatusChange = (e) => {
     setIpStatus(e.target.value);
  };

  const onSaveIP = (e) => {
    setSelected(true);
     setDisInput(true);
    setDisabled(true);
    setButActive(false);
  };

  const onEditIP = (e) => {
    setSelected(false);
    setDisabled(false);
    setButActive(true);
    setEdit(true);
    if (status === "Online") {
      setDisInput(false);
    } else {
      setDisInput(true);
    }
    setResponStatus(true);
  };

  useEffect(() => {
    if (status === "Offline") {
      setDisButton(true);
      setDisInput(true);
    }
  }, [status]);

  useEffect(() => {
     window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
  }, []);

  useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
  }, [getData]);

  function otherAction(e) {
    document.getElementById("output").innerHTML = "submit";
    e.preventDefault();
    setSelected(false);
    setDisabled(false);
    setButActive(true);
    setEdit(true);
    if (status === "Online") {
      setDisInput(false);
    } else {
      setDisInput(true);
    }
    setResponStatus(true);
  }

 
  useEffect(() => {
    window.localStorage.setItem("ipAddress", getData ? getData.ip_address : "");
 }, []);
 
  const handleSubmit = async (event) => {
   
    const data_server = JSON.stringify({
      "@type": "IP_Setting",
      ip_address: protocol + ipAddress,
      ip_name: ipName,
      ip_status: ipStatus,
    });

   
  
    try {
      const requests = [];
      
      if (getData) {
        // If getData exists, we are updating an existing setting
        // requests.push(fetch(urlEdit, {
        //   method: "PATCH",
        //   headers: {
        //     "Content-Type": "application/json",
        //     Authorization: "Basic cm9vdDpyb290",
        //   },
        //   body: data_server,
        // }));
        let response = await helper.PatchConnection(data_server, id);
         // setData(datas);
        if (response.status == 201) {
             showIpSetting();
            localStorage.removeItem("user");
            navigate("/");
            localStorage.removeItem("ipAddress");
        }
      }  
   
  
    } catch (err) {
      console.error(`${t("error_um")}: ${err}`);
    }
  };
  
  const changeProtocol = (e) => {
    setProtocol(e.target.value);
  };

 
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div class="card my-3">
          <div class="card-body">
            <h5 class="card-title mb-3 textfamily">{t("title_con")}</h5>
            <div
              class="form-check"
              style={{ width: "600px", textAlign: "left" }}
            >
              <br />
              <div class="row">
                <div class="col-4">
                  <label htmlFor="Modif"> {t("dom_add_con")}</label>
                </div>
                <div class="col-3">
                  <select
                    options={protocol}
                    onChange={(option) => changeProtocol(option)}
                    id="selectProvinsi"
                    value={protocol}
                    disabled={disInput}

                    class="form-select"
                    aria-label="Default select example"
                  >
                    <option selected>http://</option>
                    <option>https://</option>
                  </select>
                </div>
                <div class="col-5">
                  <input
                    type="text"
                    name="ipAddress"
                    disabled={disInput}
                    onChange={handleIpAddressChange}
                    placeholder="xxx.xxx.xxx.xxx"
                    value={ipAddress}
                    class={
                      errors.ipAddress === t("ip_invalid_con")
                        ? "text-field-with-error"
                        : " "
                    }
                  />
                </div>
              </div>

               <div class="row">
                <div class="col-4">
                  <label htmlFor="Modif">{t("dom_name_con")}</label>
                </div>
                <div class="col-3"></div>
                <div class="col-5">
                  <input
                    type="text"
                    name="ipName"
                    disabled={disInput}
                    onChange={handleIpNameChange}
                    value={edit != true ? getData.ip_name : ipName}
                  />
                </div>
              </div>

              <div class="row">
                <div class="col-4">
                  <label htmlFor="Modif">{t("dom_status_con")}</label>
                </div>
                <div class="col-3"></div>
                <div class="col-5">
                  <input
                    type="text"
                    name="ipStatus"
                    disabled={disInput}
                    onChange={handleIpStatusChange}
                    value={edit != true ? getData.ip_status : ipStatus}
                  />
                </div>
              </div>

              <br />

              <p style={{ color: "red" }}>{errors.ipAddress}</p>
              <p>
                {t("dom_current_con")} :{" "}
                {getData ? getData.ip_address : ipAddress}
              </p>
            </div>
            <div class="row" style={{ marginTop: "40px" }}>
              <div class="col-3">
                {butActive == true ? (
                  <button
                    type="submit"
                    class="btn btn-primary"
                    disabled={disButton}
                  >
                    {t("save_um")}
                  </button>
                ) : (
                  <button
                    type="button"
                    id="output"
                    onClick={otherAction}
                    name="edit"
                    class="mx-3 btn btn-success "
                  >
                    {t("edit_um")}
                  </button>
                )}
              </div>
              <div class="col-9">
                {status === "Offline" ? (
                  <span class="text-danger">{t("ip_offline_con")}</span>
                ) : (
                  <span></span>
                )}
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default InputConnectionServer;
