import React, { useState, useEffect } from "react";
import { useOutletContext } from "react-router-dom";
import "../../../assets/css/radio-theme.css";
import flagId from "../../../assets/images/flag/id.svg";
import flagGb from "../../../assets/images/flag/gb.svg";
import { useTranslation } from "react-i18next";

function Theme() {
   const { i18n, t } = useTranslation();

  // Get the default language from localStorage, default to 'id'
  const [bahasa, setBahasa] = useState(localStorage.getItem("bahasa") || "id");

  
  // Function to change the language in i18n and localStorage
  const changeLanguage = (lang) => {
    i18n.changeLanguage(lang);
    localStorage.setItem("bahasa", lang);
  };

  
  // Handles language selection
  const onOptionBahasa = (e) => {
    const selectedLanguage = e.target.value;
    setBahasa(selectedLanguage);
    changeLanguage(selectedLanguage);
  };

  // Retrieve color theme from localStorage
 
  return (
    <div
      className="container-xxl pt-3"
      style={{
        height: "80vh",
        marginTop: "85px",
        marginBottom: "20px",
      }}
    >
      <div className="card">
        <div className="card-body">
          <h5 className="card-title mb-3 textfamily">{t("tema_title_set")}</h5>

         
          <div className="row my-5 bg-light">
            <h5 className="m-3">{t("pilih_bhs_ap_set")}</h5>
            <div
              className="form-check"
              style={{ width: "500px", textAlign: "left" }}
            >
              <input
                type="radio"
                name="Indonesia"
                value="id"
                id="Indonesia"
                checked={bahasa === "id"}
                onChange={onOptionBahasa}
              />
              <label htmlFor="Indonesia">
                {t("bahasa_indonesia")} <img width={20} src={flagId} alt="ID" />
              </label>
              <br />

              <input
                type="radio"
                name="Inggris"
                value="en"
                id="Inggris"
                checked={bahasa === "en"}
                onChange={onOptionBahasa}
              />
              <label htmlFor="Inggris">
                {t("bahasa_inggris")} <img width={20} src={flagGb} alt="GB" />
              </label>

              <p className="mt-3">
                {t("pilihan_bahasa_set")} :{" "}
                <strong>
                  {bahasa === "en" ? "Bahasa Inggris" : "Bahasa Indonesia"}
                </strong>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Theme;
