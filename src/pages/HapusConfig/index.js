import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

function HapusConfig() {
  const navigate = useNavigate();
  let base_url = process.env.REACT_APP_API_LOCAL;

   const { t } = useTranslation();
  const [clientConSet, setClientConSet] = useState([]);
  const [clientCon, setClientCon] = useState([]);

 
  useEffect(() => {
    getDMSClientConfigSetting();
  }, []);

  const getDMSClientConfigSetting = async () => {
    // try {
    //   let url = `http://localhost:8069/db/dmsbackend/dmsconfig`;
    //   const myHeaders = new Headers();
    //   myHeaders.append("Content-Type", "application/json");
    //   myHeaders.append("Authorization", `Basic ` + btoa("root:Adm1nDMS2024"));
    //   const requestOptions = {
    //     method: "GET",
    //     headers: myHeaders,
    //   };
    //   var res = await fetch(url, requestOptions);
    //   var datas = await res.json();
      
    //   // Check if the data is an array before setting state
    // //   if (Array.isArray(datas)) {
    //     setClientConSet(datas);
    // //   } else {
    // //     console.error("Data received is not an array:", datas);
    // //   }
    // } catch (err) {
    //   console.log(`${t("error_um")}`);
    // }
  };

  useEffect(() => {
    getDMSClientConfig();
  }, []);

  const getDMSClientConfig = async () => {
    // try {
    //   let url = `http://localhost:8069/db/dmsbackend/dmsconfig/conf`;
    //   const myHeaders = new Headers();
    //   myHeaders.append("Content-Type", "application/json");
    //   myHeaders.append("Authorization",`Basic ` + btoa("root:Adm1nDMS2024"));
    //   const requestOptions = {
    //     method: "GET",
    //     headers: myHeaders,
    //   };
    //   var res = await fetch(url, requestOptions);
    //   var datas = await res.json();
    //   setClientCon(datas);
    // } catch (err) {
    //   console.log(`${t("error_um")}`);
    // }
  };

  const deleteConfig = async () => {
    try {
      const url1 = base_url + "/dmsconfig/conf"; // First API
      const url2 = base_url + "/dmsconfig"; // Second API
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization",`Basic ` + btoa("root:Adm1nDMS2024"));

    const requestOptions = {
        method: "DELETE",
        headers: myHeaders,
    };

    const responses = await Promise.all([
        fetch(url1, requestOptions),
        fetch(url2, requestOptions),
    ]);

    const allSuccessful = responses.every(res => res.ok);
    navigate("/");

if (allSuccessful) {
        alert("SUKSES DELETE");
      } else {
        alert("Some deletions failed.");
      }    
    } catch (err) {
      console.log(`${t("error_um")}`);
    }
  }; 

  return (
    <div className="container-fluid pt-3" style={{ height: "auto", marginTop: "75px" }}>
   
   {JSON.stringify(clientCon)}
   <br/>
   <br/>
   {JSON.stringify(clientConSet)}
    
      <button onClick={deleteConfig}>Hapus Config</button>
    </div>
  );
}

export default HapusConfig;
