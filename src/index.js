import React from 'react';
import ReactDOM from 'react-dom/client';
 import App from './App';
import reportWebVitals from './reportWebVitals';
import './components/Translate/i18n.js';  // Import the i18n configuration

const root = ReactDOM.createRoot(document.getElementById('root'));
if (!localStorage.getItem('i18nextLng')) {
  localStorage.setItem('i18nextLng', 'id');
}
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function

// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
