import React, { useState, useEffect } from "react";
import { Route, Routes, HashRouter, useLocation } from "react-router-dom";
import Home from "../pages/Home";
import SignIn from "../pages/SignIn";
import Searching from "../pages/Searching";
import User from "../pages/User";
import Template from "../pages/Template";
import ListTemplate from "../pages/Template/ListTemplate";
import ListTemplateClient from "../pages/Template/ListTemplateClient";
import CreateTemplate from "../pages/Template/CreateTemplate";
import Document from "../pages/Document";
import ListDocument from "../pages/Document/ListDocument";
import CreateDocument from "../pages/Document/CreateDocument";
import Setting from "../pages/Setting";
import RoleUser from "../pages/Setting/RoleUser";
import Theme from "../pages/Setting/Theme";
import ConfigureMetaData from "../pages/Setting/ConfigureMetaData";
import Sharing from "../pages/Sharing";
import Layout from "../components/Layout";
import GantiUser from "../pages/User/GantiUser";
import ListUser from "../pages/User/ListUser";
import ShareSingleDoc from "../pages/Sharing/ShareSingleDoc";
import ShareMultiDoc from "../pages/Sharing/ShareMultiDoc";
import ListPolda from "../pages/Setting/ListPolda";
import ListSatker from "../pages/Setting/ListSatker";
import Provinsi from "../pages/Setting/Provinsi";
import DocumentType from "../pages/Setting/DocumentType";
import ConnectionServer from "../pages/Setting/ConnectionServer";
import ActivityLog from "../pages/ActivityLog";
import AddVersion from "../pages/Document/AddVersion";
import Approval from "../pages/Document/Approval";
import TerimaData from "../pages/TerimaData";
import Profile from "../pages/User/Profile";
import KirimData from "../pages/KirimData";
import Compare from "../pages/Document/Compare";
import HapusConfig from "../pages/HapusConfig";
import MapsLoc from "../pages/Maps";
import VersionTrack from "../pages/VersionTrack";
import Verifikasi from "../pages/Verifikasi";
import ViewVerification from "../pages/Verifikasi/View";
import LoadSso from "../components/LoadSSO/load";

export default function Routers() {
    const [loggedIn, setLoggedIn] = useState(false);

    return (
        <HashRouter>
            <Routes>
            <Route path="/" element={<SignIn setLoggedIn={setLoggedIn} loggedIn={loggedIn} />} />
            <Route path="/login" element={<SignIn setLoggedIn={setLoggedIn} loggedIn={loggedIn} />} />
            <Route path="/load-sso" element={<LoadSso />} />

            <Route path="/home" element={<Home setLoggedIn={setLoggedIn} />} />
                <Route path="/" element={<Layout />}>
                    {/* MENU Template */}
                    <Route path="/template">
                        <Route index={true} element={<Template />} />
                        <Route path="create-template" element={<CreateTemplate />} />
                        <Route path="list-template" element={<ListTemplate />} />
                        <Route path="list-template-client" element={<ListTemplateClient />} />
                    </Route>
                    {/* MENU Document */}
                    <Route path="/document">
                        <Route index={true} element={<Document />} />
                        <Route path="list-document" element={<ListDocument />} />
                        <Route path="create-document" element={<CreateDocument />} />
                        <Route path="view-document" element={<Approval />} />
                        <Route path="compare-document" element={<Compare />} />
                        <Route path="add-version-document" element={<AddVersion />} />
                    </Route>
                    {/* MENU Searching */}
                    <Route path="/searching">
                        <Route index={true} element={<Searching />} />
                    </Route>
                    <Route path="/verifikasi">
                    <Route path="list-verifikasi" index={true} element={<Verifikasi />} />
                    <Route path="view-verifikasi" index={true} element={<ViewVerification />} />

                    </Route>
                    {/* MENU Setting */}
                    <Route path="/setting">
                        <Route index={true} element={<Setting />} />
                        <Route path="role-permission" element={<RoleUser />} />
                        <Route path="theme" element={<Theme />} />
                        <Route path="doctype" element={<DocumentType />} />
                        <Route path="configure-meta-data" element={<ConfigureMetaData />} />
                        <Route path="provinsi" element={<Provinsi />} />
                        <Route path="list-polda" element={<ListPolda />} />
                        <Route path="list-satker" element={<ListSatker />} />
                        <Route path="connection-server" element={<ConnectionServer />} />
                    </Route>
                    {/* MENU User */}
                    <Route path="/user">
                        <Route index={true} element={<User />} />
                        <Route path="list-user" element={<ListUser />} />
                        <Route path="ganti-user" element={<GantiUser />} />
                    </Route>

                {/* MENU Penunjang */}
                    <Route path="/activity-log">
                        <Route index={true} element={<ActivityLog />} />
                    </Route>
                    <Route path="/profile">
                        <Route index={true} element={<Profile />} />
                    </Route>

                    <Route path="/version-track">
                        <Route index={true} element={<VersionTrack />} />
                    </Route>
                    
                </Route>
            </Routes>
        </HashRouter>
    );
}
