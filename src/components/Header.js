import { useState, useEffect } from "react";
import { useNavigate, useLocation, Link, NavLink } from "react-router-dom";
import Efaskon from "../assets/images/logo_new/logo_new.png";
import User from "../assets/images/iconRoleUser_button.png";
import Profil from "../assets/images/icon/profile.png";
import "../assets/css/addcss.css";
import { useTranslation } from "react-i18next";
import SelectLanguage from "../pages/Home/SelectLanguage";
import utils from "../utils";
import accessManager from "../api/accessManager";

const Header = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const { t } = useTranslation();
    const [profileName, setProfileName] = useState("");
    const [arrRole, setArrRole] = useState(null);
    const [isAdmin, setIsAdmin] = useState(false);
    const [usrname, setUsrname] = useState(null);
    const [isElectron, setIsElectron] = useState(false);

    // const [isActivityAccess, setIsActivityAccess] = useState(false);
    // const [isSearchAccess, setIsSearchAccess] = useState(false);
    const [permission, setPermission] = useState({
        isActivityAccess: false,
        isSearchAccess: false,
        isListTemplate: false,
        isListSetting: false,
        isListUser: false,
        isListPolda: false,
        isListSatker: false,
        isListMetadata: false,
        isListDocType: false,
        isListProvinsi: false,
        isRoleAccess: false,
    });
    const [isRoot, setIsRoot] = useState(false);
    const [myKey, setMyKey] = useState(1);
 
    function titleCase(str) {
        var splitStr = str.toLowerCase().split(" ");
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(" ");
    }

    function generateRandomFourDigitNumber() {
        return Math.floor(1000 + Math.random() * 9000);
    }

    useEffect(() => {
        let name = localStorage.getItem("myfullname");
        let username = localStorage.getItem("user");
        let role = localStorage.getItem("myrole");
        let isAdmin = localStorage.getItem("myisadmin");
        let haveAccessLog = JSON.parse(localStorage.getItem("dms.log.access"));
        let haveAccessSearch = JSON.parse(localStorage.getItem("dms.search.access"));
        let haveAccessRole = JSON.parse(localStorage.getItem("dms.role.access"));
        let haveListUser = JSON.parse(localStorage.getItem("dms.user.list"));
        let haveListPolda = JSON.parse(localStorage.getItem("dms.polda.list"));
        let haveListSatker = JSON.parse(localStorage.getItem("dms.satker.list"));
        let haveListMetadata = JSON.parse(localStorage.getItem("dms.metadata.list"));
        let haveListDocType = JSON.parse(localStorage.getItem("dms.doctype.list"));
        let haveListProvinsi = JSON.parse(localStorage.getItem("dms.province.list"));
        let haveListTemplate = JSON.parse(localStorage.getItem("dms.template.list"));
        let haveListDocs = JSON.parse(localStorage.getItem("dms.document.list"));
        setPermission({
            isActivityAccess: haveAccessLog,
            isSearchAccess: haveAccessSearch,
            isRoleAccess: haveAccessRole,
            isListTemplate: haveListTemplate,
            isListDocument: haveListDocs,
            isListUser: haveListUser,
            isListDocType: haveListDocType,
            isListPolda: haveListPolda,
            isListSatker: haveListSatker,
            isListMetadata: haveListMetadata,
            isListProvinsi: haveListProvinsi,
            isListSetting: haveListDocType || haveListMetadata || haveListMetadata || haveListPolda || haveListSatker || haveListProvinsi || haveAccessRole,
        });

        console.log("setpermission - ", {
            isActivityAccess: haveAccessLog,
            isSearchAccess: haveAccessSearch,
            isRoleAccess: haveAccessRole,
            isListTemplate: haveListTemplate,
            isListDocument: haveListDocs,
            isListUser: haveListUser,
            isListDocType: haveListDocType,
            isListPolda: haveListPolda,
            isListSatker: haveListSatker,
            isListMetadata: haveListMetadata,
            isListProvinsi: haveListProvinsi,
            isListSetting: haveListDocType || haveListMetadata || haveListMetadata || haveListPolda || haveListSatker || haveListProvinsi || haveAccessRole,
        });
        setUsrname(username);
        if (username === "root") {
            setIsRoot(true);
        }

        if (username && username.length > 0) {
            if (username === "root") {
                setIsAdmin(true);
            }

            if (role && role != "") {
                let roleArr = JSON.parse(role);
                if (roleArr && roleArr.length > 0) {
                    setArrRole(roleArr);
                    const subUser = roleArr.filter((str) => {
                        return str.includes("SATKER") || str.includes("POLDA");
                    });
                    if (subUser && subUser.length > 0) {
                        setIsAdmin(false);
                    }

                    // const subAdmin = roleArr.filter((str) => {
                    //     return str.includes("superadmin");
                    // });
                    // if (subAdmin && subAdmin.length > 0) {
                    //     setIsAdmin(true);
                    // }
                }
            }
        }

        setProfileName(name ? name : "Admin");
    }, []);

    const toSearching = () => {
        if (permission.isSearchAccess) {
            navigate("/searching");
        } else {
            utils.SwalError(t("has_no_permission_um"));
        }
    };

    const toActivityLog = () => {
        if (permission.isActivityAccess) {
            navigate("/activity-log");
        } else {
            utils.SwalError(t("has_no_permission_um"));
        }
    };
    const titleCheck = location.pathname.slice(1).replace("-", " ");

    let patt = /^(\w|\.|-)+$/;

    const titleColor = titleCheck.replace("/", " ");

    const colorHeader = () => {
        if (titleColor.includes("template")) {
            return "#3498db";
        } else if (titleColor.includes("document")) {
            return "#3498db";
        } else if (titleColor.includes("searching")) {
            return "#3498db";
        } else if (titleColor.includes("setting")) {
            return "#3498db";
        } else if (titleColor.includes("user")) {
            return "#3498db";
        } else if (titleColor.includes("sharing")) {
            return "#3498db";
        }
    };

    const [isNavCollapsed, setIsNavCollapsed] = useState(true);

    const handleNavCollapse = () => setIsNavCollapsed(!isNavCollapsed);

    function handleChange(value) {
        navigate(`${value}`);
    }
    const toEfaskon = () => {
        if (window.electronAPI?.isElectron) {
            window.electronAPI.openBrowserWindow("https://efaskon.slog.polri.go.id/");
        } else {
            window.open("https://efaskon.slog.polri.go.id/", "_blank");
        }
    };

    const toVerifikasi = () => {
        navigate("/verifikasi/list-verifikasi");
    };

    const printPage = () => {
        if (window.electronAPI && window.electronAPI.isElectron) {
            window.electronAPI.print(); // This triggers the print dialog in Electron
        } else {
            window.print(); // Default browser print dialog
        }
    };

    const toTerimaData = () => {
        navigate("/terima-data");
    };
    useEffect(() => {
        if (window.electronAPI?.isElectron) {
          setIsElectron(true);
        }
      }, []); 
console.log("isElectron",isElectron )
    return (
        <nav className="navbar navbar-expand-md bgheadmenu  fixed-top container-fluid">
            {/* <div class="row  " style={{background:"aqua"}}> */}
            <div class="col-2">
                <a class="navbar-brand text-white font-weight-bolder">
                    <Link to="/home">
                        <img src={Efaskon} alt="Logo" width="40" height="50" className="vertical-align-middle ms-4" />
                        {/* <span className="text-white">Maxtor</span> */}
                    </Link>
                </a>
            </div>
            <div class="col-10 ">
                <div class="row">
                    <div class="row custom-toggler navbar-toggler">
                        <div class="col-6"></div>
                        <div
                            class="col-6 "
                            // style={{ backgroundColor: "green" }}
                        >
                            <button
                                class="custom-toggler navbar-toggler float-end "
                                type="button"
                                data-toggle="collapse"
                                data-target="#navbarsExample09"
                                aria-controls="navbarsExample09"
                                aria-expanded={!isNavCollapsed ? true : false}
                                aria-label="Toggle navigation"
                                onClick={handleNavCollapse}
                            >
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class={`${isNavCollapsed ? "collapse" : ""} navbar-collapse`} id="navbarsExample09">
                    <div class="col-10">
                        <ul class="navbar-nav  ">
                            <li class="nav-item active">
                                <a class="nav-link">
                                    {" "}
                                    <Link to="/home" className="text-decoration-none fw-semibold text-light">
                                        {t("home_nav")}
                                    </Link>
                                </a>
                            </li>
                            {/* {permission.isListTemplate && (
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-decoration-none fw-semibold text-light" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        {t("templat_t1")}
                                    </a>
                                    <ul class="dropdown-menu">
                                        {isAdmin ? (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/template/list-template" style={{ textDecoration: "none" }}>
                                                        {t("list_um")} {t("templat_t1")}
                                                    </Link>
                                                </a>
                                            </li>
                                        ) : (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/template/list-template-client" style={{ textDecoration: "none" }}>
                                                        {t("list_um")} {t("templat_t1")}
                                                    </Link>
                                                </a>
                                            </li>
                                        )}

                                        {isAdmin ? (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/template/create-template" style={{ textDecoration: "none" }}>
                                                        {t("buat_temp_tt")}
                                                    </Link>
                                                </a>
                                            </li>
                                        ) : null}
                                    </ul>
                                </li>
                            )} */}
                            {permission.isListDocument && (
                                <li class="nav-item active">
                                    <a class="nav-link">
                                        {" "}
                                        <Link to="/document/create-document" className="text-decoration-none fw-semibold text-light">
                                            {t("dokumen_t1")}
                                        </Link>
                                    </a>
                                </li>
                                // <li class="nav-item dropdown">
                                //     <a class="nav-link dropdown-toggle text-decoration-none fw-semibold text-light" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                //         {t("dokumen_t1")}
                                //     </a>
                                //     <ul class="dropdown-menu">
                                //         <li class="nav-item active">
                                //             <a class="nav-link">
                                //                 {" "}
                                //                 <Link to="/document/list-document" style={{ textDecoration: "none" }}>
                                //                     {t("list_um")} {t("dokumen_t1")}
                                //                 </Link>
                                //             </a>
                                //         </li>
                                //         <li class="nav-item active">
                                //             <a class="nav-link">
                                //                 {" "}
                                //                 <Link to="/document/create-document" style={{ textDecoration: "none" }}>
                                //                     {t("create_um")} {t("dokumen_t1")}
                                //                 </Link>
                                //             </a>
                                //         </li>
                                //     </ul>
                                // </li>
                            )}

                            {permission.isSearchAccess && (
                                <li class="nav-item active">
                                    <a class="nav-link" onClick={toSearching}>
                                        {" "}
                                        <Link to="#" className="text-decoration-none fw-semibold text-light">
                                            {t("pencarian_t1")}
                                        </Link>
                                    </a>
                                </li>
                            )}

                            {permission.isListSetting && (
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-decoration-none fw-semibold text-light" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        {t("pengaturan_t1")}
                                    </a>
                                    <ul class="dropdown-menu">
                                        {/* <li class="nav-item active">
                                        <a class="nav-link">
                                            <Link to="/setting/theme" style={{ textDecoration: "none" }}>
                                                {t("theme_nav")}
                                            </Link>
                                        </a>
                                    </li> */}
                                        {isAdmin ? (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/role-permission" style={{ textDecoration: "none" }}>
                                                        {t("titlle_rp")}
                                                    </Link>
                                                </a>
                                            </li>
                                        ) : null}
                                        {permission.isListMetadata && (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/doctype" style={{ textDecoration: "none" }}>
                                                        {t("titlle_dt")}
                                                    </Link>
                                                </a>
                                            </li>
                                        )}
                                        {permission.isListProvinsi && (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/provinsi" style={{ textDecoration: "none" }}>
                                                        {t("titlle_pr")}
                                                    </Link>
                                                </a>
                                            </li>
                                        )}

                                        {permission.isListPolda && (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/list-polda" style={{ textDecoration: "none" }}>
                                                        {t("titlle_lp")}
                                                    </Link>
                                                </a>
                                            </li>
                                        )}
                                        {permission.isListSatker && (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/list-satker" style={{ textDecoration: "none" }}>
                                                        {t("satuan_kerja_lsk")}
                                                    </Link>
                                                </a>
                                            </li>
                                        )}
                                        {permission.isListMetadata && (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/configure-meta-data" style={{ textDecoration: "none" }}>
                                                        {t("titlle_md")}
                                                    </Link>
                                                </a>
                                            </li>
                                        )}
                                        {isRoot && isRoot == true ? (
                                            <li class="nav-item active">
                                                <a class="nav-link">
                                                    {" "}
                                                    <Link to="/setting/connection-server" style={{ textDecoration: "none" }}>
                                                        {t("con_nav")}
                                                    </Link>
                                                </a>
                                            </li>
                                        ) : null}
                                    </ul>
                                </li>
                            )}

                            {/* <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle text-decoration-none fw-semibold text-light"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Sharing
                </a>
                <ul class="dropdown-menu">
                  <li class="nav-item active">
                    <a class="nav-link">
                      {" "}
                      <Link
                        to="/sharing/single-doc"
                        style={{ color: "#000", textDecoration: "none" }}
                      >
                        Single Document
                      </Link>
                    </a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link">
                      <Link
                        to="/sharing/multi-doc"
                        style={{ color: "#000", textDecoration: "none" }}
                      >
                        Multi Document
                      </Link>
                    </a>
                  </li>
                </ul>
              </li> */}
                            {permission.isListUser && (
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-decoration-none fw-semibold text-light" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        {t("pengguna_t1")}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item active">
                                            <a class="nav-link">
                                                {" "}
                                                <Link to="/user/list-user" style={{ textDecoration: "none" }}>
                                                    {t("list_um")} {t("pengguna_t1")}
                                                </Link>
                                            </a>
                                        </li>
                                        {/* <li class="nav-item active">
                                        <a class="nav-link">
                                            {" "}
                                            <Link to="/user/ganti-user" style={{ textDecoration: "none" }}>
                                                {t("user_change_nav")}
                                            </Link>
                                        </a>
                                    </li> */}
                                    </ul>
                                </li>
                            )}

                            {permission.isActivityAccess && (
                                <li class="nav-item active">
                                    <a class="nav-link" onClick={toActivityLog}>
                                        {" "}
                                        <Link to="#" className="text-decoration-none fw-semibold text-light">
                                            {t("titlle_al")}
                                        </Link>
                                    </a>
                                </li>
                            )}
                                 <li class="nav-item active">
                                    <a class="nav-link" onClick={toEfaskon}>
                                        {" "}
                                        <Link to="#" className="text-decoration-none fw-semibold text-light">
                                            Efaskon
                                        </Link>
                                    </a>
                                </li>
                        

                            {/* <li class="nav-item active">
                                <a class="nav-link" onClick={toTerimaData}>
                                    {" "}
                                    <Link to="#" className="text-decoration-none fw-semibold text-light">
                                        Terima Data
                                    </Link>
                                </a>
                            </li> */}
                        </ul>
                    </div>
                    <div class="col-1 float-end"></div>

                    <div class="col-1 ms-auto">
                        <div className="float-start">
                            <SelectLanguage />
                        </div>
                        <ul class="navbar-nav ">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-decoration-none fw-semibold text-light" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img src={User} width={"55%"} />
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li class="dropdown-item py-2 text-center">
                                        <img className="pe-2 mb-2" src={Profil} width={"20%"} />
                                        <br></br>
                                        <a>{profileName} </a>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider" />
                                    </li>
                                    <li class="dropdown-item">
                                        <Link to="/Profile" style={{ textDecoration: "none", color: "#000" }}>
                                            {t("profil_t1")}
                                        </Link>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider" />
                                    </li>
                                    {permission.isListSetting && (
                                        <li class="dropdown-item">
                                            <Link to="/Setting" style={{ textDecoration: "none", color: "#000" }}>
                                                {t("pengaturan_t1")}
                                            </Link>
                                        </li>
                                    )}
                             {usrname === "root" ? (

                                        <li class="dropdown-item">
                                            <Link to="/version-track" style={{ textDecoration: "none", color: "#000" }}>
                                                Version Track
                                            </Link>
                                        </li>
                                  ) : null}
                                    {/* {permission.isActivityAccess && (
                                        <li>
                                            <a class="dropdown-item" onClick={toActivityLog}>
                                                {t("titlle_al")}
                                            </a>
                                        </li>
                                    )}
                                    {permission.isActivityAccess && (
                                        <li>
                                            <hr class="dropdown-divider" />
                                        </li>
                                    )} */}

{/* const toLogout = () => {
        if (window.electronAPI?.isElectron) {
return(<li></li>)
        } else {
return(
<li class="nav-item active">
                                    <a class="nav-link">
                                        <Link to="/" style={{ textDecoration: "none" }}>
                                            {t("keluar_t1")}
                                        </Link>
                                    </a>
                                </li>
)
        }
    }; */}
                                    {window.electronAPI?.isElectron ? 
                                    <li></li> :
                                    <span>
                                    {permission.isListSetting && (
                                        <li>
                                            <hr class="dropdown-divider" />
                                        </li>
                                    )}
                                    <li class="nav-item active">
                                    <a class="nav-link">
                                        <Link to="/" style={{ textDecoration: "none" }}>
                                            {t("keluar_t1")}
                                        </Link>
                                    </a>
                                </li>
                                </span>
                                    
                                    }
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {/* </div> */}
        </nav>
    );
};

export default Header;
