import React, { useState, useEffect } from "react";
import ShowPdf from "../../pages/Searching/View";
import { useNavigate, useLocation } from "react-router-dom";

function Modal2(props) {
  const { closeModal, link, getData } = props;
  const location = useLocation();
  const path = location.pathname;

  const modalBody = () => {
    if (path === "/searching") {
      return <ShowPdf getData={getData} link={link} closeModal={closeModal} />;
    } else {
      return <div className="modal-body"></div>;
    }
  };
  return (
    <div
      class="modal show"
      tabIndex="-1"
      role="dialog"
      style={{ display: "inline-block", width: "100%" }}
    >
      <div class="modal-dialog modal-xl ">
        <div class="modal-content" style={{ height: "900px" }}>
          <div class="modal-header">
            <h5 class="modal-title">Lihat Dokumen</h5>
            <button type="button" class="btn-close " onClick={closeModal}>
              <span class="float-end" aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body">{modalBody()}</div>
        </div>
      </div>
    </div>
  );
}

export default Modal2;
