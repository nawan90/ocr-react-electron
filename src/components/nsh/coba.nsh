!include "MUI2.nsh"  ; Include the Modern UI macros

; Define installer and uninstaller side bitmaps (optional if already set in package.json)
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"  ; Installer sidebar
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"  ; Finish page sidebar

 ; Function to create PostgreSQL .pgpass file
 Section "CreateFileSection" SEC0301

  ; Set the directory path
  StrCpy $0 "C:\Users\Acer\AppData\Roaming\postgresql"

  ; Create the directory if it doesn't exist
  CreateDirectory "$0"

  ; Set the file path
  StrCpy $1 "$0\.pgpass"

  ; Open the file for writing
  FileOpen $2 $1 w

  ; Write some content to the file (optional)
  StrCpy $3 "localhost:5432:*:pgadmin:pga5L09eF45k0n2024"
  FileWrite $2 $3

  ; Close the file
  FileClose $2

  DetailPrint "File .pgpass created in C:\Users\Acer\AppData\Roaming\postgresql"

SectionEnd



; Macro definition for custom actions
!macro customInstall
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"

  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend

Section "Run MirabileOCRInstaller" 0601

    ; Set the path to the installer
    ;StrCpy $0 "C:\Users\Acer\Downloads\Installer-Aplikasi\Mirabile OCR Station\Debug\MirabileOCRInstaller.exe"
    StrCpy $0 "$PROFILE\Downloads\Installer-Aplikasi\Mirabile OCR Station\Debug\MirabileOCRInstaller.exe"

    ; Check if the installer exists
    IfFileExists $0 0 InstallerNotFound

    ; Run the installer and wait for it to finish
    ExecWait '$0'

    Goto InstallerDone

InstallerNotFound:
    MessageBox MB_ICONEXCLAMATION "Installer not found at the specified location."

InstallerDone:

SectionEnd


; Function to create the PostgreSQL database
Function CreateDatabase
  DetailPrint "Creating the PostgreSQL database 'MirabileOCR'..."

  ; Set the path for the PostgreSQL psql executable
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"

  ; Set the database name
  StrCpy $1 "MirabileOCR"

  ; Construct the SQL command to create the database
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "CREATE DATABASE $1;"'

  ; Combine the executable and the SQL command in one string
  StrCpy $3 '"$0" $2'

   ; Set the PGPASSWORD environment variable temporarily
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'

  DetailPrint "Running command: $3"

  ; Run the command using psql
  ExecWait $3 $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to create the database with exit code: $R0"
FunctionEnd


Section "CreateDatabase" SEC0802
  Call CreateDatabase
SectionEnd


; Function to create the FileUploads table
Function CreateFileUploadsTable
  DetailPrint "Creating the 'FileUploads' table..."

  ; Set the SQL command to create the table
  StrCpy $2 'CREATE TABLE "FileUploads" (
      "Id" VARCHAR(50) NOT NULL,
      "FileName" VARCHAR(255),
      "No" BIGINT,
      "Status" VARCHAR(50),
      "Progress" VARCHAR(50),
      PRIMARY KEY ("Id")
  );'

  ; Set the command to execute the SQL command
  StrCpy $3 '-U pgadmin -h localhost -d MirabileOCR -c "$2"'

  ; Combine the executable and the SQL command in one string
  StrCpy $4 '"$0" $3'

  DetailPrint "Running command: $4"

  ; Run the command to create the table using psql
  ExecWait $4 $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to create the 'FileUploads' table with exit code: $R0"
FunctionEnd

Section "CreateDatabaseAndTable" SEC0803
  Call CreateFileUploadsTable
SectionEnd