!include "MUI2.nsh"  ; Include the Modern UI macros

; Define installer and uninstaller side bitmaps
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"

; Use a unique name for your executable filename
!define MY_APP_EXECUTABLE_FILENAME "document-digitalization-and-ai-based-ocr.exe"

Function CreatePgPassFile
  DetailPrint "Creating PostgreSQL .pgpass file..."

  ; Set the .pgpass file path
  StrCpy $0 "$PROFILE\.pgpass"

  ; Create the content for .pgpass
  StrCpy $1 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"

  ; Write the content to the .pgpass file
  FileOpen $2 $0 w
  FileWrite $2 $1
  FileClose $2

  ; Set file permission to read/write for the user only
  ExecWait 'attrib +R "$0"' ; Make the file read-only
  DetailPrint ".pgpass file created at $PROFILE\.pgpass"
FunctionEnd


Function DropDatabase
  DetailPrint "Dropping the database 'guillotina_dms'..."

  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"
  StrCpy $1 "-U pgadmin -c ""DROP DATABASE IF EXISTS guillotina_dms;"""
  
  ; Combine the command
  StrCpy $2 '"$0" $1'

  ExecWait $2 $R0

  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to drop the database with exit code: $R0"
FunctionEnd



; Define custom installation process
Function customInstall
  DetailPrint "Running custom installation tasks..."
  ; Your custom installation logic
  Call CreatePgPassFile
  Call DropDatabase
FunctionEnd


Section "MainSection" SEC03
  SetOutPath "$INSTDIR"
  Call customInstall
  ; No need to call DropDatabase again here
SectionEnd
