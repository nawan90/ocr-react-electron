; Function to create the PostgreSQL database
Function CreateDatabase
  DetailPrint "Creating the PostgreSQL database 'guillotina_dms'..."

  ; Set the path for the PostgreSQL psql executable
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"

  ; Set the database name
  StrCpy $1 "guillotina_dms"

  ; Construct the SQL command to create the database
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "CREATE DATABASE $1;"'

  ; Combine the executable and the SQL command in one string
  StrCpy $3 '"$0" $2'

   ; Set the PGPASSWORD environment variable temporarily
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'

  DetailPrint "Running command: $3"

  ; Run the command using psql
  ExecWait $3 $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to create the database with exit code: $R0"
FunctionEnd


Section "CreateDatabase" SEC0601
  Call CreateDatabase
SectionEnd
