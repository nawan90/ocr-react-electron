!include "MUI2.nsh"
!include "FileFunc.nsh"
!include "LogicLib.nsh"

; Function to check if the application is running
Function IsAppRunning
  StrCpy $0 "Document Digitalization and AI Based OCR"  ; Replace with your app's window title
  FindWindow $1 "" $0
  StrCmp $1 "" 0 +2
  MessageBox MB_OK|MB_ICONWARNING "The application is running. Please close it and click Retry to continue."
  Abort
FunctionEnd

 
Section "TestMessageBox" SEC01
  MessageBox MB_OK|MB_ICONWARNING "This is a warning message."
SectionEnd

; Function to create PostgreSQL .pgpass file
Function CreatePgPassFile
  DetailPrint "Creating PostgreSQL .pgpass file..."

  ; Set the .pgpass file path in the %APPDATA%\postgresql directory (Windows)
  StrCpy $0 "$APPDATA\PostgreSQL\pgpass.conf"

  ; Create the directory if it doesn't exist
  CreateDirectory "$APPDATA\PostgreSQL"

  ; Create the content for .pgpass
  StrCpy $1 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"

  ; Write the content to the .pgpass file
  FileOpen $2 $0 w
  FileWrite $2 $1
  FileClose $2

  ; Make the file hidden and read-only (optional)
  ExecWait 'attrib +H +R "$0"'

  DetailPrint ".pgpass file created at $0 with content: $1"
FunctionEnd

; Function to drop the PostgreSQL database
Function DropDatabase
  DetailPrint "Dropping the PostgreSQL database..."

  ; Set the path for PostgreSQL executable
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\psql.exe"'

  ; Set the database name
  StrCpy $1 "guillotina_dms"

  ; Set the PGPASSWORD environment variable
  StrCpy $2 'set PGPASSWORD=pga5L09eF45k0n2024 &&'

  ; Construct the command to drop the database
  StrCpy $3 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS $1;"'

  ; Combine the executable and command
  StrCpy $4 "$2 $0 $3"

  ; Log the command being executed
  DetailPrint "Running command: $4"

  ; Execute the drop command
  ExecWait "$4" $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Database drop failed with exit code: $R0"
FunctionEnd

Section "CreatePgPassFileSection" SEC02
  Call CreatePgPassFile
SectionEnd

Section "DropDatabaseSection" SEC03
  Call DropDatabase
SectionEnd

!macro customInstall
  ; Check if the application is running before proceeding
  Call IsAppRunning

  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"
  
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend
