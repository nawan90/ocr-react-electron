
; Function to Delete Program Files Efaskon
Function DeleteProgramFilesEfaskon
  StrCpy $0 "C:\Program Files\efaskon"
  Push $0      ; Push the path onto the stack
  Call FolderExists
  Pop $0       ; Get the result back
  
  ${If} $0 == "1"
    RMDir /r "C:\Program Files\efaskon"
    DetailPrint "Folder 'Program Files Efaskon' deleted successfully."
  ${Else}
    DetailPrint "Folder 'Program Files Efaskon' does not exist. Clear path."
  ${EndIf}
FunctionEnd

; Section to Delete Program Files Efaskon
Section "Delete Program Files Efaskon" SEC0502
  Call DeleteProgramFilesEfaskon
SectionEnd
