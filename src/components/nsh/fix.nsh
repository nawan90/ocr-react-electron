!include "MUI2.nsh"  ; Include the Modern UI macros

; Define installer and uninstaller side bitmaps
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"

; Use a unique name for your executable filename
!define MY_APP_EXECUTABLE_FILENAME "document-digitalization-and-ai-based-ocr.exe"

; Function to restore the PostgreSQL database
Function RestoreDatabase
  DetailPrint "Restoring the PostgreSQL database..."
  
  ; Set the paths for PostgreSQL and the SQL dump file
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"
  StrCpy $1 "C:\Users\Acer\Documents\backend\gdms_2024-10-16.dump"
  
  ; Construct the command to restore the database
  StrCpy $2 '"$0" -U postgres -d guillotina_dms -f "$1"'
  
  ; Set the PGPASSWORD environment variable temporarily
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'

  ; Log the command being executed
  DetailPrint "Running command: $2"
  
  ; Execute the restoration command
  ExecWait '$2' $R0
  
  ; Log the exit code
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Database restoration failed with exit code: $R0"
FunctionEnd

Function customInstall
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${MY_APP_EXECUTABLE_FILENAME}"

  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${MY_APP_EXECUTABLE_FILENAME} %1"
FunctionEnd

Function StopEfaskonbe
  DetailPrint "Stopping Efaskonbe service..."
  ExecWait 'sc stop "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to stop Efaskonbe service with exit code: $R0"
FunctionEnd

Section "Stop Service Section" SEC01
  Call StopEfaskonbe
SectionEnd




 ;File /r "C:\Users\Public\efaskon\.EasyOCR\*"
  ;File /r "C:\Users\Acer\Downloads\Installer-Aplikasi\.EasyOCR\*"




Section "Copy .EasyOCR folder to User Profile" SEC02
  ; Set the output path to the user's profile directory and create the .EasyOCR folder
  SetOutPath "$PROFILE\\.EasyOCR"
  
  ; Copy all files from the .EasyOCR directory recursively
  File /r "C:\Users\Public\efaskon\.EasyOCR\*"
  
  DetailPrint ".EasyOCR folder and its contents have been copied to $PROFILE\\.EasyOCR"
SectionEnd

Section "MainSection" SEC03
  SetOutPath "$INSTDIR"
  Call customInstall

  StrCpy $0 "C:\Users\Public\Aplikasi Backend\efaskon.zip"
  StrCpy $1 "C:\" 
  StrCpy $2 "C:\Program Files\7-Zip\7z.exe"

  ; Log the command being executed
  DetailPrint "Running command: $2 x $0 -o$1"

  ; Execute the extraction
  ExecWait '"$2" x "$0" -o"$1"' $R0

  ; Log the exit code
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Extraction failed with exit code: $R0"

  ; Call the database restoration function
  Call RestoreDatabase
SectionEnd

Function StartEfaskonbe
  DetailPrint "Starting Efaskonbe service..."
  ExecWait 'sc start "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to start Efaskonbe service with exit code: $R0"
FunctionEnd

Section "Start Service Section" SEC04
  Call StartEfaskonbe
SectionEnd





 

Function CopyEasyOCRtoSystem32
    DetailPrint "Copying EasyOCR folder from Downloads to System32\config\systemprofile..."

    ; Check if source directory exists
    IfFileExists "C:\Users\Acer\Downloads\Document-Digitalization-And-Ai-Based-Ocr\.EasyOCR" 0 source_not_found

    ; Create destination directory if it doesn't exist
    CreateDirectory "C:\Windows\System32\config\systemprofile\.EasyOCR"

    ; Copy all files from the source folder to the destination folder recursively
    SetOutPath "C:\Windows\System32\config\systemprofile\.EasyOCR"
    File /r "C:\Users\Acer\Downloads\Document-Digitalization-And-Ai-Based-Ocr\.EasyOCR\*.*"

    ; Check for success
    IfErrors copy_failed copy_success

    copy_success:
        DetailPrint "EasyOCR folder successfully copied to System32\config\systemprofile."
        Goto end

    copy_failed:
        MessageBox MB_OK "Error: Failed to copy the EasyOCR folder to System32\config\systemprofile."
        Goto end

    source_not_found:
        MessageBox MB_OK "Error: Source directory does not exist."

    end:
FunctionEnd

Section "CopyEasyOCRtoSystem32" SEC06
    Call CopyEasyOCRtoSystem32
SectionEnd


 !include "MUI2.nsh"  ; Include the Modern UI macros

; Define installer and uninstaller side bitmaps (optional if already set in package.json)
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"  ; Installer sidebar
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"   ; Finish page sidebar



!macro customInstall 

  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"

  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend

Function StopEfaskonbe
  DetailPrint "Stopping Efaskonbe service..."
  ExecWait 'sc stop "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to stop Efaskonbe service with exit code: $R0"
FunctionEnd

 Section "Stop Service Section" SEC01
  Call StopEfaskonbe
SectionEnd

 Function CreatePgPassFile
  DetailPrint "Creating PostgreSQL .pgpass file..."

  ; Set the .pgpass file path in the %APPDATA%\postgresql directory (Windows)
  StrCpy $0 "$APPDATA\PostgreSQL\pgpass.conf"

  ; Create the directory if it doesn't exist
  CreateDirectory "$APPDATA\PostgreSQL"

  ; Create the content for .pgpass
  StrCpy $1 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"

  ; Write the content to the .pgpass file
  FileOpen $2 $0 w
  FileWrite $2 $1
  FileClose $2

  ; Make the file hidden and read-only (optional)
  ExecWait 'attrib +H +R "$0"'

  DetailPrint ".pgpass file created at $0 with content: $1"
FunctionEnd

Section "CreatePgPassFile" SEC02
  Call CreatePgPassFile
SectionEnd


Function DropDatabase
  DetailPrint "Dropping the PostgreSQL database guillotina_dms"

  ; Set the path for the PostgreSQL executable
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"

  ; Construct the command (now using only one string)
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS guillotina_dms;"'

  ; Combine the executable and command in one string
  StrCpy $2 '"$0" $1'

  ; Log the command for debugging
  DetailPrint "Running: $2"

  ; Run the command using psql (which will use the credentials from .pgpass)
  ExecWait $2 $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to drop the database with exit code: $R0"
FunctionEnd

Section "DropDatabase" SEC03
  Call DropDatabase
SectionEnd


Function CreateDatabase
  DetailPrint "Creating the PostgreSQL database 'guillotina_dms'..."

  ; Set the path for the PostgreSQL psql executable
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"

  ; Set the database name
  StrCpy $1 "guillotina_dms"

  ; Construct the SQL command to create the database
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "CREATE DATABASE $1;"'

  ; Combine the executable and the SQL command in one string
  StrCpy $3 '"$0" $2'

  ; Log the command for debugging purposes
  DetailPrint "Executing: $3"

  ; Run the command using psql
  ExecWait $3 $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to create the database with exit code: $R0"
FunctionEnd


Section "CreateDatabase" SEC05
  Call CreateDatabase
SectionEnd










; Function to drop the PostgreSQL database
 Function DropDatabase
  DetailPrint "Dropping the PostgreSQL database..."

  ; Set the path for PostgreSQL executable
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\psql.exe"'
  
  ; Set the database name
  StrCpy $1 "guillotina_dms"

  ; Build the command with the password directly in the ExecWait
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS $1;"'

  ; Combine the command to run
  StrCpy $3 "$0 $2"

  ; Set the PGPASSWORD environment variable temporarily
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'

  DetailPrint "Running command: $3"

  ; Execute the drop command
  ExecWait "$3" $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Database drop failed with exit code: $R0"
FunctionEnd


Section "DropDatabaseSection" SEC02
  Call DropDatabase
SectionEnd

























































; Function to create PostgreSQL .pgpass file
Function CreatePgPassFile
  DetailPrint "Creating PostgreSQL .pgpass file..."

  ; Set the .pgpass file path in the %APPDATA%\postgresql directory (Windows)
  StrCpy $0 "$APPDATA\PostgreSQL\pgpass.conf"

  ; Create the directory if it doesn't exist
  CreateDirectory "$APPDATA\PostgreSQL"

  ; Create the content for .pgpass
  StrCpy $1 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"

  ; Write the content to the .pgpass file
  FileOpen $2 $0 w
  FileWrite $2 $1
  FileClose $2

  ; Make the file hidden and read-only (optional)
  ExecWait 'attrib +H +R "$0"'

  DetailPrint ".pgpass file created at $0 with content: $1"
FunctionEnd



Section "CreatePgPassFileSection" SEC01
  Call CreatePgPassFile
SectionEnd

Section "DropDatabaseSection" SEC02
  Call DropDatabase
SectionEnd







