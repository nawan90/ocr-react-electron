; Function to check if a folder exists
Function FolderExists
  Exch $0       ; Get the path from the stack
  StrCpy $1 0   ; Default to "not exists"
  
  # Check if the folder exists
  IfFileExists $0 $0 +2
    StrCpy $1 1 ; Set to "exists"
  
  # Return the result
  Exch $1      ; Move result to the top of the stack
FunctionEnd

; Function to Delete Documents Efaskon
Function DeleteDocumentsEfaskon
  StrCpy $0 "$USERPROFILE\Documents\efaskon"
  Push $0      ; Push the path onto the stack
  Call FolderExists
  Pop $0       ; Get the result back
  
  ${If} $0 == "1"
    RMDir /r "$USERPROFILE\Documents\efaskon"
    DetailPrint "Folder 'Documents Efaskon' deleted successfully."
  ${Else}
    DetailPrint "Folder 'Documents Efaskon' does not exist. Clear path."
  ${EndIf}
FunctionEnd

; Section to Delete Documents Efaskon
Section "Delete Documents Efaskon" SEC0501
  Call DeleteDocumentsEfaskon
SectionEnd
