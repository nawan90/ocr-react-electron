Section "Copy MirabileStationCPU Folder" SEC0801
  ; Define source and target directories
  StrCpy $0 "$USERPROFILE\Downloads\Installer-Aplikasi\MirabileStationCPU"
  StrCpy $1 "C:\MirabileStationCPU"

  ; Check if the MirabileStationCPU folder already exists on C:\
  IfFileExists "$1\*" 0 folder_not_found

  ; If folder exists, delete it
  DetailPrint "MirabileStationCPU folder exists on C:. Deleting it..."
  RMDir /r "$1"

folder_not_found:
  ; Copy the folder contents from source to destination
  DetailPrint "Copying MirabileStationCPU folder from source..."
  CreateDirectory "$1"
  CopyFiles "$0\*" "$1"

  DetailPrint "MirabileStationCPU folder and its contents have been copied to C:"
SectionEnd