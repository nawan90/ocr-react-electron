!include "MUI2.nsh"
!include "FileFunc.nsh"

; Define installer and uninstaller side bitmaps (optional if already set in package.json)
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"

!macro customInstall 
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend

; Function to create PostgreSQL .pgpass file

Function CreatePgPassFile
  StrCpy $0 "$LOCALAPPDATA\postgresql\.pgpass"
  
  ; Buat direktori jika belum ada
  CreateDirectory "$LOCALAPPDATA\postgresql"
  
  FileOpen $1 $0 w
  StrCpy $2 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"
  FileWrite $1 $2
  FileClose $1
  
  DetailPrint "File .pgpass created at $0"
FunctionEnd

Section "CreatePgPassFile" SEC01
  Call CreatePgPassFile
SectionEnd

 Function DropDatabase
  DetailPrint "Terminating connections and dropping the PostgreSQL database..."
  
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\psql.exe"'
  
  ; Terminate active connections
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = ''guillotina_dms'';"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'
  
  DetailPrint "Running command to terminate connections: $2"
  ExecWait "$2" $R0
  DetailPrint "Terminate connections exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to terminate connections with exit code: $R0"

  ; Check for active connections
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "SELECT * FROM pg_stat_activity WHERE datname = ''guillotina_dms'';"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  DetailPrint "Running command to check active connections: $2"
  ExecWait "$2" $R0
  DetailPrint "Active connections check exit code: $R0"

  ; Now drop the database
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS guillotina_dms;"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  DetailPrint "Running command to drop the database: $2"
  ExecWait "$2" $R0
  DetailPrint "Drop database exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to drop the database with exit code: $R0. Check output.log for details."
FunctionEnd

Section "DropDatabase" SEC02
  Call DropDatabase
SectionEnd 

 ; Function to create the PostgreSQL database
Function CreateDatabase
  DetailPrint "Creating the PostgreSQL database 'guillotina_dms'..."
  
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"
  StrCpy $1 "guillotina_dms"
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "CREATE DATABASE $1;"'
  StrCpy $3 "$0 $2"
  
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'
  
  DetailPrint "Running command: $3"
  ExecWait "$3" $R0
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to create the database with exit code: $R0"
FunctionEnd

Section "CreateDatabase" SEC03
  Call CreateDatabase
SectionEnd 



; Function to restore the PostgreSQL database
Function RestoreDatabase
  DetailPrint "Restoring the PostgreSQL database 'guillotina_dms'..."
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\pg_restore.exe"'
  StrCpy $1 "C:/Users/Acer/Downloads/Document-Digitalization-And-Ai-Based-Ocr/gdms_2024-10-16.dump"
  StrCpy $2 '-U pgadmin -d guillotina_dms -W -v --clean --if-exists'
  StrCpy $3 "$0 $2 $1"
  
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'
  DetailPrint "Running command: $3"
  ExecWait "$3" $R0
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Database restore failed with exit code: $R0"
FunctionEnd

Section "RestoreDatabaseSection" SEC04
  Call RestoreDatabase
SectionEnd
