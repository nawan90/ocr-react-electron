Function StartEfaskonbe
  DetailPrint "Starting Efaskonbe service..."
  ExecWait 'sc start "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to start Efaskonbe service with exit code: $R0"
FunctionEnd

 Section "Start Service Section" SEC1301
  Call StartEfaskonbe
SectionEnd