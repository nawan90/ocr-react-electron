!include "MUI2.nsh"
!include "FileFunc.nsh"

!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"

Function StopEfaskonbe
  DetailPrint "Stopping Efaskonbe service..."
  ExecWait 'sc stop "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to stop Efaskonbe service with exit code: $R0"
FunctionEnd

Section "Stop Service Section" SEC01
  Call StopEfaskonbe
SectionEnd

Function CheckPgPassFile
  StrCpy $0 "$LOCALAPPDATA\postgresql\.pgpass"

  ; Check if the .pgpass file exists
  ${If} ${FileExists} "$0"
    DetailPrint "File .pgpass found at $0"
    Return
  ${EndIf}

  ; This part executes if the file doesn't exist
  DetailPrint "File .pgpass not found at $0"

  ; Create the directory if it doesn't exist
  CreateDirectory "$LOCALAPPDATA\postgresql"

  ; Create the .pgpass file
  FileOpen $1 $0 w
  StrCpy $2 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"
  FileWrite $1 $2
  FileClose $1

  DetailPrint "File .pgpass created at $0"
FunctionEnd

Section "CheckPgPassFileSection" SEC02
  Call CheckPgPassFile
SectionEnd


 Function DropDatabase
  DetailPrint "Terminating connections and dropping the PostgreSQL database..."
  
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\psql.exe"'
  
  ; Terminate active connections
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = ''guillotina_dms'';"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'
  
  DetailPrint "Running command to terminate connections: $2"
  ExecWait "$2" $R0
  DetailPrint "Terminate connections exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to terminate connections with exit code: $R0"

  ; Check for active connections
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "SELECT * FROM pg_stat_activity WHERE datname = ''guillotina_dms'';"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  DetailPrint "Running command to check active connections: $2"
  ExecWait "$2" $R0
  DetailPrint "Active connections check exit code: $R0"

  ; Now drop the database
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS guillotina_dms;"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  DetailPrint "Running command to drop the database: $2"
  ExecWait "$2" $R0
  DetailPrint "Drop database exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to drop the database with exit code: $R0. Check output.log for details."
FunctionEnd

Section "DropDatabase" SEC03
  Call DropDatabase
SectionEnd 

 ; Function to Copy EasyOCR to System32
Function CopyEasyOCR
  DetailPrint "Copying .EasyOCR folder..."
  
  ; Construct the xcopy command
  StrCpy $0 'cmd.exe /C xcopy "$PROFILE\Downloads\Installer-Aplikasi\.EasyOCR" "C:\Windows\System32\config\systemprofile\.EasyOCR" /E /I /Y'
  
  ; Use double quotes around the command to prevent issues with spaces
  StrCpy $0 '"$0"'
  
  ; Execute the command
  ExecWait $0 $R0
  DetailPrint "xcopy exit code: $R0"
  
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to copy .EasyOCR folder with exit code: $R0."
FunctionEnd




Section "Install Section" SEC04
  Call CopyEasyOCR
SectionEnd

!macro customInstall 
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend
