Function StopEfaskonbe
  DetailPrint "Stopping Efaskonbe service..."
  ExecWait 'sc stop "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to stop Efaskonbe service with exit code: $R0"
FunctionEnd

 Section "Stop Service Section" SEC0201
  Call StopEfaskonbe
SectionEnd