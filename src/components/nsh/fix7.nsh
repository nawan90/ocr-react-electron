!include "MUI2.nsh"
!include "FileFunc.nsh"

; Define installer and uninstaller side bitmaps (optional if already set in package.json)
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"

!macro customInstall 
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME}"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME} %1"
!macroend


Function StopEfaskonbe
  DetailPrint "Stopping Efaskonbe service..."
  ExecWait 'sc stop "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to stop Efaskonbe service with exit code: $R0"
FunctionEnd

Section "Stop Service Section" SEC01
  Call StopEfaskonbe
SectionEnd

; Function to create PostgreSQL .pgpass file
Function CreatePgPassFile
  StrCpy $0 "$LOCALAPPDATA\postgresql\.pgpass"
  
  ; Buat direktori jika belum ada
  CreateDirectory "$LOCALAPPDATA\postgresql"
  
  FileOpen $1 $0 w
  StrCpy $2 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"
  FileWrite $1 $2
  FileClose $1
  
  DetailPrint "File .pgpass created at $0"
FunctionEnd

Section "CreatePgPassFile" SEC02
  Call CreatePgPassFile
SectionEnd

 Function DropDatabase
  DetailPrint "Terminating connections and dropping the PostgreSQL database..."
  
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\psql.exe"'
  
  ; Terminate active connections
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = ''guillotina_dms'';"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'
  
  DetailPrint "Running command to terminate connections: $2"
  ExecWait "$2" $R0
  DetailPrint "Terminate connections exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to terminate connections with exit code: $R0"

  ; Check for active connections
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "SELECT * FROM pg_stat_activity WHERE datname = ''guillotina_dms'';"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  DetailPrint "Running command to check active connections: $2"
  ExecWait "$2" $R0
  DetailPrint "Active connections check exit code: $R0"

  ; Now drop the database
  StrCpy $1 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS guillotina_dms;"'
  StrCpy $2 "$0 $1 > output.log 2>&1"
  
  DetailPrint "Running command to drop the database: $2"
  ExecWait "$2" $R0
  DetailPrint "Drop database exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to drop the database with exit code: $R0. Check output.log for details."
FunctionEnd

Section "DropDatabase" SEC03
  Call DropDatabase
SectionEnd 

 ; Function to create the PostgreSQL database
Function CreateDatabase
  DetailPrint "Creating the PostgreSQL database 'guillotina_dms'..."
  
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"
  StrCpy $1 "guillotina_dms"
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "CREATE DATABASE $1;"'
  StrCpy $3 "$0 $2"
  
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'
  
  DetailPrint "Running command: $3"
  ExecWait "$3" $R0
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to create the database with exit code: $R0"
FunctionEnd

Section "CreateDatabase" SEC04
  Call CreateDatabase
SectionEnd 

Section "Copy .EasyOCR folder to User Profile" SEC05
  ; Set the output path to the user's profile directory and create the .EasyOCR folder
  SetOutPath "$PROFILE\\.EasyOCR"
  
  ; Copy all files from the .EasyOCR directory recursively
  File /r "C:\Users\Public\efaskon\.EasyOCR\*"
  
  DetailPrint ".EasyOCR folder and its contents have been copied to $PROFILE\\.EasyOCR"
SectionEnd

Section "EkstrakZipBE" SEC06
  SetOutPath "$INSTDIR"

  StrCpy $0 "$PROFILE\\Downloads\Installer-Aplikasi\efaskon.zip"
  StrCpy $1 "C:\"
  StrCpy $2 "C:\Program Files\7-Zip\7z.exe"

  ; Log the command being executed
  DetailPrint "Running command: $2 x $0 -o$1"

  ; Execute the extraction
  ExecWait '"$2" x "$0" -o"$1"' $R0

  ; Log the exit code
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Extraction failed with exit code: $R0"
SectionEnd
 
 Function StartEfaskonbe
  DetailPrint "Starting Efaskonbe service..."
  ExecWait 'sc start "Efaskonbe"' $R0
  StrCmp $R0 0 +2
  MessageBox MB_OK "Failed to start Efaskonbe service with exit code: $R0"
FunctionEnd

Section "Start Service Section" SEC07
  Call StartEfaskonbe
SectionEnd