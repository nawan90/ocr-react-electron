Section "Run MirabileOCRInstaller" SEC0701

    ; Set the path to the installer
     StrCpy $0 "$USERPROFILE\Downloads\Installer-Aplikasi\Mirabile OCR Station\Debug\MirabileOCRInstaller.exe"

    ; Check if the installer exists
    IfFileExists $0 0 InstallerNotFound

    ; Run the installer and wait for it to finish
    ExecWait '$0'

    Goto InstallerDone

InstallerNotFound:
    MessageBox MB_ICONEXCLAMATION "Installer not found at the specified location."

InstallerDone:

SectionEnd