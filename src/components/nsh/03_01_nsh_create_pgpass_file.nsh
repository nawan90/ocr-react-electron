; Function to create PostgreSQL .pgpass file
 Section "CreateFileSection" SEC0301

  ; Set the directory path
  StrCpy $0 "$USERPROFILE\AppData\Roaming\postgresql"

  ; Create the directory if it doesn't exist
  CreateDirectory "$0"

  ; Set the file path
  StrCpy $1 "$0\.pgpass"

  ; Open the file for writing
  FileOpen $2 $1 w

  ; Write some content to the file (optional)
  StrCpy $3 "localhost:5432:*:pgadmin:pga5L09eF45k0n2024"
  FileWrite $2 $3

  ; Close the file
  FileClose $2

  DetailPrint "File .pgpass created in $USERPROFILE\AppData\Roaming\postgresql"

SectionEnd