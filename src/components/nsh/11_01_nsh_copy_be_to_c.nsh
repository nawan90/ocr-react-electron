Section "MainSection" SEC1101
  
  StrCpy $0 "$USERPROFILE\Downloads\Installer-Aplikasi\efaskon.zip"
  StrCpy $1 "C:\" 
  StrCpy $2 "C:\Program Files\7-Zip\7z.exe"

; Log the command being executed
DetailPrint "Running command: $2 x $0 -o$1 -y"

; Execute the extraction with the -y switch to overwrite existing files
ExecWait '"$2" x "$0" -o"$1" -y' $R0

  ; Log the exit code
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Extraction failed with exit code: $R0"

  
SectionEnd