 ; Function to drop the PostgreSQL database
 Function DropDatabase
  DetailPrint "Dropping the PostgreSQL database..."

  ; Set the path for PostgreSQL executable
  StrCpy $0 '"C:\Program Files\PostgreSQL\16\bin\psql.exe"'
  
  ; Set the database name
  StrCpy $1 "guillotina_dms"

  ; Build the command with the password directly in the ExecWait
  StrCpy $2 '-U pgadmin -h localhost -d postgres -c "DROP DATABASE IF EXISTS $1;"'

  ; Combine the command to run
  StrCpy $3 "$0 $2"

  ; Set the PGPASSWORD environment variable temporarily
  System::Call 'kernel32::SetEnvironmentVariable(t "PGPASSWORD", t "pga5L09eF45k0n2024")'

  DetailPrint "Running command: $3"

  ; Execute the drop command
  ExecWait "$3" $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Database drop failed with exit code: $R0"
FunctionEnd


Section "DropDatabaseSection" SEC0401
  Call DropDatabase
SectionEnd

