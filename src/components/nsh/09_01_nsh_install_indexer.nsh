Section "Run IndexerInstaller" SEC0901

    ; Set the path to the installer
    StrCpy $0 "$PROFILE\Downloads\Installer-Aplikasi\Indexer\Debug\setup.exe"

    ; Check if the installer exists
    IfFileExists $0 0 InstallerNotFound

    ; Run the installer silently (without user interaction)
    ; Try common silent installation flags
    ExecWait '"$0" /S'
    ; ExecWait '"$0" /SILENT'
    ; ExecWait '"$0" /VERYSILENT'

    Goto InstallerDone

InstallerNotFound:
    MessageBox MB_ICONEXCLAMATION "Installer not found at the specified location."

InstallerDone:
    ; Continue with other installation steps here
SectionEnd

