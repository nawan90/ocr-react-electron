!include "MUI2.nsh"

; Define installer and uninstaller side bitmaps
!define MUI_WELCOMEPAGE_BITMAP "src/assets/images/installer/install.bmp"
!define MUI_FINISHPAGE_BITMAP "src/assets/images/installer/uninstall.bmp"
!define MY_APP_EXECUTABLE_FILENAME "document-digitalization-and-ai-based-ocr.exe"

 
 Function CreatePgPassFile
  DetailPrint "Creating PostgreSQL .pgpass file..."

  ; Set the .pgpass file path in the %APPDATA%\postgresql directory (Windows)
  StrCpy $0 "$APPDATA\PostgreSQL\pgpass.conf"

  ; Create the directory if it doesn't exist
  CreateDirectory "$APPDATA\PostgreSQL"

  ; Create the content for .pgpass
  StrCpy $1 "localhost:5432:guillotina_dms:pgadmin:pga5L09eF45k0n2024"

  ; Write the content to the .pgpass file
  FileOpen $2 $0 w
  FileWrite $2 $1
  FileClose $2

  ; Make the file hidden and read-only (optional)
  ExecWait 'attrib +H +R "$0"'

  DetailPrint ".pgpass file created at $0 with content: $1"
FunctionEnd



; Function to drop the PostgreSQL database
Function DropDatabase
  DetailPrint "Dropping the PostgreSQL database..."

  ; Set the path for PostgreSQL executable
  StrCpy $0 "C:\Program Files\PostgreSQL\16\bin\psql.exe"

  ; Set the database and credentials
  StrCpy $1 "guillotina_dms"  ; Database name
  StrCpy $2 "pgadmin"  ; Username
  StrCpy $3 "pga5L09eF45k0n2024"  ; Password

  ; Construct the command to drop the database
  StrCpy $4 '"$0" -U $2 -d postgres -c "DROP DATABASE IF EXISTS $1;"'

  ; Log the command being executed
  DetailPrint "Running command: $4"

  ; Execute the drop command
  ExecWait $4 $R0

  ; Check if the command was successful
  DetailPrint "Exit code: $R0"
  StrCmp $R0 0 +2
  MessageBox MB_OK "Database drop failed with exit code: $R0"
FunctionEnd

Function customInstall
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${MY_APP_EXECUTABLE_FILENAME}"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${MY_APP_EXECUTABLE_FILENAME} %1"
FunctionEnd

Section "MainSection" SEC03
  SetOutPath "$INSTDIR"
  Call customInstall
  Call CreatePgPassFile
  Call DropDatabase

   
SectionEnd
