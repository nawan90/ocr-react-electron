!include "MUI2.nsh"  ; Include the Modern UI macros

!define APP_EXECUTABLE_FILENAME_NEW "Document Digitalization And Ai Based Ocr Setup 0.20.6-7.24.11.1.exe"
!define LOG_FILE "install_log.txt"

!macro customInstall 
  DetailPrint "Register document-digitalization-and-ai-based-ocr URI Handler"

  ; Clean up previous registry entries
  DeleteRegKey HKCR "document-digitalization-and-ai-based-ocr"
  DeleteRegKey HKCR "evehq-ng"

  ; Register new URI handler
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "" "URL:document-digitalization-and-ai-based-ocr"
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr" "URL Protocol" ""
  WriteRegStr HKCR "evehq-ng\DefaultIcon" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME_NEW}"

  ; Create shell entries
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open" "" ""
  WriteRegStr HKCR "document-digitalization-and-ai-based-ocr\shell\Open\command" "" "$INSTDIR\${APP_EXECUTABLE_FILENAME_NEW} %1"
!macroend
