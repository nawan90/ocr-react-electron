import React, { useState, useEffect } from "react";
import excel from "../../assets/images/icon/icon_excel.png";
import helper from "../../api/helper";
import utils from "../../utils";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import { useTranslation } from "react-i18next";

const ButtonWithSelect = (props) => {
    const { polda, satker, isAdmin } = props;
    const { t } = useTranslation();

    const [poldaSelectedOption, setPoldaSelectedOption] = useState("");
    const [poldaSelectedOptionLabel, setPoldaSelectedOptionLabel] = useState("");
    const [poldaOptionsHtml, setPoldaOptionsHtml] = useState([]);
    const [doctypeSelectedOption, setDoctypeSelectedOption] = useState("");
    const [doctypeSelectedOptionLabel, setDoctypeSelectedOptionLabel] = useState("");
    const [doctypeOptionsHtml, setDoctypeOptionsHtml] = useState([]);
    const [poldaRef, setPoldaRef] = useState(null);
    const [doctypeRef, setDoctypeRef] = useState(null);
    const [dataIp, setDataIp] = useState("");
    const [satkerRef, setSatkerRef] = useState(null);
    const [satkerSelectedOption, setSatkerSelectedOption] = useState("");
    const [satkerOptionsHtml, setSatkerOptionsHtml] = useState([]);
    const ip = localStorage.getItem("ipAddress");

    let base_url = process.env.REACT_APP_API_CREATE_DOCUMENT;

    const currentYear = new Date().getFullYear();
    const startYear = 2024; // Adjust as needed
    const [selectedYear, setSelectedYear] = useState("");
    const [selectedMonth, setSelectedMonth] = useState("");
    const months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    useEffect(() => {
        const ipServer = ip ? `${ip}/db/dmsbackend/` : "";

        const urlValid = () => {
            if (ip === "116.90.165.46") {
                setDataIp(base_url + ""); // Assuming base_url is defined elsewhere
            } else {
                setDataIp(ipServer + "");
            }
        };

        urlValid();
    }, [ip]);

    useEffect(() => {
        if (dataIp && !dataIp.includes("undefined")) {
            loadPolda();
            loadDoctype();

            console.log("isAdmin", isAdmin, polda, satker);
            if (!isAdmin) {
                if (polda && polda != "") {
                    console.log("useEffect-dataIp-loadSatker", polda, 'satker', satker, 'iadmin', !isAdmin);
                    loadSatker(polda);
                    setPoldaSelectedOption(polda);
                    const result = poldaRef ? poldaRef.find((item) => item.polda_id == polda) : null;
                    console.log("result", result);
                    if (result) {                        
                        setPoldaSelectedOptionLabel(result.polda_name);
                    }

                    if (satker) {
                        console.log("setSatkerSelectedOption", satker);
                        setSatkerSelectedOption(satker);
                    }
                }
            }
            else {
                if (polda && polda != "") {
                    console.log("useEffect-dataIp-loadSatker", polda, 'satker', satker, 'iadmin', isAdmin);
                    loadSatker(polda);
                    setPoldaSelectedOption(polda);
                    const result = poldaRef ? poldaRef.find((item) => item.polda_id == polda) : null;
                    console.log("result", result);
                    if (result) {                        
                        setPoldaSelectedOptionLabel(result.polda_name);
                    }

                    if (satker) {
                        console.log("setSatkerSelectedOption", satker);
                        setSatkerSelectedOption(satker);
                    }
                }
            }
        }
    }, [dataIp]);

    const handleSelectPoldaChange = (event) => {
        setPoldaSelectedOption(event.target.value);
        const label = event.target.options[event.target.selectedIndex].text;
        setPoldaSelectedOptionLabel(label);
        loadSatker(event.target.value)
    };

    const handleSelectSatkerChange = (event) => {
        setSatkerSelectedOption(event.target.value);
        // const label = event.target.options[event.target.selectedIndex].text;
        // setSatkerSelectedOptionLabel(label);
    };

    const handleSelectDoctypeChange = (event) => {
        setDoctypeSelectedOption(event.target.value);
        const label = event.target.options[event.target.selectedIndex].text;
        setDoctypeSelectedOptionLabel(label);
    };

    const handleExportRequest = () => {
        let doc_saker_id = ""
        if (!poldaSelectedOption) {
            utils.SwalError("Pilih filter Korwil");
            return;
        }
        if (!doctypeSelectedOption) {
            utils.SwalError("Pilih filter Tipe Dokumen");
            return;
        }
        if (satkerSelectedOption && satkerSelectedOption != "null") {
            doc_saker_id = "&doc_satker_id="+satkerSelectedOption
        }
        let url = dataIp + `/@export_data_doc?doc_type_id=${doctypeSelectedOption}&doc_polda_id=${poldaSelectedOption}${doc_saker_id}`;
        let nameFile = "Data_Hasil_Ocr_" + doctypeSelectedOptionLabel + "_" + poldaSelectedOptionLabel;
        if (selectedYear && selectedYear != "") {
            const idxMonth = months.indexOf(selectedMonth);
            if (selectedMonth && selectedMonth != "") {
                let dategte = `${selectedYear}-${idxMonth + 1}-01T00:00:00`;
                let datelte = idxMonth == 1 ? `${selectedYear}-${idxMonth + 1}-28T23:59:59` : `${selectedYear}-${idxMonth + 1}-30T23:59:59`;
                url = url + "&creation_date__gte=" + dategte + "&creation_date__lte=" + datelte;
                nameFile = nameFile + "_" + selectedMonth + "_" + selectedYear;
            } else {
                let dategte = `${selectedYear}-01-01T00:00:00`;
                let datelte = `${selectedYear}-12-31T23:59:59`;
                url = url + "&creation_date__gte=" + dategte + "&creation_date__lte=" + datelte;
                nameFile = nameFile + "_" + selectedYear;
            }
        }

        console.log("nameFile", nameFile);
        requestDataExport(url, nameFile);
    };

    const requestDataExport = async (url, nameFile) => {
        try {
            let datas = await helper.GetDataExportOcr(url);
            if (datas.status == 200 || 201) {
                let data = datas.data;
                if (data && data.length > 0) {
                    convertToExcel(data, nameFile);
                } else {
                    utils.SwalError("Data tidak ditemukan");
                }
            }

            // console.log("requestDataExport", datas);
        } catch (err) {
            console.log("button export-loadPolda", err);
        }
    };

    const convertToExcel = (data, nameFile) => {
        // Membuat workbook baru
        const wb = XLSX.utils.book_new();

        // Mengubah data array menjadi sheet
        const ws = XLSX.utils.json_to_sheet(data);

        // Menambahkan sheet ke workbook
        XLSX.utils.book_append_sheet(wb, ws, "Data Polda");

        // Mengonversi workbook menjadi file Excel
        const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });

        // Mengunduh file Excel
        const excelFile = new Blob([excelBuffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        saveAs(excelFile, nameFile + ".xlsx");
    };

    const handleYearChange = (e) => {
        setSelectedYear(e.target.value);
    };

    const handleChange = (event) => {
        setSelectedMonth(event.target.value);
    };

    const loadPolda = async () => {
        try {
            let datas = await helper.GetListPolda(dataIp + "/polridocument/@list_polda");
            setPoldaRef(datas);
            modifyPoldaToOptionsHtml(datas);
        } catch (err) {
            console.log("button export-loadPolda", err);
        }
    };

    const modifyPoldaToOptionsHtml = (datas) => {
        if (datas) {
            let options = [];
            // setDocTypeRef(datas);
            datas.map(function (data, index) {
                options.push(<option value={data.polda_id}>{data.polda_name}</option>);
            });

            setPoldaOptionsHtml(options);
        }
    };

    const loadSatker = async (poldaId) => {
        console.log("loadSatker", poldaId);
        try {
            let options = [];
            let res = await helper.GetListSatker(dataIp + "/polridocument/" + poldaId + "/@list_satker");
            console.log("loadSatker", res);
            // if (res && (res.status == 200 || 201 || 202 || 204)) {
            setSatkerRef(res);

            res.map(function (data, index) {
                options.push(<option value={data.satker_id}>{data.satker_name}</option>);
            });

            setSatkerOptionsHtml(options);
            // }
        } catch (err) {
            console.log("loadSatker error", err);
        }
    };

    const modifyData = (data) => {
        if (data && data.length > 0) {
            return data.sort((a, b) => a.doc_type_index_no - b.doc_type_index_no);
        } else {
            return [];
        }
    };

    const loadDoctype = async () => {
        try {
            let options = [];
            if (dataIp && dataIp != "") {
                let url = dataIp + "/doctype/@list_doctype";
                let res = await helper.GetListDocType(url);
                if ((res && res.status == 200) || 201 || 202 || 204) {
                    let datas = modifyData(res.data); //res.data;
                    setDoctypeRef(datas);

                    datas.map(function (data, index) {
                        options.push(<option value={data.doc_type_id}>{`${data.doc_type_title} - ${data.doc_type_code}`}</option>);
                    });

                    setDoctypeOptionsHtml(options);
                }
            }
        } catch (err) {
            console.log("Button Export - loadDoctype, error", err);
        }
    };

    return (
        <div className="dropdown w-auto">
            <button className="btn btn dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <img src={excel} width={"30px"} />
            </button>
            <ul className="dropdown-menu w-auto p-4" aria-labelledby="dropdownMenuButton">
                <p calss="text-success">{t("expor_hasil_but")}</p>
                <li className="mb-3">
                    <select disabled={!isAdmin} className="form-select w-auto" aria-placeholder="Pilih Korwil" value={poldaSelectedOption} onChange={handleSelectPoldaChange}>
                        <option value="" selected>{t("select_um")} Korwil</option>
                        {poldaOptionsHtml}
                    </select>
                </li>
                <li className="mb-3">
                    <select disabled={!isAdmin && (satker && satker != "null")} className="form-select w-100" aria-placeholder="Pilih Satker" value={satkerSelectedOption} onChange={handleSelectSatkerChange}>
                        <option value="" selected>{t("select_um")} Satker</option>
                        {satkerOptionsHtml}
                    </select>
                </li>
                <li className="mb-3">
                    <select className="form-select" value={doctypeSelectedOption} onChange={handleSelectDoctypeChange}>
                        <option selected>{t("choose_doc_type_td")}</option>
                        {doctypeOptionsHtml}
                    </select>
                </li>
                <li className="mb-3">
                    <select id="month" className="form-select w-50" value={selectedMonth} onChange={handleChange}>
                        <option value="">-- {t("bulan_t1")} --</option>
                        {months.map((month, index) => (
                            <option key={index} value={month}>
                                {month}
                            </option>
                        ))}
                    </select>
                </li>
                <li className="mb-3">
                    <select id="year" className="form-select w-50" value={selectedYear} onChange={handleYearChange}>
                        <option value="">-- {t("tahun_t1")} --</option>
                        {Array.from({ length: currentYear - startYear + 1 }, (_, i) => (
                            <option key={i} value={startYear + i}>
                                {startYear + i}
                            </option>
                        ))}
                    </select>
                </li>
                <li>
                    <button className="btn btn-success w-10 mt-2" onClick={handleExportRequest}>
                        {t("ekspor_um")}
                    </button>
                </li>
            </ul>
        </div>
    );
};

export default ButtonWithSelect;
