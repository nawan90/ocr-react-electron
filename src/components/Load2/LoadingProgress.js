// import React, { useState, useEffect } from "react";

// const LoadingProgress = () => {
//     const [progress, setProgress] = useState(0);

//     useEffect(() => {
//         if (progress < 100) {
//             const interval = setInterval(() => {
//                 setProgress((prevProgress) => prevProgress + 10); // Meningkatkan progres setiap 1 detik
//             }, 1000);
//             return () => clearInterval(interval);
//         }
//     }, [progress]);

//     return (
//         <div style={{ width: "50%", margin: "auto", textAlign: "center" }}>
//             <h3>Proses Loading...</h3>
//             {/* <ProgressBar now={progress} label={`${progress}%`} /> */}
//             <div class="progress-bar animated" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
//                 <span class="sr-only">{`${progress}%`}</span>
//             </div>
//         </div>
//     );
// };

// export default LoadingProgress;


import React, { useState, useEffect } from 'react';
import './Loading-bs.css';
import { useTranslation } from "react-i18next";

// import './Loading.css'; // Mengimpor file CSS untuk styling

const LoadingProgress = (props) => {
  const { stateUpload, stateOcr, stateOcrProgress } = props;
  const [progress, setProgress] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [intervalTime, setIntervalTime] = useState(1000);
  const { t } = useTranslation();

  useEffect(() => {
    if (stateUpload != 1) {
      if (progress < 20) {
        const interval = setInterval(() => {
          setProgress(prevProgress => prevProgress + 1); // Menambah progress setiap 50ms
        }, intervalTime);
        return () => clearInterval(interval);
      }
    }
    else if (stateUpload == 1) {
      if (progress < 80) {
        const interval = setInterval(() => {
          setProgress(prevProgress => prevProgress + 1); // Menambah progress setiap 50ms
        }, intervalTime);
        return () => clearInterval(interval);
      }
    }
    setTimeout(() => setIsLoading(false), 500); // Setelah selesai, sembunyikan loading
  }, [progress]);

  useEffect(() => {
    if (stateUpload == 1 && stateOcr == 0) {
      // setProgress(stateOcrProgress)
      if (stateOcrProgress > progress) {
        setProgress(stateOcrProgress)
      }
      else {
        setProgress(stateOcrProgress)
      }
    }
  }, [stateUpload]);

  useEffect(() => {
    if (stateOcr == 1) {
      setProgress(100)
    }
  }, [stateOcr]);

  useEffect(() => {
    if (stateUpload == 1) {
      if (stateOcrProgress > progress) {
        setProgress(stateOcrProgress)
      }
    }
  }, [stateOcrProgress]);

  return (
    // <div className="loading-container">
    //     <div className="loading-content">
    //       <div className="progress-bar ">
    //         <div className="progress-bar-fill " style={{ width: `${progress}%` }}></div>
    //       </div>
    //       <h3 className="loading-text" style={{ textAlign: "center",}}>{stateUpload != 1? 'Upload Dokumen...' : 'Proses OCR...'} {progress}%</h3>
    //     </div>
    // </div>
    <div className="loading-container">
      <div className="loading-content">
        <div 
          className="progress" 
          role="progressbar" 
          aria-label="Animated striped example" 
          aria-valuenow={progress} 
          aria-valuemin="0" 
          aria-valuemax="100"
        >
          <div
            className="progress-bar progress-bar-striped progress-bar-animated"
            style={{ width: `${progress}%` }}
          ></div>
        </div>
        <h3 className="loading-text" style={{ textAlign: "center",}}>{stateUpload != 1? `${t("unggah_memuat_kemajuan")}...` : `${t("proses_memuat_kemajuan")}...`} {progress}%</h3>
      </div>
    </div>
  );
};

export default LoadingProgress;
