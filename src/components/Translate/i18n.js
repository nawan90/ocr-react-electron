import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import english from "../../assets/dictionary/en.json"
import indonesia from "../../assets/dictionary/id.json"

// Translation files
const resources = {
  en: {
    translation: english
  },
  id: {
    translation: indonesia
  },
};

// Initialize i18n
i18n
  .use(LanguageDetector) // Automatically detects language
  .use(initReactI18next) // Passes i18n down to react-i18next
  .init({
    lng: localStorage.getItem('i18nextLng') || 'id',  // Set language from localStorage
    fallbackLng: 'id',  // Fallback to 'id' if no language is found
    debug: true,  // Enable debug to ensure it's working correctly

    interpolation: {
      escapeValue: false,  // React already escapes values
    },

    resources: {
      en: {
        translation: english
      },
      id: {
        translation: indonesia
      },
    },
  });
 

 

export default i18n;
