import "../../assets/css/loading.css";
import { useNavigate, useLocation,  } from "react-router-dom";

const LoadSpinner = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const path = location.pathname;
  
  const cssClass = () => {
  if (path === '/searching') {
    return "component-spinner"
  } else {
    return "main"
  }
  } 


  return (
    <div id={cssClass() === "component-spinner" ? "load-body-component" : "load-body"}>
      <div id="loader-container">
        <svg id="loader" width="300" height="400" fill="none" viewBox="0 0 200 200">
          <linearGradient id="a11">
            <stop offset="0" stop-color="#d9ae0f" stop-opacity="0"></stop>
            <stop offset="1" stop-color="#b60d20"></stop>
          </linearGradient>
          <circle fill="none" stroke="url(#a11)" stroke-width="30" stroke-linecap="round" stroke-dasharray="0 44 0 44 0 44 0 44 0 360" cx="100" cy="100" r="70" transform-origin="center">
            <animateTransform type="rotate" attributeName="transform" calcMode="discrete" dur="2" values="360;324;288;252;216;180;144;108;72;36" repeatCount="indefinite"></animateTransform>
          </circle>
        </svg>
      </div>
    </div>
  );
};
export default LoadSpinner;
