import { useState , useEffect} from "react";
import { Outlet, useOutletContext } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";

const Layout = () => {
      const [selected, setSelected] = useState(false); 
  return (
    <div  >
      <Header />
      <Outlet context={[ selected,setSelected ]} style={{ height: "100vh" }} />
      {/* <Outlet /> */}
      <Footer selected={selected} setSelected={setSelected} />
    </div>
  );
};

export default Layout;
export function useUser() {
  return useOutletContext();
}
