import { useState, useEffect, useRef, React } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate, useLocation, Link, NavLink } from "react-router-dom";
import moment from "moment";
import "moment/locale/id";

function ModalHapusTemplate(getData) {
    // const {getData, handlerDelete} = props
    const notify = () =>
        // toast('Delete Data Berhasil', {
        //     icon: '👏',
        // });
        {
            getData.onConfirm(getData.getData);
        };
    const location = useLocation();
    const navigate = useNavigate();
    const path = location.pathname;
    const modalBody = () => {
        if (path === "/sharing/single-doc") {
            return (
                <div className="modal-body">
                    Nama Dokumen : {getData.getData.namaDocument} <br />
                    Tipe Dokumen : {getData.getData.typeDocument} <br />
                </div>
            );
        } else if (path === "/sharing/multi-doc") {
            return (
                <div className="modal-body">
                    Nama Dokumen : {getData.getData.namaDocument} <br />
                    Tipe Dokumen : {getData.getData.typeDocument} <br />
                </div>
            );
        } else if (path === "/user/list-user") {
            return (
                <div className="modal-body">
                    Nama User : {getData.getData.namaUser} <br />
                    Satker : {getData.getData.satker} <br />
                </div>
            );
        } else if (path === "/user/ganti-user") {
            return (
                <div className="modal-body">
                    Nama User : {getData.getData.namaUser} <br />
                    Satker : {getData.getData.satker} <br />
                </div>
            );
        } else if (path === "/template/list-template") {
            return (
                <div className="modal-body">
                    Nama Template : {getData.getData.template_name} <br />
                    Tipe Template : {getData.getData.template_doc_type_name} <br />
                    User : {getData.getData.template_polda_name} <br />
                </div>
            );
        } else if (path === "/setting/provinsi") {
            return (
                <div className="modal-body">
                    Kode Provinsi : {getData?.provinsi_code} <br />
                    Nama Provinsi : {getData?.provinsi_name} <br />
                    Deskripsi : {getData?.provinsi_desc} <br />
                </div>
            );
        } else if (path === "/setting/configure-meta-data") {
            return (
                <div className="modal-body">
                    Nama metadata : {getData.getData?.metadata_name} <br />
                </div>
            );
        } else if (path === "/setting/list-polda") {
            return (
                <div className="modal-body">
                    Nama Polda : {getData?.polda_name} <br />
                    Provinsi : {getData?.polda_propinsi_name} <br />
                </div>
            );
        } else if (path === "/setting/role-permission") {
            return (
                <div className="modal-body">
                    Nama User : {getData.getData.namaUser} <br />
                    Satker : {getData.getData.satker} <br />
                </div>
            );
        } else if (path === "/setting/doctype") {
            return (
                <div className="modal-body">
                    Document Type Name : {getData?.doc_type_title} <br />
                    Tanggal Buat : {moment(getData?.doc_type_created_date).format("DD MMMM YYYY")} <br />
                    Deskripsi : {getData?.doc_type_desc} <br />
                </div>
            );
        }
    };
    return (
        <div class="modal fade " id="modalHapus" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                            Anda Yakin Ingin Menghapus Data
                        </h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {modalBody()}
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-primary"
                            data-bs-dismiss="modal"
                            onClick={() => {
                                notify();
                                // handlerDelete()
                            }}
                        >
                            Iya
                        </button>

                        <button type="button" className="btn btn-danger" data-bs-dismiss="modal">
                            Tidak
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModalHapusTemplate;
