import { useState, useEffect, useRef, React } from "react";
import "../../assets/css/modal.css";
import InputRolePermission from "../../pages/Setting/RoleUser/input";
import { useNavigate, useLocation, Link, NavLink } from "react-router-dom";
import InputUser from "../../pages/User/ListUser/input";
import InputMetaData from "../../pages/Setting/ConfigureMetaData/input";
import InputPolda from "../../pages/Setting/ListPolda/input";
import InputSatker from "../../pages/Setting/ListSatker/input";
import InputDocType from "../../pages/Setting/DocumentType/input";
import InputProvinsi from "../../pages/Setting/Provinsi/input";
import { useTranslation } from "react-i18next";
import ChangePassword from "../../pages/User/Profile/change_password";
import ViewLog from "../../pages/ActivityLog/view";

function ModalView(props) {
    const { getAllTable, onSelected, action, showTable, getData } = props;
    const [disButton, setDisButton] = useState(true);
    const location = useLocation();
    const path = location.pathname;
    const handleClose = (e) => {
        e.preventDefault();
        setDisButton(true);
    };
    const { t } = useTranslation();

    const titleMap = {
        "/setting/role-permission": t("titlle_rp"),
        "/user/list-user": t("pengguna_t1"),
        "/setting/configure-meta-data": t("titlle_md"),
        "/setting/provinsi": t("titlle_pr"),
        "/setting/doctype": t("titlle_dt"),
        "/setting/list-polda": `${t("list_um")} ${t("titlle_lp")}`,
        "/setting/list-satker": `${t("list_um")} ${t("titlle_ls")}`,
        "/Profile": `${t("change_pass_um")}`,
        "/activity-log": `${t("titlle_al")}`,
    };
    const componentMap = {
        "/setting/role-permission": <InputRolePermission handleClose={handleClose} disButton={disButton} setDisButton={setDisButton} />,
        "/user/list-user": <InputUser handleClose={handleClose} disButton={disButton} setDisButton={setDisButton} getAllTable={getAllTable} onSelected={onSelected} action={action} showTable={showTable} getData={getData} />,
        "/setting/configure-meta-data": <InputMetaData handleClose={handleClose} disButton={disButton} setDisButton={setDisButton} getAllTable={getAllTable} onSelected={onSelected} action={action} showTable={showTable} getData={getData} />,
        "/setting/list-polda": <InputPolda handleClose={handleClose} disButton={disButton} setDisButton={setDisButton} getAllTable={getAllTable} onSelected={onSelected} action={action} showTable={showTable} getData={getData} />,
        "/setting/list-satker": (
            <InputSatker
                handleClose={handleClose}
                disButton={disButton}
                setDisButton={setDisButton}
                getAllTable={getAllTable}
                onSelected={onSelected}
                action={action}
                showTable={showTable}
                getData={getData}
                // closeButton={closeButton} setCloseButton={setCloseButton} validSatkerCode={validSatkerCode} setValidSatkerCode={setValidSatkerCode} validSatkerName={validSatkerName} setValidSatkerName={setValidSatkerName} validSatkerPoldaName={validSatkerPoldaName} setValidSatkerPoldaName={setValidSatkerPoldaName}
            />
        ),
        "/setting/doctype": (
            <InputDocType
                // validDocCode={validDocCode} setValidDocCode={setValidDocCode} validDocText={validDocText} setValidDocText={setValidDocText} validDocDesc={validDocDesc} setValidDocDesc={setValidDocDesc}
                handleClose={handleClose}
                disButton={disButton}
                setDisButton={setDisButton}
                getAllTable={getAllTable}
                onSelected={onSelected}
                action={action}
                showTable={showTable}
                getData={getData}
                // closeButton={closeButtonDocType} setCloseButton={setCloseButtonDocType}
            />
        ),
        "/setting/provinsi": <InputProvinsi handleClose={handleClose} disButton={disButton} setDisButton={setDisButton} getAllTable={getAllTable} onSelected={onSelected} action={action} showTable={showTable} getData={getData} />,
        "/Profile": <ChangePassword handleClose={handleClose} disButton={disButton} setDisButton={setDisButton} />,
        "/activity-log": <ViewLog getData={getData} handleClose={handleClose} />,
    };

    return (
        <div class="modal fade " id="ModalInput" aria-hidden="true" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" onClose={handleClose}>
            <div className="modal-dialog modal-input modal-xl">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="ModalInputLabel">
                            {t("titlle_al")}
                        </h5>
                        <button type="button" id="closeModal" className="btn-close" data-bs-dismiss="modal" onClose={handleClose}></button>
                    </div>
                    <div className="modal-body">{componentMap[path]}</div>
                </div>
            </div>
        </div>
    );
}

export default ModalView;
