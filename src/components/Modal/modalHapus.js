import moment from "moment";
import "moment/locale/id";
import { React } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";

function ModalHapus(props) {
    const { getData, handlerDelete } = props;
        const { t } = useTranslation();
    
    const notify = () =>
        toast("Delete Data Berhasil", {
            icon: "👏",
        });
    const location = useLocation();
    const navigate = useNavigate();
    const path = location.pathname;
    const modalBody = () => {
        if (path === "/sharing/single-doc") {
            return (
                <div className="modal-body">
                    {t("doc_name_td")} : {getData.getData.namaDocument} <br />
                    {t("titlle_dt")} : {getData.getData.typeDocument} <br />
                </div>
            );
        } else if (path === "/sharing/multi-doc") {
            return (
                <div className="modal-body">
                    {t("doc_name_td")} : {getData.getData.namaDocument} <br />
                    {t("titlle_dt")} : {getData.getData.typeDocument} <br />
                </div>
            );
        } else if (path === "/user/list-user") {
            return (
                <div className="modal-body">
                    {t("nama_pengguna_tu")} : {getData.fullname} <br />
                    Satker : {getData.satker_name} <br />
                </div>
            );
        } else if (path === "/user/ganti-user") {
            return (
                <div className="modal-body">
                    {t("nama_pengguna_tu")} : {getData.fullname} <br />
                    Satker : {getData.satker_name} <br />
                </div>
            );
        } else if (path === "/template/list-template") {
            return (
                <div className="modal-body">
                    {t("name_temp_tt")} : {getData.getData.template_name} <br />
                    {t("tipe_um")}  {t("templat_t1")}  : {getData.getData.template_doc_type_name} <br />
                    {t("pengguna_t1")} : {getData.getData.template_polda_name} <br />
                </div>
            );
        } else if (path === "/setting/provinsi") {
            return (
                <div className="modal-body">
                    {t("prov_kode_pr")} : {getData?.provinsi_code} <br />
                    {t("prov_name_pr")} : {getData?.provinsi_name} <br />
                    {t("description_um")} : {getData?.provinsi_desc} <br />
                </div>
            );
        } else if (path === "/setting/configure-meta-data") {
            return (
                <div className="modal-body">
                    {t("meta_name_md")} : {getData.getData?.metadata_name} <br />
                </div>
            );
        } else if (path === "/setting/list-polda") {
            return (
                <div className="modal-body">
                    {t("polda_name_lp")} : {getData?.polda_name} <br />
                    {t("titlle_pr")} : {getData?.polda_propinsi_name} <br />
                </div>
            );
        } else if (path === "/setting/list-satker") {
            return (
                <div className="modal-body">
                    {t("satker_name_ls")} : {getData?.satker_name} <br />
                    Korwil : {getData?.satker_polda_name} <br />
                </div>
            );
        } else if (path === "/setting/role-permission") {
            return (
                <div className="modal-body">
                     {t("role_tu")} : {getData?.rolename} <br />
                </div>
            );
        } else if (path === "/setting/doctype") {
            return (
                <div className="modal-body">
                    {t("doc_type_name_dt")}  : {getData?.doc_type_title} <br />
                    {t("create_date_um")}  : {moment(getData?.doc_type_created_date).format("DD MMMM YYYY")} <br />
                    {t("description_um")} : {getData?.doc_type_desc} <br />
                </div>
            );
        }
    };
    return (
        <div class="modal fade " id="modalHapus" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                        {t("anda_yakin_hapus_um")}
                        </h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {modalBody()}
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-primary"
                            data-bs-dismiss="modal"
                            onClick={() => {
                                // notify();
                                handlerDelete(getData);
                            }}
                        >
                            {t("iya_um")}
                        </button>

                        <button type="button" className="btn btn-danger" data-bs-dismiss="modal">
                        {t("tidak_um")}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModalHapus;
