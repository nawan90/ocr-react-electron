import moment from "moment";
import "moment/locale/id";
import { useState, React, useEffect } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate, useLocation } from "react-router-dom";
import LoadSpinner from "../Load/load";
import axios from "axios";
import helper from "../../api/helper";
import Swal from "sweetalert2";
import LocalStorageHelper from "../../api/localStorageHelper";
function ModalOtp(props) {
    const userData = LocalStorageHelper.getUserData()
    // const jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJha3NlcyI6IiIsImNyZWF0ZWQiOjE3MzE5MDg3NTA4MzAsImV4cCI6MTczMjUxMzU1MCwiaW5pdGlhbF9kZXZpY2VfZGlzcGxheV9uYW1lIjoiU3VwZXIgQWRtaW4iLCJpc19hZG1pbiI6ZmFsc2UsImlzX3N1cGVyYWRtaW4iOnRydWUsImtlcG9saXNpYW5fbGV2ZWwiOiIiLCJrb2RlX2tvcndpbCI6IiIsImtvZGVfc2F0a2VyIjoiIiwibmFtYV9rb3J3aWwiOiIiLCJuYW1hX3NhdGtlciI6IiIsIm5hbWUiOiJTdXBlciBBZG1pbiIsIm5ycCI6Ijg4ODg4ODg4Iiwicm9sZXMiOm51bGwsInNwcG0iOiIiLCJzdWIiOiJzdXBlcmFkbWluIiwidXNlcl9uYW1lIjoic3VwZXJhZG1pbiIsInVzZXJfdXVpZCI6IjM5MDRmMzdjLTcwNjItNDM4Yy04NmE5LWE2N2E2ZGQxNTJlNSJ9.U5otKqa4lSfvq6brhpZX6u81tmbcr5148CLLfzWsGTs"
    const efaskon_url = process.env.REACT_APP_API_EFASKON;
    const { getData, handlerDelete, readyToDownload } = props;
    const [otpCode, setOtpCode] = useState("");
    const [loading, setLoading] = useState(false)
    const [timer, setTimer] = useState(2); 
    // const [timer, setTimer] = useState(1 * 60); // Initial time in seconds (5 minutes)
    const notify = () =>
        toast("Download Data Berhasil", {
            icon: "👏",
        });
    const location = useLocation();
    const navigate = useNavigate();
    const path = location.pathname;

   

    const onEnterOTPCode = (e) => {
        const value = e.target.value;

        // Memastikan hanya angka yang dapat dimasukkan
        if (/^\d*$/.test(value)) {
            setOtpCode(value); // Update OTP hanya jika input berupa angka
        }
    };
    const sendRequestOTP = async() =>{
        setOtpCode("")
        resetTimer()
        setLoading(true)
        const jwt = userData.jwt_efaskon;
        const datesnya = Date.now().toString()
        const hmacHash = helper.getHmac(datesnya)
        const bearer = `${hmacHash}_${jwt}`

        const url = `${efaskon_url}/v1/otp/request`
        try{
            axios.post(url,{},
                {
                    headers:{
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${bearer}`,
                        Dates: datesnya,
                    }
                }
            )
            .then((response) => {
                console.log("response Req OTP : ",response)
                if(response.status == 200){
                    Swal.fire("OTP Sukses","Silahkan Cek Email Anda!","success")
                }
            }).catch((err) =>{
    
            }).finally(()=>{
                setLoading(false)
            })
        }catch(e){
            setLoading(false)
        }
        
    }

    const  sendValidateOTP = async() =>{
        setLoading(true)
        const jwt = userData.jwt_efaskon;
        const datesnya = Date.now().toString()
        const hmacHash = helper.getHmac(datesnya)
        const bearer = `${hmacHash}_${jwt}`
        const url = `${efaskon_url}/v1/otp/validate`
        try{
            axios.post(url,{"otp" : otpCode},
                {
                    headers:{
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${bearer}`,
                        Dates: datesnya,
                    }
                }
            )
            .then((response) => {
                if(response.status == 200){
                    let idmodal = document.getElementById("btn_close_modal");
                    idmodal.click();
                    readyToDownload();
                    setOtpCode("")

                }
                
            }).catch((err) =>{
                Swal.fire("OTP Gagal","Kode OTP tidak cocok!","error")
            }).finally(()=>{
                // readyToDownload();
                // let idmodal = document.getElementById("btn_close_modal");
                // idmodal.click();
                // setOtpCode("")
                setLoading(false)
            })
        }catch(e){
            setLoading(false)
            Swal.fire("OTP Gagal","Kode OTP tidak cocok!","error")
        }
        
    }
    
  
    useEffect(() => {
        let interval;
        if (timer > 0) {
          interval = setInterval(() => {
            setTimer((prev) => prev - 1);
          }, 1000); // Decrease timer every second
        } else if (timer === 0) {
          clearInterval(interval); // Stop the timer when it reaches 0
        }
    
        return () => clearInterval(interval); // Cleanup interval on unmount
      }, [timer]);
    
      const resetTimer = () => {
        setTimer(5 * 60); // Reset to 5 minutes
      };
    
      // Format time to MM:SS
      const formatTime = (time) => {
        
        const minutes = Math.floor(time / 60);
        const seconds = time % 60;
        return `${minutes.toString().padStart(2, "0")}:${seconds
          .toString()
          .padStart(2, "0")}`;
      };

    const modalBody = () => {
        if (path === "/document/create-document" || path === "/searching") {
            return (
                <div className="modal-body">
                    <div class="d-flex flex-column justify-content-center align-items-center mt-4 mb-2">
                        
                        {timer <= 0 ? (
                            <button
                                className="btn btn-warning mx-2"
                                onClick={() => {
                                    sendRequestOTP();
                                }}
                            >
                                Kirim Permintaan OTP ke Email {userData.myemail}
                            </button>
                        ) : (
                            <button className="btn btn-warning mx-2"  disabled> Kirim ulang OTP dalam {formatTime(timer)}</button>
                        )}
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <input value={otpCode} type="text" placeholder="Kode OTP" class="form-control w-25 mt-2 mb-4" maxLength="6" onChange={onEnterOTPCode} />
                    </div>
                </div>
            );
        }
    };
    return (
        <div class="modal fade " id="modalOTP" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
            {
                loading ? 
                (<LoadSpinner/>):""
            }
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                            Masukkan Kode OTP
                        </h5>
                        <button type="button" id="btn_close_modal" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {modalBody()}
                    <div className="modal-footer d-flex flex-column justify-content-center align-items-center">
                        <button
                            type="button"
                            className="btn btn-primary"
                            data-bs-dismiss="modal"
                            disabled={otpCode.length !== 6} 
                            onClick={() => {
                                // notify();
                                // handlerDelete(getData);
                                sendValidateOTP();
                            }}
                        >
                            Konfirmasi Kode OTP 
                        </button>

                        {/* <button type="button" className="btn btn-danger" data-bs-dismiss="modal">
                            Tidak
                        </button> */}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModalOtp;
