import { useState, React } from "react";
import "../../assets/css/modal.css";
import { useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import ShowPdf from "../../pages/Searching/View";

function ModalPdfView(props) {
    const { link, isDownload, docName } = props;
    const [disButton, setDisButton] = useState(true);
    const location = useLocation();
    const path = location.pathname;
    const handleClose = (e) => {
        e.preventDefault();
        setDisButton(true);
    };
    // console.log("ModalPdfView", link, isDownload, docName);
    const { t } = useTranslation();

    const titleMap = {
        "/document/create-document": `${t("titlle_al")}`,
    };
    const componentMap = {
        "/document/create-document": <ShowPdf link={link} isDownload={isDownload}></ShowPdf>,
    };

    return (
        <div class="modal fade " id="ModalPdfView" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
            <div className="modal-dialog modal-xl">
                <div className="modal-content">
                    <div className="modal-header">
                        <h6 className="modal-title" id="exampleModalLabel">
                            Dokumen {docName}
                        </h6>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">{componentMap[path]}</div>
                </div>
            </div>
        </div>
    );
}

export default ModalPdfView;
