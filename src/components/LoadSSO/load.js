import "../../assets/css/loading.css";
import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import CryptoJS from "crypto-js"; // Import CryptoJS
import axios from "axios";
import { SignJWT } from "jose";
import Helper from "../../api/helper";
import { useTranslation } from "react-i18next";
import accessManager from "../../api/accessManager";
import utils from "../../api/utils";
import "./load.css";
import logo from "../../assets/images/icon/logo_slog.png";
import LocalStorageHelper from "../../api/localStorageHelper";

const ListPerm = [
    "dms.polda.add",
    "dms.polda.modify",
    "dms.polda.view",
    "dms.polda.list",
    "dms.satker.add",
    "dms.satker.modify",
    "dms.satker.view",
    "dms.satker.list",
    "dms.metadata.add",
    "dms.metadata.modify",
    "dms.metadata.view",
    "dms.metadata.list",
    "dms.doctype.add",
    "dms.doctype.modify",
    "dms.doctype.view",
    "dms.doctype.list",
    "dms.template.add",
    "dms.template.modify",
    "dms.template.view",
    "dms.template.list",
    "dms.province.add",
    "dms.province.modify",
    "dms.province.view",
    "dms.province.list",
    "dms.document.add",
    "dms.document.modify",
    "dms.document.view",
    "dms.document.list",
    "dms.document.version",
    "dms.document.approve",
    "dms.theme.access",
    "dms.user.add",
    "dms.user.modify",
    "dms.user.view",
    "dms.user.list",
    "dms.search.access",
    "dms.log.access",
    "dms.role.access",
];

const LoadSso = ({ props }) => {
    const location = useLocation();
    const navigate = useNavigate();
    const path = location.pathname;
    let base_url = process.env.REACT_APP_API_URL;
    let id_ip = "IP20240605040259";
    const [usrnm, setUsrnm] = useState("");
    const [data, setData] = useState(null);
    const [iniToken, setIniToken] = useState(null);
    const [profile, setProfile] = useState();
    const [profileSso, setProfileSso] = useState([]);
    const [loading, setLoading] = useState(true);
    const [dataIp, setDataIp] = useState([]);
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState(false);
    const [pwrd, setPwrd] = useState("");
    const [ayaJwt, setAyaJwt] = useState("");
    const [retrievedObject, setRetrievedObject] = useState("");
    const [isLoginSSOFinished, setIsLoginSSOFinished] = useState(null);
    const [mac, setMac] = useState([]);
    const [versionTrack, setVersionTrack] = useState([]);
    const VERSION_TRACKING_URL = "https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking";
    const poldaname = localStorage.getItem("mypoldaname");
    const userData = LocalStorageHelper.getUserData()  

    useEffect(() => {
        window.localStorage.setItem("ipAddress", dataIp ? dataIp.ip_address : "");
    }, [usrnm]);

    useEffect(() => {
        // getIpSetting();
        if (base_url && base_url != "null") {
            getIpSetting();
        }
    }, [base_url]);

    const getIpSetting = async () => {
        console.log("getIpSetting");
        try {
            let url = base_url + "ipsetting/" + id_ip;

            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            console.log("getIpSetting", url);
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
            setDataIp(datas);
        } catch (err) {
            console.log(`${t("error_um")}`);
        }
    };

    useEffect(() => {
        console.log("useEffect - dataIp", dataIp, dataIp && dataIp.hasOwnProperty("ip_address"));
        if (dataIp && dataIp.hasOwnProperty("ip_address")) {
            localStorage.setItem("ipAddress", dataIp.ip_address ? dataIp.ip_address : "");
            console.log("useEffect - dataIp - profileSso", dataIp, profileSso);
            if (profileSso) {
                onRequestLoginSso();
            }
        } else {
            console.log("useEffect - else - dataIp", dataIp);
        }
    }, [dataIp]);

    const queryVariables = new URLSearchParams(location.search);
    useEffect(() => {
        const queryParamsString = location.search || location.hash.split("?")[1] || "";
        const queryParams = new URLSearchParams(queryParamsString);
        const message = queryParams.get("message");
        const timestamp = queryParams.get("timestamp");
        setData({ message, timestamp });
    }, [location]);

    useEffect(() => {
        if (data) {
            localStorage.setItem("data web", JSON.stringify(data));
            setRetrievedObject(data);
        } else {
            localStorage.setItem("data web", "null");
        }
    }, [data]);

    const getHashHmac = async () => {
        const sessionKey = retrievedObject?.message;
        const secret_prod = "214844b1-bc09-4a4b-942a-b28023bb799b";
        const datesnya = Date.now().toString();
        const hash = CryptoJS.HmacSHA256(datesnya, secret_prod);
        const hmacHash = hash.toString(CryptoJS.enc.Hex);
        console.log("LOADSSO - getHashHmac", hmacHash);
        try {
            const res = await axios.get(`https://efaskon.slog.polri.go.id/v1/session/restore/${sessionKey}`, {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${hmacHash}_`,
                    Dates: datesnya,
                },
            });

            if (res.data.data) {
                if (!iniToken || (iniToken && iniToken != res.data.data)) {
                    setIniToken(res.data.data);
                    localStorage.setItem("jwt_efaskon", res.data.data);
                } else {
                    console.log("iniToken sama ", iniToken, res.data.data);
                }
                // setIniToken(res.data.data);
                // localStorage.setItem("jwt_efaskon", res.data.data);
            }
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        if (retrievedObject && retrievedObject.message) {
            getHashHmac();
        } else {
            console.error("retrievedObject or retrievedObject.message is undefined or null");
        }
    }, [retrievedObject]);


    const getProfile = async () => {
        const sessionKey = retrievedObject?.message;
        const secret_prod = "214844b1-bc09-4a4b-942a-b28023bb799b";
        const datesnya = Date.now().toString();
        const hash = CryptoJS.HmacSHA256(datesnya, secret_prod);
        const hmacHash = hash.toString(CryptoJS.enc.Hex);

        try {
            const res = await axios.get(`https://efaskon.slog.polri.go.id/v1/profile`, {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${hmacHash}_${iniToken}`,
                    Dates: datesnya,
                },
            });

            if (res.data.data) {
                setProfile(res.data.data);
                setLoading(false); // Update loading state setelah berhasil mengambil profile
            }
        } catch (e) {
            console.error(e);
            setLoading(false);
        }
    };

    useEffect(() => {
        if (iniToken) {
            getProfile();
        } else if (!iniToken) {
            console.error("retrievedObject or retrievedObject.message is undefined or null");
        }
    }, [iniToken]);

    useEffect(() => {
        if (profile && profile.hasOwnProperty("user")) {
            let level = profile ? profile?.user?.level : null;
            let akses = profile ? profile?.user?.akses : null;
            let kode_satker = profile?.user?.kode_satker;
            let kode_korwil = profile?.user?.kode_korwil;
            if (level && level == "MABES") {
                kode_korwil = kode_korwil ? kode_korwil : "060018100";
                kode_satker = kode_satker ? kode_satker : "060018100648275000KP";
            }
            console.log("setsso", kode_korwil, kode_satker);
            setProfileSso({
                nrp: profile ? profile?.nrp : null,
                nama: profile ? profile?.nama : null,
                pangkat: profile ? profile?.pangkat : null,
                jabatan: profile ? profile?.jabatan : null,
                satker: profile ? profile?.satker : null,
                username: profile ? profile?.user.username : null,
                phone: profile ? profile?.user.phone : null,
                is_superadmin: profile ? profile?.user.is_superadmin : null,
                email: profile ? profile?.user.email : null,
                kode_satker: kode_satker, // ? level == "MABES" ? kode_satker : profile ? profile?.user.kode_satker : null,
                kode_korwil: kode_korwil, //level == "MABES" ? kode_korwil : profile ? profile?.user.kode_korwil : null,
                korwils: profile ? profile?.user.korwils : null,
                akses: profile ? profile?.user?.akses : null,
                level: profile ? profile?.user.level : null,
            });
        }
    }, [profile]);

    const createJWT = async (payload) => {
        const secretKey = new TextEncoder().encode("Ocr_efaskon");
        const jwt = await new SignJWT(payload).setProtectedHeader({ alg: "HS256" }).sign(secretKey);
        return jwt;
    };

    useEffect(() => {
        if (profileSso != null || profileSso !== undefined) {
            console.log("useEffectsso dasds das ds ad as ", profileSso);
            onRequestLoginSso();
            // navigate("/login")
        } else if (profileSso == null || profileSso === undefined) {
            console.log("useEffect - profileSso", profileSso);
            navigate("/login");
        } else {
            console.log("useEffect - profileSso- else", profileSso);
        }
    }, [profileSso]);

    const getListHistory = async () => {
        const url = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking/@list_versiontrack';
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", `Bearer ${userData.token}`);

        const requestOptions = {
            method: "GET",
            headers: myHeaders,
        };

        try {
            await axios.get(url, requestOptions)
                .then((response) => {
                    var datas = response.data;
                    setVersionTrack(datas)
                })
                .catch((e) => {

                })

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const getListMac = async () => {
        let url = 'https://efaskon-dms.slog.polri.go.id/db/dmsbackend/versiontracking/@list_versiontrack';
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", `Bearer ${userData.token}`);

        const requestOptions = {
            method: "GET",
            headers: myHeaders,
        };

        try {
            await axios.get(url, requestOptions)
                .then((response) => {
                    var datas = response.data;
                    setVersionTrack(datas)
                })
                .catch((e) => {

                })

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const sendVersionTrackingData = async () => {
        const systemInfo = await window.electronAPI.getSystemInfo();
        console.log("System Info:", systemInfo);
    
        // Directly use systemInfo instead of mac state
        const matchedObj = versionTrack.find(item => item.versiontrack_macaddress === systemInfo.macAddress);
    
        if (!matchedObj) {
            console.warn("No matching object found for MAC address:", systemInfo.macAddress);
            return;
        }
    
        // Construct and send payload
        const payload = {
            "@type": "VersionTrack",
            versiontrack_user_id: [profileSso?.username || "", ...(matchedObj?.versiontrack_user_id || [])],
            versiontrack_login_date: [systemInfo?.lastOpened || "", ...(matchedObj?.versiontrack_login_date || [])],
            versiontrack_polda_satker: poldaname || "",
            versiontrack_macaddress: systemInfo?.macAddress || "",
            versiontrack_serial_number: systemInfo?.serialNumber || "",
            versiontrack_current_version: systemInfo?.appVersion || "",
            versiontrack_current_version_release_date: systemInfo?.lastOpened || "",
            versiontrack_update_version: systemInfo?.typeConnection || "",
            versiontrack_platform: systemInfo?.platform || "",
            versiontrack_notification_update: false,
        };
    
        console.log("Payload:", payload);
    
        try {
            const response = await fetch(VERSION_TRACKING_URL, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${userData.token}`,
                },
                body: JSON.stringify(payload),
            });
    
            if (!response.ok) {
                throw new Error(`Failed to send data. Status: ${response.status}`);
            }
    
            console.log("Version tracking data sent successfully.");
        } catch (error) {
            console.error("Error while sending version tracking data:", error);
        }
    };
    

    const onRequestLoginSso = async () => {
        const payload = { profileSso };
        const token = await createJWT(payload);
        console.log("onRequestLoginSso", payload, dataIp, dataIp.hasOwnProperty("ip_address"));
        getListHistory()

        try {
            if (dataIp && dataIp.hasOwnProperty("ip_address")) {
                let baseUrl = dataIp.ip_address + "/db/dmsbackend/";
                setIsLoading(true);

                let response = await Helper.SignInSso(profileSso, baseUrl);
                if ((response && response.status == 200) || response.status == 201 || response.status == 202 || response.status == 203 || response.status == 204) {
                    localStorage.removeItem("user", response.data.userid);
                    localStorage.removeItem("myscrtpwd", pwrd);
                    localStorage.removeItem("mytoken", response.data.token);
                    localStorage.removeItem("myfullname", response.data.fullname);
                    localStorage.removeItem("mypoldaid", response.data.poldaid);
                    localStorage.removeItem("mypoldaname", response.data.poldaname);
                    localStorage.removeItem("mysatkerid", response.data.satkerid);
                    localStorage.removeItem("mysatkername", response.data.satkername);
                    localStorage.removeItem("mynip", response.data.nip);
                    localStorage.removeItem("myrole", JSON.stringify(response.data.role));

                    localStorage.setItem("user", response.data.userid);
                    localStorage.setItem("myscrtpwd", pwrd);
                    localStorage.setItem("mytoken", response.data.token);
                    localStorage.setItem("myfullname", response.data.fullname);
                    localStorage.setItem("mypoldaid", response.data.poldaid);
                    localStorage.setItem("mypoldaname", response.data.poldaname);
                    localStorage.setItem("mysatkerid", response.data.satkerid);
                    localStorage.setItem("mysatkername", response.data.satkername);
                    localStorage.setItem("mynip", response.data.nip);
                    localStorage.setItem("myrole", JSON.stringify(response.data.role));
                    localStorage.setItem("email", response.data.email);
                    let roleArr = response.data.role;
                    let isAdmin = false;
                    let isRoot = false;
                    if (roleArr.includes("root")) {
                        isRoot = true;
                    }
                    if (roleArr.includes("superadmin") || roleArr.includes("root")) {
                        isAdmin = true;
                        localStorage.setItem("myisadmin", true);
                    } else {
                        isAdmin = false;
                        localStorage.setItem("myisadmin", false);
                    }

                    let listperm = null;
                    let strListPerm = null;
                    if (isRoot) {
                        localStorage.setItem("mylistperm", ListPerm);
                        // initializePermission(ListPerm);
                        accessManager.setAccessData(ListPerm);
                    } else {
                        listperm = await Helper.GetListPermission(baseUrl);
                        strListPerm = listperm ? JSON.stringify(listperm.data.lispermrole) : null;
                        localStorage.setItem("mylistperm", strListPerm);
                        // initializePermission(listperm.data.lispermrole);
                        accessManager.setAccessData(listperm.data.lispermrole);
                        // let value = listperm.data.roles.includes("dms");
                        let roles = listperm.data.roles;
                        let roleName = null;
                        for (let key in roles) {
                            if (key.includes("dms")) {
                                roleName = roles[key]; //key.split(".")[1];
                                localStorage.setItem("myrolename", roleName);
                                break;
                            }
                        }
                    }

                    console.log("onRequestLoginSso - MASUKKK HOME");
                    getListMac()
                    setIsLoginSSOFinished(true);
                    setIsLoading(false);
                    setAyaJwt(true);
                    sendVersionTrackingData();
                    console.log("onRequestLoginSso - MASUKKK HOME2");
                    const cekLisensi = localStorage.getItem("lisensi");
                    console.log("cekLisensi >>>>>", cekLisensi)
                    if (cekLisensi === "true") {
                        console.log("cekLisensi is true, navigating to /home");
                        navigate("/home");
                    } else if (cekLisensi === "false") {
                        console.log("cekLisensi is false or not set.");
                        navigate("/login");
                        utils.sweetAlertWarn("Out");
                    }
                }
            } else {
                console.log('dataIp.hasOwnProperty("ip_address")', dataIp.hasOwnProperty("ip_address"));
            }
        } catch (err) {
            if (err.status == 409) {
                utils.sweetAlertError(`${t("swat_alert_error1")}`);
            }
            if (err.status == 401) {
                utils.sweetAlertError(`${t("swat_alert_error1")}`);
                localStorage.clear();
                navigate("/login");
            }
            setIsLoading(false);
            console.log(err.message);
        }
    };
    // if (matchedObj) {
    //     const licenseStatus = matchedObj.versiontrack_license_status;
    //     localStorage.setItem("lisensi", licenseStatus);
    //     console.log(`License status (${licenseStatus}) stored in localStorage.`);
    // } else {
    //     console.log("No matching MAC address found.");
    // }

    console.log("versionTrack", versionTrack)

    return (
        <div id="loading-screen">
            <img src={logo} alt="Logo" width="100" />
            <div class="container mt-4">
                <div class="📦"></div>
                <div class="📦"></div>
                <div class="📦"></div>
                <div class="📦"></div>
                <div class="📦"></div>
            </div>
        </div>
    );
};
export default LoadSso;
