import React, { useState, useEffect } from 'react';

const DynamicProgressBar = ({ startDownload, setLoadingComplete }) => {
    const [progress, setProgress] = useState(0);
    useEffect(() => {
        let interval;

        const startProgress = () => {
            interval = setInterval(() => {
                setProgress(prevProgress => {
                    const newProgress = prevProgress + 10;
                    return newProgress <= 100 ? newProgress : 100;
                });
            }, 1000);

            // Simulate data loading completion
            setTimeout(() => {
                setLoadingComplete(true);
                clearInterval(interval);
            }, 10000); // Simulating 5 seconds of loading time
        };

        if (startDownload) {
            startProgress();
        }

        return () => clearInterval(interval);
    }, [startDownload]);

    return (
        <div className="progress">
            <div
                className="progress-bar"
                role="progressbar"
                style={{ width: `${progress}%` }}
                aria-valuenow={progress}
                aria-valuemin="0"
                aria-valuemax="100" >
                {progress}%
            </div>
        </div>
    );
};

export default DynamicProgressBar;