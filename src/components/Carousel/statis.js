import '../../assets/css/carousel.css'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import Carousel0 from "../../assets/images/carousel/carousel-00.jpg"
import Carousel1 from "../../assets/images/carousel/carousel-01.jpg" 
import Carousel2 from "../../assets/images/carousel/carousel-00.jpg"
import Carousel3 from "../../assets/images/carousel/carousel-01.jpg" 
import Carousel4 from "../../assets/images/carousel/carousel-00.jpg"

const CarouselDocStatic = (props) => {
    const { imgsOcr } = props

    return (
            <div
                className="owl-carousel push-bottom"
                data-plugin-options='{"items": 1, "autoHeight": true}'
            >
                <Carousel
                    autoPlay
                    key={7}
                    showThumbs={true}
                >
                    <div>
                        <img 
                            alt="carousel1"
                            src={Carousel0}
                        />
                    </div>
                    <div>
                        <img 
                            alt="carousel2"
                            src={Carousel1}
                        />
                    </div>
                    <div>
                        <img 
                            alt="carousel3"
                            src={Carousel2}
                        />
                    </div>
                    <div>
                        <img
                            alt="carousel4"
                            src={Carousel3}
                        />
                    </div>
                    <div>
                        <img
                            alt="carousel5"
                            src={Carousel4}
                        />
                    </div>
                </Carousel>
            </div>
        
    );

}
export default CarouselDocStatic;

