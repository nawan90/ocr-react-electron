import '../../assets/css/carousel.css'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

const CarouselDoc = (props) => {
  const { imgsOcr } = props

  return (
      <div
        className="owl-carousel push-bottom"
        data-plugin-options='{"items": 1, "autoHeight": true}'
      >
        <Carousel
          autoPlay
          key={7}
          showThumbs={true}
        >
            {
              imgsOcr.map((data, index) => (
                <div >
                  <img src={data} alt="..." />
                </div>
              ))
            }
         </Carousel>
      </div>
   
  );

}
export default CarouselDoc;

