import Mode from "../../package.json";
import { useState, useEffect } from "react";
import "../assets/css/footer.css";
import ConLogo from "../assets/images/connect.png";
import ConNotLogo from "../assets/images/con-not.png";
import { useTranslation } from "react-i18next";

const Footer = (props) => {
    let base_url = process.env.REACT_APP_API_URL;
    const [status, setStatus] = useState("");
     const [dataIp, setDataIp] = useState("");

    const addIp = localStorage.getItem("ipAddress");

    let id_ip = "IP20240605040259";

    const { t } = useTranslation();
    
    useEffect(() => {
        showIpSetting();
    }, []);

    const showIpSetting = async () => {
        try {
            let url = base_url + "ipsetting/" + id_ip;
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
            setDataIp(datas);
        } catch (err) {
           
        }
    };
    useEffect(() => {
        if (dataIp && dataIp.hasOwnProperty("ip_address")) {
            window.localStorage.setItem("ipAddress", dataIp ? dataIp.ip_address : "");
        }
    }, [dataIp]);

    const getOnLineStatus = () => (typeof navigator !== "undefined" && typeof navigator.onLine === "boolean" ? navigator.onLine : true);

    const useNavigatorOnLine = () => {
        const [status, setStatus] = useState(getOnLineStatus());

        const setOnline = () => {
            setStatus(true);
        };
        const setOffline = () => {
            setStatus(false);
        };
        useEffect(() => {
            window.addEventListener("online", setOnline);
            window.addEventListener("offline", setOffline);

            return () => {
                window.removeEventListener("online", setOnline);
                window.removeEventListener("offline", setOffline);
            };
        }, []);

        return status;
    };
    const isOnline = useNavigatorOnLine();
    useEffect(() => {
        if (isOnline == true) {
            setStatus("Online");
        } else {
            setStatus("Offline");
        }
    }, [isOnline]);

    const showUrl = () =>{
        const isLatest = /latest/.test(Mode.version);

if (isLatest) {
  console.log('Latest version detected');
} else {
  console.log('Not the latest version');
  return(
    <span>
         |&nbsp;&nbsp;IP Address :<span style={status === "Online" ? { color: "green", fontWeight: "bold" } : { color: "red", fontWeight: "bold" }}>{addIp === undefined ? "Ip Address tidak terdaftar" : addIp}</span>
    </span>
  )
}
    }


  
   
    return (
        <div
            style={{
                marginTop: "auto",
                bottom: 0,
                width: "100%",
            }}
        >
            <footer className="py-4 bg-light mt-auto">
                <div className="container-fluid px-4">
                    <div className="d-flex align-items-center justify-content-between">
                        <div className="text-muted">Copyright &copy; {Mode.author.name} </div>
                        <div class="form-check form-switch d-flex align-items-center justify-content-between">
                            <input checked={status === "Online" ? true : false} class="form-check-input custom-control-input hr180 " type="checkbox" role="switch" />
                            {status === "Online" ? (
                                <label class="form-check-label text-success fw-bolder ">
                                    Online <img src={ConLogo} width="20" class="ml-3" />
                                </label>
                            ) : (
                                <label class="form-check-label text-danger fw-bolder ">
                                    Offline <img src={ConNotLogo} width="20" class="ml-3" />
                                </label>
                            )}
                            {/* <span>Connection : </span>
              <span style={status === "Online" ? { color: 'green', fontWeight: "bold" } : { color: 'red', fontWeight: "bold" }}>{conIp}</span> */}
                           
                            {showUrl()}
                      
                        </div>
                        <div>
                            {/* <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a> */}
                            versi {Mode.version}

                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
};

export default Footer;
