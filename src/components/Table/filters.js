import plus from "../../assets/images/iconConfigure_button.png";
import { useTranslation } from 'react-i18next';

const Filters = ({ filtering, setFiltering }) => {
  const { t } = useTranslation();

  return (
    <div mb={6} spacing={3}>
      <div class=" mb-4">
        <div class="d-flex form-inputs input-group">
          <input
            type="text"
            value={filtering}
            onChange={(e) => setFiltering(e.target.value)}
            class="form-control  border-right-0"
            placeholder={t('cari')}
          />
          {/* <span class="input-group-append bg-white border-left-0">
            <img  class="input-group-text bg-transparent" src={plus}/>
           </span> */}
        </div>
      </div>
    </div>
  );
};
export default Filters;
