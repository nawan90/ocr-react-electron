import { useTranslation } from "react-i18next";

const Paginations = ({ table }) => {
    const { t } = useTranslation();

    return (
        <div mb={6} spacing={3}>
            <div className="row">
                <div className="col-xl-6">
                    <div className="h-5" />
                    <div className="flex items-center gap-2">
                        <button className="border rounded p-1 me-1 text-primary fw-bold" onClick={() => table.setPageIndex(0)} disabled={!table.getCanPreviousPage()}>
                            {"<<"}
                        </button>
                        <button className="border rounded p-1 me-1 text-primary fw-bold" onClick={() => table.previousPage()} disabled={!table.getCanPreviousPage()}>
                            {"<"}
                        </button>
                        <button className="border rounded p-1 m-1 text-primary fw-bold" onClick={() => table.nextPage()} disabled={!table.getCanNextPage()}>
                            {">"}
                        </button>
                        <button className="border rounded p-1 text-primary fw-bold" onClick={() => table.setPageIndex(table.getPageCount() - 1)} disabled={!table.getCanNextPage()}>
                            {">>"}
                        </button>
                    </div>
                </div>
                <div className="col-xl-6 text-end">
                    <span className="flex items-center gap-1">
                        {t("page_table")}
                        <strong>
                            {table.getState().pagination.pageIndex + 1} of {table.getPageCount()}
                        </strong>
                    </span>
                    <span className="flex items-center gap-1">
                        &nbsp;|&nbsp; {t("go_to_table")}: &nbsp;
                        <input
                            type="number"
                            defaultValue={table.getState().pagination.pageIndex + 1}
                            onChange={(e) => {
                                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                                table.setPageIndex(page);
                            }}
                            className="border p-1 rounded w-16"
                        />
                    </span>
                    <select
                        className="ms-2 me-2"
                        value={table.getState().pagination.pageSize}
                        onChange={(e) => {
                            table.setPageSize(Number(e.target.value));
                        }}
                    >
                        {[10, 20, 30, 40, 50].map((pageSize) => (
                            <option key={pageSize} value={pageSize}>
                                {t("lihat_um")} {pageSize}
                            </option>
                        ))}
                    </select>
                    {table.getPrePaginationRowModel().rows.length} {t("rows_table")}
                </div>
            </div>
        </div>
    );
};
export default Paginations;
