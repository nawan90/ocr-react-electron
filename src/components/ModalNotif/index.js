import React, { useState, useEffect } from "react";

import { useNavigate, useLocation } from "react-router-dom";

function ModalNotif(props) {
  const { closeModal, mode, body } = props;
  
  const header = () =>{

    if(mode ==="error"){
        return (
            <h5 class="modal-title">Error</h5>
        )
    }else if(mode ==="Warning"){
        <h5 class="modal-title">Waring</h5>
    }else{
        return(
            <h5 class="modal-title"></h5>    
        )
    }
  }

  
  return (
    <div
      class="modal show"
      tabIndex="-1"
      role="dialog"
      style={{ display: "inline-block", width: "100%" }}
    >
      <div class="modal-dialog modal-xs">
        <div class="modal-content" style={{ height: "200px" }}>
          <div class="modal-header">
            {header()}
            <button type="button" class="btn-close " onClick={closeModal}>
              <span class="float-end" aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body">
             <p>{body}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalNotif;
