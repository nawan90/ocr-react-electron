import React, { useState, useEffect } from "react";
import { useNavigate, useOutletContext, Link } from "react-router-dom";
import axios from "axios";
import LoadSpinner from "../Load/load";
import ImageCarousel from ".";
import { useTranslation } from "react-i18next";
import Swal from 'sweetalert2'
import helper from "../../api/helper";
import ShowPdf from "../../pages/Searching/View";
import { Card, CardBody } from "reactstrap";
import LocalStorageHelper from "../../api/localStorageHelper";



const ShowDocumentServer = ({ docUrl }) => {
    const { t } = useTranslation();
    const BASE_URL = process.env.REACT_APP_API_URL;
    const [document, setDocument] = useState(null)
    const [fileUrl, setFileUrl] = useState("")

    const [loading, setLoading] = useState(false);

    const [docType, setDocType] = useState(null);
    const [template, setTemplate] = useState(null);
    const userData = LocalStorageHelper.getUserData()
    let base_url = process.env.REACT_APP_API_URL;
    useEffect(() => {
       
        setFileUrl("")
        let api = userData.myip
        if(api && api != null && api != "null"){
          callDocument()
        }
        
        const url = docUrl+"/@download/file"
        setFileUrl(url)
    },[docUrl])

    const callDocument = async ()=> {
        setLoading(true);
        const url = userData.myip+'/db/dmsbackend/'+docUrl
        
        try {
            await axios.get(url, await helper.GetToken())
            .then( (response) => {
                if(response.status == 200){
                    const data = response.data
                    setDocument(data)
                    loadDocType(data.doc_type_id, data.doc_template_id);
                    
                }
            })
            .catch((e) => {

            })
            
        } catch (error) {
        console.error('Error fetching data:', error);
        }
    };
    const loadDocType = async (type, template) => {
        
        try {
          let url = userData.myip+ "/db/dmsbackend/@list_doctype?doc_type_id=" + type;
          const myHeaders = new Headers();
          myHeaders.append("Content-Type", "application/json");
          myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));
    
          const requestOptions = {
            method: "GET",
            headers: myHeaders,
          };
          var res = await fetch(url, requestOptions);
          var datas = await res.json();
          let options = {};
          var doc_type = datas[0].doc_type_title;
          setDocType(doc_type);
          loadTemplate(template);
        } catch (err) {
          alert(`${t('error_um')}`);
          setLoading(false);
        }
    };

    const loadTemplate = async (template) => {
    try {
        let url = userData.myip + "/db/dmsbackend/@list_template?template_id=" + template;
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", "Basic " + btoa("root:Adm1nDMS2024"));

        const requestOptions = {
        method: "GET",
        headers: myHeaders,
        };
        var res = await fetch(url, requestOptions);
        var datas = await res.json();
        let options = {};

        // datas.map(function (data, index) {
        //   options[data.id] = data
        // });
        var doc_template = datas[0].template_name;

        setTemplate(doc_template);
        setLoading(false);
        // loadState()
    } catch (err) {
        setLoading(false);
        
    }
    };
    return (
        <>
           
            
            <div class="row">
                <Card>
                    <CardBody>
                    <div class="row mt-1 bg-light">
                        <div class="col-4 ">
                            <label class="col-form-label fw-bolder">{t("doc_name_td")}</label>
                            <div class="input-group">
                            <label>- {document && document.doc_name}</label>
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="  col-form-label fw-bolder">{t("titlle_dt")}</label>
                            <div class="input-group">
                            <label>- {document && docType}</label>
                            {/* <label>{document && docType[document.doc_type_id]}</label> */}
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="  col-form-label fw-bolder">{t("templat_t1")}</label>
                            <div class="input-group">
                            <label>- {document && template}</label>
                            </div>
                        </div>
                    </div>
                        <hr></hr>
                    <div class="row mt-3">
                        {fileUrl && <ShowPdf link={fileUrl}/>}
                    </div>
                    <div class="row mt-1">
              <div class="col-12 p-3">
                <hr></hr>
                <h6 class="card-title ">
                  <p class="text-decoration-underline text-primary">
                  {t("tag_um")} {t("titlle_md")}
                  </p>
                </h6>
              </div>
            </div>
            {/* WILL */}
            <div class="row mt-1 bg-light">
              <div class="col-6 ">
                <label class="col-form-label fw-bolder">{t("tag_um")}</label>
                {document &&
                  document.doc_metadata_by_template.map((data, index) => (
                    <div class="row py-1">
                      <div class="col">{data.metadata_tmp_name}</div>
                    </div>
                  ))}
              </div>
              <div class="col-6">
                <label class="  col-form-label fw-bolder">{t("res_um")}</label>
                {document &&
                  document.doc_metadata_by_template.map((data, index) => (
                    <div class="row py-1">
                      <div class="col">{data.metadata_tmp_value}</div>
                    </div>
                  ))}
              </div>
            </div>

            {/* END */}
            <div class="row mt-1">
              <div class="col-12 p-3">
                <hr></hr>
                <h6 class="card-title ">
                  <p class="text-decoration-underline text-primary">
                  {t("pol_md_td")}
                  </p>
                </h6>
              </div>
            </div>
            {/* WILL */}
            <div class="row mt-1 bg-light">
              <div class="col-6 ">
                <label class="col-form-label fw-bolder">{t("tag_um")}</label>
                {document &&
                  document.doc_polda_metadata.map((data, index) => (
                    <div class="row py-3">
                      <div class="col">{data.metadata_pol_name}</div>
                    </div>
                  ))}
              </div>
              <div class="col-6">
                <label class="  col-form-label fw-bolder">{t("res_um")}</label>
                {document &&
                  document.doc_polda_metadata.map((data, index) => (
                    <div class="row py-3">
                      <div class="col">{data.metadata_pol_value}</div>
                    </div>
                  ))}
              </div>
            </div>
                    </CardBody>
                </Card>
            </div>
            
        </>
    )
    
}
export default  ShowDocumentServer