import React, { useEffect, useState } from 'react';
import axios from 'axios'; // For making API calls
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // Import carousel styles
import '../../assets/css/carousel.css';
import { useTranslation } from "react-i18next";

const ImageCarousel = ({ imgApiUrls }) => {
  const [images, setImages] = useState([]);
  const { t } = useTranslation();

  // Fetch images from each API endpoint
  useEffect(() => {
    const fetchImages = async () => {
      try {
        // Create an array of promises for each API call
        const promises = imgApiUrls.map(url =>
            
        axios.get(`http://localhost:8069/db/dmsbackend/${url}`, {
            responseType: 'arraybuffer',
            headers: {
              'Authorization': "Basic " + btoa("root:Adm1nDMS2024") // Basic Authentication
            }
          })
        );

        // Wait for all promises to resolve
        const responses = await Promise.all(promises);

        // Convert each response data to Base64
        const imageData = responses.map(response => {
          const base64String = btoa(
            new Uint8Array(response.data).reduce((data, byte) => data + String.fromCharCode(byte), '')
          );
          return `data:image/png;base64,${base64String}`; // Change 'image/png' to the correct type if necessary
        });

        // Update the state with the image data
        setImages(imageData);
      } catch (error) {
        console.error("Error fetching images:", error);
      }
    };

    fetchImages();
  }, [imgApiUrls]); // Added imgApiUrls as a dependency

  return (
    <div className="owl-carousel push-bottom"
    data-plugin-options='{"items": 1, "autoHeight": true}'>
      {images.length > 0 ? (
        <Carousel showThumbs={true} infiniteLoop={true} autoPlay={true} interval={3000}>
          {images.map((image, index) => (
            <div key={index}>
              <img src={image} alt={`Image ${index + 1}`} />
            </div>
          ))}
        </Carousel>
      ) : (
        <p>{t("memuat_gambar")}...</p>
      )}
    </div>


  );
};

export default ImageCarousel;
