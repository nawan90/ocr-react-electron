import React, { useState, useEffect } from "react";
import ConNotLogo from "../../../assets/images/con-not.png";
import { useNavigate, useLocation, Link, NavLink } from "react-router-dom";

const IpNotValidPage = () => {
  const status = localStorage.getItem("onlinStatus");
  const location = useLocation();
  const navigate = useNavigate();
  const path = location.pathname;
  const TableData = () => {
    if (path === '/searching') {
      return "Tidak Ada Data"
    } else {
      return "IP Address"
    }
  } 

  return (
    <div>
      {status === "Offline" ? (
        <span>
          {" "}
          <label class="form-check-label text-danger fw-bolder ">
            Offline <img src={ConNotLogo} width="20" class="ml-3" />
          </label>
          <br />{" "}
        </span>
      ) : (
        <span> {TableData()}</span>
      )}
    </div>
  );
};

export default IpNotValidPage;
