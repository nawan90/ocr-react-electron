import React, { useState, useEffect } from "react";
import Routers from "./router";
import { useTranslation } from "react-i18next";
import './index.css';
import '@fontsource/lexend'; 

function App() {
    const [dataIp, setDataIp] = useState([]);
    let base_url = process.env.REACT_APP_API_URL;
    let id_ip = "IP20240605040259";
    useEffect(() => {
        window.localStorage.setItem("ipAddress", dataIp ? dataIp.ip_address : "");
    }, []);

    const { t } = useTranslation();
    const getOnLineStatus = () => (typeof navigator !== "undefined" && typeof navigator.onLine === "boolean" ? navigator.onLine : true);

    const useNavigatorOnLine = () => {
        const [status, setStatus] = useState(getOnLineStatus());

        const setOnline = () => {
            setStatus(true);
        };
        const setOffline = () => {
            setStatus(false);
        };
        useEffect(() => {
            window.addEventListener("online", setOnline);
            window.addEventListener("offline", setOffline);

            return () => {
                window.removeEventListener("online", setOnline);
                window.removeEventListener("offline", setOffline);
            };
        }, []);

        return status;
    };
    const isOnline = useNavigatorOnLine();
    useEffect(() => {
        if (isOnline == true) {
            localStorage.setItem("onlinStatus", "Online");
        } else {
            localStorage.setItem("onlinStatus", "Offline");
        }
    }, [isOnline]);

    useEffect(() => {
        if (isOnline == true) {
            localStorage.setItem("onlinStatus", "Online");
        } else {
            localStorage.setItem("onlinStatus", "Offline");
        }
    }, []);

    useEffect(() => {
        if (isOnline == true) {
            getIpSetting();
        }
    }, []);

    const getIpSetting = async () => {
        try {
            let url = base_url + "ipsetting/" + id_ip;
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            const requestOptions = {
                method: "GET",
                headers: myHeaders,
            };
            var res = await fetch(url, requestOptions);
            var datas = await res.json();
            setDataIp(datas);
        } catch (err) {
            
        }
    };

    if (dataIp) {
        if (dataIp && dataIp.hasOwnProperty("ip_address")) {
            window.localStorage.setItem("ipAddress", dataIp ? dataIp.ip_address : "");
        }
    }

    return <Routers basename="/app" />;
}

export default App;
