import React from "react";
const ListPerm = [
    "dms.polda.add",
    "dms.polda.modify",
    "dms.polda.view",
    "dms.polda.list",
    "dms.satker.add",
    "dms.satker.modify",
    "dms.satker.view",
    "dms.satker.list",
    "dms.metadata.add",
    "dms.metadata.modify",
    "dms.metadata.view",
    "dms.metadata.list",
    "dms.doctype.add",
    "dms.doctype.modify",
    "dms.doctype.view",
    "dms.doctype.list",
    "dms.template.add",
    "dms.template.modify",
    "dms.template.view",
    "dms.template.list",
    "dms.province.add",
    "dms.province.modify",
    "dms.province.view",
    "dms.province.list",
    "dms.document.add",
    "dms.document.modify",
    "dms.document.view",
    "dms.document.list",
    "dms.document.version",
    "dms.document.approve",
    "dms.theme.access",
    "dms.user.add",
    "dms.user.modify",
    "dms.user.view",
    "dms.user.list",
    "dms.search.access",
    "dms.log.access",
    "dms.role.access",
];

class AccessManager {
    constructor() {
        this.accessPoldaAdd = JSON.parse(localStorage.getItem("dms.polda.add"));
        this.accessPoldaModify = JSON.parse(localStorage.getItem("dms.polda.modify"));
        this.accessPoldaView = JSON.parse(localStorage.getItem("dms.polda.view"));
        this.accessPoldaList = JSON.parse(localStorage.getItem("dms.polda.list"));
        this.accessSatkerAdd = JSON.parse(localStorage.getItem("dms.satker.add"));
        this.accessSatkerModify = JSON.parse(localStorage.getItem("dms.satker.modify"));
        this.accessSatkerView = JSON.parse(localStorage.getItem("dms.satker.view"));
        this.accessSatkerList = JSON.parse(localStorage.getItem("dms.satker.list"));
        this.accessMetadataAdd = JSON.parse(localStorage.getItem("dms.metadata.add"));
        this.accessMetadataModify = JSON.parse(localStorage.getItem("dms.metadata.modify"));
        this.accessMetadataView = JSON.parse(localStorage.getItem("dms.metadata.view"));
        this.accessMetadataList = JSON.parse(localStorage.getItem("dms.metadata.list"));
        this.accessDocTypeAdd = JSON.parse(localStorage.getItem("dms.doctype.add"));
        this.accessDocTypeModify = JSON.parse(localStorage.getItem("dms.doctype.modify"));
        this.accessDocTypeView = JSON.parse(localStorage.getItem("dms.doctype.view"));
        this.accessDocTypeList = JSON.parse(localStorage.getItem("dms.doctype.list"));
        this.accessTemplateAdd = JSON.parse(localStorage.getItem("dms.template.add"));
        this.accessTemplateModify = JSON.parse(localStorage.getItem("dms.template.modify"));
        this.accessTemplateView = JSON.parse(localStorage.getItem("dms.template.view"));
        this.accessTemplateList = JSON.parse(localStorage.getItem("dms.template.list"));
        this.accessProvinceAdd = JSON.parse(localStorage.getItem("dms.province.add"));
        this.accessProvinceModify = JSON.parse(localStorage.getItem("dms.province.modify"));
        this.accessProvinceView = JSON.parse(localStorage.getItem("dms.province.view"));
        this.accessProvinceList = JSON.parse(localStorage.getItem("dms.province.list"));
        this.accessDocumentAdd = JSON.parse(localStorage.getItem("dms.document.add"));
        this.accessDocumentModify = JSON.parse(localStorage.getItem("dms.document.modify"));
        this.accessDocumentView = JSON.parse(localStorage.getItem("dms.document.view"));
        this.accessDocumentList = JSON.parse(localStorage.getItem("dms.document.list"));
        this.accessDocumentVersion = JSON.parse(localStorage.getItem("dms.document.version"));
        this.accessDocumentApprove = JSON.parse(localStorage.getItem("dms.document.approve"));
        this.accessTheme = JSON.parse(localStorage.getItem("dms.theme.access"));
        this.accessUserAdd = JSON.parse(localStorage.getItem("dms.user.add"));
        this.accessUserModify = JSON.parse(localStorage.getItem("dms.user.modify"));
        this.accessUserView = JSON.parse(localStorage.getItem("dms.user.view"));
        this.accessUserList = JSON.parse(localStorage.getItem("dms.user.list"));
        this.accessSearch = JSON.parse(localStorage.getItem("dms.search.access"));
        this.accessLog = JSON.parse(localStorage.getItem("dms.log.access"));
        this.accessRole = JSON.parse(localStorage.getItem("dms.role.access"));
    }

    getAccessPoldaAdd() {
        return this.accessPoldaAdd;
    }

    getAccessPoldaModify() {
        return this.accessPoldaModify;
    }

    getAccessPoldaView() {
        return this.accessPoldaView;
    }

    getAccessPoldaList() {
        return this.accessPoldaList;
    }

    getAccessSatkerAdd() {
        return this.accessSatkerAdd;
    }

    getAccessSatkerModify() {
        return this.accessSatkerModify;
    }

    getAccessSatkerView() {
        return this.accessSatkerView;
    }

    getAccessSatkerList() {
        return this.accessSatkerList;
    }

    getAccessMetadataAdd() {
        return this.accessMetadataAdd;
    }

    getAccessMetadataModify() {
        return this.accessMetadataModify;
    }

    getAccessMetadataView() {
        return this.accessMetadataView;
    }

    getAccessMetadataList() {
        return this.accessMetadataList;
    }

    getAccessDocTypeAdd() {
        return this.accessDocTypeAdd;
    }

    getAccessDocTypeModify() {
        return this.accessDocTypeModify;
    }

    getAccessDocTypeView() {
        return this.accessDocTypeView;
    }

    getAccessDocTypeList() {
        return this.accessDocTypeList;
    }

    getAccessTemplateAdd() {
        return this.accessTemplateAdd;
    }

    getAccessTemplateModify() {
        return this.accessTemplateModify;
    }

    getAccessTemplateView() {
        return this.accessTemplateView;
    }

    getAccessTemplateList() {
        return this.accessTemplateList;
    }

    getAccessProvinceAdd() {
        return this.accessProvinceAdd;
    }

    getAccessProvinceModify() {
        return this.accessProvinceModify;
    }

    getAccessProvinceView() {
        return this.accessProvinceView;
    }

    getAccessProvinceList() {
        return this.accessProvinceList;
    }

    getAccessUserAdd() {
        return this.accessUserAdd;
    }

    getAccessUserModify() {
        return this.accessUserModify;
    }

    getAccessUserView() {
        return this.accessUserModify;
    }

    getAccessUserList() {
        return this.accessUserList;
    }

    getAccessDocumentAdd() {
        return this.accessDocumentAdd;
    }

    getAccessDocumentModify() {
        return this.accessDocumentModify;
    }

    getAccessDocumentView() {
        return this.accessDocumentView;
    }

    getAccessDocumentList() {
        return this.accessDocumentList;
    }

    getAccessDocumentVersion() {
        return this.accessDocumentVersion;
    }

    getAccessDocumentApprove() {
        return this.accessDocumentApprove;
    }

    getAccessTheme() {
        return this.accessTheme;
    }

    getAccessRole() {
        return this.accessRole;
    }

    getAccessSearch() {
        return this.accessSearch;
    }

    getAccessLog() {
        return this.accessLog;
    }

    setAccessData(data) {
        if (data && data.length > 0) {
            
            ListPerm.forEach(function (permission) {
                let key = permission; //.replace(/\./g, "");
                let value = data.includes(permission);
                localStorage.setItem(key, value); // Menyimpan value sebagai boolean true
            });
        }
    }

    // setAccessLog(value) {
    //     localStorage.setItem("dms.log.access", value);
    //     this.haveAccessLog = value; // Update nilai di dalam kelas
    // }

    // setAccessSearch(value) {
    //     localStorage.setItem("dms.search.access", value);
    //     this.haveAccessSearch = value; // Update nilai di dalam kelas
    // }
}

const accessManager = new AccessManager();
export default accessManager;
