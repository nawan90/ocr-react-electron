import React from "react";
import Swal from "sweetalert2";
const toEfaskon = () => {
  if (window.electronAPI?.isElectron) {
      window.electronAPI.openBrowserWindow("https://efaskon.slog.polri.go.id/");
  } else {
      window.open("https://efaskon.slog.polri.go.id/", "_blank");
  }
};
export default {
    sweetAlertError(body) {
        Swal.fire({
            title: "Error!",
            text: "" + body,
            icon: "error",
            confirmButtonText: "OK",
        });
    },

    sweetAlertSuccess(body) {
        Swal.fire({
            title: "Success!",
            text: "" + body,
            icon: "success",
            confirmButtonText: "OK",
        });
    },
    
    sweetAlertWarn(body) {
           Swal.fire({
                   title: "<strong>Perangkat Anda Tidak Memiliki Lisensi</strong>",
                   icon: "warning",
                   html: `Silahkan Hubungi Admin Slog`,
                   showCloseButton: true,
                   showCancelButton: false,
                   focusConfirm: false,
                   confirmButtonText: `<i class="fa fa-thumbs-up"></i> Hubungi Admin`,
                   confirmButtonAriaLabel: "Thumbs up, great!",
               }).then((result) => {
                   if (result.isConfirmed) {
                       toEfaskon(); // Redirects to Efaskon if confirmed
                   }
               });
    }
    
};
