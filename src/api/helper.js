import React from "react";
import axios from "axios";
import CryptoJS from "crypto-js";
const BASE_URL = process.env.REACT_APP_API_URL;
const userClient = process.env.REACT_APP_USER_CLIENT;
const userPass = process.env.REACT_APP_USER_PASS;
const getToken = () => {
    return localStorage.getItem("mytoken");
};

const BASEIP = async () => {
    return await localStorage.getItem("ipAddress");
};

const getConfigOnlyAuth = () => {
    let config = {
        headers: {
            Authorization: `Bearer ${getToken()}`,
        },
    };

    return config;
};

const getConfig = () => {
    let config = {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getToken()}`,
        },
    };

    return config;
};

const getUserClient = () => {
    const credential = {
        user: userClient,
        pass: userPass,
    };
    return credential;
};

export default {
    getHmac(datesnya) {
        const SECRET_KEY_DEV = "80b61d85-dec6-407b-84d2-ca158e41576b";
        const secret_prod = process.env.REACT_APP_SECRET_KEY;

        const hash = CryptoJS.HmacSHA256(`${datesnya}`, `${secret_prod}`);
        const hmacHash = hash.toString(CryptoJS.enc.Hex);
        return hmacHash;
    },
    async GetToken() {
        return getConfig();
    },

    async GetHeaderOnlyAuth() {
        return getConfigOnlyAuth();
    },

    async GetBaseUrl() {
        return BASEIP + "/db/dmsbackend/";
    },

    userClient() {
        const credential = {
            user: userClient,
            pass: userPass,
        };
        return credential;
    },

    async SignInNew(payload, baseurl) {
        return new Promise((resolve, reject) => {
            let url = baseurl + "/@login";
            let config = {
                headers: {
                    "Content-Type": "application/json",
                },
            };
            axios
                .post(url, payload, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async SignInSso(payload, baseurl) {
        return new Promise((resolve, reject) => {
            let url = baseurl + "/@loginsso";
            let config = {
                headers: {
                    "Content-Type": "application/json",
                },
            };
            axios
                .post(url, payload, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async PatchConnection(data_server, id) {
        return new Promise((resolve, reject) => {
            let url = BASE_URL + "ipsetting/" + id;
            let config = getConfig();
            let body = data_server;

            axios
                .patch(url, body, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListDataDashboard(dataIp) {
        return new Promise((resolve, reject) => {
            let url = dataIp;
            let config = getConfig();
            axios
                .get(url, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListPermission(baseurl) {
        return new Promise((resolve, reject) => {
            let url = baseurl + "/@listuser_info";
            let config = getConfig();
            axios
                .get(url, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async DMSClientConfig(data) {
        return new Promise((resolve, reject) => {
            let url = "http://localhost:8069/db/dmsbackend/dmsconfig/conf";
            let config = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
                },
            };

            // axios
            //     .patch(url, data, config)
            //     .then((response) => resolve(response))
            //     .catch((error) => reject(error.response));
        });
    },

    async GetListProvinsi(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async SubmitProvinsi(path, data, method) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            if (method == "edit") {
                axios
                    .patch(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            } else {
                axios
                    .post(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            }
        });
    },

    async DeleteProvinsi(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListMetadata(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async SubmitMetadata(path, data, method) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            if (method == "edit") {
                axios
                    .patch(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            } else {
                axios
                    .post(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            }
        });
    },

    async DeleteMetadata(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListPolda(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path; // ? path : BASE_URL + "/polridocument/@list_polda";
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response.data))
                .catch((error) => reject(error.response));
        });
    },

    async SubmitPolda(path, data, method) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            if (method == "edit") {
                axios
                    .patch(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            } else {
                axios
                    .post(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            }
        });
    },

    async DeletePolda(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async SubmitSatker(path, data, method) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            if (method == "edit") {
                axios
                    .patch(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            } else {
                axios
                    .post(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            }
        });
    },

    async GetListSatker(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path; //? path : BASE_URL + "/polridocument/@list_satker";
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response.data))
                .catch((error) => reject(error.response));
        });
    },

    async DeleteSatker(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListGroups(path) {
        // example: http://116.90.165.46:8069/db/dmsbackend/groups/@items?include=name,title&page_size=100
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListUsers(path, page) {
        return new Promise((resolve, reject) => {
            // let myUrl = path + "/@items?page=1&page_size=8000";
            let mPage = page && page < 1 ? 1 : page;
            let myUrl = path + `/@items?page=${mPage}&page_size=2000`;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetDetailUser(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async AddUser(path, data) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .post(myUrl, data, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async ModifyUser(path, data) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .patch(myUrl, data, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async DeleteUser(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListTemplate(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path ? path : BASE_URL + "/@list_template";
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListTemplateClient(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path ? path : BASE_URL + "/@list_template";
            // let config = getConfig();
            let userClient = getUserClient();
            let config = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
                },
            };

            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async SynchronizeTemplate(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path ? path : "http://localhost:8069/db/dmsbackend/@template_sync";
            let userClient = getUserClient();
            let config = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Basic " + btoa("root:Adm1nDMS2024"),
                },
            };

            // let config = getConfig();
            axios
                .patch(myUrl, {}, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async DeleteTemplate(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListActivityUser(url) {
        return new Promise((resolve, reject) => {
            let myUrl = url; //BASE_URL + "/@search?type_name=Log_Activity_User&_sort_asc=creation_date&_metadata=log_type_name,log_category,log_created_date,log_obj_id,log_by_user,log_title,log_desc,log_id,log_obj_path&_size=1000";
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListDocType(url) {
        return new Promise((resolve, reject) => {
            let myUrl = url;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async SubmitDocType(path, data, method) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            if (method == "edit") {
                axios
                    .patch(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            } else {
                axios
                    .post(myUrl, data, config)
                    .then((response) => resolve(response))
                    .catch((error) => reject(error.response));
            }
        });
    },

    async DeleteDocType(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetDetailDocument(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response.data))
                .catch((error) => reject(error.response));
        });
    },

    async AddNewRole(path, data) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .post(myUrl, data, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async ModifyRole(path, data) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .patch(myUrl, data, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetListRole(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetDetailRole(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async DeleteRole(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .delete(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetDataExportOcr(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

    async GetDashboardPerPolda(path) {
        return new Promise((resolve, reject) => {
            let myUrl = path;
            let config = getConfig();
            axios
                .get(myUrl, config)
                .then((response) => resolve(response))
                .catch((error) => reject(error.response));
        });
    },

};
